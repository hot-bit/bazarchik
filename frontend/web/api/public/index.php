<?php

require_once __DIR__ . '/../bootstrap.php';

if (!$app['debug']) {

    $logError = function (\Exception $e) {
        $logName = date("Y-m-d") . '.log';
        $log     = "[" . date("Y-m-d H:i:s") . "] " . $e->getMessage() . " (" . $e->getCode() . ") \n" . $e->getTraceAsString() . "\n";
        file_put_contents(__DIR__ . '/../log/' . $logName, $log, FILE_APPEND);
    };

    $app->error(function (\Exception $e, $code) use ($app, $logError) {
        $logError($e);

        return $app->errorResponse(\BZR\Translation\Keys::UNKNOWN_ERROR, $e->getMessage(), $code, 500);
    });

    \Symfony\Component\Debug\ErrorHandler::register();
    $errorHandler = \Symfony\Component\Debug\ExceptionHandler::register();

    $errorHandler->setHandler(function (\Exception $e) use ($app, $logError) {
        $logError($e);

        $app->errorResponse(\BZR\Translation\Keys::UNKNOWN_ERROR, $e->getMessage(), $e->getCode(), 500)
            ->sendHeaders()
            ->sendContent();
    });
}

//set_include_path(get_include_path() . PATH_SEPARATOR . __DIR__ .'/vendor/google/apiclient/src'); //зачем это нужно?

$app->run();
