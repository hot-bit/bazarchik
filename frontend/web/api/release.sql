CREATE TABLE `users_auth_mass` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `mobile_token` varchar(255) NOT NULL,
  `auth_token` varchar(255) NOT NULL,
  `last_login` int(11) NOT NULL,
  `lng` varchar(255) NOT NULL,
  `lat` varchar(255) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE `user_push_token` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `type` enum('ios','android') NOT NULL,
  `token` varchar(255) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `user_push_token_user_id_index` (`user_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE `user_category_subscription` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `category_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `user_category_subscription_user_id_category_id_uindex` (`user_id`,`category_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;



CREATE TABLE `ads_last_date_user` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `adv_id` int(11) NOT NULL,
  `id_category` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;


CREATE TABLE `category_updates` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `category_id` int(11) NOT NULL,
  `count` int(11) NOT NULL,
  `send_time` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `category_updates_category_id_uindex` (`category_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE `message_adv` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `adv_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `phone` varchar(255) NOT NULL,
  `email` varchar(255) NOT NULL,
  `text` text NOT NULL,
  `date` int(11) NOT NULL,
  `status` int(11) NOT NULL DEFAULT '1' COMMENT '1- новое 2- прочитанное',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE `password_recovery` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `code` varchar(32) NOT NULL,
  `time_code` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE `mobile_payment` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `payment_id` varchar(255) NOT NULL,
  `created_at` int(11) NOT NULL,
  `updated_at` int(11) NOT NULL,
  `status` tinyint(4) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `mobile_payment_payment_id_uindex` (`payment_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;




ALTER TABLE `adv` ADD COLUMN title_original varchar(255) NOT NULL DEFAULT '';

ALTER TABLE `adv_params` ADD INDEX adv_params_name_idx (name);

ALTER TABLE `users`
  ADD COLUMN locale varchar(255) NOT NULL DEFAULT 'En-en',
  ADD COLUMN send_push tinyint(3) NOT NULL DEFAULT '1' AFTER ref_status;

ALTER TABLE `categories` ADD COLUMN `iconCategoryMobile` varchar(255) NOT NULL DEFAULT '';

UPDATE categories SET iconCategoryMobile="convertible.png" WHERE id=1;
UPDATE categories SET iconCategoryMobile="cottage.png" WHERE id=2;
UPDATE categories SET iconCategoryMobile="administrator.png" WHERE id=3;
UPDATE categories SET iconCategoryMobile="service_bell.png" WHERE id=4;
UPDATE categories SET iconCategoryMobile="cloakroom.png" WHERE id=5;
UPDATE categories SET iconCategoryMobile="hand_planting.png" WHERE id=6;
UPDATE categories SET iconCategoryMobile="electrical.png" WHERE id=7;
UPDATE categories SET iconCategoryMobile="baseball.png" WHERE id=8;
UPDATE categories SET iconCategoryMobile="dog.png" WHERE id=9;
UPDATE categories SET iconCategoryMobile="business.png" WHERE id=10;
UPDATE categories SET iconCategoryMobile="search.png" WHERE id=753;
UPDATE categories SET iconCategoryMobile="one_free.png" WHERE id=752;

ALTER TABLE `favorite` ADD COLUMN `id` int(11) NOT NULL PRIMARY AUTO_INCREMENT;
ALTER TABLE `favorite` ADD COLUMN `favorite_date` int(11) NOT NULL;

ALTER TABLE favorite ADD INDEX favorite_user_id_idx (user_id);

CREATE TABLE `payment` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `type_payment` int(11) NOT NULL,
  `id_payment` varchar(11) NOT NULL,
  `price` float NOT NULL,
  `date` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

ALTER TABLE `adv_images` ADD COLUMN `craftMobile` int(11) NOT NULL;
ALTER TABLE `adv_images` ADD INDEX `adv_images_adv_id_idx` (`adv_id`);