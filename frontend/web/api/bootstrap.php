<?php
$envPath = __DIR__.'/config/env';
$env     = 'prod';

if (file_exists($envPath)) {
    $env = file_get_contents($envPath);
}

define('DEV', $env === 'dev');

$config = require_once __DIR__.'/config/config.php';
$loader = require_once __DIR__.'/vendor/autoload.php';

$loader->addPsr4('BZR\\', __DIR__ . '/app/');
//$loader->addPsr4('Sly\\', __DIR__ . '/sly/');

if (DEV) {
    ini_set("display_errors",1);
    error_reporting(E_ALL);
}

$localConfigPath = __DIR__.'/config/local.php';

if (file_exists($localConfigPath)) {
    $localConfig = require_once $localConfigPath;
    $config = \BZR\Arr::merge($config, $localConfig);
}

$app = new BZR\Application;

$app['config'] = $config;
$app['debug']  = (bool) $config['debug'];

$app->register(new Silex\Provider\ValidatorServiceProvider());
$app->register(new Silex\Provider\TranslationServiceProvider, [
    'locale_fallbacks' => ['En-en'],
]);

$app->register(new \BZR\ServiceProvider\Spot($config['dsn']));
$app->register(new \BZR\ServiceProvider\Payment($config['paypal']['clientId'], $config['paypal']['clientSecret']));
$app->register(new \BZR\ServiceProvider\Notification($config['notifications']['apnsCertificatePath'], $config['notifications']['apnsPassPhrase'], $config['notifications']['gcnApiToken']));
$app->register(new \BZR\ServiceProvider\CommtorSms(
    $config['sms']['commtor']['url'],
    $config['sms']['commtor']['authKey'],
    $config['sms']['commtor']['header'],
    $config['sms']['commtor']['countryCode']
));

require_once __DIR__.'/config/translations.php';
require_once __DIR__.'/config/routes.php';