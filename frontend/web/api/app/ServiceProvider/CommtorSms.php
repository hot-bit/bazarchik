<?php
namespace BZR\ServiceProvider;



use BZR\Sms\CommtorSender;
use Silex\Application;
use Silex\ServiceProviderInterface;

class CommtorSms implements ServiceProviderInterface
{
    protected $url;
    protected $header;
    protected $authKey;
    protected $countryCode;


    public function __construct($url, $authKey, $header, $countryCode)
    {
        $this->url         = $url;
        $this->authKey     = $authKey;
        $this->header      = $header;
        $this->countryCode = $countryCode;
    }

    /**
     * {@inheritdoc}
     */
    public function register(Application $app)
    {
        $url         = $this->url;
        $authKey     = $this->authKey;
        $header      = $this->header;
        $countryCode = $this->countryCode;

        $app['sms.sender'] = $app->share(function () use ($url, $authKey, $header, $countryCode) {
            $sender = new CommtorSender($url, $authKey, $header, $countryCode);
            return $sender;
        });
    }

    /**
     * {@inheritdoc}
     */
    public function boot(Application $app)
    {
    }
}