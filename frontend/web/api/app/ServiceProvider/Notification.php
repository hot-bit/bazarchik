<?php
namespace BZR\ServiceProvider;


use BZR\Notification\Sender;
use BZR\Payment\Handler;
use PayPal\Auth\OAuthTokenCredential;
use PayPal\Rest\ApiContext;
use Silex\Application;
use Silex\ServiceProviderInterface;
use Spot\Config;
use Spot\Locator;

class Notification implements ServiceProviderInterface
{
    /**
     * @var string
     */
    private $certificatePath;

    /**
     * @var string
     */
    private $certificatePassPhrase;

    /**
     * @var string
     */
    private $apiKey;

    /**
     * Notification constructor.
     * @param $certificatePath
     * @param $certificatePassPhrase
     * @param $apiKey
     */
    public function __construct($certificatePath, $certificatePassPhrase, $apiKey)
    {
        $this->certificatePath = $certificatePath;
        $this->certificatePassPhrase = $certificatePassPhrase;
        $this->apiKey          = $apiKey;
    }

    /**
     * {@inheritdoc}
     */
    public function register(Application $app)
    {
        $certificatePath = $this->certificatePath;
        $certificatePassPhrase = $this->certificatePassPhrase;
        $apiKey          = $this->apiKey;

        $app['notification.sender'] = $app->share(function () use ($certificatePath, $certificatePassPhrase, $apiKey, $app) {
            $sender = new Sender($certificatePath, $certificatePassPhrase, $apiKey, $app->spot());
            return $sender;
        });
    }

    /**
     * {@inheritdoc}
     */
    public function boot(Application $app)
    {
    }
}