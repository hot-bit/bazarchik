<?php

namespace BZR\ServiceProvider;


use BZR\Spot\Locator;
use Silex\Application;
use Silex\ServiceProviderInterface;
use Spot\Config;

class Spot implements ServiceProviderInterface
{
    /**
     * Database dsn
     *
     * @access private
     * @var string
     */
    private $dsn;

    /**
     * Constructor
     *
     * @param string $dsn
     */
    public function __construct($dsn)
    {
        $this->dsn = $dsn;
    }

    /**
     * {@inheritdoc}
     */
    public function register(Application $app)
    {
        $config = $this->connect();
        $spot   = new Locator($config);

        $app['spot'] = $spot;
    }

    /**
     * {@inheritdoc}
     */
    public function boot(Application $app)
    {
    }

    /**
     * Connect to the database using the dsn
     *
     * @return \Spot\Config
     */
    private function connect()
    {
        $cfg = new Config;
        $cfg->addConnection('conn_db', $this->dsn);

        return $cfg;
    }
}