<?php
namespace BZR\ServiceProvider;


use BZR\Payment\Handler;
use PayPal\Auth\OAuthTokenCredential;
use PayPal\Rest\ApiContext;
use Silex\Application;
use Silex\ServiceProviderInterface;
use Spot\Config;
use Spot\Locator;

class Payment implements ServiceProviderInterface
{
    /**
     * @var string
     */
    private $clientId;

    /**
     * @var string
     */
    private $clientSecret;

    /**
     * PayPal constructor.
     * @param $clientId
     * @param $clientSecret
     */
    public function __construct($clientId, $clientSecret)
    {
        $this->clientId     = $clientId;
        $this->clientSecret = $clientSecret;
    }

    /**
     * {@inheritdoc}
     */
    public function register(Application $app)
    {
        $clientId     = $this->clientId;
        $clientSecret = $this->clientSecret;

        $app['paypal.context'] = $app->share(function () use ($clientId, $clientSecret) {
            $apiContext = new ApiContext(new OAuthTokenCredential($clientId, $clientSecret));
            return $apiContext;
        });

        $app['payment.handler'] = $app->share(function () use ($app) {
            return new Handler($app);
        });
    }

    /**
     * {@inheritdoc}
     */
    public function boot(Application $app)
    {
    }
}