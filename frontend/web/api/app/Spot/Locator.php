<?php
namespace BZR\Spot;


class Locator extends \Spot\Locator
{
    /**
     * @param null $name
     * @return \Doctrine\DBAL\Connection
     * @throws \Spot\Exception
     */
    public function getConnection($name = null)
    {
        return $this->config()->connection($name);
    }

    /**
     * @param \Closure $func
     * @throws \Exception
     */
    public function transactional(\Closure $func)
    {
        $connection = $this->getConnection();
        $connection->transactional($func);
    }

    public function beginTransaction()
    {
        $connection = $this->getConnection();
        $connection->beginTransaction();
    }

    public function commit()
    {
        $connection = $this->getConnection();
        $connection->commit();
    }

    public function rollBack()
    {
        $connection = $this->getConnection();
        $connection->rollBack();
    }

    public function createSavepoint($savepoint)
    {
        $connection = $this->getConnection();
        $connection->createSavepoint($savepoint);
    }

    public function releaseSavepoint($savepoint)
    {
        $connection = $this->getConnection();
        $connection->releaseSavepoint($savepoint);
    }

    public function rollbackSavepoint($savepoint)
    {
        $connection = $this->getConnection();
        $connection->rollbackSavepoint($savepoint);
    }
}