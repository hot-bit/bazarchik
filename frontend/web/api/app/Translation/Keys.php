<?php
/**
 * Created by PhpStorm.
 * User: user
 * Date: 19.05.16
 * Time: 1:43
 */

namespace BZR\Translation;

class Keys
{
    const UNKNOWN_ERROR        = 'unknown_error';
    const TOKEN_MISSING        = 'token_missing';
    const ERROR_AUT_EX         = 'authorization_exists';
    const ERROR_CODE_TIME      = 'code time';
    const ERROR_AUT_EMAIL      = 'email empty';
    const ERROR_ID_NO          = 'id no';
    const ERROR_AUT_FB         = 'not_successful_authorization';
    const ERROR_TOKEN_EMPTY    = 'token is empty';
    const ERROR_AUT            = 'error authorization';
    const ERROR_NOT_UPDATE     = 'not update';
    const ERROR_USER_ID        = 'user_does_not_exist'; 
    const ERROR_SELECT         = 'select failed';
    const ERROR_INSERT         = 'insert failed';
    const ERROR_PUT            = 'put failed';
    const ERROR_NO_EMAIL       = 'email does not exist';
    const ERROR_EMAIL_SENT     = 'email not sent';
    const SUCCESS_EMAIL_SENT   = 'mail successfully sent';
    const SUCCESS_LOGAUT       = 'success logaut';

    const ERROR_PAYMENT        = 'payment';
    const ERROR_PAYMENT_ID     = 'payment does not exist';
    const ERROR_PAYMENT_ITEM   = 'item does not exist';

    const TO_DAY_DATE       = 'to_day_date';

    //Property params
    const PARAM_CITY       = 'param_city';
    const PARAM_AREA       = 'param_area';
    const PARAM_NOBED       = 'param_nobed';
    const PARAM_NOBED_DEFAULT = 'nobed_default';
    
    const PARAM_COVER       = 'param_cover';
    const PARAM_SIZE    = 'param_size';

    const ERROR_CONFIRMATION_SMS_LIMIT = 'sms_limit_reached';
    const ERROR_CONFIRMATION_NO_PHONE = 'phone_doesnt_set';
    const ERROR_CONFIRMATION_WRONG_CODE = 'confirmation_code_wrong';
    const ERROR_CONFIRMATION_NO_EMAIL = 'email_doesnt_set';


    // Cars parameters

    const SEARCH_ISSET_PHOTOS       = 'searchIssetPhotos';
    const PARAM_VIN       = 'vin';
    const CATEGORY_ST_WHEEL       = 'driving_wheel_assistance';
    const PARAM_ST_DEFAULT       = 'param_default';
    const PARAM_ST_WHEEL_1       = 'param_st_1';
    const PARAM_ST_WHEEL_2       = 'param_st_2';
    const PARAM_ST_WHEEL_3       = 'param_st_3';

    const CATEGORY_CLIMAT       = 'category_climat';
    const CATEGORY_CLIMAT_DEFAULT     = 'param_climat_default';
    const PARAM_CLIMAT_1       = 'param_climat_1';
    const PARAM_CLIMAT_2       = 'param_climat_2';
    const PARAM_CLIMAT_3       = 'param_climat_3';
    const PARAM_CLIMAT_WHEEL_CONTR       = 'param_climate_wheel_contr';
    const PARAM_CLIMATE_ATERN       = 'param_climate_aterm';

    const CATEGORY_SALON       = 'category_salon';
    const PARAM_SALON_DEFAULT       = 'param_salon_default';
    const PARAM_SALON_1       = 'param_salon_1';
    const PARAM_SALON_2       = 'param_salon_2';
    const PARAM_SALON_3       = 'param_salon_3';
    const PARAM_SALON_4       = 'param_salon_4';
    const PARAM_SALON_LEATHER_WHELE       = 'param_salon_leather_wheel';
    const PARAM_SALON_HATCH       = 'param_salon_hatch';

    const CAREGORY_HEATING       = 'category_heating';
    const PARAM_HEATING_FRONT_SEAT       = 'param_heating_front_seat';
    const PARAM_HEATING_REAR_SEAT       = 'param_heating_rear_seat';
    const PARAM_HEATING_MIRRORS       = 'param_heating_mirrors';
    const PARAM_HEATING_REAR_WINDOW      = 'param_heating_rear_window';
    const PARAM_HEATING_WHEEL       = 'param_heating_wheel';

    const CATEGORY_EL       = 'category_el';
    const PARAM_EL_WHEEL_CONTR       = 'param_el_wheel_contr';
    const PARAM_EL_FRONT_SEATS       = 'param_el_front_seats';
    const PARAM_EL_REAR_SEATS       = 'param_el_rear_seats';
    const PARAM_EL_MIRRORS       = 'param_el_mirrors';
    const PARAM_EL_WHEEL       = 'param_el_wheel';
    const PARAM_EL_FOLDING_MIRRORS       = 'param_el_folding_mirrors';

    const CATEGORY_GLASS       = 'electric_power_windows';
    const PARAM_GLASS_DEFAULT       = 'param_glass_defaul';
    const PARAM_GLASS_1       = 'param_glass_1';
    const PARAM_GLASS_2       = 'param_glass_2';

    const CATEGORY_MEM       = 'category_mem';
    const PARAM_MEM_FRONT_SEATS      = 'param_mem_front_seats';
    const PARAM_MEM_REAR_SEATS       = 'param_mem_rear_seats';
    const PARAM_MEM_MIRRORS       = 'param_mem_mirrors';
    const PARAM_MEM_WHEEL       = 'param_mem_wheel';

    const CATEGORY_HELP       = 'category_help';
    const PARAM_HELP_JOCKEY      = 'param_help_jockey';
    const PARAM_HELP_RAIN_SENSOR       = 'param_help_rain_sensor';
    const PARAM_HELP_LIGHT_SENSOR       = 'param_help_light_sensor';
    const PARAM_HELP_R_PARK_SENSOR       = 'param_help_r_park_sensor';
    const PARAM_HELP_F_PARK_SENSOR       = 'param_help_f_park_sensor';
    const PARAM_HELP_HELP_BLIND_SPOT       = 'param_help_blind_spot';
    const PARAM_HELP_REAR_CAMER       = 'param_help_rear_camera';
    const PARAM_HELP_CRUISE_CONTROL       = 'param_help_cruise_control';
    const PARAM_HELP_COMPUTER      = 'param_help_computer';

    const CATEGORY_HIJACK       = 'category_hijack';
    const PARAM_HIJACK_SIGNAL     = 'param_hijack_signal';
    const PARAM_HIJACK_CENTRAL_LOCK       = 'param_hijack_central_lock';
    const PARAM_HIJACK_IMMOBI       = 'param_hijack_immobi';
    const PARAM_HIJACK_SPUTNIK       = 'param_hijack_sputnik';

    const CATEGORY_AIRBAGS       = 'category_airbags';
    const PARAM_AIRBAGS_FRONT       = 'param_airbags_front';
    const PARAM_AIRBAGS_KNEE       = 'param_airbags_knee';
    const PARAM_AIRBAGS_BLINDS       = 'param_airbags_blinds';
    const PARAM_AIRBAGS_FRONT_SIDE       = 'param_airbags_front_side';
    const PARAM_AIRBAGS_REAR_SIDE       = 'param_airbags_rear_side';


    const CATEGORY_HELP_SECURE       = 'category_help_secure';
    const PARAM_HELP_SECURE_ABS       = 'param_help_secure_abs';
    const PARAM_HELP_SECURE_ASR       = 'param_help_secure_asr';
    const PARAM_HELP_SECURE_ESP       = 'param_help_secure_esp';
    const PARAM_HELP_SECURE_EBD       = 'param_help_secure_ebd';
    const PARAM_HELP_SECURE_EBA       = 'param_help_secure_eba';
    const PARAM_HELP_SECURE_EDS       = 'param_help_secure_eds';
    const PARAM_HELP_SECURE_PDS       = 'param_help_secure_pds';


    const CATEGORY_MEDIA       = 'category_media';
    const PARAM_MEDIA_AUDIO_DEFAULT       = 'param_media_audio_default';
    const PARAM_MEDIA_AUDIO_1       = 'param_media_audio_1';
    const PARAM_MEDIA_AUDIO_2       = 'param_media_audio_2';
    const PARAM_MEDIA_AUDIO_3       = 'param_media_audio_3';
    const PARAM_MEDIA_AUDIO_4       = 'param_media_audio_4';
    const PARAM_MEDIA_SUB      = 'param_media_sub';


    const CATEGORY_MEDIA2       = 'category_media2';
    const PARAM_MEDIA_CD      = 'param_media_cd';
    const PARAM_MEDIA_MP3       = 'param_media_mp3';
    const PARAM_MEDIA_TV       = 'param_media_tv';
    const PARAM_MEDIA_RADIO       = 'param_media_radio';
    const PARAM_MEDIA_VIDEO       = 'param_media_video';
    const PARAM_MEDIA_WHEEL_CONTROL       = 'param_media_wheel_control';
    const PARAM_MEDIA_USB       = 'param_media_usb';
    const PARAM_MEDIA_AUX       = 'param_media_aux';
    const PARAM_MEDIA_BLUETOOTH       = 'param_media_bluetooth';
    const PARAM_MEDIA_GPS       = 'param_media_gps';


    const CATEGORY_LIGHT       = 'category_light';
    const PARAM_LIGHT_DEFAULT       = 'param_light_default';
    const PARAM_LIGHT_1       = 'param_light_1';
    const PARAM_LIGHT_2      = 'param_light_2';
    const PARAM_LIGHT_3       = 'param_light_3';
    const PARAM_LIGHT_SENSOR       = 'param_help_light_sensor';
    const PARAM_LIGHT_AFOG       = 'param_light_afog';
    const PARAM_LIGHT_CLEANER      = 'param_light_cleaner';
    const PARAM_LIGHT_ADAPTIVE       = 'param_light_adaptive';

    const CATEGORY_TI       = 'category_TI';
    const PARAM_TI_SERVICE_BOOK       = 'param_TI_service_book';
    const PARAM_TI_DEALER       = 'param_TI_dealer';
    const PARAM_TI_WARRANTY       = 'param_TI_warranty';
    const COLOR       = 'color';
    const RED       = 'red';
    const BROWN       = 'brown';
    const ORANGE       = 'orange';
    const BEIGE       = 'beige';
    const YELLOW       = 'yellow';
    const GREEN       = 'green';
    const BLUE       = 'blue';
    const DARK_BLUE       = 'dark_blue';
    const VIOLET       = 'violet';
    const PURPLE       = 'purple';
    const PINK       = 'pink';
    const WHITE       = 'white';
    const GREY       = 'grey';
    const BLACK       = 'black';
    const GOLD       = 'gold';
    const SILVER       = 'silver';



    const BRAND       = 'Brand';
    const MODEL       = 'Model';
    const BODY       = 'Body';
    const SEDAN       = 'Sedan';
    const VERSATILE       = 'Versatile';
    const CONVERTIBLE       = 'Convertible';
    const COUPE       = 'Coupe';
    const MINIVAN       = 'Minivan';
    const VAN       = 'Van';
    const HATCHBACK       = 'Hatchback';
    const SUV       = 'SUV';
    const CROSSOVER       = 'Crossover';
    const LIMOUSINE       = 'Limousine';
    const PICK_UP       = 'Pick_up';
    const MINIBUS       = 'Minibus';
    const YEAR       = 'year';
    const AFTER       = 'After';
    const BEFORE       = 'Before';
    const RUNNING       = 'Running';
    const THOUS_KM       = 'thous.km.';
    const TRANSMISSION       = 'Transmission';
    const MECHANICS       = 'Mechanics';
    const AUTOMATIC       = 'Automatic';
    const ROBOT       = 'Robot';
    const CVT       = 'CVT';
    const ENGINE       = 'Engine';
    const PETROL       = 'Petrol';
    const DIESEL       = 'Diesel';
    const HYBRID       = 'Hybrid';
    const ELECTRO       = 'Electro';
    const GAS       = 'Gas';
    const VOLUME       = 'Volume';
    const LITERS       = 'liters';
    const DRIVE       = 'drive';
    const FORWARD       = 'Forward';
    const FULL       = 'Full';
    const REAR       = 'Rear';
    const PRICE       = 'Price';
    const EURO       = 'euro';
    const WHEEL       = 'wheel';
    const RIGHT       = 'Right';
    const LEFT       = 'Left';
    const DOORS       = 'doors';




    const PARAM_BOX       = 'box';
    const PARAM_NOT_SPECIFY       = 'Not_specify';
    const PARAM_CHOOSE_YEAR      = 'Choose_year';

    const TIRES_AND_WHEELS      = 'Tires_and_wheels';
    const TIRES_AND_WHEELS_DEFAULT      = 'Tires_and_wheels_default';

    const PARAM_DIAM      = 'diam';
    const PARAM_HEIGHT_PROF      = 'height_prof';
    const PARAM_WIDTH_PROF      = 'width_prof';

    const PARAM_DIAM_DEFAULT      = 'default_diam';
    const PARAM_HEIGHT_PROF_DEFAULT       = 'default_height_prof';
    const PARAM_WIDTH_PROF_DEFAULT       = 'default_width_prof';


    const CATEGORY_ROUND      = 'category_round';
    const PARAM_ROUND_1      = 'param_round_1';
    const PARAM_ROUND_2     = 'param_round_2';
    const PARAM_ROUND_3     = 'param_round_3';


    const CATEGORY_DISC_TYPE      = 'category_disc_type';
    const PARAM_DISC_TYPE_DEFAULT      = 'disc_type_default';

    const CATEGORY_HOLES      = 'category_holes';

    const CATEGORY_TYPE      = 'category_type';
    const PARAM_TYPE_DEFAULT      = 'type_default';
    const PARAM_TYPE_FORGED      = 'Forged';
    const PARAM_TYPE_CAST      = 'Cast';
    const PARAM_TYPE_STAMPED      = 'Stamped';
    const PARAM_TYPE_SPOKES      = 'Spokes';
    const PARAM_TYPE_NATIONAL      = 'National';

    const CATEGORY_WIDTH      = 'category_width';

    const CATEGORY_HOLES_DIAMETER      = 'category_holes_diameter';
    const PARAM_HOLES_DIAMETER_DEFAULT      = 'width_holes_default';

    const CATEGORY_OUTFLY      = 'category_outfly';
    const PARAM_OUTFLY_DEFAULT      = 'outfly_default';

    const YOUR_NAME      = 'your_name';
    const YOUR_EMAIL      = 'your_email';
    const YOUR_PHONE      = 'your_phone';
    const YOUR_CITY      = 'your_city';
    const YOUR_AREA      = 'your_area';

    const TITLE      = 'text';
    const TEXT_ADV      = 'text_adv';
    const PRICE_P      = 'price';
    const PHOTO      = 'photo';

    const NOT_EMAIL      = 'not_email';
    const NOT_SKYPE      = 'not_skype';


    const TRANSACTION_ADD_BALANCE  = 'addBalanceTransaction';
    const TRANSACTION_COLOR        = 'colorTransaction';
    const TRANSACTION_UP           = 'upTransaction';
    const TRANSACTION_VIP          = 'vipTransaction';
    const TRANSACTION_PREMIUM      = 'premiumTransaction';

}