<?php

namespace BZR;
use Symfony\Component\Validator\Mapping\ClassMetadata;
use Symfony\Component\Validator\Constraints as Assert;

class ErrorChecking
{
	static public function errorChecking($arrayErrorChecking, $app, $mapperRow, $corrector)
    {     
    	$constraint_array = [];
        $arrayValidation = $app['config']['validation'];
        $arrayValidationCorrector = $app['config']['validationCorrector'];

    	foreach ($arrayErrorChecking as $key => $value) {

            if($key == 'email'){
                
                $countRow = 0;

                if($corrector == true){
                    $countRow = ErrorChecking::connectUntity($mapperRow, $key, $value);
                }

                if($countRow > 0){             
                    $constraint_array = Arr::unshift($constraint_array, $key, new Assert\Callback(ErrorChecking::formingError($app, $mapperRow, $arrayValidationCorrector[$key][0]))); 
                } else {
                    $constraint_array = Arr::unshift($constraint_array, $key, array(new Assert\NotBlank(), new Assert\Email()));
                }
            }

            if($key == 'phone'){

                $countRow = 0;

                if($corrector == true){
                    $countRow = ErrorChecking::connectUntity($mapperRow, $key, $value);
                }

                if($countRow > 0){             
                    $constraint_array = Arr::unshift($constraint_array, $key, new Assert\Callback(ErrorChecking::formingError($app, $mapperRow, $arrayValidationCorrector[$key][0]))); 
                } else {
                    $constraint_array = Arr::unshift($constraint_array, $key, array(new Assert\NotBlank(), new Assert\Length(array('min' => $arrayValidation[$key]['length']))));
                }

            }

            if($key == 'name'){
            	$constraint_array = Arr::unshift($constraint_array, $key, array(new Assert\NotBlank(), new Assert\Length(array('min' => $arrayValidation[$key]['length']))));
            }
            
            if($key == 'password'){
            	$constraint_array = Arr::unshift($constraint_array, $key, array(new Assert\NotBlank(), new Assert\Length(array('min' => $arrayValidation[$key]['length']))));
            }

            if($key == 'b_day'){
            	$constraint_array = Arr::unshift($constraint_array, $key, array(new Assert\NotBlank(), new Assert\Length(array('min' => $arrayValidation[$key]['length']))));
            }

            if($key == 'b_month'){
            	$constraint_array = Arr::unshift($constraint_array, $key, array(new Assert\NotBlank(), new Assert\Length(array('min' => $arrayValidation[$key]['length']))));
            }

            if($key == 'b_year'){
            	$constraint_array = Arr::unshift($constraint_array, $key, array(new Assert\NotBlank(), new Assert\Length(array('min' => $arrayValidation[$key]['length']))));
            }

            if($key == 'skype'){
            	$constraint_array = Arr::unshift($constraint_array, $key, array(new Assert\NotBlank(), new Assert\Length(array('min' => $arrayValidation[$key]['length']))));
            }

            if($key == 'type'){
            	$constraint_array = Arr::unshift($constraint_array, $key, array(new Assert\NotBlank(), new Assert\Length(array('min' => $arrayValidation[$key]['length']))));
            }

            if($key == 'city'){
            	$constraint_array = Arr::unshift($constraint_array, $key, array(new Assert\NotBlank(), new Assert\Length(array('min' => $arrayValidation[$key]['length']))));
            }

            if($key == 'area'){
            	$constraint_array = Arr::unshift($constraint_array, $key, array(new Assert\NotBlank(), new Assert\Length(array('min' => $arrayValidation[$key]['length']))));
            }

            if($key == 'avatar'){
            	$constraint_array = Arr::unshift($constraint_array, $key, array(new Assert\NotBlank(), new Assert\Length(array('min' => $arrayValidation[$key]['length']))));
            }

            if($key == 'account_type'){
                $constraint_array = Arr::unshift($constraint_array, $key, array(new Assert\NotBlank(), new Assert\Length(array('min' => $arrayValidation[$key]['length']))));
            }
            
            if($key == 'title'){
                $constraint_array = Arr::unshift($constraint_array, $key, array(new Assert\NotBlank(), new Assert\Length(array('min' => $arrayValidation[$key]['length']))));
            }
            
            if($key == 'text'){
                $constraint_array = Arr::unshift($constraint_array, $key, array(new Assert\NotBlank(), new Assert\Length(array('min' => $arrayValidation[$key]['length']))));
            }

            if($key == 'price'){
                $constraint_array = Arr::unshift($constraint_array, $key, array(new Assert\NotBlank(), new Assert\Length(array('min' => $arrayValidation[$key]['length']))));
            }

            if($key == 'adv_id'){
                $constraint_array = Arr::unshift($constraint_array, $key, array(new Assert\NotBlank(), new Assert\Length(array('min' => $arrayValidation[$key]['length']))));
            }

            if($key == 'category'){
                $constraint_array = Arr::unshift($constraint_array, $key, array(new Assert\NotBlank(), new Assert\Length(array('min' => $arrayValidation[$key]['length']))));
            }             

            if($key == 'code'){
                $constraint_array = Arr::unshift($constraint_array, $key, array(new Assert\NotBlank(), new Assert\Length(array('min' => $arrayValidation[$key]['length'], 'max' => $arrayValidation[$key]['length']))));
            }

            if($key == 'id'){
                $constraint_array = Arr::unshift($constraint_array, $key, array(new Assert\NotBlank(), new Assert\Length(array('min' => $arrayValidation[$key]['length']))));
            }



            if($key == 'mimeType'){
                    $constraint_array = Arr::unshift($constraint_array, $key,
                 new Assert\File(array('mimeTypes' => $arrayValidation['mimeTypeImage']['mimeTypes'])));
            }  

    	}

        $constraint = new Assert\Collection($constraint_array);
        $errors = $app['validator']->validate($arrayErrorChecking, $constraint);

        return $errors;
    }

    static public function connectUntity($mapperRow, $key, $value)
    {  
        $entityCount = $mapperRow->all()->where([$key => $value])->count();
        return $entityCount;
    }

    static public function formingError($app, $mapperRow, $textError)
    {  
        return function ($object, \Symfony\Component\Validator\Context\ExecutionContextInterface $context) use ($app, $mapperRow, $textError){
            $context->buildViolation($textError)
                ->addViolation();
        };
    }
}