<?php

namespace BZR\Entity;

use BZR\Exception;
use BZR\Translation\Keys;
use \Silex\Application\TranslationTrait;

class News extends Base
{
    protected static $table = 'news';

    protected static $mapper = 'BZR\Entity\Mapper\News';

    public static function fields()
    {
        return [
            'id'       => ['type' => 'integer', 'autoincrement' => true],
            'title_ru' => ['type' => 'string'],
            'title_en' => ['type' => 'string'],
            'title_gr' => ['type' => 'string'],
            'text_ru'  => ['type' => 'string'],
            'text_en'  => ['type' => 'string'],
            'text_gr'  => ['type' => 'string'],
            'image'    => ['type' => 'string'],
            'date'     => ['type' => 'integer'],
            'status'   => ['type' => 'integer'],
            'link'     => ['type' => 'string'],
        ];
    }
    
}