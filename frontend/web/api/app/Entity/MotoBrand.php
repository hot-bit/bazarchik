<?php

namespace BZR\Entity;

class MotoBrand extends Base
{
    static public $table = 'moto_brand';

    static public function fields()
    {
        return [
            'id' => ['type' => 'integer', 'primary' => true],
            'name' => ['type' => 'string'],
            'count' => ['type' => 'integer'],
        ];
    }
}
