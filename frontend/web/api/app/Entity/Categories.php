<?php

namespace BZR\Entity;
use BZR\Application;
use BZR\Arr;

/**
 * Class Categories
 * @package BZR\Entity
 *
 * @property int $id
 * @property int $parent
 */
class Categories extends Base
{
    const NO_PARENT = 0;

    protected static $table = 'categories';

    protected static $mapper = 'BZR\Entity\Mapper\Categories';

    protected $translationMap = [
        Application::LOCALE_EN => 'name_en',
        Application::LOCALE_RU => 'name_ru',
        Application::LOCALE_GR => 'name_gr',
    ];

    public static function fields()
    {
        return [
            'id'         => ['type' => 'integer', 'autoincrement' => true],
            'name_ru'    => ['type' => 'string'],
            'name_en'    => ['type' => 'string'],
            'name_gr'    => ['type' => 'string'],
            'parent'     => ['type' => 'integer'],
            'level'      => ['type' => 'integer'],
            'filter'     => ['type' => 'string'],
            'url'        => ['type' => 'text'],
            'slave'      => ['type' => 'integer'],
            'position'   => ['type' => 'integer'],
            'status'     => ['type' => 'integer'],
            'header'     => ['type' => 'integer'],
            'count'      => ['type' => 'integer'],

            'hidden_for_mobile' => ['type' => 'integer'],
        ];
    }

    /**
     * @param $locale
     * @return string
     */
    public function getTranslatedName($locale)
    {
        $default = 'name_en';
        $field   = Arr::get($this->translationMap, $locale, $default);

        return $this->$field;
    }
}