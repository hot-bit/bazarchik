<?php

namespace BZR\Entity;

class Money extends Base
{
    protected static $table = 'money';

    public static function fields()
    {
        return [
            'id'      => ['type' => 'integer', 'autoincrement' => true],
            'user_id' => ['type' => 'integer'],
            'price'   => ['type' => 'integer'],
            'date'    => ['type' => 'integer'],
            'comment' => ['type' => 'string']
        ];
    }
}