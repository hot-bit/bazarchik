<?php

namespace BZR\Entity;

use Spot\EntityInterface;
use Spot\MapperInterface;

/**
 * Class UsersCategoriesSubscription
 * @package BZR\Entity
 *
 * @property int $id
 * @property int $user_id
 * @property int $category_id
 *
 * @property \Spot\Relation\BelongsTo | Profile $profile
 * @property \Spot\Relation\BelongsTo | Categories $category
 */
class UsersCategoriesSubscription  extends Base
{
    public static $table = 'user_category_subscription';

    protected static $mapper = 'BZR\Entity\Mapper\UsersCategoriesSubscription';

    public static function fields()
    {
        return [
            'id'           => ['type' => 'integer', 'primary' => true, 'autoincrement' => true],
            'user_id'      => ['type' => 'integer'],
            'category_id'  => ['type' => 'integer'],
        ];
    }

    public static function relations(MapperInterface $mapper, EntityInterface $entity)
    {
        return [
            'profile' => $mapper->belongsTo($entity, 'BZR\Entity\Profile', 'user_id'),
            'category' => $mapper->belongsTo($entity, 'BZR\Entity\Categories', 'category_id'),
        ];
    }
}
