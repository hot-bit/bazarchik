<?php

namespace BZR\Entity;

class UsersAuthMass  extends Base
{
    static public $table = 'users_auth_mass';

    static public function fields()
    {
        return [
            'id'           => ['type' => 'integer', 'primary' => true, 'autoincrement' => true],
            'user_id'      => ['type' => 'integer'],
            'mobile_token' => ['type' => 'string'],
            'auth_token'   => ['type' => 'string'],
            'lat'          => ['type' => 'string'],
            'lng'          => ['type' => 'string'],
            'last_login'   => ['type' => 'integer'],
        ];
    }
}
