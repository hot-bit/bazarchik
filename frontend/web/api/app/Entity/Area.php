<?php

namespace BZR\Entity;

class Area extends Base
{
    static public $table = 'area';

    static public function fields()
    {
        return [
            'id'      => ['type' => 'integer', 'primary' => true],
            'name_ru' => ['type' => 'string'],
            'name_en' => ['type' => 'string'],
            'name_gr' => ['type' => 'string'],
            'city'    => ['type' => 'integer'],
            'lat'     => ['type' => 'integer'],
            'lng'     => ['type' => 'integer']
        ];
    }
}
