<?php

namespace BZR\Entity;

class Brand extends Base
{
    static public $table = 'brand';

    static public function fields()
    {
        return [
            'id' => ['type' => 'integer', 'primary' => true],
            'name' => ['type' => 'string'],
            'count_new' => ['type' => 'integer'],
            'count_used' => ['type' => 'integer'],
            'count_rent' => ['type' => 'integer'],
        ];
    }
}
