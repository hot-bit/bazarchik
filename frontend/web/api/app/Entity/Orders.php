<?php

namespace BZR\Entity;
use Spot\EntityInterface;
use Spot\MapperInterface;

/**
 * Class Orders
 * @package BZR\Entity
 *
 * @property $id
 * @property $id_adv
 * @property $user_id
 * @property $user_email
 * @property $price
 * @property $date
 * @property $items
 * @property $token
 * @property $status
 * @property $type
 * @property $transaction_id
 *
 * @property Adv $adv
 */
class Orders extends Base
{
    protected static $table = 'orders';

    protected static $mapper = 'BZR\Entity\Mapper\Orders';

    public static function fields()
    {
        return [
            'id'           => ['type' => 'integer', 'autoincrement' => true],
            'id_adv'       => ['type' => 'integer'],
            'user_id'      => ['type' => 'integer'],
            'user_email'   => ['type' => 'string'],
            'price'        => ['type' => 'integer'],
            'date'         => ['type' => 'integer'],
            'items'        => ['type' => 'text'],
            'token'        => ['type' => 'string'],
            'status'       => ['type' => 'integer'],
            'type'         => ['type' => 'integer'],
            'transaction_id' => ['type' => 'string'],
        ];
    }

    static public function relations(MapperInterface $mapper, EntityInterface $entity)
    {
        return [
            'adv' => $mapper->belongsTo($entity, 'BZR\Entity\Adv', 'id_adv'),
        ];
    }

}