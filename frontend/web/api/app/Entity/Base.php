<?php

namespace BZR\Entity;

use BZR\Exception;
use Spot\Mapper;


abstract class Base extends \Spot\Entity
{
    /**
     * @param $relationName
     * @return Mapper
     * @throws Exception
     */
    public function getRelationMapper($relationName)
    {
        if (!isset($this->$relationName)) {
            throw new Exception('Unknown relation for entity "' . get_called_class() . '": "$relationName"');
        }

        $relation = $this->$relationName;

        return $relation->getMapper($relation->entityName());
    }
}