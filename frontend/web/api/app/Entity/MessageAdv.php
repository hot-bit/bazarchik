<?php

namespace BZR\Entity;

class MessageAdv extends Base
{
    static public $table = 'message_adv';

    static public function fields()
    {
        return [
            'id'      => ['type' => 'integer', 'autoincrement' => true],
            'user_id' => ['type' => 'integer', 'required' => true],
            'adv_id'  => ['type' => 'integer', 'required' => true],
            'name'    => ['type' => 'string', 'required' => true],
            'phone'   => ['type' => 'string', 'required' => true],
            'email'   => ['type' => 'string', 'required' => true],
            'text'    => ['type' => 'text', 'required' => true],
            'date'    => ['type' => 'integer', 'value' => ''],
            'status'  => ['type' => 'text', 'value' => ''],
        ];
    }
}
