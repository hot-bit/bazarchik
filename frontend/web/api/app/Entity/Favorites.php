<?php

namespace BZR\Entity;

class Favorites extends Base
{
    //добавить уник ключ к user_id
    static public $table = 'favorite';

    static public function fields()
    {
        return [
            'user_id' => ['type' => 'integer', 'primary' => true],
            'adv_id'  => ['type' => 'integer'],
            'favorite_date'  => ['type' => 'integer'],
        ];
    }
}
