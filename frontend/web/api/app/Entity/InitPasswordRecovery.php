<?php

namespace BZR\Entity;

class InitPasswordRecovery extends Base
{

    static public $table = 'password_recovery';

    static public function fields()
    {
        return [
            'id' => ['type' => 'integer', 'autoincrement' => true, 'primary' => true],
            'user_id' => ['type' => 'integer'],
            'code' => ['type' => 'string'],
            'time_code' => ['type' => 'integer']
        ];
    }
}