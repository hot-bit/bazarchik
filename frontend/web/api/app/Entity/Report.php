<?php

namespace BZR\Entity;

/**
 * Class Report
 * @package BZR\Entity
 */
class Report extends Base
{
    protected static $table = 'reports';

    public static function fields()
    {
        return [
            'id'     => ['type' => 'integer', 'autoincrement' => true],
            'adv_id' => ['type' => 'integer'],
            'text'   => ['type' => 'string'],
            'date'   => ['type' => 'integer'],
            'ip'     => ['type' => 'string'],
        ];
    }
}