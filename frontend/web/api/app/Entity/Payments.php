<?php

namespace BZR\Entity;

use BZR\Exception;
use BZR\Translation\Keys;
use \Silex\Application\TranslationTrait;

class Payments extends Base
{
    protected static $table = 'payment';

    protected static $mapper = 'BZR\Entity\Mapper\Payments';

    public static function fields()
    {
        return [
            'id'           => ['type' => 'integer', 'autoincrement' => true],
            'user_id'      => ['type' => 'integer'],
            'type_payment' => ['type' => 'integer'],
            'id_payment'   => ['type' => 'string'],
            'price'        => ['type' => 'string'],
            'date'         => ['type' => 'integer']
        ];
    }
    
}