<?php

namespace BZR\Entity;

/**
 * Class CategoryUpdate
 * @package BZR\Entity
 *
 * @property int $id
 * @property int $category_id
 * @property int $count
 * @property int $send_time
 */
class CategoryUpdate extends Base
{
    protected static $table = 'category_updates';

    public static function fields()
    {
        return [
            'id'          => ['type' => 'integer', 'autoincrement' => true],
            'category_id' => ['type' => 'integer'],
            'count'       => ['type' => 'integer'],
            'send_time'   => ['type' => 'integer'],
        ];
    }

    public function inc()
    {
        $this->count = $this->count + 1;
    }

    public function clear()
    {
        $this->count = 0;
        $this->send_time = time();
    }
}