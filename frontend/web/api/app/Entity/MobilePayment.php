<?php

namespace BZR\Entity;

use Spot\Entity;
use Spot\EntityInterface;
use Spot\EventEmitter;
use Spot\Mapper;
use Spot\MapperInterface;

class MobilePayment extends \Spot\Entity
{
    protected static $table = 'mobile_payment';

    public static function fields()
    {
        return [
            'id'         => ['type' => 'integer', 'autoincrement' => true, 'primary' => true],
            'user_id'    => ['type' => 'integer',  'required' => true],
            'payment_id' => ['type' => 'string',  'required' => true],
            'created_at' => ['type' => 'integer',  'required' => true],
            'updated_at' => ['type' => 'integer',  'required' => true],
            'status'     => ['type' => 'integer',  'required' => true],
        ];
    }

    public static function events(EventEmitter $eventEmitter)
    {
        $eventEmitter->on('beforeSave', function (Entity $entity, Mapper $mapper) {
            if ($entity->isNew()) {
                $entity->created_at = time();
            }

            if ($entity->isModified()) {
                $entity->updated_at = time();
            }
        });
    }

    public static function relations(MapperInterface $mapper, EntityInterface $entity)
    {
        return [
            'profile' => $mapper->belongsTo($entity, 'BZR\Entity\Profile', 'user_id')
        ];
    }

    public function isCompleted()
    {
        return $this->status == 1;
    }

    public function setCompleted()
    {
        $this->status = 1;
    }

    public function setCancelled()
    {
        $this->status = 2;
    }

}