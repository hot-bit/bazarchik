<?php

namespace BZR\Entity;

class Model extends Base
{
    static public $table = 'model';

    static public function fields()
    {
        return [
            'id' => ['type' => 'integer', 'primary' => true],
            'brand_id' => ['type' => 'integer'],
            'name' => ['type' => 'string'],
            'count_used' => ['type' => 'integer'],
            'count_new' => ['type' => 'integer'],
            'count_rent' => ['type' => 'integer'],
        ];
    }
}
