<?php

namespace BZR\Entity;

class AdsLastDateUser  extends Base
{
    protected static $mapper = 'BZR\Entity\Mapper\AdsLastDateUser';

    static public $table = 'ads_last_date_user';

    static public function fields()
    {
        return [
            'id' => ['type' => 'integer', 'primary' => true, 'autoincrement' => true],
            'id_category' => ['type' => 'integer'],
            'user_id' => ['type' => 'integer'],  
            'visit_date' => ['type' => 'integer'],
        ];
    }

}
