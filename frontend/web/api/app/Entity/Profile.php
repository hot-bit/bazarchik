<?php

namespace BZR\Entity;

use BZR\Exception;
use Spot\EntityInterface;
use Spot\MapperInterface;

/**
 * Class Profile
 *
 * @property $id
 * @property $success_phone
 * @property $success_email
 * @property $confirm_site
 * @property $lat
 * @property $lng
 *
 * @property  \Spot\Relation\HasMany | UsersCategoriesSubscription[] $subscriptions
 * @property  \Spot\Relation\HasMany | UsersPushToken[] $pushTokens
 * @property  \Spot\Relation\HasMany | Adv[] $advertisements
 * @property  \Spot\Relation\HasMany | Adv[] $activeAdvertisements
 * @property  \Spot\Relation\HasMany | MessageAdv[] $newMessage
 *
 */
class Profile extends Base
{
    protected static $table = 'users';

    protected static $mapper = 'BZR\Entity\Mapper\Profile';

    public static function fields()
    {
        return [
            'id'            => ['type' => 'integer', 'autoincrement' => true],
            'name'          => ['type' => 'string', 'required' => true],
            'role'          => ['type' => 'integer', 'value' => ''],
            'type'          => ['type' => 'integer', 'value' => ''],
            'status'        => ['type' => 'integer', 'value' => ''],
            'password'      => ['type' => 'string', 'value' => ''],
            'salt'          => ['type' => 'string', 'value' => ''],
            'phone'         => ['type' => 'string', 'value' => ''],
            'email'         => ['type' => 'string', 'required' => true, 'unique' => 'email'],
            'avatar'        => ['type' => 'string', 'value' => ''],
            'skype'         => ['type' => 'string', 'value' => ''],
            'website'       => ['type' => 'string', 'value' => ''],
            'company'       => ['type' => 'string', 'value' => ''],
            'lat'           => ['type' => 'string', 'value' => ''],
            'lng'           => ['type' => 'string', 'value' => ''],
            'gender'        => ['type' => 'integer', 'value' => ''],
            'last_active'   => ['type' => 'integer', 'value' => ''],
            'b_day'         => ['type' => 'integer', 'value' => ''],
            'b_month'       => ['type' => 'integer', 'value' => ''],
            'b_year'        => ['type' => 'integer', 'value' => ''],
            'last_login'    => ['type' => 'integer', 'value' => ''],
            'date_reg'      => ['type' => 'integer', 'value' => ''],
            'gmt'           => ['type' => 'integer', 'value' => ''],
            'hash'          => ['type' => 'string', 'value' => ''],
            'ip'            => ['type' => 'string', 'value' => ''],
            'city'          => ['type' => 'integer', 'value' => ''],
            'area'          => ['type' => 'integer', 'value' => ''],
            'fav'           => ['type' => 'string', 'value' => ''],
            'sms_code'      => ['type' => 'integer', 'value' => ''],
            'email_code'    => ['type' => 'string', 'value' => ''],
            'success_phone' => ['type' => 'boolean', 'value' => ''],
            'success_email' => ['type' => 'boolean', 'value' => ''],
            'fb_id'         => ['type' => 'string', 'value' => ''],
            'g_id'          => ['type' => 'string', 'value' => ''],
            'fb_access'     => ['type' => 'string', 'value' => ''],
            'money'         => ['type' => 'float', 'value' => ''],
            'count_sms'     => ['type' => 'integer', 'value' => ''],
            'confirm_site'  => ['type' => 'integer', 'value' => ''],
            'send_sms'      => ['type' => 'integer', 'value' => ''],
            'send_push'     => ['type' => 'integer', 'value' => ''],
            'locale'        => ['type' => 'string', 'value' => ''],
        ];
    }

    public static function relations(MapperInterface $mapper, EntityInterface $entity)
    {
        return [
            'subscriptions'  => $mapper->hasMany($entity, 'BZR\Entity\UsersCategoriesSubscription', 'user_id'),
            'pushTokens'     => $mapper->hasMany($entity, 'BZR\Entity\UsersPushToken', 'user_id'),
            'newMessage'     => $mapper->hasMany($entity, 'BZR\Entity\MessageAdv', 'user_id')->where(['status' => 1]),
            'advertisements' => $mapper->hasMany($entity, 'BZR\Entity\Adv', 'user'),
            'activeAdvertisements' => $mapper->hasMany($entity, 'BZR\Entity\Adv', 'user')->where(['status' => [2, 4]]),
        ];
    }

    /**
     * @param $amount
     * @throws Exception
     */
    public function addMoney($amount)
    {
        if ($amount < 0) {
            throw new Exception("Incorrect money operation: \"add $amount\"");
        }
        $this->money = $this->money + $amount;
    }

    /**
     * @param $amount
     * @throws Exception
     */
    public function takeMoney($amount)
    {
        if ($amount < 0 || ($this->money - $amount) < 0) {
            throw new Exception("Incorrect money operation: \"take $amount\"");
        }
        $this->money = $this->money - $amount;
    }

    /**
     * @return bool
     */
    public function needToSendPushNotifications()
    {
        return (bool) $this->send_push;
    }

    public function enablePushNotifications()
    {
        $this->send_push = 1;
    }

    public function disablePushNotifications()
    {
        $this->send_push = 0;
    }

    /**
     * @param Categories $category
     * @return bool
     */
    public function isUserSubscribed(Categories $category)
    {
        foreach ($this->subscriptions as $subscription) {
            if ($subscription->category_id === $category->id) {
                return true;
            }
        }

        return false;
    }

    public function isPhoneConfirmed()
    {
        return $this->success_phone == 1;
    }

    public function isEmailConfirmed()
    {
        return $this->success_email == 1;
    }

    public function isSiteConfirmed()
    {
        return $this->confirm_site == 1;
    }
}