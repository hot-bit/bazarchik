<?php

namespace BZR\Entity;

class AdvImages extends Base
{
    static public $table = 'adv_images';

    static public function fields()
    {
        return [
            'id'     => ['type' => 'integer', 'autoincrement' => true, 'primary' => true],
            'adv_id' => ['type' => 'integer'],
            'name'   => ['type' => 'string'],
            'dir'    => ['type' => 'string'],
            'main'   => ['type' => 'smallint'],
            'm'           => ['type' => 'smallint'],
            'l'           => ['type' => 'smallint'],
            's'           => ['type' => 'smallint'],
            'craftMobile' => ['type' => 'smallint'],

        ];
    }
}