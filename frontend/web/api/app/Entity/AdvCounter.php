<?php

namespace BZR\Entity;

class AdvCounter extends Base
{

    static public $table = 'adv_counter';

    static public function fields()
    {
        return [
            'id' => ['type' => 'integer', 'primary' => true],
            'total' => ['type' => 'integer'],
            'today' => ['type' => 'integer'],
            'last_date' => ['type' => 'integer']
        ];
    }
}
