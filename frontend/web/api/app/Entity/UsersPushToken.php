<?php

namespace BZR\Entity;

use Spot\EntityInterface;
use Spot\MapperInterface;

/**
 * Class UsersPushToken
 * @package BZR\Entity
 *
 * @property int $id
 * @property int $user_id
 * @property string $type
 * @property string $token
 *
 * @property Profile $profile
 */
class UsersPushToken  extends Base
{
    const TYPE_IOS     = 'ios';
    const TYPE_ANDROID = 'android';

    public static $table = 'user_push_token';

    protected static $mapper = 'BZR\Entity\Mapper\UsersPushToken';

    public static function fields()
    {
        return [
            'id'      => ['type' => 'integer', 'primary' => true, 'autoincrement' => true],
            'user_id' => ['type' => 'integer'],
            'type'    => ['type' => 'string'],
            'token'   => ['type' => 'string'],
        ];
    }

    public static function relations(MapperInterface $mapper, EntityInterface $entity)
    {
        return [
            'profile' => $mapper->belongsTo($entity, 'BZR\Entity\Profile', 'user_id')
        ];
    }

    /**
     * @return bool
     */
    public function isAndroid()
    {
        return $this->type === self::TYPE_ANDROID;
    }

    /**
     * @return bool
     */
    public function isIos()
    {
        return $this->type === self::TYPE_IOS;
    }
}
