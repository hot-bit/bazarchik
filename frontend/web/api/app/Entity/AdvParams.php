<?php

namespace BZR\Entity;

class AdvParams extends Base
{

    static public $table = 'adv_params';

    static public function fields()
    {
        return [
            'id' => ['type' => 'integer', 'autoincrement' => true, 'primary' => true],
            'adv_id' => ['type' => 'integer'],
            'name' => ['type' => 'string'],
            'value' => ['type' => 'string']
        ];
    }
}
