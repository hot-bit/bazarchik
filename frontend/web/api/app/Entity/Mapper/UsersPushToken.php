<?php

namespace BZR\Entity\Mapper;

use Spot\Mapper;
use \BZR\Entity\UsersPushToken as UsersPushTokenEntity;

/**
 * Class UsersPushToken
 * @package BZR\Entity\Mapper
 */
class UsersPushToken extends Mapper
{
    /**
     * @param array $userIdList
     * @return array
     */
    public function getTokenArray(array $userIdList)
    {

        $query = $this->select(["token", "type"])->where(['user_id' => $userIdList]);
        $stmt  = $query->builder()->execute();
        $stmt->setFetchMode(\PDO::FETCH_ASSOC);

        $entityName = $this->entity();
        $result     = [
            UsersPushTokenEntity::TYPE_ANDROID => [],
            UsersPushTokenEntity::TYPE_IOS     => [],
        ];

        foreach ($stmt as $data) {
            $data = $this->convertToPHPValues($entityName, $data);

            if ($data['type'] === UsersPushTokenEntity::TYPE_ANDROID) {
                $result[UsersPushTokenEntity::TYPE_ANDROID][] = $data['token'];
            } elseif ($data['type'] === UsersPushTokenEntity::TYPE_IOS) {
                $result[UsersPushTokenEntity::TYPE_IOS][] = $data['token'];
            }
        }

        $stmt->closeCursor();

        return $result;
    }
}
