<?php

namespace BZR\Entity\Mapper;

use BZR\Arr;
use BZR\Exception;
use Spot\Mapper;

class AdsLastDateUser extends Mapper
{
    public function visit($categoryId, $userId)
    {
        $visit = $this->first(['id_category' => $categoryId, 'user_id' => $userId]);

        if (!$visit) {
            $visit = $this->build(['id_category' => $categoryId, 'user_id' => $userId]);
        }

        $visit->visit_date = time();

        $this->save($visit);
    }
}