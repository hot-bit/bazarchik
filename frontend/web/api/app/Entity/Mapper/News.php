<?php

namespace BZR\Entity\Mapper;

use BZR\Arr;
use BZR\Entity\Adv;
use BZR\Exception;
use Spot\Mapper;

class News extends Mapper
{
    public function getCount()
    {
        return $this->where(['status' => 1])->count();
    }

    public function getPage($page, $limit = 10)
    {
        $list = $this->where(['status' => 1])
            ->order(['date' => 'DESC'])
            ->offset($limit * max(0, $page - 1))
            ->limit($limit);

        return $list;
    }

}