<?php

namespace BZR\Entity\Mapper;

use BZR\Arr;
use BZR\Exception;
use Spot\Mapper;

class Categories extends Mapper
{
    public function getCategoriesWithParent($parent = \BZR\Entity\Categories::NO_PARENT)
    {
        return $this->where(['parent' => $parent]);
    }

    public function getCategoriesIdWithParent($parent = \BZR\Entity\Categories::NO_PARENT)
    {
        $categories = $this->getCategoriesWithParent($parent);
        $result    = [];

        foreach ($categories as $category) {
            $result[] = $category->id;
        }

        return $result;
    }

    protected $cacheFile = '/tmp/bazar.category.cache';

    /**
     * @return array
     */
    protected function getCache()
    {
        if (file_exists($this->cacheFile)) {
            $time = filemtime($this->cacheFile);

            if ($time + 3600 > time()) {
                $content = file_get_contents($this->cacheFile);
                $data = [];

                if ($content) {
                    $data = json_decode($content, true);
                }

                return is_array($data) ? $data : [];
            }
        }

        return [];
    }

    /**
     * @param $categoryId
     * @param $parentCategoryId
     */
    protected function saveParentCategoryToCache($categoryId, $parentCategoryId)
    {

        $data = $this->getCache();
        $data[$categoryId] = $parentCategoryId;
        file_put_contents($this->cacheFile, json_encode($data));
    }

    /**
     * @param $categoryId
     * @return int | false
     */
    protected function getParentCategoryFromCache($categoryId)
    {
        $data = $this->getCache();
        return Arr::get($data, $categoryId, false);
    }

    /**
     * @param int | \BZR\Entity\Categories $category
     * @return bool | \BZR\Entity\Categories
     * @throws Exception
     */
    public function getParentCategory($category)
    {
        if (is_numeric($category)) {
            $categoryId = $category;
        } elseif ($category instanceof \BZR\Entity\Categories) {
            $categoryId = $category->id;
        } else {
            throw new Exception("Invalid argument");
        }

        $parentCategoryId = $this->getParentCategoryFromCache($categoryId);

        if (!$parentCategoryId) {
            if (is_numeric($category)) {
                $category = $this->first(['id' => $category]);
            }

            $iterations = 0;
            while ($iterations < 5 && $category->parent !== \BZR\Entity\Categories::NO_PARENT) {
                $category = $this->first(['id' => $category->parent]);
                $iterations++;
            }

            if ($category) {
                $this->saveParentCategoryToCache($categoryId, $category->id);
            }
        } else {
            $category = $this->first(['id' => $parentCategoryId]);
        }

        return $category;
    }

    /**
     * @param $categoryId
     */
    protected function doUpdateCategory($categoryId)
    {
        $mapper = $this->getMapper('\BZR\Entity\CategoryUpdate');
        /**
         * @var \BZR\Entity\CategoryUpdate $entity
         */
        $entity = $mapper->first(['category_id' => $categoryId]);

        if (!$entity) {
            $entity = $mapper->build([
                'category_id' => $categoryId,
                'count'       => 0,
                'send_time'   => strtotime(date("d-m-Y"))
            ]);
        }

        $entity->inc();

        $mapper->save($entity);
    }

    /**
     * @param $categoryId
     */
    public function updateCategory($categoryId)
    {
        $parentCategory = $this->getParentCategory($categoryId);
        if ($parentCategory) {
            $this->doUpdateCategory($parentCategory->id);
        }
        $this->doUpdateCategory($categoryId);
    }
}