<?php

namespace BZR\Entity\Mapper;

use BZR\Arr;
use BZR\Entity\Adv;
use BZR\Exception;
use Spot\Mapper;

class Payments extends Mapper
{
    protected static $typeMap = [
        Adv::ITEM_ADD_BALANCE => 1,
        Adv::ITEM_COLOR => 2,
        Adv::ITEM_UP => 3,
        Adv::ITEM_VIP => 4,
        Adv::ITEM_PREMIUM => 5,
    ];

    public function getUpgradeTypeId($type)
    {
        if (!isset(self::$typeMap[$type])) {
            throw new Exception("Unknown balance upgrade type");
        }

        return self::$typeMap[$type];
    }

    public function getTypeTranslationKey($type)
    {
        $key = array_search($type, self::$typeMap);

        if ($key == false) {
            throw new Exception("Unknown balance select type");
        }

        return $key;
    }
}