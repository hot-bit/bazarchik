<?php

namespace BZR\Entity\Mapper;

use Spot\Mapper;

class Profile extends Mapper
{
    public function urlPicturesAvatar ($app, $enityProfile){
        //преобразуем аватар в ссылку
        $substr_count = substr_count($enityProfile[0]->avatar ,'//');

        if ($substr_count == 0) {
            $avatar = $app['config']['urlPicturesAvatar'] . '/' . $enityProfile[0]->id . '/small_user_'.$enityProfile[0]->id.'.jpg';
            $enityProfile[0]->avatar = $avatar;
        }
    }
}