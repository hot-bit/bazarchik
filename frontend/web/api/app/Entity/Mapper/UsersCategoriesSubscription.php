<?php

namespace BZR\Entity\Mapper;

use BZR\Application;
use Spot\Mapper;

class UsersCategoriesSubscription extends Mapper
{
    /**
     * @param $categoryId
     * @param $parentCategoryId
     * @return array ['locale' => [userId, ...], ...]
     */
    public function getSubscribedUserIdList($categoryId, $parentCategoryId)
    {
        $profileMapper = $this->getMapper('\BZR\Entity\Profile');

        $table = $this->table();
        $profileTable = $profileMapper->table();

        $query = "SELECT sub.user_id as user_id, profile.locale as locale FROM $table as sub 
                  LEFT JOIN $profileTable as profile ON profile.id = sub.user_id 
                  WHERE sub.category_id = :categoryId AND profile.send_push = 1";

        $stmt = $this->connection()->executeQuery($query, [':categoryId' => $categoryId]);
        $stmt->setFetchMode(\PDO::FETCH_ASSOC);

        $usersList = [];
        $usersExcludeList = [];
        foreach ($stmt as $data) {
            $locale = $data['locale'];
            $userId = (int) $data['user_id'];
            if (!$locale) {
                $locale = Application::LOCALE_EN;
            }

            $usersList[$userId] = $locale;
        }
        $stmt->closeCursor();

        if (count($usersList) > 0 && $parentCategoryId && $categoryId != $parentCategoryId) {
            $idList = implode(", ", array_keys($usersList));

            $query = "SELECT sub.user_id as user_id FROM $table as sub 
                  WHERE sub.category_id = :parentCategoryId AND sub.user_id IN($idList)";

            $stmt = $this->connection()->executeQuery($query, [':parentCategoryId' => $parentCategoryId]);
            $stmt->setFetchMode(\PDO::FETCH_ASSOC);

            foreach ($stmt as $data) {
                $userId = (int) $data['user_id'];
                $usersExcludeList[$userId] = true;
            }

            $stmt->closeCursor();
        }

        $userIdList = [];

        foreach ($usersList as $userId => $locale) {
            if (isset($usersExcludeList[$userId])) {
                continue;
            }

            if (!isset($userIdList[$locale])) {
                $userIdList[$locale] = [];
            }

            $userIdList[$locale][] = $userId;
        }

        return $userIdList;
    }
}
