<?php

namespace BZR\Entity\Response;

use BZR\Application;
use \BZR\Entity\Profile;

class Subscriptions extends Base
{
    private $app;
    private $profile;
    private $categories;
    private $subscriptions;

    /**
     * User constructor.
     * @param Application $app
     * @param Profile $profile
     * @param \BZR\Entity\Categories[] $categories
     * @param \BZR\Entity\UsersCategoriesSubscription[] $subscriptions
     */
    public function __construct(Application $app, Profile $profile, $categories, $subscriptions)
    {
        $this->profile       = $profile;
        $this->categories    = $categories;
        $this->subscriptions = $subscriptions;
        $this->app           = $app;
    }

    /**
     * @return array
     */
    public function toArray()
    {
        $profile       = $this->profile;
        $subscriptions = $this->subscriptions;
        $categories    = $this->categories;
        $app           = $this->app;

        $subscribed = [];

        foreach ($subscriptions as $subscription) {
            $subscribed[] = $subscription->category_id;
        }

        $result = [
            "subscriptions" => [],
            "push"          => $profile->needToSendPushNotifications()
        ];

        foreach ($categories as $category) {
            $icon = $category->iconCategoryMobile;

            $result["subscriptions"][] = [
                'id'      => $category->id,
                'enabled' => in_array($category->id, $subscribed),
                'icon'    => $icon ? ($app->config('rootIconMainCategories', '') . $icon) : null,
                'title'   => [
                    'name_ru' => $category->name_ru,
                    'name_en' => $category->name_en ,
                    'name_gr' => $category->name_gr
                ],
            ];
        }

        return $result;
    }
}