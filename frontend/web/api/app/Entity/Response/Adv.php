<?php

namespace BZR\Entity\Response;

use BZR\Application;
use BZR\Controller\UploadImages;
use \BZR\Entity\Adv as AdvEntity;
use BZR\Arr;
use BZR\Entity\Profile;
use BZR\Exception;
use Spot\Relation\RelationAbstract;

class Adv
{
    /**
     * @var AdvEntity
     */
    private $adv;
    private $needUserInfo;

    /**
     * Adv constructor.
     * @param AdvEntity $adv
     * @param bool $needUserInfo
     */
    public function __construct(AdvEntity $adv, $needUserInfo = false)
    {
        $this->adv = $adv;
        $this->needUserInfo = $needUserInfo;
    }

    public function jsonSerialize($app, $request = false)
    {
        $summaTotal = null;
        $avatar = null;
        $photos = $this->adv->photos->toArray();
        $photosArray = [];
        $favorite = false;
        $summaTotal = 0;
        $summaToDay = 0;

        //проверяем вытаскивать аватарку или нет
        if ($request != false) {
            $avatar = $app->getAvatar($this->adv->user);
            $avatar = $app['config']['urlPicturesAvatar'].'user_'.$this->adv->user.'/'.$avatar;
        }

        //сумма для общего числа показов
        if (!is_null($this->adv->counter)) {
            $summaTotal = $this->adv->counter->total;
            $summaToDay = $this->adv->counter->today;
        }

        //корректировка изображений, дописывания URL
        foreach($photos as $photo) {

            $urlPictures = $app['config']['urlPictures'];
            $urlPicturesWm = $app['config']['urlPicturesWm'];
            $arrayComplect = [];

            //тестовый вариант, при переносе убрать craftMobile
            /*if($photo['craftMobile'] == 1){
                $urlPictures = $app['config']['urlPicturesTest'];
            }*/

            //добавляем id ищображения
            $arrayComplect['id'] = $photo['id'];
            $arrayComplect['main'] = (bool) $photo['main'];

            //набираем изображения если существуют прификсы
            if($photo['l'] > 0){
                $arrayComplect['l'] = UploadImages::getMarkedImageUrl($app, $photo['dir'], 'L_'.$photo['name']);
                //$urlPicturesWm.$photo['dir'].'/L_'.$photo['name'];
            } else {
                $arrayComplect['l'] = null;
            }

            if($photo['m'] > 0){
                $arrayComplect['m'] = $urlPictures.$photo['dir'].'/M_'.$photo['name'];
            } else {
                $arrayComplect['m'] = null;
            }

            if($photo['s'] > 0){
                $arrayComplect['s'] = $urlPictures.$photo['dir'].'/S_'.$photo['name'];
            } else {
                $arrayComplect['s'] = null;
            }

            $arrayComplect['original'] = $urlPictures.$photo['dir'].'/'.$photo['name'];


            $AD = $arrayComplect;
            if ($arrayComplect['main']) {
                $photosArray = Arr::merge([$AD], $photosArray);
            } else {
                $photosArray = Arr::merge($photosArray, [$AD]);
            }

        }

        //причастность человека к объявлению
        /**
         * @var Application $app
         */
        $getCurrentProfile = $app->getCurrentProfile();
        $own = false;

        if($getCurrentProfile){

            if($getCurrentProfile->id == $this->adv->user){
                $own = true;
            }
        }

        //распарсивание текстового массива
        $explodeText = '';
        $explodeParams = '';
        $parameters = '';
        //парсинг нужен только для категорий АВТО
        if($this->adv->category == 742 or $this->adv->category == 743){
            $explode = explode('<p>', $this->adv->text_rdy);

            if(isset($explode[1])){
                $explodeText = explode('</p>', $explode[1]);
                $explodeParams = $explodeText[1];
                $explodeText = $explode[0];
            } else {
                $explodeText = $this->adv->text_rdy;
            }
        } else {
            $explodeText = $this->adv->text_rdy;
        }

        $found = preg_match_all('/<div\s+class="params-gr".+?>(.+?)<\/div><span>(.+?)<\/span>/', $explodeParams, $matches);

        if ($found) {
            $keys = Arr::get($matches, 1);
            $values = Arr::get($matches, 2);

            foreach ($keys as $index => $key) {
                $value = Arr::get($values, $index);
                $parameters .= '<p>' . '<b>' . $key . ':</b> ' . $value . '</p>';
            }
        } else {
            $parameters = $explodeParams;
        }

        //просматриваем явлеться ли объявление добавленным в избранные у пользователя
        $getFavoriteUserAdv = $app->getFavoriteUserAdv();

        if($getFavoriteUserAdv){
            $getFavoriteUserAdv = $getFavoriteUserAdv->toArray();

            foreach($getFavoriteUserAdv as $advFavorite) {

                if($advFavorite['adv_id'] == $this->adv->id){
                    $favorite = true;
                }
            }
        }
        /**
         * @var \BZR\Application $app
         */
        $urlPattern      = $app->config(['site', 'advUrl'], '');
        $langCorrelation = $app->config(['site', 'langs'], []);
        $lang            = Arr::get($langCorrelation, $app['locale'], 'en');
        $absoluteUrl     = str_replace(['%lang%', '%url%', '%id%'], [$lang, $this->adv->url, $this->adv->id], $urlPattern);

        $userRelation = $this->adv->userProfile;

        if ($userRelation instanceof Profile) {
            $user = $userRelation;
        } elseif ($userRelation instanceof RelationAbstract) {
            $user = $userRelation->execute();
        } else {
            $user = false;
        }


        $phoneToShow = '';

        //if ($own || ($user && $user->isPhoneConfirmed())) {
            $phoneToShow = $this->adv->phone;
        //}

        $result =  [
            'id'             => $this->adv->id,
            'title'          => $this->adv->title,
            'title_original' => $this->adv->title_original ? $this->adv->title_original : $this->adv->title,
            'phone'          => $phoneToShow,
            'avatar'         => $avatar,
            'author'         => $this->adv->name,
            'date_create'    => $this->adv->date_create,
            'views'          => $summaTotal,
            'viewsToDay'     => $summaToDay,
            'user'           => $this->adv->user,
            'price'          => $this->adv->price,
            'area'           => $this->adv->area,
            'city'           => $this->adv->city,
            'email'          => $this->adv->email,
            'category'       => $this->adv->category,
            'not_email'      => $this->adv->not_email,
            'not_skype'      => $this->adv->not_skype,
            'text'           => $explodeText,
            'text_original'  => $this->adv->text,
            'favorite'       => $favorite,
            'photos'         => $photosArray,
            'params'         => $parameters,
            'own'            => $own,
            'status'         => $this->adv->status,
            'url'            => $this->adv->url,
            'abs_url'        => $absoluteUrl,
            'vip'            => ((bool) $this->adv->vip) && ($this->adv->vip_start > (time()-3600*24*7)),
            'color'          => ((bool) $this->adv->color) && ($this->adv->color_start > (time()-3600*24*7)),
            'premium'        => ((bool) $this->adv->premium) && ($this->adv->prem_start > (time()-3600*24*7)),
            'canUpForFree'   => $own ? ((bool) $this->adv->canUpForFree()) : false
        ];

        if ($this->needUserInfo) {

            if ($user) {
                $userInfo = [
                    'skype'    => $user->skype,
                    'email'    => $user->isEmailConfirmed() ? $user->email : null,
                    'phone'    => $user->isPhoneConfirmed() ? $user->phone : null,
                    'name'     => $user->name,
                    'date_reg' => $user->date_reg,
                    'website'  => $user->isSiteConfirmed() ? $user->website : null,
                    'lat'      => $user->lat,
                    'lng'      => $user->lng,

                ];

                $result = $result + ['profile' => $userInfo];
            }

        }

        return $result;
    }


}