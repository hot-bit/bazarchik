<?php

namespace BZR\Entity\Response;

use BZR\Entity\News as NewsEntity;

class News extends Base
{
    private $news;

    public function __construct(NewsEntity $news)
    {
        $this->news  = $news;
    }

    /**
     * @return array
     */
    public function toArray()
    {
        $news   = $this->news;
        $result = [
            'id' => $news->id,
            'title_ru' => $news->title_ru,
            'title_en' => $news->title_en,
            'title_gr' => $news->title_gr,
            'text_ru'  => $news->text_ru,
            'text_en'  => $news->text_en,
            'text_gr'  => $news->text_gr,
            'image'    => 'https://www.bazar-cy.com/upload/news/' . $news->image,
            'link'     => 'https://www.bazar-cy.com/en/news/' . $news->link,
            'date'     => $news->date,
        ];

        return $result;
    }
    
}