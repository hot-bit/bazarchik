<?php

namespace BZR\Entity\Response;

use Symfony\Component\HttpFoundation\JsonResponse;

abstract class Base implements \JsonSerializable
{
    abstract public function toArray();

    /**
     * @inheritdoc
     */
    public function jsonSerialize()
    {
        return $this->toArray();
    }
}