<?php

namespace BZR\Entity\Response;

use \BZR\Entity\Profile as ProfileEntity;

class User
{
    /**
     * @var ProfileEntity
     */
    private $user;

    /**
     * User constructor.
     * @param ProfileEntity $profile
     */
    public function __construct(ProfileEntity $user)
    {
        $this->user = $user;
    }

    /**
     * @return array
     */
    public function jsonSerialize($app)
    {
        $result = [
            'email'           => $this->user->email,
            'name'            => $this->user->name,
            'birthdate'       => $this->user->b_day.'.'.$this->user->b_month.'.'.$this->user->b_year,
            'email_confirmed' => (bool) $this->user->success_email,
            'phone_confirmed' => (bool) $this->user->success_phone,
            'phone'           => $this->user->phone,
            'skype'           => $this->user->skype,
            'account_type'    => $this->user->type,
            'city_id'         => $this->user->city,
            'district_id'     => $this->user->area,
            'avatar'          => $this->user->avatar,
            'website'         => $this->user->website,
            'company'         => $this->user->company,
            'lat'             => $this->user->lat,
            'lng'             => $this->user->lng,
            'coordinates'     => $this->user->user_lat.','.$this->user->user_lng,
            'money'           => $this->user->money,
            'send_sms'        => $this->user->send_sms,
            'send_push'       => $this->user->send_push,
            'newMessage'      => $this->user->newMessage->count(),
            'adv_count'       => $this->user->activeAdvertisements->count(),

            'phone_confirmation_count' => $this->user->count_sms,
        ];

        return $this->appendDevInfo($result);
    }

    /**
     * @param array $result
     * @return array
     */
    protected function appendDevInfo(array $result)
    {
        if (defined('DEV') && DEV) {
            $devInfo = [
                'sms_code'   => $this->user->sms_code,
                'email_code' => $this->user->email_code
            ];

            return $result + ['dev' => $devInfo];
        }

        return $result;
    }
}