<?php

namespace BZR\Entity;

class City extends Base
{
    static public $table = 'city';

    static public function fields()
    {
        return [
            'id' => ['type' => 'integer', 'primary' => true],
            'name_ru' => ['type' => 'string'],
            'name_en' => ['type' => 'string'],
            'name_gr' => ['type' => 'string'],
            'lat' => ['type' => 'integer'],
            'lng' => ['type' => 'integer']
        ];
    }
}
