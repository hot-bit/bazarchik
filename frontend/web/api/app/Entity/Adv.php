<?php

namespace BZR\Entity;

use BZR\Exception;
use Spot\EntityInterface as Entity;
use Spot\MapperInterface as Mapper;

/**
 * Class Adv
 * @package BZR\Entity
 *
 * @property Profile $userProfile
 *
 * @property $id
 * @property $title
 * @property $title_original
 * @property $vip
 * @property $vip_start
 * @property $premium
 * @property $prem_start
 * @property $color
 * @property $color_start
 */
class Adv extends Base
{
    const ITEM_ADD_BALANCE   = 'addBalance';
    const ITEM_COLOR         = 'color';
    const ITEM_UP            = 'up';
    const ITEM_VIP           = 'vip';
    const ITEM_PREMIUM       = 'premium';

    protected static $items = [
        self::ITEM_COLOR,
        self::ITEM_UP,
        self::ITEM_VIP,
        self::ITEM_PREMIUM,
    ];

    protected static $table = 'adv';

    public static function fields()
    {
        return [
            'id'             => ['type' => 'integer', 'autoincrement' => true],
            'title'          => ['type' => 'string', 'required' => true],
            'title_original' => ['type' => 'string'],
            'img'            => ['type' => 'text', 'value' => ""],
            'url'            => ['type' => 'string', 'value' => ""],
            'status'         => ['type' => 'smallint', 'value' => ""],
            'text'           => ['type' => 'text', 'required' => true],
            'params'         => ['type' => 'text', 'value' => ""],
            'text_rdy'       => ['type' => 'text', 'value' => ""],
            'category'       => ['type' => 'integer', 'required' => true],
            'user'           => ['type' => 'integer', 'value' => ""],
            'name'           => ['type' => 'string', 'value' => ""],
            'email'          => ['type' => 'string', 'value' => ""],
            'phone'          => ['type' => 'string', 'value' => ""],
            'area'           => ['type' => 'integer', 'required' => true],
            'city'           => ['type' => 'integer', 'required' => true],
            'address'        => ['type' => 'text', 'value' => ""],
            'price'          => ['type' => 'integer', 'required' => true],
            'currency'       => ['type' => 'string', 'value' => ""],
            'date_create'    => ['type' => 'integer', 'value' => ""],
            'date'           => ['type' => 'integer', 'value' => ""],
            'block'          => ['type' => 'string', 'value' => ""],
            'views'          => ['type' => 'integer', 'value' => ""],
            'premium'        => ['type' => 'smallint', 'value' => ""],
            'vip'            => ['type' => 'smallint', 'value' => ""],
            'color'          => ['type' => 'smallint', 'value' => ""],
            'vip_start'      => ['type' => 'integer', 'value' => ""],
            'prem_start'     => ['type' => 'integer', 'value' => ""],
            'color_start'    => ['type' => 'integer', 'value' => ""],
            'up_start'       => ['type' => 'integer', 'value' => ""],
            'owner'          => ['type' => 'integer', 'value' => ""],
            'from_id'        => ['type' => 'integer', 'value' => ""],
            'from_site'      => ['type' => 'integer', 'value' => ""],
            'ppg_type'       => ['type' => 'integer', 'value' => ""],
            'from_type'      => ['type' => 'integer', 'value' => ""],
            'not_email'      => ['type' => 'integer', 'value' => ""],
            'not_skype'      => ['type' => 'integer', 'value' => ""],
            'added_ip'       => ['type' => 'string', 'value' => ""],
            'show_cont'      => ['type' => 'integer', 'value' => ""],
            'show_map'       => ['type' => 'smallint', 'value' => ""],
            'show_website'   => ['type' => 'smallint', 'value' => ""],
            'upd'            => ['type' => 'smallint', 'value' => ""]
        ];
    }

    static public function relations(Mapper $mapper, Entity $entity)
    {
        return [
            'userProfile' => $mapper->belongsTo($entity, 'BZR\Entity\Profile', 'user'),
            'photos' => $mapper->hasMany($entity, 'BZR\Entity\AdvImages', 'adv_id'),
            'ad_params' => $mapper->hasMany($entity, 'BZR\Entity\AdvParams', 'adv_id'),
            'counter'   => $mapper->hasOne($entity, 'BZR\Entity\AdvCounter', 'id'),
        ];
    }

    /**
     * @return array
     */
    public static function getItems()
    {
        return self::$items;
    }

    /**
     * @param string $item
     * @throws Exception
     */
    public function upgrade($item)
    {
        switch ($item) {
            case self::ITEM_COLOR:
                $this->upgradeColor();
                break;
            case self::ITEM_UP:
                $this->upgradeUp();
                break;
            case self::ITEM_VIP:
                $this->upgradeVip();
                break;
            case self::ITEM_PREMIUM:
                $this->upgradePremium();
                break;
            default:
                throw new Exception("Unknown adv upgrade type");
        }
    }

    public function canUpForFree()
    {
        return strtotime('-14 day') > $this->date;
    }

    public function upForFree()
    {
        if ($this->canUpForFree()) {
            $this->upgradeUp();
        }
    }

    protected function upgradeColor()
    {
        $this->color = 1;
        $this->color_start = time();
    }

    protected function upgradeUp()
    {
        //$this->up = 1;
        $this->up_start = time();
        $this->date = time();
    }

    protected function upgradeVip()
    {
        $this->vip = 1;
        $this->vip_start = time();
    }

    protected function upgradePremium()
    {
        $this->premium = 1;
        $this->prem_start = time();
    }
}
