<?php

namespace BZR\Sms;

use BZR\Arr;
use BZR\Entity\Profile;
use BZR\Entity\UsersPushToken;
use BZR\Exception;
use BZR\Notification\Adapter\Fcm;
use BZR\Spot\Locator;
use Sly\NotificationPusher\Adapter\Apns;
use Sly\NotificationPusher\Collection\DeviceCollection;
use Sly\NotificationPusher\Model\Device;
use Sly\NotificationPusher\Model\Message;
use Sly\NotificationPusher\Model\Push;
use Sly\NotificationPusher\PushManager;

class CommtorSender implements Sender
{

    protected $url;
    protected $header;
    protected $authKey;
    protected $countryCode;

    /**
     * CommtorSender constructor.
     * @param $url
     * @param $authKey
     * @param $header
     * @param $countryCode
     */
    public function __construct($url, $authKey, $header, $countryCode)
    {
        $this->url         = $url;
        $this->authKey     = $authKey;
        $this->header      = $header;
        $this->countryCode = $countryCode;
    }

    /**
     * @param string | int $phoneNumber
     * @return string
     * @throws Exception
     */
    protected function normalizePhoneNumber($phoneNumber)
    {
        $phoneNumber = str_replace(['+', '(', ')', '-', ' '], '', $phoneNumber);

        if (substr($phoneNumber, 0, 1) == '9' && strlen($phoneNumber) == 8) {
            $phoneNumber = $this->countryCode . $phoneNumber;
        } elseif (substr($phoneNumber, 0, 3) != $this->countryCode || strlen($phoneNumber) != 11) {
            throw new Exception('Cannot normalize phone: "' . $phoneNumber . '"');
        }

        return $phoneNumber;

        $phone = str_replace('+', '', $_POST['field']);
        if(substr($phone, 0, 1) == '9' && strlen($phone == 8))
            $phone = '357'.$phone;
        elseif(substr($phone, 0, 3) == '357' && strlen($phone == 11)){

        }
        else{
            $data['error'] = 1;
        }
    }

    /**
     * @inheritdoc
     */
    public function send($phoneNumber, $message)
    {
        try {
            $phoneNumber = $this->normalizePhoneNumber($phoneNumber);
        } catch (Exception $e) {
            return;
        }
        $curl = curl_init();

        if ($curl) {
            $postData = [
                'AuthKey'   => $this->authKey,
                'Telephone' => $phoneNumber,
                'Header'    => $this->header,
                'Body'      => $message
            ];

            $postString = http_build_query($postData, null, '&', PHP_QUERY_RFC3986);

            curl_setopt($curl, CURLOPT_URL, $this->url);
            curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
            curl_setopt($curl, CURLOPT_POST, true);
            curl_setopt($curl, CURLOPT_POSTFIELDS, $postString);

            $response = curl_exec($curl);
            curl_close($curl);
        }
    }
}
