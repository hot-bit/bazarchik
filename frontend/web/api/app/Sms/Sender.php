<?php
/**
 * Created by PhpStorm.
 * User: root
 * Date: 15.08.2016
 * Time: 13:46
 */

namespace BZR\Sms;


interface Sender
{
    /**
     * @param $phoneNumber
     * @param $message
     * @return mixed
     */
    public function send($phoneNumber, $message);
}