<?php

namespace BZR\Notification\Fcm;

use ZendService\Google\Exception;
use Zend\Json\Json;

/**
 * Class Message
 * @package BZR\Notification\Fcm
 */
class Message extends \ZendService\Google\Gcm\Message
{
    /**
     * @inheritdoc
     */
    public function toJson()
    {
        $json = array();
        if ($this->registrationIds) {
            $json['registration_ids'] = $this->registrationIds;
        }
        if ($this->collapseKey) {
            $json['collapse_key'] = $this->collapseKey;
        }
        if ($this->data) {
            $json['data'] = $this->data;
        }
        if ($this->delayWhileIdle) {
            $json['delay_while_idle'] = $this->delayWhileIdle;
        }
        if ($this->timeToLive != 2419200) {
            $json['time_to_live'] = $this->timeToLive;
        }
        if ($this->restrictedPackageName) {
            $json['restricted_package_name'] = $this->restrictedPackageName;
        }
        if ($this->dryRun) {
            $json['dry_run'] = $this->dryRun;
        }
        return Json::encode($json);
    }
}
