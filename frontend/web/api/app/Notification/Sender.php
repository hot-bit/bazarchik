<?php

namespace BZR\Notification;

use BZR\Arr;
use BZR\Entity\Profile;
use BZR\Entity\UsersPushToken;
use BZR\Notification\Adapter\Fcm;
use BZR\Spot\Locator;
use Sly\NotificationPusher\Adapter\Apns;
use Sly\NotificationPusher\Collection\DeviceCollection;
use Sly\NotificationPusher\Model\Device;
use Sly\NotificationPusher\Model\Message;
use Sly\NotificationPusher\Model\Push;
use Sly\NotificationPusher\PushManager;

class Sender
{
    protected $certificatePath;
    protected $certificatePassPhrase;
    protected $apiKey;

    /**
     * @var Locator
     */
    protected $spot;

    protected $iosAdapter;
    protected $androidAdapter;

    /**
     * Sender constructor.
     * @param $certificatePath
     * @param $certificatePassPhrase
     * @param $apiKey
     * @param Locator $locator
     */
    public function __construct($certificatePath, $certificatePassPhrase, $apiKey, Locator $locator)
    {
        $this->certificatePath = $certificatePath;
        $this->certificatePassPhrase = $certificatePassPhrase;
        $this->apiKey          = $apiKey;
        $this->spot            = $locator;
    }

    /**
     * @return Apns
     */
    protected function getIosAdapter()
    {
        if (is_null($this->iosAdapter)) {
            $config = [
                'certificate' => $this->certificatePath,
            ];

            if ($this->certificatePassPhrase) {
                $config['passPhrase'] = $this->certificatePassPhrase;
            }

            $this->iosAdapter = new Apns($config);
        }

        return $this->iosAdapter;
    }

    /**
     * @return Fcm
     */
    protected function getAndroidAdapter()
    {
        if (is_null($this->androidAdapter)) {
            $this->androidAdapter = new Fcm([
                'apiKey' => $this->apiKey,
            ]);
        }

        return $this->androidAdapter;
    }

    /**
     * @param array $tokens
     * @param string $message
     * @param array $options
     * @param $type
     */
    public function broadcast(array $tokens, $message, array $options, $type)
    {
        if (count($tokens) === 0) {
            return;
        }

        if ($type === UsersPushToken::TYPE_IOS) {
            $adapter = $this->getIosAdapter();
            $options = ['custom' => $options];
        } else {
            $adapter = $this->getAndroidAdapter();
        }

        $deviceList = [];

        foreach ($tokens as $token) {
            if ($adapter->supports($token)) {
                $deviceList[] = new Device($token);
            }
        }

        $devices = new DeviceCollection($deviceList);
        $message = new Message($message, $options);
        $push    = new Push($adapter, $devices, $message);

        $adapter->setEnvironment(PushManager::ENVIRONMENT_PROD);
        $adapter->push($push);

        /*foreach ($pushedDevices as $device) {
            var_dump(get_class($device));
        }*/
        //var_dump($adapter->getResponse()->getResponse());
        //TODO remove not notified devices
    }

    /**
     * @param $token
     * @param $message
     * @param array $options
     * @param $type
     */
    public function send($token, $message, array $options, $type)
    {
        $this->broadcast([$token], $message, $options, $type);
    }

    /**
     * @param Profile $user
     * @param $message
     * @param array $options
     */
    public function sendToUser(Profile $user, $message, array $options = [])
    {
        $iosTokens     = [];
        $androidTokens = [];

        foreach ($user->pushTokens as $token) {
            if ($token->isAndroid()) {
                $androidTokens[] = $token->token;
            } elseif ($token->isIos()) {
                $iosTokens[] = $token->token;
            }
        }

        $this->broadcast(array_reverse($iosTokens), $message, $options, UsersPushToken::TYPE_IOS);
        $this->broadcast(array_reverse($androidTokens), $message, $options, UsersPushToken::TYPE_ANDROID);
    }

    /**
     * @param array $userIdList - array of user_id
     * @param string $message
     * @param array $options
     */
    public function sendToUsers(array $userIdList, $message, array $options = [])
    {
        if (count($userIdList) === 0) {
            return;
        }

        /**
         * @var $mapper \BZR\Entity\Mapper\UsersPushToken
         */
        $mapper = $this->spot->mapper('\BZR\Entity\UsersPushToken');
        $tokens = $mapper->getTokenArray($userIdList);

        $iosTokens     = Arr::get($tokens, UsersPushToken::TYPE_IOS, []);
        $androidTokens = Arr::get($tokens, UsersPushToken::TYPE_ANDROID, []);

        $this->broadcast($iosTokens, $message, $options, UsersPushToken::TYPE_IOS);
        $this->broadcast($androidTokens, $message, $options, UsersPushToken::TYPE_ANDROID);
    }
}
