<?php
namespace BZR\Catalog;


interface PropertyInterface
{
    public function getName();

    public function isParameter();

    public function isColumn();

    public function apply(Query $query);

    public function getFixedName();
}