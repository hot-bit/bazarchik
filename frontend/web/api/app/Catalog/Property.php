<?php
/**
 * Created by PhpStorm.
 * User: user
 * Date: 07.12.16
 * Time: 19:51
 */

namespace BZR\Catalog;


abstract class Property implements PropertyInterface
{
    protected $name;

    public function getFixedName()
    {
        if ($this->name === 'paramCity') {
            return 'city';
        }

        if ($this->name === 'paramArea') {
            return 'area';
        }

        return $this->name;
    }

    public function getName()
    {
        return $this->name;
    }

    public function isParameter()
    {
        $parameters = [
            'paramCity', 'paramArea', 'nobed', 'cover', 'body', 'brand', 'model', 'year', 'running', 'box', 'engine',
            'volume', 'drive', 'wheel'
        ];

        return in_array($this->name, $parameters);
    }

    public function isColumn()
    {
        $columns = [
            'city', 'area', 'price'
        ];

        return in_array($this->name, $columns);
    }
}