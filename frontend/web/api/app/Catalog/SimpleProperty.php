<?php
namespace BZR\Catalog;


class SimpleProperty extends Property
{
    protected $name;
    protected $value;

    public function __construct($name, $value)
    {
        $this->name = $value;
        $this->value = $value;
    }

    public function apply(Query $query)
    {
        $name = $this->getFixedName();
        $value = $this->value;

        if ($this->isColumn()) {
            $query->addCondition("a.$name = $value");
        } elseif ($this->isParameter()) {
            $index = $query->getJoinsCount();
            $alias = 'p' . $index;
            $query->addJoin("INNER JOIN adv_params $alias ON a.id = $alias.adv_id");
            $query->addCondition("$alias.name = '$name' AND $alias.value = '$value'");
        }
    }


}