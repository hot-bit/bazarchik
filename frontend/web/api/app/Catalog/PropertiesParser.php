<?php
/**
 * Created by PhpStorm.
 * User: user
 * Date: 07.12.16
 * Time: 18:38
 */

namespace BZR\Catalog;


class PropertiesParser
{
    protected $propertiesString;

    public function __construct($propertiesString)
    {
        $this->propertiesString = $propertiesString;
    }

    protected function isMinMaxValue($value)
    {
        return substr_count($value, '|') > 0;
    }

    protected function isListValue($value)
    {
        return substr_count($value, ';') > 0;
    }

    public function parse()
    {
        $propertiesString = $this->propertiesString;

        $pairs = explode(',', $propertiesString);
        $properties = [];

        foreach ($pairs as $pair) {
            list($propertyName, $value) = explode(':', $pair);

            if ($this->isMinMaxValue($value)) {
                $property = new MinMaxProperty($propertyName, $value);
            } elseif ($this->isListValue($value)) {
                $property = new ListProperty($propertyName, $value);
            } else {
                $property = new SimpleProperty($propertyName, $value);
            }

            $properties[$propertyName] = $property;
        }

        return $properties;

        //недвижка
        //$paramsSearch = 'paramCity:2,paramArea:28,nobed:2,cover:14|486,price:0|200,';

        //авто
        //$paramsSearch = 'brand:1,model:1,body:1;2;3;,year:1960|2107,running:0|185000,box:1;2;,engine:1;2;,volume:0|185000,drive:1;2;,engine:1;2;,price:0|185000,wheel:1,';

        $countParams = substr_count($paramsSearch, ',');
        $where = '';
        $innerJoin = '';
        $sql = '';
        $s = 0; //исчисляемое

        $arrayAdvParams = ['paramCity' => 'select', 'paramArea' => 'select', 'nobed' => 'select', 'cover' => 'MinMax', 'body' => 'select', 'brand' => 'select', 'model' => 'select', 'year' => 'MinMax', 'running' => 'MinMax', 'box' => 'select', 'engine' => 'select', 'volume' => 'MinMax', 'drive' => 'select', 'wheel' => 'select'];
        $arrayAdv = ['city' => 'select', 'area' => 'select', 'price' => 'MinMax'];

        if($countParams > 0){
            $explodeParams = explode(',', $paramsSearch);

            for ($i=0; $i < $countParams; $i++) {
                $bdLitter = '';
                $explodeOneParam = explode(':', $explodeParams[$i]);

                //определяем причстной к таблицам
                if(isset($arrayAdv[$explodeOneParam[0]])){
                    $bdLitter = "a";
                }

                if(isset($arrayAdvParams[$explodeOneParam[0]])){
                    $bdLitter = 'ap';
                }

                //смотрим если есть MinMax
                $countMinMax = substr_count($explodeOneParam[1], '|');

                if($countMinMax > 0 and !empty($bdLitter)){
                    $explodeMinMax = explode('|', $explodeOneParam[1]);

                    if($bdLitter == 'a'){
                        $sql .= " and $bdLitter.$explodeOneParam[0] >= $explodeMinMax[0] and $bdLitter.$explodeOneParam[0] <= $explodeMinMax[1] ";
                    }

                    if($bdLitter == 'ap'){
                        $innerJoin .= " INNER JOIN adv_params p$s ON a.id = p$s.adv_id ";
                        $where .= " AND (p$s.name = '$explodeOneParam[0]' AND p$s.value >= '$explodeMinMax[0]' AND p$s.value <= '$explodeMinMax[1]' ) ";
                    }
                    $s++;
                }

                //смотрим если есть вложенные опции (;)
                $countParentOption= substr_count($explodeOneParam[1], ';');

                if ($countParentOption > 0 and !empty($bdLitter)) {

                    $explodeOption = explode(';', $explodeOneParam[1]);
                    $or = '';
                    $orSimbol = ' OR ';

                    for ($r=0; $r < $countParentOption; $r++) {

                        $r_p = $r + 1;
                        if($r_p == $countParentOption){
                            $orSimbol = '';
                        }

                        $or .= " p$s.name = '$explodeOneParam[0]' and p$s.value = '$explodeOption[$r]' $orSimbol ";

                    }

                    $innerJoin .= " INNER JOIN adv_params p$s ON a.id = p$s.adv_id ";

                    $where .= " AND ( $or ) ";
                    $s++;

                }

                //если просто значение
                if ($countMinMax == 0 and $countParentOption == 0 and !empty($bdLitter)) {

                    if($bdLitter == 'a'){
                        $sql .= " and $bdLitter.$explodeOneParam[0] = $explodeOneParam[1] ";
                    }

                    if($bdLitter == 'ap'){

                        if($explodeOneParam[0] == 'paramCity'){
                            $explodeOneParam[0] = 'city';
                        }

                        if($explodeOneParam[0] == 'paramArea'){
                            $explodeOneParam[0] = 'area';
                        }

                        $innerJoin .= " INNER JOIN adv_params p$s ON a.id = p$s.adv_id ";
                        $where .= " AND (p$s.name = '$explodeOneParam[0]' AND p$s.value = '$explodeOneParam[1]' ) ";
                    }
                    $s++;
                }
            }
        }

        return ['where' => $where, 'innerJoin' => $innerJoin, 'sql' => $sql];
    }
}