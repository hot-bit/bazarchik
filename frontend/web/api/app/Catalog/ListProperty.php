<?php
namespace BZR\Catalog;


class ListProperty extends Property
{
    protected $name;

    protected $values;

    public function __construct($name, $value)
    {
        $values = explode(';', $value);

        $this->name = $name;
        $this->values = $values;
    }



    public function apply(Query $query)
    {
        $name = $this->getFixedName();
        $values  = $this->values;

        $index = $query->getJoinsCount();
        $alias = 'p' . $index;

        $query->addJoin("INNER JOIN adv_params $alias ON a.id = $alias.adv_id");

        $parts = [];

        foreach ($values as $value) {
            $parts[] = "($alias.name = '$name' and $alias.value = '$value')";
        }

        $query->addCondition(implode(" OR ", $parts));
    }
}