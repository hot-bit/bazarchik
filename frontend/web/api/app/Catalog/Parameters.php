<?php
namespace BZR\Catalog;

use Symfony\Component\HttpFoundation\Request;

class Parameters
{
    const ORDER_ASC  = 'ASC';
    const ORDER_DESC = 'DESC';

    const SORT_DATE = 'date';


    protected $pageNumber;

    protected $itemsPerPage;

    protected $sortType;

    protected $sortOrder;

    protected $onlyWithPhoto;

    protected $categoryId;

    protected $vipOnly;

    protected $premiumOnly;

    protected $city;

    protected $area;

    protected $premiumFirst;

    protected $vipFirst;

    protected $onlyNew;

    protected $searchByTitle;

    protected $queryString;

    protected $properties = [];

    /**
     * @return mixed
     */
    public function getVipFirst()
    {
        return $this->vipFirst;
    }

    /**
     * @param mixed $vipFirst
     */
    public function setVipFirst($vipFirst)
    {
        $this->vipFirst = $vipFirst;
    }

    /**
     * @return array | PropertyInterface[]
     */
    public function getProperties()
    {
        return $this->properties;
    }

    /**
     * @param array $properties
     */
    public function setProperties($properties)
    {
        $this->properties = $properties;
    }

    /**
     * @return mixed
     */
    public function getPageNumber()
    {
        return $this->pageNumber;
    }

    /**
     * @param mixed $pageNumber
     */
    public function setPageNumber($pageNumber)
    {
        $this->pageNumber = $pageNumber;
    }

    /**
     * @return mixed
     */
    public function getItemsPerPage()
    {
        return $this->itemsPerPage;
    }

    /**
     * @param mixed $itemsPerPage
     */
    public function setItemsPerPage($itemsPerPage)
    {
        $this->itemsPerPage = $itemsPerPage;
    }

    /**
     * @return mixed
     */
    public function getSortType()
    {
        return $this->sortType;
    }

    /**
     * @param mixed $sortType
     */
    public function setSortType($sortType)
    {
        $this->sortType = $sortType;
    }

    /**
     * @return mixed
     */
    public function getSortOrder()
    {
        return $this->sortOrder;
    }

    /**
     * @param mixed $sortOrder
     */
    public function setSortOrder($sortOrder)
    {
        $this->sortOrder = $sortOrder;
    }

    /**
     * @return mixed
     */
    public function getOnlyWithPhoto()
    {
        return $this->onlyWithPhoto;
    }

    /**
     * @param mixed $onlyWithPhoto
     */
    public function setOnlyWithPhoto($onlyWithPhoto)
    {
        $this->onlyWithPhoto = $onlyWithPhoto;
    }

    /**
     * @return mixed
     */
    public function getCategoryId()
    {
        return $this->categoryId;
    }

    /**
     * @param mixed $categoryId
     */
    public function setCategoryId($categoryId)
    {
        $this->categoryId = $categoryId;
    }

    /**
     * @return mixed
     */
    public function getVipOnly()
    {
        return $this->vipOnly;
    }

    /**
     * @param mixed $vipOnly
     */
    public function setVipOnly($vipOnly)
    {
        $this->vipOnly = $vipOnly;
    }

    /**
     * @return mixed
     */
    public function getPremiumOnly()
    {
        return $this->premiumOnly;
    }

    /**
     * @param mixed $premiumOnly
     */
    public function setPremiumOnly($premiumOnly)
    {
        $this->premiumOnly = $premiumOnly;
    }

    /**
     * @return mixed
     */
    public function getCity()
    {
        return $this->city;
    }

    /**
     * @param mixed $city
     */
    public function setCity($city)
    {
        $this->city = $city;
    }

    /**
     * @return mixed
     */
    public function getArea()
    {
        return $this->area;
    }

    /**
     * @param mixed $area
     */
    public function setArea($area)
    {
        $this->area = $area;
    }

    /**
     * @return mixed
     */
    public function getPremiumFirst()
    {
        return $this->premiumFirst;
    }

    /**
     * @param mixed $premiumFirst
     */
    public function setPremiumFirst($premiumFirst)
    {
        $this->premiumFirst = $premiumFirst;
    }

    /**
     * @return mixed
     */
    public function getOnlyNew()
    {
        return $this->onlyNew;
    }

    /**
     * @param mixed $onlyNew
     */
    public function setOnlyNew($onlyNew)
    {
        $this->onlyNew = $onlyNew;
    }

    /**
     * @return mixed
     */
    public function getSearchByTitle()
    {
        return $this->searchByTitle;
    }

    /**
     * @param mixed $searchByTitle
     */
    public function setSearchByTitle($searchByTitle)
    {
        $this->searchByTitle = $searchByTitle;
    }

    /**
     * @return mixed
     */
    public function getQueryString()
    {
        return $this->queryString;
    }

    /**
     * @param mixed $queryString
     */
    public function setQueryString($queryString)
    {
        $this->queryString = $queryString;
    }


    public static function parseRequest(Request $request)
    {
        $pageNumber = intval($request->get('page'));
        $itemsPerPage = intval($request->get('per-page'));

        $sortType  = $request->get('sort', self::SORT_DATE);
        $sortOrder = $request->get('sort-order');

        $withPhoto = boolval($request->get('photoIsset'));
        $categoryId = intval($request->get('category_id'));
        $vipOnly = $request->get('vip') === "true";
        $premiumOnly = $request->get('premium') === "true";

        $paramsSearch = $request->get('paramsSearch');
        $queryString = $request->get('paramsTitle');

        $city = $request->get('city');
        $area = $request->get('area');

        $premiumFirst = $request->get('premiumFirst') === "true";

        $onlyNew = $request->get('new', 'false') === 'true';
        $byTitle = $request->get('by_title', 'false') === 'true';

        $instance = new static();
        $instance->setPageNumber(max($pageNumber - 1, 0));
        $instance->setItemsPerPage($itemsPerPage);
        $instance->setSortType($sortType);
        $instance->setSortOrder(($sortOrder === 'asc') ? self::ORDER_ASC : self::ORDER_DESC);
        $instance->setOnlyWithPhoto($withPhoto);
        $instance->setCategoryId($categoryId);
        $instance->setVipOnly($vipOnly);
        $instance->setPremiumOnly($premiumOnly);
        $instance->setCity($city);
        $instance->setArea($area);
        $instance->setPremiumFirst($premiumFirst);
        $instance->setVipFirst($instance->getPremiumFirst());
        $instance->setOnlyNew($onlyNew);
        $instance->setSearchByTitle($byTitle);

        $instance->setQueryString($queryString);

        $parser = new PropertiesParser($paramsSearch);
        $instance->setProperties($parser->parse());
    }
}