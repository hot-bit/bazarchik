<?php
namespace BZR\Catalog;


class MinMaxProperty extends Property
{
    protected $name;

    protected $min;

    protected $max;

    public function __construct($name, $value)
    {
        list($min, $max) = explode('|', $value);

        $this->name = $name;
        $this->min = $min;
        $this->max = $max;
    }

    public function apply(Query $query)
    {
        $name = $this->getFixedName();
        $min  = $this->min;
        $max  = $this->max;

        if ($this->isColumn()) {
            $query->addCondition("a.$name >= $min and a.$name <= $max");
        } elseif ($this->isParameter()) {
            $index = $query->getJoinsCount();
            $alias = 'p' . $index;
            $query->addJoin("INNER JOIN adv_params $alias ON a.id = $alias.adv_id");
            $query->addCondition("$alias.name = '$name' AND $alias.value >= '$min' AND $alias.value <= '$max'");
        }
    }
}