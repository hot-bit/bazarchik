<?php
namespace BZR\Catalog;


use BZR\Application\SpotTrait;
use BZR\Entity\Mapper\Categories;
use BZR\Spot\Locator;
use Doctrine\DBAL\Connection;
use Spot\Mapper;

class Query
{
    /**
     * @var Parameters
     */
    protected $parameters;

    /**
     * @var \BZR\Spot\Locator
     */
    protected $spot;

    protected $joins = [];

    protected $conditions = [];

    protected $order;

    public function __construct(Parameters $parameters, Locator $spot)
    {
        $this->parameters = $parameters;
        $this->spot = $spot;
    }

    protected function handleSort()
    {
        $sortType = $this->parameters->getSortType();
        $sortOrder = $this->parameters->getSortOrder();

        if ($sortType === 'popular'){
            $this->joins[] = ' LEFT JOIN adv_counter ac ON (a.id = ac.id) ';
            $this->order = " ORDER BY ac.total $sortOrder, a.id DESC ";
        } elseif ($sortType === 'price') {
            $this->order = " ORDER BY a.price $sortOrder, a.id DESC ";
        } elseif ($sortType === 'rand') {
            $this->order = " ORDER BY RAND() ";
        } else {
            $this->order = "ORDER BY a.date $sortOrder, a.id DESC";
        }
    }


    protected function handleCity()
    {
        $city = $this->parameters->getCity();
        if ($city) {
            $this->conditions[] = "a.city = $city";
        }
    }


    protected function handleArea()
    {
        $area = $this->parameters->getArea();
        if ($area) {
            $this->conditions[] = "a.area = $area";
        }
    }


    protected function handleOnlyNew()
    {
        $onlyNew = $this->parameters->getOnlyNew();
        if ($onlyNew) {
            $this->conditions[] = "a.is_new = 1";
        }
    }


    protected function handleQueryString()
    {
        $queryString = $this->parameters->getQueryString();
        $byTitle = $this->parameters->getSearchByTitle();

        if ($queryString) {

            $words = explode(' ', $queryString);

            $searchText  = [];
            $searchTitle = [];

            $connection = $this->spot->getConnection();

            foreach ($words as $word) {
                $word = $connection->quote("%$word%");
                $searchTitle[] = " a.title LIKE $word";

                if (!$byTitle) {
                    $searchText[] = " a.text_rdy LIKE $word";
                }
            }

            $searchText = implode(' AND ', $searchText);
            $searchTitle = implode(' AND ', $searchTitle);

            $parts = [];

            if ($searchText) {
                $parts[] = "($searchText)";
            }

            if ($searchTitle) {
                $parts[] = "($searchTitle)";
            }

            $this->conditions[] = "(" . implode(" OR ", $parts) . ")";
        }
    }

    protected function handlePremium()
    {
        $premiumCondition = false;

        if ($this->parameters->getPremiumOnly()) {
            $premiumCondition = "a.premium = 1 and prem_start > " . (time()-3600*24*7);
        }

        if ($this->parameters->getPremiumFirst()) {
            $premiumCondition = "and (a.premium <> 1 and prem_start < " . (time()-3600*24*7) . ")";
        }

        if ($premiumCondition) {
            $this->conditions[] = $premiumCondition;
        }
    }

    protected function handleVip()
    {
        $vipCondition = false;

        if ($this->parameters->getVipOnly()) {
            $vipCondition = "a.vip = 1 and vip_start > " . (time()-3600*24*7);
        }

        if ($this->parameters->getVipFirst()) {
            $vipCondition = "and (a.vip <> 1 and vip_start < " . (time()-3600*24*7) . ")";
        }

        if ($vipCondition) {
            $this->conditions[] = $vipCondition;
        }
    }

    protected function handlePhoto()
    {
        $withPhoto = $this->parameters->getOnlyWithPhoto();

        if ($withPhoto) {
            $this->joins[] = 'LEFT JOIN adv_images as ai ON (a.id = ai.adv_id)';
            $this->conditions[] .= 'NOT ISNULL(ai.id)';
        }
    }

    protected function handleCategory()
    {
        $categoryId = $this->parameters->getCategoryId();

        if ($categoryId) {
            $categoriesChain = $this->getCategoriesChain($categoryId);
            $categoryText = implode(", ", array_filter($categoriesChain));
            $this->conditions[] = "a.category IN ($categoryText)";
        }
    }

    public function addCondition($condition)
    {
        $this->conditions[] = $condition;
    }

    public function addJoin($join)
    {
        $this->joins[] = $join;
    }

    public function getJoinsCount()
    {
        return count($this->joins);
    }

    protected function handleProperties()
    {
        $properties = $this->parameters->getProperties();

        foreach ($properties as $property) {
            $property->apply($this);
        }
    }

    public function execute()
    {
        $this->handleSort();
        $this->handleCity();
        $this->handleArea();
        $this->handleOnlyNew();
        $this->handleQueryString();
        $this->handleVip();
        $this->handlePremium();
        $this->handlePhoto();

        $this->handleCategory();
        $this->handleProperties();

        //TODO $premiumFirst = ($premiumFirst === "true");

        $joins = implode("\n", $this->joins);
        $conditions = "(" . implode(") \nAND (", $this->conditions) . ")";
        $order = $this->order;

        $offset = 0;//TODO
        $limit = 10;//TODO

        $query = "
                SELECT a.id FROM adv a
                $joins
                WHERE a.status = 2 
                AND $conditions
                GROUP BY a.id
                $order
                LIMIT $offset, $limit
            ";

    }

    protected function getCategoriesChain($categoryId)
    {
        $array = [];
        $number = 0;
        $complete = false;
        /**
         * @var Mapper $mapperCategories
         */
        $mapperCategories = $this->spot->mapper('BZR\Entity\Categories');
        $number = 0;

        while ($number < 6) {

            $leftJoin = '';
            for ($i = 0; $i < $number; $i++) {
                $iReal = $i + 1;
                $leftJoin  .= " LEFT JOIN categories g$iReal ON g$i.id = g$iReal.parent ";
            }

            $whereAnd = '';


            for ($i = 0; $i <= $number; $i++) {
                $whereAnd  .= " and g$i.header = 0 ";
            }

            $query = "SELECT * FROM categories g0 $leftJoin WHERE g0.parent = $categoryId $whereAnd";

            $result = $mapperCategories->query($query);
            $result = $result->toArray();

            if (!empty($result)) {

                foreach ($result as $key => $value) {
                    $array[$value['id']] = $value['id'];
                    $complete = true;
                }

            } else {
                $array[$categoryId] = $categoryId;
                $complete = true;
            }

            if ($complete) {
                $number = $number + 1;
                $complete = false;
            } else {
                $number = 10;
            }
        }

        return $array;
    }

}