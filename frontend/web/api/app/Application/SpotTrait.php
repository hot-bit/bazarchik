<?php

namespace BZR\Application;

trait SpotTrait
{
    /**
     * @return \BZR\Spot\Locator
     */
    public function spot()
    {
        return $this['spot'];
    }
}