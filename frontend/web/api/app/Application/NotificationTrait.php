<?php

namespace BZR\Application;

use BZR\Notification\Sender;

trait NotificationTrait
{
    /**
     * @return Sender
     */
    public function notification()
    {
        return $this['notification.sender'];
    }
}