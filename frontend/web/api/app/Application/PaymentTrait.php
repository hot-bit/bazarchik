<?php

namespace BZR\Application;

use BZR\Payment\Handler;

trait PaymentTrait
{
    /**
     * @return Handler
     */
    public function payment()
    {
        return $this['payment.handler'];
    }
}