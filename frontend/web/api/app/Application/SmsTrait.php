<?php

namespace BZR\Application;

use BZR\Sms\Sender;

trait SmsTrait
{
    /**
     * @return Sender
     */
    public function sms()
    {
        return $this['sms.sender'];
    }
}