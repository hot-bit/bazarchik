<?php
namespace BZR;

use Symfony\Component\HttpFoundation\JsonResponse;

class ErrorResponse extends JsonResponse
{
    public function __construct(ErrorData $data = null, $status = 200, $headers = array())
    {
        parent::__construct('', $status, $headers);

        if (null === $data) {
            $data = new ErrorData('unknown', 'unknown', 0);
        }

        $this->setData($data);
    }
}