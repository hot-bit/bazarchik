<?php

namespace BZR\Controller;

use BZR\Application;
use BZR\Translation\Keys;
use Spot\Entity;
use Symfony\Component\HttpFoundation\Request;

class Reports extends Base
{
    public function sendReport(Application $app, Request $request)
    {
        $id     = $request->get('adv_id');
        $reason = $request->get('reason', $request->getContent());

        if (!$id) {
            return $app->errorResponse(Keys::ERROR_ID_NO, Keys::ERROR_ID_NO, 1, Application::NOT_FOUND_ERROR);
        }

        if (!$reason) {
            return $app->errorResponse(Keys::ERROR_ID_NO, Keys::ERROR_ID_NO, 2, Application::NOT_FOUND_ERROR);
        }

        $advMapper = $app->spot()->mapper('BZR\Entity\Adv');

        $adv = $advMapper->first(['id' => $id]);

        if (!$adv) {
            return $app->errorResponse(Keys::ERROR_ID_NO, Keys::ERROR_ID_NO, 3, Application::NOT_FOUND_ERROR);
        }

        $reportsMapper = $app->spot()->mapper('BZR\Entity\Report');

        $report = $reportsMapper->build([
            'adv_id' => $id,
            'text'   => $reason,
            'date'   => time(),
            'ip'     => $request->getClientIp()
        ]);

        $reportsMapper->save($report);

        return $app->generalResponse(null, Application::PUT_GET_OK);
    }
}
