<?php

namespace BZR\Controller;

use BZR\Application;
use BZR\Entity\Adv;
use BZR\Entity\Orders;
use BZR\Entity\Payments;
use BZR\Entity\Response\Adv as ResponseAdv;
use BZR\Entity\Response\User;
use BZR\Translation\Keys;
use Spot\Entity;
use Symfony\Component\HttpFoundation\Request;

class Payment extends Base
{
    /**
     * @param Application $app
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\JsonResponse
     */
    public function pay(Application $app, Request $request)
    {
        $profile   = $app->getCurrentProfile();
        $paymentId = $request->get('payment_id');

        if (!$paymentId) {
            return $app->errorResponse(Keys::ERROR_PAYMENT_ID, Keys::ERROR_PAYMENT_ID, 1, Application::NOT_FOUND_ERROR);
        }

        $paymentMapper = $app->spot()->mapper('BZR\Entity\MobilePayment');
        $paymentEntity = $paymentMapper->first(['payment_id' => $paymentId]);

        if (!$paymentEntity) {
            $paymentEntity = $paymentMapper->build([
                'user_id'    => $profile->id,
                'payment_id' => $paymentId,
                'status'     => 0
            ]);

            $paymentMapper->save($paymentEntity);
        }

        if (!$paymentEntity->isCompleted()) {

            $app->spot()->beginTransaction();

            try {
                $amount = $app->payment()->getPaymentAmount($paymentId);

                $profileMapper    = $app->spot()->mapper('BZR\Entity\Profile');
                $paymentsMapper   = $app->spot()->mapper('BZR\Entity\Orders');

                $paymentsEntity = $paymentsMapper->build([
                    'id_adv'         => 0,
                    'user_id'        => $profile->id,
                    'user_email'     => $profile->email,
                    'price'          => $amount,
                    'type'           => 1,
                    'items'          => 'payment',
                    'token'          => $paymentId,
                    'status'         => 1,
                    'transaction_id' => $app->Random(),
                    'date'           => time()
                ]);

                $paymentEntity->setCompleted();
                $profile->addMoney($amount);

                $this->tryToSave($profile, $profileMapper);

                $this->tryToSave($paymentsEntity, $paymentsMapper);

                $app->spot()->commit();

            } catch (\Exception $e) {
                $app->spot()->rollBack();
                return $app->errorResponse(Keys::ERROR_PAYMENT, $e->getMessage(), 1, Application::SERVER_ERROR);
            }
        }

        $enityProfile = $profileMapper->where(['id' => $profile->id])
            ->with(['newMessage'])
            ->execute();
        //дорабатываем аватар - URL
        $profileMapper->urlPicturesAvatar($app, $enityProfile);

        $responseEntity = new User($enityProfile[0]);
        return $app->generalResponse($responseEntity->jsonSerialize($app), Application::PUT_GET_OK);
    }

    /**
     * @param Application $app
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\JsonResponse
     */
    public function buy(Application $app, Request $request)
    {
        $availableItems = Adv::getItems();
        $item = $request->get('item');

        if (!in_array($item, $availableItems)) {
            return $app->errorResponse(Keys::ERROR_PAYMENT_ITEM, Keys::ERROR_PAYMENT_ITEM, 1, Application::NOT_FOUND_ERROR);
        }

        $advId = $request->get('adv_id');

        if (!$advId) {
            return $app->errorResponse(Keys::ERROR_ID_NO, Keys::ERROR_ID_NO, 2, Application::NOT_FOUND_ERROR);
        }

        $profile = $app->getCurrentProfile();

        $advMapper = $app->spot()->mapper('BZR\Entity\Adv');
        $adv       = $advMapper->where(['id' => $advId, 'user' => $profile->id])
            ->with(['photos','ad_params','counter'])
            ->first();

        if (!$adv) {
            return $app->errorResponse(Keys::ERROR_ID_NO, Keys::ERROR_ID_NO, 3, Application::NOT_FOUND_ERROR);
        }

        /**
         * @var Adv $adv
         */

        $price = $app->config(['price', $item], null);

        if (is_null($price)) {
            return $app->errorResponse(Keys::ERROR_ID_NO, Keys::ERROR_ID_NO, 4, Application::NOT_FOUND_ERROR);
        }

        $app->spot()->beginTransaction();

        try {
            $profile->takeMoney($price);
            $adv->upgrade($item);
            //TODO logMoneyOperation;

            $profileMapper = $app->spot()->mapper('BZR\Entity\Profile');
            /*$paymentsMapper   = $app->spot()->mapper('BZR\Entity\Payments');

            $typeId = $paymentsMapper->getUpgradeTypeId($item);

            $paymentsEntity = $paymentsMapper->build([
                    'user_id'      => $profile->id,
                    'type_payment' => $typeId,
                    'id_payment'   => $app->Random(),
                    'price'        => $price,
                    'date'         => time()
                ]);*/
            $paymentsMapper   = $app->spot()->mapper('BZR\Entity\Orders');

            $paymentsEntity = $paymentsMapper->build([
                'id_adv'         => $adv->id,
                'user_id'        => $profile->id,
                'user_email'     => $profile->email,
                'price'          => $price,
                'type'           => 0,
                'items'          => json_encode([$item => true]),
                'token'          => '',
                'status'         => 1,
                'transaction_id' => $app->Random(),
                'date'           => time()
            ]);

            $this->tryToSave($paymentsEntity, $paymentsMapper);
            $this->tryToSave($adv, $advMapper);
            $this->tryToSave($profile, $profileMapper);
            
            $app->spot()->commit();

        } catch (\Exception $e) {
            $app->spot()->rollBack();
            return $app->errorResponse(Keys::ERROR_NOT_UPDATE, $e->getMessage(), 5, Application::NOT_FOUND_ERROR);
        }

        $responseEntity = new ResponseAdv($adv);
        return $app->generalResponse($responseEntity->jsonSerialize($app), Application::POST_OK);
    }

    public function upForFree(Application $app, Request $request)
    {
        $advId = $request->get('adv_id');

        if (!$advId) {
            return $app->errorResponse(Keys::ERROR_ID_NO, Keys::ERROR_ID_NO, 2, Application::NOT_FOUND_ERROR);
        }

        $profile = $app->getCurrentProfile();

        $advMapper = $app->spot()->mapper('BZR\Entity\Adv');
        $adv       = $advMapper->where(['id' => $advId, 'user' => $profile->id])
            ->with(['photos','ad_params','counter'])
            ->first();

        if (!$adv) {
            return $app->errorResponse(Keys::ERROR_ID_NO, Keys::ERROR_ID_NO, 3, Application::NOT_FOUND_ERROR);
        }

        /**
         * @var $adv Adv
         */
        $adv->upForFree();
        $advMapper->save($adv);

        $responseEntity = new ResponseAdv($adv);
        return $app->generalResponse($responseEntity->jsonSerialize($app), Application::POST_OK);
    }

    public function priceList(Application $app, Request $request)
    {
        $profile = $app->getCurrentProfile();

        $result = [
            "items" => [],
            "money" => $profile->money
        ];

        $items = Adv::getItems();

        foreach ($items as $item) {
            $price = $app->config(['price', $item], null);

            if (!is_null($price)) {
                $result["items"][$item] = $price;
            }
        }

        return $app->generalResponse($result, Application::POST_OK);
    }

    public function myTransactions(Application $app, Request $request)
    {
        $profile      = $app->getCurrentProfile();
        $i            = 0;
        $array        = [];
        $headers      = [];
        $inspection = true;
        $perPage      = (int)($request->get('per-page'));//число для показов
        $pageCorrect  = (int)($request->get('page'));//страница
        $withdrawFrom = ($pageCorrect-1) * $perPage;

        /**
         * @var $paymentsMapper \BZR\Entity\Mapper\Orders
         */
        $paymentsMapper     = $app->spot()->mapper('BZR\Entity\Orders');
        $paymentEntityCount = $paymentsMapper->where(['user_id' => $profile->id])->count();

        //ПОСТРАНИЧНАЯ НАВИГАЦИЯ
        $pageCount = 0;

        if ($perPage > 0) {
            $pageCount = $paymentEntityCount / (int)($perPage);
        }

        $pageCount = ceil($pageCount);

        if(empty($pageCorrect) or empty($perPage) or $withdrawFrom < 0 or $perPage < 0 or $pageCorrect > $pageCount){

            $withdrawFrom = 0;
            $perPage      = 10;
            $pageCorrect  = 1;

            $pageCount = $paymentEntityCount / $perPage;
            $pageCount = ceil($pageCount);
            $inspection = false;

            if(!$request->get('page') or !$request->get('per-page')){
                $inspection = true;
            }
        }
        //ПОСТРАНИЧНАЯ НАВИГАЦИЯ

        if($paymentEntityCount > 0 && $inspection){
            
            $paymentEntity = $paymentsMapper->where(['user_id' => $profile->id])
                ->with('adv')
                ->limit($perPage,$withdrawFrom)
                ->order(['date' => 'DESC']);

            /**
             * @var Orders $payment
             */
            foreach($paymentEntity as $payment) {

                $keyTypeTranslationKey = $paymentsMapper->getTypeTranslationKey($payment->items);

                if($payment->type == 0){
                    $correctPrice = '-';
                } else {
                    $correctPrice = '+';
                }

                $array[$i] = [
                    'advTitle'     => $payment->adv->title,
                    'advId'        => $payment->adv->id,
                    'typePayment'  => 0, //$payment->type,
                    'typeTitle'    => $keyTypeTranslationKey, //$app->trans($keyTypeTranslationKey.'Transaction'),
                    'id_payment'   => $payment->transaction_id,
                    'price'        => $correctPrice.$payment->price,
                    'date'         => date('d.m.Y', $payment->date),
                ];
                $i++;
            }

            //ПОСТРАНИЧНАЯ НАВИГАЦИЯ
            $headers = [
                'X-Pagination-Current-Page' => $pageCorrect,
                'X-Pagination-Page-Count'   => $pageCount,
                'X-Pagination-Per-Page'     => $perPage,
                'X-Pagination-Total-Count'  => $paymentEntityCount
            ];
            //ПОСТРАНИЧНАЯ НАВИГАЦИЯ

        } else {
            return $app->generalResponse([], Application::PUT_GET_OK);
        }

        return $app->generalResponse($array, Application::PUT_GET_OK, $headers);
    }

}
