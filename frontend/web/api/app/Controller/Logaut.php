<?php

namespace BZR\Controller;

use BZR\Application;
use BZR\Translation\Keys;

class Logaut extends Base
{
    public function logaut(Application $app)
    {
        $mapperRow = $app->spot()->mapper('BZR\Entity\UsersAuthMass');
        $getCurrentProfile = $app->getCurrentProfile();

        if($getCurrentProfile){
            $result = $mapperRow->first(['mobile_token' => $app->getMobileToken(), 'user_id ' => $getCurrentProfile->id]);

            $result->auth_token = '';
            $resultUpdate = $mapperRow->update($result);

            if($resultUpdate){
                return $app->errorResponse(Keys::SUCCESS_LOGAUT, Keys::SUCCESS_LOGAUT, 1, Application::POST_OK);
            } else {
                return $app->errorResponse(Keys::ERROR_NOT_UPDATE, Keys::ERROR_NOT_UPDATE, 1, Application::NOT_FOUND_ERROR);
            }
        }
    }
}