<?php

namespace BZR\Controller;

use BZR\Application;
use BZR\Translation\Keys;
use Symfony\Component\HttpFoundation\Request;

class Brand //extends Base
{
    public function getBrand(Application $app, Request $request)
    {
    	$mapperRow = $app->spot()->mapper('BZR\Entity\Brand');		
		$results = $mapperRow->all();
		$array = [];
		$i = 0;

		if($results->count() > 0){
            foreach($results as $result) {
                $array[$i] = [
                    'id' => $result->id,
                    'name'=> $result->name,
                    'count_new'=> $result->count_new,
                    'count_used'=> $result->count_used,
                    'count_rent'=> $result->count_rent,
              	];
                $i++;
            }

            return $app->generalResponse($array, Application::PUT_GET_OK);

        } else {
            return $app->errorResponse(Keys::ERROR_SELECT, Keys::ERROR_SELECT, 1, Application::NOT_FOUND_ERROR);
        }
    }

    static public function getOneBrand($app, $id)
    {
        $mapperRow = $app->spot()->mapper('BZR\Entity\Brand');
        $results = $mapperRow->first(['id' => $id]);
        return $results->name;
    }
}