<?php

namespace BZR\Controller;

use BZR\Application;
use BZR\Arr;
use BZR\Entity\Categories;
use BZR\Entity\UsersCategoriesSubscription;
use BZR\Exception;
use BZR\Translation\Keys;
use BZR\ErrorChecking;
use BZR\Entity\Response\Adv;
use BZR\Controller\FieldsToEnter;
use BZR\Controller\UploadImages;
use BZR\Controller\ParamFields;
use BZR\Controller\City;
use BZR\Controller\ImagesDelete;
use \Silex\Application\TranslationTrait;
use Symfony\Component\HttpFoundation\Request;

class MainAdv extends Base
{
    public function postCreatAdv(Application $app, Request $request)
    {
        //добавляем id пользователя к объявления, делаем причастность.
        $mapperRow = $app->spot()->mapper('BZR\Entity\UsersAuthMass');
        $getCurrentProfile = $app->getCurrentProfile();

        $categoryId = $request->get('category');

        if ($getCurrentProfile) {
            $photosIsset = false;
            $errorsImages = null;

            //////////ERRORS CHECKED//////////////////////////////////////
            $arrayErrorChecking = [
                'category'       => $request->get('category'),
                'phone'          => $request->get('phone'),
                'city'           => $request->get('city'),
                'title'          => $request->get('title'),
                'text'           => $request->get('text'),
            ];

            $errors = errorChecking::errorChecking($arrayErrorChecking, $app, $mapperRow, $corrector=false);

            //валидация ошибок связанных с изображениями
            if($request->files->get("photo1")){

                for ($i=1; $i < 10; $i++) {
                    $photos = $request->files->get("photo$i");

                    if(isset($photos)){
                        $photosIsset = true;
                        $arrayErrorChecking['mimeType'] = $photos;
                        $errorsImages = errorChecking::errorChecking($arrayErrorChecking, $app, $mapperRow, $corrector=false);
                    }
                }
            }

            if (count($errors) > 0) {
                return $app->errorResponseArray($errors, 1, Application::VALIDATION_ERROR);
            }

            if (count($errorsImages) > 0) {
                return $app->errorResponseArray($errorsImages, 1, Application::VALIDATION_ERROR);
            }
            //////////ERRORS CHECKED//////////////////////////////////////

            $mapperRow = $app->spot()->mapper('BZR\Entity\Adv');
            //формируем url из title как в скриптах на сайте
            $url = strtolower($this->Translit($request->get('title')));

            //обезательные поля для добавления///////////////
            $entity = $mapperRow->build([
                'category'       => $request->get('category'),
                'name'           => $getCurrentProfile->name,
                'phone'          => $request->get('phone'),
                'email'          => $getCurrentProfile->email,
                'city'           => (int)($request->get('city')),
                'area'           => (int)($request->get('area')),
                'title'          => $request->get('title'),
                'title_original' => $request->get('title'),
                'text'           => $request->get('text'),
                'text_rdy'       => $request->get('text'),
                'price'          => (int)($request->get('price')),
                'currency'       => 'euro',
                'not_email'      => (int)($request->get('not_email')),
                'not_skype'      => (int)($request->get('not_skype')),

                'user'           => $getCurrentProfile->id,
                'status'         => 1,
                //'status'         => 2,
                'date'           => time(),
                'date_create'    => time(),
                'added_ip'       => $_SERVER['REMOTE_ADDR'],
                'url'            => $url
            ]);
            //обезательные поля для добавления///////////////

            //добавление в таблицу adv
            $lastId = $mapperRow->insert($entity);

            if ($lastId) {
                //добавления к счётчику в категориях
                $categoriesMapper = $app->spot()->mapper('BZR\Entity\Categories');
                $entityCategories = $categoriesMapper->first(['id' => $request->get('category')]);
                $entityCategories->count = $entityCategories->count + 1;
                $categoriesMapper->update($entityCategories);

                //добавление ошибок если они существуют
                if($photosIsset == true){
                    UploadImages::uploadImages($app, $request, $lastId);
                }

                //создание параметров
                $this->creatParams($app, $lastId, $request);
                //создаём счётчик для объявлений
                $this->creatCounter($app, $lastId);
                //дополнительная доработка title+url+text_rdy
                $this->additionalRefinement($app, $request, $lastId, $entity);

                try {
                    /**
                     * @var \BZR\Entity\Mapper\Categories $categoriesMapper
                     */
                    $categoriesMapper = $app->spot()->mapper('\BZR\Entity\Categories');
                    $categoriesMapper->updateCategory($categoryId);

                } catch (\Exception $e) {
                    //TODO logging
                }

                return $app->generalResponse(["id" => $lastId], Application::POST_OK);
            } else {
                return $app->errorResponse(Keys::ERROR_INSERT, Keys::ERROR_INSERT, 1, Application::NOT_FOUND_ERROR);
            }
        } else {
            return $app->errorResponse(Keys::ERROR_AUT_EX, Keys::ERROR_AUT_EX, 1, Application::AUTH_ERROR);
        }
    }

    public function postEditAdv(Application $app, Request $request)
    {
        $getCurrentProfile = $app->getCurrentProfile();
        $mapperAdv = $app->spot()->mapper('BZR\Entity\Adv');
        $mapperAdvParams = $app->spot()->mapper('BZR\Entity\AdvParams');
        $errorsImages = null;
        $photosIsset = false;

        //////////ERRORS CHECKED//////////////////////////////////////
        $arrayErrorChecking = [
            'phone'          => $request->get('phone'),
            'city'           => $request->get('city'),
            'title'          => $request->get('title'),
            'text'           => $request->get('text'),
        ];

        $errors = errorChecking::errorChecking($arrayErrorChecking, $app, $mapperAdv, $corrector=false);

        //валидация ошибок связанных с изображениями
        if($request->files->get("photo1")){

            for ($i=1; $i < 10; $i++) {
                $photos = $request->files->get("photo$i");

                if(isset($photos)){
                    $photosIsset = true;
                    $arrayErrorChecking['mimeType'] = $photos;
                    $errorsImages = errorChecking::errorChecking($arrayErrorChecking, $app, [], $corrector=false);
                }
            }
        }

        if (count($errors) > 0) {
            return $app->errorResponseArray($errors, 1, Application::VALIDATION_ERROR);
        }

        if (count($errorsImages) > 0) {
            return $app->errorResponseArray($errorsImages, 1, Application::VALIDATION_ERROR);
        }

        //////////ERRORS CHECKED//////////////////////////////////////

        //проверка на причастность пользователя к объявлению, чтобы не дать возможность редактировать чужие объявления.
        $resultAdvCount = $mapperAdv->first(['id' => $request->get('adv_id'), 'user' => $getCurrentProfile->id]);

        if ($resultAdvCount) {

            //добавление ошибок если они существуют
            if ($photosIsset == true) {
                UploadImages::uploadImages($app, $request, $request->get('adv_id'));
            }
            
            //удаляем параметры
            $mapperAdvParams->delete(['adv_id' => (int)($request->get('adv_id'))]);
            //создание параметров
            $this->creatParams($app, (int)($request->get('adv_id')), $request);

            //удаляем изображения
            if ($request->get('idsImages')) {
                $imagesObject = new ImagesDelete;
                $imagesObject->imagesDelete ($app, $request->get('adv_id'), $request->get('idsImages'));
            }

            $entityAdv = $mapperAdv->where(['id' => (int)($request->get('adv_id')), 'user' => $getCurrentProfile->id])
                ->with(['photos','ad_params','counter'])
                ->execute();

            if ($entityAdv) {

                $entityAdv = $entityAdv->first();

                $entityAdv->phone          = $request->get('phone');
                $entityAdv->city           = (int)($request->get('city'));
                $entityAdv->area           = (int)($request->get('area'));
                $entityAdv->text           = $request->get('text');
                $entityAdv->text_rdy       = $request->get('text');
                $entityAdv->price          = (int)($request->get('price'));
                $entityAdv->status         = 1;
                $entityAdv->not_email      = (int)($request->get('not_email'));
                $entityAdv->not_skype      = (int)($request->get('not_skype'));
                $entityAdv->title_original = $request->get('title');//title_original - оригинальный
                $entityAdv->title          = $request->get('title');

                //дополнительная доработка title+url+text_rdy
                $this->additionalRefinement($app, $request, $entityAdv->id, $entityAdv);
                //основные параметры
                $mapperAdv->update($entityAdv);

                $result = new Adv($entityAdv);
                return $app->generalResponse($result->jsonSerialize($app, $request), Application::POST_OK);
            }

        } else {

            return $app->errorResponse(Keys::ERROR_SELECT, Keys::ERROR_SELECT, 1, Application::FORBIDDEN_ERROR);
        }
    }
    
    public function getAdv(Application $app, Request $request)
    {
        $mapperAdv = $app->spot()->mapper('BZR\Entity\Adv');
        $result = $mapperAdv->where(['id' => $request->get('adv_id')])
            ->with(['photos','ad_params', 'counter', 'userProfile'])
            ->execute();
        $e = 0;

        if (count($result) > 0) {
            //плюсуем просмотры
            $this->counterUp($app, $request->get('adv_id'));
            $resultFirst = $result->first();

            $result = new Adv($resultFirst, true);
            $result = ['ad' => $result->jsonSerialize($app, $request)];

            //создаём массив со случайными объявлениями
            $resultSimmilar = $this->simmilar($app, $result['ad']['category'], $mapperAdv, $request);

            foreach($resultSimmilar as $resultSimmilarone) {
                $result['simmilar'][$e] = $resultSimmilarone;
                $e++;
            }

            if ($result['ad']['own'] === false) {
                //throw new Exception(var_export(boolval($app->getCurrentProfile()), true) . ' ' . var_export($app->getMobileToken(), true) . ' ' . var_export($app->getAuthToken(), true));
            }

            return $app->generalResponse($result, Application::PUT_GET_OK);
        } else {
            return $app->errorResponse(Keys::ERROR_SELECT, Keys::ERROR_SELECT, 1, Application::NOT_FOUND_ERROR);
        }
    }

    public function stoppedAdv(Application $app, Request $request)
    {
        $inspector = false;
        $getCurrentProfile = $app->getCurrentProfile();

        $mapperAdv = $app->spot()->mapper('BZR\Entity\Adv');
        $entityAdv = $mapperAdv->first(['id' => (int)($request->get('adv_id')), 'user' => $getCurrentProfile->id]);

        if ($entityAdv) {
            $entityAdv->status = 4;//остановленое
            $mapperAdv->update($entityAdv);
            $inspector = true;
        }

        return $app->generalResponse(['status' => $entityAdv->status], Application::PUT_GET_OK);
    }

    public function restoreAdv(Application $app, Request $request)
    {
        $inspector = false;
        $getCurrentProfile = $app->getCurrentProfile();

        $mapperAdv = $app->spot()->mapper('BZR\Entity\Adv');
        $entityAdv = $mapperAdv->first(['id' => (int)($request->get('adv_id')), 'user' => $getCurrentProfile->id]);

        if ($entityAdv) {
            $entityAdv->status = 2;//востановить
            $mapperAdv->update($entityAdv);
            $inspector = true;
        }

        return $app->generalResponse(['status' => $entityAdv->status], Application::PUT_GET_OK);
    }

    public function deleteAdv(Application $app, Request $request)
    {
        $mapperAdv = $app->spot()->mapper('BZR\Entity\Adv');
        $mapperParams = $app->spot()->mapper('BZR\Entity\AdvParams');
        $mapperAdvCounter = $app->spot()->mapper('BZR\Entity\AdvCounter');
        $mapperFavorites = $app->spot()->mapper('BZR\Entity\Favorites');
        $mapperAdsLastDateUser = $app->spot()->mapper('BZR\Entity\AdsLastDateUser');

        $getCurrentProfile = $app->getCurrentProfile();
        $result = $mapperAdv->first(['id' => (int)($request->get('adv_id')), 'user' => $getCurrentProfile->id]);

        if ($result and $getCurrentProfile) {

            //добавления к счётчику в категориях
            $categoriesMapper = $app->spot()->mapper('BZR\Entity\Categories');
            $entityCategories = $categoriesMapper->first(['id' => $result->category]);
            $entityCategories->count = $entityCategories->count - 1;
            $categoriesMapper->update($entityCategories);

            //УДАЛЯЕМ ОБЪЯВЛЕНИЕ

            //удаляем изображения
            $imagesObject = new ImagesDelete;
            $imagesObject->imagesDelete ($app, $request->get('adv_id'));
            //удаляем параметры
            $mapperParams->delete(['adv_id' => (int)($request->get('adv_id'))]);
            //удаляем объявление
            $mapperAdv->delete(['id' => (int)($request->get('adv_id'))]);
            //удаляем счётчик
            $mapperAdvCounter->delete(['id' => (int)($request->get('adv_id'))]);
            //удаляем из избранные
            $mapperFavorites->delete(['adv_id' => (int)($request->get('adv_id')), 'user_id' => $getCurrentProfile->id ]);

            return $app->generalResponse([(int)($request->get('adv_id'))], Application::PUT_GET_OK);
        }

        return $app->errorResponse(Keys::ERROR_SELECT, Keys::ERROR_SELECT, 1, Application::NOT_FOUND_ERROR);
    }

    public function simmilar($app, $category, $mapperRow, $request)
    {
        $r = 0;
        $arraySimilar = [];

        //тяним случайные записи
        $resultSimilarRand = $mapperRow->query("SELECT id FROM adv WHERE status = 2 and category = ".$category." ORDER BY RAND() LIMIT ".$app['config']['simmilar']."");

        $resultArrayRand = $resultSimilarRand->resultsIdentities();

        $resultSimilar = $mapperRow->where(['id' => $resultArrayRand])
            ->with(['photos','ad_params','counter']);

        //если переменной категории нет, у нас массив с похожиме объявлениями не существует.
        if (isset($resultSimilar)) {

            foreach($resultSimilar as $randResult) {
                $result = new Adv($resultSimilar[$r]);
                $arraySimilar[$r] = $result->jsonSerialize($app, $request);
                $r++;
            }
        }

        return $arraySimilar;
    }

    public function counterUp($app, $id)
    {
        $mapperRow = $app->spot()->mapper('BZR\Entity\AdvCounter');
        $result = $mapperRow->first(['id' => $id]);

        if ($result) {
            $now = date('d_m_y');
            $last_count = date('d_m_y', $result->last_date);

            if ($now !== $last_count) {
                $result->today = 0;
            }

            $result->total = $result->total + 1;
            $result->today = $result->today + 1;
            $result->last_date = time();

            $mapperRow->update($result);
        }
    }

    public function creatParams($app, $lastId, $request)
    {
        //создание записей в БД по параметрам
        $arrayMapper = ParamFields::paramFieldsEnter($app, $request);
        $mapperRow = $app->spot()->mapper('BZR\Entity\AdvParams');

        foreach ($arrayMapper as $key => $value) {
            //исключения
            if($key == 'city_param'){
                $key = 'city';
            }
            //исключения
            if($key == 'area_param'){
                $key = 'area';
            }

            if($value['value'] == 'none'){
                $value['value'] = 0;
            }

            $entityParams = $mapperRow->build([
                'adv_id' => $lastId,
                'name' => $key,
                'value' => $value['value'],
            ]);

            $mapperRow->insert($entityParams);
        }
    }

    public function creatCounter($app, $lastId)
    {
        //добавляем запись в таблицу adv_counter
        $mapperRow = $app->spot()->mapper('BZR\Entity\AdvCounter');
        $entityCounter = $mapperRow->build([
            'id'         => $lastId,
            'total'      => 0,
            'today'      => 0,
            'last_date'  => time()
        ]);

        $mapperRow->insert($entityCounter);
    }

    public function advUserView($app, $idAdv)
    {
        $getCurrentProfile = $app->getCurrentProfile();

        if ($getCurrentProfile) {
            $mapperRow = $app->spot()->mapper('BZR\Entity\Adv');

            $last_active = $getCurrentProfile->last_login;
            $time = time();
            $summa = $time - $last_active;

            if($summa > 259200){
                $summa = $last_active;
            } else {
                $summa = $time - 259200;
            }

            $resultCountAdv = $mapperRow->query("
                SELECT a.id, a.category FROM adv a
                LEFT JOIN ads_last_date_user aldu ON(a.id = aldu.adv_id)
                WHERE a.date_create >= $summa and a.status = 2 and aldu.adv_id = $idAdv and aldu.user_id = ".$getCurrentProfile->id."
            ")->count();

            if($resultCountAdv == 0){

                $mapperAdsLastDateUser = $app->spot()->mapper('BZR\Entity\AdsLastDateUser');
                $resultisset = $mapperAdsLastDateUser->first(['adv_id' => $idAdv]);

                if(!$resultisset){
                    $result = $mapperRow->first(['id' => $idAdv]);

                    $entity = $mapperAdsLastDateUser->build([
                        'adv_id'      => $result->id,
                        'id_category' => $result->category,
                        'user_id'     => $getCurrentProfile->id,
                    ]);

                    $mapperAdsLastDateUser->insert($entity);
                }
            }
        }
    }

    public function Translit($str) {
        $iso9_table = [
            'А' => 'A', 'Б' => 'B', 'В' => 'V', 'Г' => 'G', 'Ѓ' => 'G',
            'Ґ' => 'G`', 'Д' => 'D', 'Е' => 'E', 'Ё' => 'YO', 'Є' => 'YE',
            'Ж' => 'ZH', 'З' => 'Z', 'Ѕ' => 'Z', 'И' => 'I', 'Й' => 'Y',
            'Ј' => 'J', 'І' => 'I', 'Ї' => 'YI', 'К' => 'K', 'Ќ' => 'K',
            'Л' => 'L', 'Љ' => 'L', 'М' => 'M', 'Н' => 'N', 'Њ' => 'N',
            'О' => 'O', 'П' => 'P', 'Р' => 'R', 'С' => 'S', 'Т' => 'T',
            'У' => 'U', 'Ў' => 'U', 'Ф' => 'F', 'Х' => 'H', 'Ц' => 'TS',
            'Ч' => 'CH', 'Џ' => 'DH', 'Ш' => 'SH', 'Щ' => 'SHH', 'Ъ' => '',
            'Ы' => 'YI', 'Ь' => '', 'Э' => 'E', 'Ю' => 'YU', 'Я' => 'YA',
            'а' => 'a', 'б' => 'b', 'в' => 'v', 'г' => 'g', 'ѓ' => 'g',
            'ґ' => 'g', 'д' => 'd', 'е' => 'e', 'ё' => 'yo', 'є' => 'ye',
            'ж' => 'zh', 'з' => 'z', 'ѕ' => 'z', 'и' => 'i', 'й' => 'y',
            'ј' => 'j', 'і' => 'i', 'ї' => 'yi', 'к' => 'k', 'ќ' => 'k',
            'л' => 'l', 'љ' => 'l', 'м' => 'm', 'н' => 'n', 'њ' => 'n',
            'о' => 'o', 'п' => 'p', 'р' => 'r', 'с' => 's', 'т' => 't',
            'у' => 'u', 'ў' => 'u', 'ф' => 'f', 'х' => 'h', 'ц' => 'ts',
            'ч' => 'ch', 'џ' => 'dh', 'ш' => 'sh', 'щ' => 'shh', 'ь' => '',
            'ы' => 'yi', 'ъ' => "", 'э' => 'e', 'ю' => 'yu', 'я' => 'ya'
        ];

        $str = strtr( $str, $iso9_table );
        $str = preg_replace( "/[^A-Za-z0-9`'_\-\.]/", '-', $str );

        return $str;
    }

    public function generateCarTitle($app, $request)
    {
        $ret = '';

        if((int)($request->get('brand')) > 0){
            $brand = Brand::getOneBrand($app, $request->get('brand'));
            $ret .= $brand ? $brand : '' ;
        }

        if((int)($request->get('model')) > 0){
            $model = Model::getOneModel($app, $request->get('model'));
            $ret .= $model ? ' '.$model : '' ;
        }

        if((int)($request->get('brand')) == 0 or (int)($request->get('model')) > 0){
            $ret .= $request->get('title');
        }

        $ret .= (int)($request->get('year')) > 0 ? ' (' . $request->get('year') . ')' : '' ;
        $ret .= (int)($request->get('volume')) > 0 ? ' ' . $request->get('volume') . 'L' : '' ;

        if((int)($request->get('engine')) > 0){
            $optionValue = FieldsToEnter::handlerFields($app, 'engine');

            if(is_array($optionValue)){
                foreach ($optionValue as $key => $value) {
                    if(key($value) == $request->get('engine')){
                        $optionValue = $value[$request->get('engine')];
                        $ret .= ' '.$optionValue;
                    }
                }
            }
        }

        if((int)($request->get('box')) > 0){
            $optionValue = FieldsToEnter::handlerFields($app, 'box');

            if(is_array($optionValue)){
                foreach ($optionValue as $key => $value) {
                    if(key($value) == $request->get('box')){
                        $optionValue = $value[$request->get('box')];
                        $ret .= ' '.$optionValue;
                    }
                }
            }
        }

        if((int)($request->get('city')) > 0){
            $city = City::getOneCity($app, $request->get('city'));

            if(empty($ret)){
                $ret .= 'for sale '.$city;
            } else {
                $ret .= ' for sale '.$city;
            }
        }

        return $ret;
    }

    public function generateCarDesc($app, $request, $arrayParamFields)
    {
        $ret = null;
        //Volkswagen Tiguan (2010), SUV 1.4L Petrol-Mechanics

        if((int)($request->get('brand')) > 0){
            $brand = Brand::getOneBrand($app, $request->get('brand'));
            $ret .= $brand ? $brand : '' ;
        }

        if((int)($request->get('model')) > 0){
            $model = Model::getOneModel($app, $request->get('model'));
            $ret .= $model ? ' '.$model : '' ;
        }

        if(!isset($brand) and !empty($brand) and !isset($model) and !empty($model)){
            $ret .= $request->get('title');
        }

        $ret .= (int)($request->get('year')) > 0 ? ' (' . $request->get('year') . ')' : '' ;
        $ret .= (int)($request->get('volume')) > 0 ? ' ' . $request->get('volume') . 'L' : '' ;

        if((int)($request->get('engine')) > 0){
            $optionValue = FieldsToEnter::handlerFields($app, 'engine');

            if(is_array($optionValue)){
                foreach ($optionValue as $key => $value) {
                    if(key($value) == $request->get('engine')){
                        $optionValue = $value[$request->get('engine')];
                        $ret .= ' '.$optionValue;
                    }
                }
            }
        }

        if((int)($request->get('box')) > 0){
            $optionValue = FieldsToEnter::handlerFields($app, 'box');

            if(is_array($optionValue)){
                foreach ($optionValue as $key => $value) {
                    if(key($value) == $request->get('box')){
                        $optionValue = $value[$request->get('box')];
                        $ret .= ' '.$optionValue;
                    }
                }
            }
        }

        $ret .= (int)($request->get('running')) > 0 ? ' ' . $request->get('running') . 'km' : '' ;

        if((int)($request->get('color')) > 0){
            $optionValue = FieldsToEnter::handlerFields($app, 'color');

            if(is_array($optionValue)){
                foreach ($optionValue as $key => $value) {
                    if(key($value) == $request->get('color')){
                        $optionValue = $value[$request->get('color')];
                        $ret .= ' '.$optionValue;
                    }
                }
            }
        }

        if((int)($request->get('doors')) > 0){
            $optionValue = FieldsToEnter::handlerFields($app, 'doors');

            if(is_array($optionValue)){
                foreach ($optionValue as $key => $value) {
                    if(key($value) == $request->get('doors')){
                        $optionValue = $value[$request->get('doors')];
                        $ret .= ' '.$app->trans('doors').'-'.$optionValue;
                    }
                }
            }
        }

        if((int)($request->get('drive')) > 0){
            $optionValue = FieldsToEnter::handlerFields($app, 'drive');

            if(is_array($optionValue)){
                foreach ($optionValue as $key => $value) {
                    if(key($value) == $request->get('drive')){
                        $optionValue = $value[$request->get('drive')];
                        $ret .= ' '.$app->trans('drive').'-'.$optionValue;
                    }
                }
            }
        }

        if((int)($request->get('wheel')) > 0){
            $optionValue = FieldsToEnter::handlerFields($app, 'wheel');

            if(is_array($optionValue)){
                foreach ($optionValue as $key => $value) {
                    if(key($value) == $request->get('wheel')){
                        $optionValue = $value[$request->get('wheel')];
                        $ret .= ' '.$app->trans('wheel').':'.$optionValue;
                    }
                }
            }
        }

        return $ret;
    }

    public function generateCarText($app, $request, $arrayParamFields, $paramFields)
    {
        $desc1 = $this->generateCarDesc($app, $request, $arrayParamFields, $paramFields);

        $newDesc = preg_replace('/(AIR COND(?:.*)details from owner:)/isU', '', $request->get('text'));
        $newDesc = preg_replace('/(AIR COND(?:.*))$/i', '', $newDesc);
        $newDesc = str_replace(array('<p>', '</p>'), '', $newDesc);

        $paramsDesc = $this->GenerateCarParams($app, $request, $arrayParamFields, $paramFields);

        $newDesc = $desc1."<p>".$newDesc."</p>".$paramsDesc;

        return $newDesc;
    }

    public function GenerateCarParams($app, $request, $arrayParamFields, $paramFields)
    {
        //удаляем не нужны переменные которые не требуют вывода
        //$groups - массив с нужными опциями
        $groups = ['climat' => 'climat', 'glass' => 'glass', 'st_wheel' => 'st_wheel', 'salon' => 'salon', 'heating' => 'heating', 'el' => 'el', 'mem' => 'mem', 'help' => 'help', 'hijack' => 'hijack', 'airbags' => 'airbags', 'help_secure' => 'help_secure', 'media' => 'media', 'light' => 'light', 'TI' => 'TI', 'climate' => 'climate'];

        foreach ($paramFields as $key => $value) {
            if(!isset($groups[$value['mainName']])){
                unset($paramFields[$key]);
            }
        }
        //удаляем не нужны переменные которые не требуют вывода

        $noActive = [];//множественные значения у одной опции
        $ret = '<div class="adv-params">';

        foreach ($paramFields as $keyA => $valueA) {

            $optionNameSearch = $valueA['header'];
            $nameGroupe = '';

            if($valueA['type'] == 'select' or $valueA['type'] == 'radio' or $valueA['type'] == 'checkbox'){

                if(!isset($noActive[$valueA['header']])){

                    //ИЩЕМ множественные значения у одной опции и соедениняем их
                    foreach ($paramFields as $keyB => $valueB) {

                        if($optionNameSearch == $valueB['header']){
                            if(empty($nameGroupe)){
                                $nameGroupe .= $valueB['valueText'];
                            } else {
                                $nameGroupe .= ', '.$valueB['valueText'];
                                $noActive[$valueA['header']] = $valueA['header'];
                            }
                        }
                    }
                    //ИЩЕМ множественные значения у одной опции и соедениняем их

                    $ret .= '<div style="margin-bottom: 5px;"><div class="params-gr" style="display: inline-block;">'.$valueA['header'].'</div><span>'.$nameGroupe.'</span></div>';
                }
            }
        }

        $ret .= '</div>';

        return $ret;
    }

    public function additionalRefinement($app, $request, $advId, $entity)
    {
        $mapperRow = $app->spot()->mapper('BZR\Entity\Adv');
        $category = $request->get('category');
        $nobed = $request->get('nobed');
        $city = $request->get('city');
        $area = $request->get('area');
        $rent = '';
        $generateHomeTitle = '';
        $typeHouse = '';
        $rentType = '';

        if ($category == 742 or $category == 743) {
            //получаем массив парметров для генерации title
            $arrayParamFields = ParamFields::paramFieldsEnter($app, $request);
            $paramFields = ParamFields::paramFields($app, $request, $advId);

            //генерация title для отдельных категорий
            $generateCarTitle = $this->generateCarTitle($app, $request);
            //формируем url из title как в скриптах на сайте
            if ($generateCarTitle) {
                $title = strtolower($this->Translit($generateCarTitle));
                $entity->url = $title . '_' . $advId;
                $entity->title = $generateCarTitle;
            }

            //генерация text_rdy
            $generateCarText = $this->generateCarText($app, $request, $arrayParamFields, $paramFields);
            if ($generateCarText) {
                $entity->text_rdy = $generateCarText;
            }
        }

        //НЕДВИЖИМОСТЬ
        if ($category == 143 or $category == 144 or $category == 145 or $category == 146 or $category == 148 or $category == 149 or $category == 150 or $category == 151 or $category == 153 or $category == 154 or $category == 155 or $category == 156 or $category == 158 or $category == 160 or $category == 163 or $category == 164 or $category == 165 or $category == 166 or $category == 168 or $category == 169 or $category == 170 or $category == 171 or $category == 873 or $category == 874 or $category == 875) {

            //отдельное условие для посуточной оренды
            if ($category == 873 or $category == 874 or $category == 875){
                $rent = 'rent by day ';
            }

            //определяем Продаёться, Покупаеться, Сдаётся или снимается
            if ($category == 143 or $category == 148 or $category == 153 or $category == 158 or $category == 163 or $category == 168){
                $rentType = 'sale';
            }

            //определяем Продаёться, Покупаеться, Сдаётся или снимается
            if ($category == 144 or $category == 149 or $category == 154 or $category == 164 or $category == 169){
                $rentType = 'rent';
            }

            //определяем Продаёться, Покупаеться, Сдаётся или снимается
            if ($category == 145 or $category == 150 or $category == 155 or $category == 160 or $category == 165 or $category == 170){
                $rentType = 'buy';
            }

            //определяем Продаёться, Покупаеться, Сдаётся или снимается
            if ($category == 146 or $category == 151 or $category == 156 or $category == 166 or $category == 171){
                $rentType = 'for to rent';
            }

            //определеяем тип жилья
            if ($category == 143  or $category == 144  or $category == 145  or $category == 146  or $category == 873) {
                $typeHouse = ' Apartment ';
            }

            //определеяем тип жилья
            if ($category == 148  or $category == 149  or $category == 150  or $category == 151 or $category == 874) {
                $typeHouse = ' Studio ';
            }

            //определеяем тип жилья
            if ($category == 153  or $category == 154  or $category == 155  or $category == 156 or $category == 875) {
                $typeHouse = ' Villas ';
            }

            //определеяем тип жилья
            if ($category == 158  or $category == 160) {
                $typeHouse = ' Land ';
            }

            //определеяем тип жилья
            if ($category == 163  or $category == 164  or $category == 165  or $category == 166) {
                $typeHouse = ' Business or Investment ';
            }

            //определеяем тип жилья
            if ($category == 168  or $category == 169  or $category == 170  or $category == 171) {
                $typeHouse = ' Property abroad ';
            }

            //достаём город
            if(!empty($city)){
                $city = City::getOneCity($app, $request->get('city'));

                if(!$city){
                    $city = '';
                }
            }

            //достаём район
            if(!empty($area)){
                $area = Area::getOneArea($app, $request->get('area'));

                if(!$area){
                    $area = '';
                }
            }

            //параметры
            $paramFields = ParamFields::paramFields($app, $request, $advId);

            if (isset($paramFields['nobed'])) {
                $nobed = $paramFields['nobed']["valueText"];
            }

            $generateHomeTitle = 'For '. (!empty($rent) ? $rent : $rentType) .''. (!isset($nobed) || $nobed == 0 ? "" : ' '.$nobed. " bedrooms " ) .''. (!empty($typeHouse) ? $typeHouse : '') .'in'. (!empty($city) ? ' '.$city.'' : '') .''. (!empty($area) ? ' '.$area : '') .'';

            $title = strtolower($this->Translit($generateHomeTitle));
            $entity->url      = $title/* . '_' . $advId*/;
            $entity->title    = $generateHomeTitle;
            $entity->text_rdy = $request->get('text');
        }
            //сохроняем нововидения
            $mapperRow->update($entity);
    }

}
