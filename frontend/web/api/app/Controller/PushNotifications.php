<?php

namespace BZR\Controller;

use BZR\Application;
use BZR\Entity\Response\Adv;
use BZR\Entity\Response\User;
use BZR\Entity\UsersPushToken;
use BZR\Exception;
use BZR\Notification\Sender;
use BZR\Translation\Keys;
use Spot\Entity;
use Symfony\Component\HttpFoundation\Request;

class PushNotifications extends Base
{
    public function addToken(Application $app, Request $request)
    {
        $profile = $app->getCurrentProfile();

        $token = $request->get('token');
        $type  = $request->get('type');

        $availableTypes = [UsersPushToken::TYPE_ANDROID, UsersPushToken::TYPE_IOS];

        if (!$token) {
            return $app->errorResponse(Keys::ERROR_ID_NO, Keys::ERROR_ID_NO, 1, Application::NOT_FOUND_ERROR);
        }

        if (!in_array($type, $availableTypes)) {
            return $app->errorResponse(Keys::ERROR_ID_NO, Keys::ERROR_ID_NO, 2, Application::NOT_FOUND_ERROR);
        }

        $mapper      = $app->spot()->mapper('BZR\Entity\UsersPushToken');
        $tokenEntity = $mapper->first(['user_id' => $profile->id, 'type' => $type, 'token' => $token]);

        if (!$tokenEntity) {
            $tokenEntity = $mapper->build([
                'user_id' => $profile->id,
                'type'    => $type,
                'token'   => $token,
            ]);

            $mapper->save($tokenEntity);
        }

        return $app->generalResponse(null, Application::PUT_GET_OK);
    }

    /**
     * @param Application $app
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\JsonResponse
     */
    /*public function toggle(Application $app, Request $request)
    {
        $profile = $app->getCurrentProfile();
        $enabled = (bool) $request->get('enabled');

        if (!$enabled) {
            $profile->disablePushNotifications();
        } else {
            $profile->enablePushNotifications();
        }

        $app->spot()->mapper('BZR\Entity\Profile')->save($profile);

        $response = new User($profile);
        return $app->generalResponse($response->jsonSerialize($app), Application::PUT_GET_OK);
    }*/

    public function test(Application $app, Request $request)
    {
        $profile = $app->getCurrentProfile();
        $mapper   = $app->spot()->mapper('\BZR\Entity\Adv');
        $adv = $mapper->where()->with(['photos','ad_params','counter'])->first();
        /*$categoriesMapper   = $app->spot()->mapper('\BZR\Entity\Categories');
        $parentCategory = $categoriesMapper->getParentCategory(69);

        var_dump($parentCategory);*/

        /*$app->notification()->broadcast(
            [
                "f012259522aa1ee36c66a9e688bb9bab7d1e7e624a8d52af0cd15b865a1945fe",
            ],
            "Test message",
            UsersPushToken::TYPE_IOS
        );*/
        $response = new Adv($adv);
        return $app->generalResponse($response->jsonSerialize($app, $request), Application::PUT_GET_OK);
    }
}
