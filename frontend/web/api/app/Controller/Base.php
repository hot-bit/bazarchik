<?php
namespace BZR\Controller;
use BZR\Exception;
use Spot\Entity;
use Spot\Mapper;
use Symfony\Component\HttpFoundation\Request;

abstract class Base
{
    protected function tryToSave(Entity $entity, Mapper $mapper)
    {
        if (!$mapper->save($entity)) {
            throw new Exception("Entity save error");
        }
    }

    /**
     * @param $className
     * @return \BZR\Entity\Response\Base
     * @throws Exception
     */
    protected function response($className)
    {
        if (!class_exists($className)) {
            throw new Exception("Invalid response className: \"$className\"");
        }

        $arguments = func_get_args();
        array_shift($arguments);

        $reflection = new \ReflectionClass($className);
        $response   = $reflection->newInstanceArgs($arguments);

        return $response;
    }
}