<?php

namespace BZR\Controller;

use BZR\Application;
use BZR\Arr;
use BZR\Translation\Keys;
use BZR\Controller\Ads;
use \Silex\Application\TranslationTrait;
use Symfony\Component\HttpFoundation\Request;

class Categories //extends Base
{
    public function getCategories(Application $app, Request $request)
    {
        $mapperRow = $app->spot()->mapper('BZR\Entity\Categories');
        $parent = $request->get('parent');
        $array = [];
        $i = 0;
        $countAdvNew = [];

        if(empty($parent)){
            $parent = 0;
        }

        $parent = intval($parent);

        $isRootCategoriesRequested = $parent === 0;

        $entity = $mapperRow->where(['parent' => $parent, 'header' => '0', 'hidden_for_mobile' => 0]);

        if ($entity->count() > 0){

            $categoriesGroupBy = [];

            if ($isRootCategoriesRequested) {
                $getCurrentProfile = $app->getCurrentProfile();

                if ($getCurrentProfile) {
                    $rootCategoriesId = [];
                    foreach ($entity as $category) {
                        $rootCategoriesId[] = $category->id;
                    }
                    $categoriesGroupBy = $this->categoriesGroupBy($app, $rootCategoriesId);
                }
            }

            foreach ($entity as $categoriesResult) {

                $adsNew = 0;

                if ($isRootCategoriesRequested) {
                    if (isset($categoriesGroupBy[$categoriesResult->id])) {
                        $adsNew = $categoriesGroupBy[$categoriesResult->id];
                    }
                }

                //существуют дочернии категории
                $parentResult = $this->parentResult($app, $categoriesResult->id, $mapperRow);

                $iconCategoryMobile = $categoriesResult->iconCategoryMobile;

                if(!empty($iconCategoryMobile)){
                    $iconCategoryMobile = $app['config']['rootIconMainCategories'].$iconCategoryMobile;
                } else {
                    $iconCategoryMobile = null;
                }

                //echo "<pre>";var_dump($categoriesResult);
                $array[$i] = [
                    'title'=> ['name_ru' => $categoriesResult->name_ru, 'name_en'   => $categoriesResult->name_en , 'name_gr' => $categoriesResult->name_gr],
                    'ads_new'=> $adsNew,
                    'icon'=> $iconCategoryMobile,
                    'ads_count'=> $categoriesResult->count,
                    'category_id'=> $categoriesResult->id,
                    'parent_id'=> $categoriesResult->parent,
                    'paramsResult'=> $parentResult
              	];
                $i++;
            }

        } else {
            //return $app->errorResponse(Keys::ERROR_SELECT, Keys::ERROR_SELECT, 1, Application::NOT_FOUND_ERROR);
            return $app->generalResponse($array, Application::PUT_GET_OK);
        }

        return $app->generalResponse($array, Application::PUT_GET_OK);   
    }

    public function categoriesGroupBy($app, array $rootCategoriesId)
    {  
        $mapperAdv = $app->spot()->mapper('BZR\Entity\Adv');
        $mapperAdsLastDateUser = $app->spot()->mapper('BZR\Entity\AdsLastDateUser');
        $array = [];
        $summa = 0;
        $result = [];

        //достаём последний вход в аккаунт, и сравниваем с текущей датой. Разница не может быть больше чем 3 дня.
        // 3 дня - 259200 секунд
        $getCurrentProfile = $app->getCurrentProfile();

        if ($getCurrentProfile) {

            $last_active = $getCurrentProfile->last_login;
            $time = time();
            $summa = $time - $last_active;

            if($summa > 259200){
                $summa = $last_active;
            } else {
                $summa = $time - 259200;
            }
            //echo "$summa";

            $resultAdv = $mapperAdv->query("
                SELECT category as category_id, date_create FROM adv
                WHERE date_create >= $summa
            ");

            //выборка массива просмотренных объявлений, в зависимости от пользователя
            $resultAdsLastDateUser = $mapperAdv->query("
                SELECT aldu.id_category as category_id, aldu.visit_date as visit_date FROM ads_last_date_user aldu
                WHERE aldu.user_id = ".$getCurrentProfile->id."
            ");

            $resultAdv = $resultAdv->toArray();
            $resultAdsLastDateUser = $resultAdsLastDateUser->toArray();

            $lastVisitDateMap = [];

            foreach ($resultAdsLastDateUser as $lastVisitRow) {
                $categoryId = $lastVisitRow['category_id'];
                $visitDate  = $lastVisitRow['visit_date'];

                $lastVisitDateMap[$categoryId] = $visitDate;
            }

            $subcategoriesMap = [];

            foreach ($rootCategoriesId as $rootCategoryId) {
                $subcategories = Categories::allCategories($app, $rootCategoryId);

                foreach ($subcategories as $subcategoryId) {
                    $subcategoriesMap[$subcategoryId] = $rootCategoryId;
                }
            }

            foreach ($resultAdv as $advRow) {
                $categoryId   = $advRow['category_id'];
                $creationDate = $advRow['date_create'];

                $rootCategoryId = isset($subcategoriesMap[$categoryId]) ? $subcategoriesMap[$categoryId] : $categoryId;

                $lastVisit = isset($lastVisitDateMap[$rootCategoryId]) ? $lastVisitDateMap[$rootCategoryId] : 0;

                if ($creationDate > $lastVisit) {
                    if (!isset($result[$rootCategoryId])) {
                        $result[$rootCategoryId] = 0;
                    }

                    $result[$rootCategoryId] += 1;
                }
            }
        }

        return $result;
    }

    static public function allCategories($app, $categoryId, $array = array(), $number = 0)
    { 
        $complite = false;
        $mapperCategories = $app->spot()->mapper('BZR\Entity\Categories');
        $number = 0;


        /*
        $result = $mapperCategories->query("
        SELECT g0.id AS id1, g0.parent, g1.id AS id2, g1.parent, g2.id AS id3, g2.parent, g3.id AS id4, g3.parent, g4.id AS id5, g4.parent, g5.id AS id6, g5.parent
        FROM categories AS g0
        LEFT JOIN categories AS g1 ON g0.id = g1.parent
        LEFT JOIN categories AS g2 ON g1.id = g2.parent
        LEFT JOIN categories AS g3 ON g2.id = g3.parent
        LEFT JOIN categories AS g4 ON g3.id = g4.parent
        LEFT JOIN categories AS g5 ON g4.id = g5.parent
        WHERE g0.parent = 0
        AND g0.header =0
        ");
        */

        while($number < 6){

            $leftJoin = '';
            for ($i = 0; $i < $number; $i++) { 
                $iReal = $i + 1;
                $leftJoin  .= " LEFT JOIN categories g$iReal ON g$i.id = g$iReal.parent ";
            }
            
            //раскоментировать если нужно искать только по конечным категориям.
            $whereAnd = '';
            //for ($i = 0; $i <= $number; $i++) { 
                
                //if($i == $number){
                    //$whereAnd  .= " and g$i.slave = 1 ";
                //} else {
                    //$whereAnd  .= " and g$i.slave = 0 ";
                //}               
            //}

            for ($i = 0; $i <= $number; $i++) {
                $whereAnd  .= " and g$i.header = 0 ";
            }

            //echo $leftJoin.'</br>';
            $query = "SELECT * FROM categories g0 $leftJoin WHERE g0.parent = $categoryId $whereAnd";

            $result = $mapperCategories->query($query);
            $result = $result->toArray();

            if(!empty($result)){

                foreach ($result as $key => $value) {
                    $array[$value['id']] = $value['id'];
                    $complite = true;
                }

            } else {
                $array[$categoryId] = $categoryId;
                $complite = true;
            }

            if($complite){
                $number = $number + 1;
                $complite = false;
            } else {
                $number = 10;
            }
        }

       return $array;
    }

    public function parentResult($app, $id, $mapperRow)
    {
        $entity = $mapperRow->where(['parent' => $id, 'hidden_for_mobile' => 0])->count();
        $array = ['issetParentCategory' => ''];

        if($entity > 0){
            $array['issetParentCategory'] = 'yes';
        } else {
            $array['issetParentCategory'] = 'no';
        }

        return $array;
    }

    public function oneCategories(Application $app, Request $request)
    {
        $mapperCategories = $app->spot()->mapper('BZR\Entity\Categories');
        $id = $request->get('id');
        $entity = $mapperCategories->first(['id' => $id]);
        $nameCategories = '';
        $locale = '';

        if(isset($app["config"]["locale"])){
            $locale = $app["config"]["locale"];
        }

        if ($entity) {

            return $app->generalResponse(['name_ru' => $nameCategories = $entity->name_ru, 'name_en' => $nameCategories = $entity->name_en, 'name_gr' => $nameCategories = $entity->name_gr, 'parent_id' => $nameCategories = $entity->parent ], Application::PUT_GET_OK);
        }

        return $app->generalResponse([], Application::PUT_GET_OK);
    }



}
