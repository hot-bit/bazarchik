<?php

namespace BZR\Controller;

use BZR\Application;
use BZR\Arr;
use BZR\Controller\FieldsToEnter;
use BZR\Controller\Brand;
use BZR\Controller\Model;
use BZR\Controller\MotoBrand;
use BZR\Entity\Response\Adv;
use Symfony\Component\HttpFoundation\Request;

class ParamFields extends Base
{
	public static function paramFields(Application $app, Request $request, $advId = false)
	{
		$arrayAdvOptions = [];

		if($request->get('adv_id')){
			$adv_id = $request->get('adv_id');
		}

		if($advId){
			$adv_id = $advId;
		}

		if($adv_id){
			//получаем список всех параметров
			$mapperRow = $app->spot()->mapper('BZR\Entity\AdvParams');
			$resultParams = $mapperRow->where(['adv_id' => $adv_id]);

			//получаем категорию объявления
			$mapperRow = $app->spot()->mapper('BZR\Entity\Adv');
			$resultAdv = $mapperRow->first(['id' => $adv_id]);

			if($resultAdv){
				//Весь массив со всеми значениями, Значении нужно отдавать у radio и select.
				$arrayOptions = FieldsToEnter::fieldsToEnter($app, $request, $resultAdv->category, $giveArrayCompact = '2');
				//echo "<pre>";var_dump($arrayOptions);
				$resultParams = $resultParams->toArray();

				//перебираем Главный массив данных по опциям для категорий
				foreach ($arrayOptions as $keyOptions => $valueOptions) {

					if($keyOptions == 'option1' and count($arrayOptions['option1']) > 1 or $keyOptions == 'option2' and  count($arrayOptions['option2']) > 1  ){

						foreach ($valueOptions as $keyA => $valueA) {
							//echo "<pre>";var_dump($valueB['name']);
							foreach ($valueA as $keyB => $valueB) {

								if($valueB['type'] == 'checkbox'){

									foreach ($resultParams as $keyC => $valueC) {

										foreach ($valueB['checkbox'] as $keyD => $valueD) {

											$arrayObrabotka	= FieldsToEnter::handlerFields($app, $valueB['checkbox']);

											if(key($valueD) == $valueC['name']){
												//echo "<pre>";var_dump($valueOptions);

												$arrayAdvOptions[$valueD[key($valueD)]['name']] = [
													'header' => $valueB['header'],
													'name' => $valueD[key($valueD)]['name'],
													'mainName' => $valueA[0]['name'],
													'type' => $valueB['type'],
													'value' => $valueC['value'],
													'valueText' => $valueD[key($valueD)]['header'],
												];
											}
										}
									}

								} else {

									foreach ($resultParams as $keyC => $valueC) {

										if($valueB['type'] == 'select' or $valueB['type'] == 'radio'){

											//дописываем переменные исключение
											//brand; model; moto_brand
											//для них нужно отдельное подключение к БД
											if($valueB['name'] == $valueC['name'] and $valueC['name'] != 'brand' and $valueC['name'] != 'model' and $valueC['name'] != 'moto_brand'){

												$arrayObrabotka	= FieldsToEnter::handlerFields($app, $valueC['name']);

												foreach ($arrayObrabotka as $keyD => $valueD) {

													$valueValue = $valueC['value'];

													if($valueC['value'] == 0){
														$valueValue = 'none';
													}

													if(key($valueD) == $valueValue){
														$arrayAdvOptions[$valueB['name']] = [
															'header' => $valueB['header'],
															'name' => $valueB['name'],
															'mainName' => $valueB['name'],
															'type' => $valueB['type'],
															'value' => $valueC['value'],
															'valueText' => $valueD[$valueValue],
														];
													}
												}
											}

											/////ИСКЛЮЧЕНИЯ ТРЕБУЮЩИЕ ПОДКЛЮЧЕНИЕ В БД
											//если brand
											if($valueB['name'] == $valueC['name'] and $valueC['name'] == 'brand'){

												$results = Brand::getOneBrand($app, $valueC['value']);
												$valueText = null;

												if($results){
													$valueText = $results;
												}

												$arrayAdvOptions[$valueB['name']] = [
													'header' => $valueB['header'],
													'name' => $valueB['name'],
													'mainName' => $valueB['name'],
													'type' => $valueB['type'],
													'value' => $valueC['value'],
													'valueText' => $valueText,
												];
											}

											//если model
											if($valueB['name'] == $valueC['name'] and $valueC['name'] == 'model'){

												$results = Model::getOneModel($app, $valueC['value']);
												$valueText = null;

												if($results){
													$valueText = $results;
												}

												$arrayAdvOptions[$valueB['name']] = [
													'header' => $valueB['header'],
													'name' => $valueB['name'],
													'mainName' => $valueB['name'],
													'type' => $valueB['type'],
													'value' => $valueC['value'],
													'valueText' => $valueText,
												];
											}

											//если moto_brand
											if($valueB['name'] == $valueC['name'] and $valueC['name'] == 'moto_brand'){

												$results = MotoBrand::getOneMotoBrand($app, $valueC['value']);
												$valueText = null;

												if($results){
													$valueText = $results;
												}

												$arrayAdvOptions[$valueB['name']] = [
													'header' => $valueB['header'],
													'name' => $valueB['name'],
													'mainName' => $valueB['name'],
													'type' => $valueB['type'],
													'value' => $valueC['value'],
													'valueText' => $valueText,
												];
											}
											/////ИСКЛЮЧЕНИЯ ТРЕБУЮЩИЕ ПОДКЛЮЧЕНИЕ В БД

										} else {

											if($valueB['name'] == $valueC['name']){
												//для все текстовых переменых
												$arrayAdvOptions[$valueB['name']] = [
													'header' => $valueB['header'],
													'name' => $valueB['name'],
													'mainName' => $valueB['name'],
													'type' => $valueB['type'],
													'value' => $valueC['value'],
													'valueText' => $valueC['value'],
												];
											}
										}
									}
								}
							}
						}
					}
				}
			}
		}

		//этот вариант вывода нужен только при формировании DESCRIPTION при добавлении объявления
		if($advId){
			//echo "<pre>";var_dump($arrayAdvOptions);
			return $arrayAdvOptions;
		}

		//echo "<pre>";var_dump($arrayAdvOptions);
		return $app->generalResponse($arrayAdvOptions, Application::PUT_GET_OK);
	}

	public static function paramFieldsEnter($app, $request)
	{
		//Поиск по переменным и отдача, массива с присланными значениями
		$arrayFields = FieldsToEnter::fieldsToEnter($app, $request, $request->get('category'), $giveArrayCompact = '1');
		$arrayMapper = [];

		foreach ($arrayFields as $key => $value) {

			if(isset($key) and !empty($value)){
				$arrayMapper = ParamFields::bildingMapper($key, $value, $arrayMapper, $request);
			}

		}
		return $arrayMapper;
	}


	static public function bildingMapper($key, $value, $arrayMapper, $request)
	{
		foreach ($value as $keyTwo => $valueTwo) {

			//если существует ставим "on"
			if($key == "checkbox"){

				if($request->get($keyTwo)){
					$arrayMapper[$keyTwo] = ['type' => $key, 'value' => 'on'];
				}

			} else {


				if($request->get($keyTwo)){
					$arrayMapper[$keyTwo] = ['type' => $key, 'value' => $request->get($keyTwo)];
				}
			}
		}

		return $arrayMapper;
	}

}