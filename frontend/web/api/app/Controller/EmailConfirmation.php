<?php

namespace BZR\Controller;

use BZR\Application;
use BZR\Entity\Response\User;
use BZR\Entity\UsersPushToken;
use BZR\Exception;
use BZR\Notification\Sender;
use BZR\Translation\Keys;
use Spot\Entity;
use Symfony\Component\HttpFoundation\Request;

class EmailConfirmation extends Base
{
    /**
     * @param Application $app
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\JsonResponse
     */
    public function request(Application $app, Request $request)
    {
        $profile = $app->getCurrentProfile();

        $email = $profile->email;

        if (!$email) {
            return $app->errorResponse(Keys::ERROR_CONFIRMATION_NO_EMAIL, Keys::ERROR_CONFIRMATION_NO_EMAIL, 1, Application::FORBIDDEN_ERROR);
        }

        $code       = mt_rand(1000, 9999);
        $messageKey = $app->config(['email', 'confirmationMessage'], '');
        $message    = $app->trans($messageKey);
        $message    = str_replace("%code%", $code, $message);

        $profile->email_code  = $code;

        $headers = $app->config(['email', 'confirmationMessageHeaders']);
        $subject = $app->trans($app->config(['email', 'confirmationMessageSubject'], ''));

        if (!mail($email, $subject, $message, $headers)){
            return $app->errorResponse(Keys::ERROR_EMAIL_SENT, Keys::ERROR_EMAIL_SENT, 1, 500);
        }

        $mapper = $app->spot()->mapper('BZR\Entity\Profile');

        $mapper->save($profile);

        $responseEntity = new User($profile);
        return $app->generalResponse($responseEntity->jsonSerialize($app), Application::PUT_GET_OK);
    }

    /**
     * @param Application $app
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\JsonResponse
     */
    public function confirm(Application $app, Request $request)
    {
        $profile = $app->getCurrentProfile();

        $code = $request->get('code');

        if (!$code) {
            return $app->errorResponse(Keys::ERROR_ID_NO, Keys::ERROR_ID_NO, 1, Application::NOT_FOUND_ERROR);
        }

        if ($code == $profile->email_code) {
            $profile->success_email = 1;
            $profile->email_code = 0;

            $mapper = $app->spot()->mapper('BZR\Entity\Profile');
            $mapper->save($profile);

            $responseEntity = new User($profile);
            return $app->generalResponse($responseEntity->jsonSerialize($app), Application::PUT_GET_OK);
        } else {
            return $app->errorResponse(Keys::ERROR_CONFIRMATION_WRONG_CODE, Keys::ERROR_CONFIRMATION_WRONG_CODE, 1, Application::FORBIDDEN_ERROR);
        }
    }
}
