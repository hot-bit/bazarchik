<?php

namespace BZR\Controller;

use BZR\Application;
use BZR\Entity\Categories;
use BZR\Entity\Profile;
use BZR\Translation\Keys;
use Spot\Entity;
use Symfony\Component\HttpFoundation\Request;

class Subscriptions extends Base
{
    /**
     * @param Application $app
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\JsonResponse
     */
    public function get(Application $app, Request $request)
    {
        $profile  = $app->getCurrentProfile();
        $response = $this->getSubscriptionsResponse($app, $profile);

        return $app->generalResponse($response, Application::PUT_GET_OK);
    }

    /**
     * @param Application $app
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\JsonResponse
     */
    public function isSubscribed(Application $app, Request $request)
    {
        $profile = $app->getCurrentProfile();
        $categoryId = $request->get('category_id');

        if (!is_numeric($categoryId)) {
            return $app->errorResponse(Keys::ERROR_ID_NO, Keys::ERROR_ID_NO, 1, Application::NOT_FOUND_ERROR);
        }

        $subscription = $profile->subscriptions->where(['category_id' => $categoryId])->first()->query();

        return $app->generalResponse(['subscribed' => (bool)$subscription], Application::PUT_GET_OK);
    }

    /**
     * @param Application $app
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\JsonResponse
     */
    public function set(Application $app, Request $request)
    {
        $profile    = $app->getCurrentProfile();
        $categoryId = $request->get('category_id');
        $subscribed = (bool) $request->get('subscribed');

        if (!is_numeric($categoryId)) {
            return $app->errorResponse(Keys::ERROR_ID_NO, Keys::ERROR_ID_NO, 1, Application::NOT_FOUND_ERROR);
        }

        $subscription = $profile->subscriptions->where(['category_id' => $categoryId])->first()->query();
        $mapper       = $app->spot()->mapper('BZR\Entity\UsersCategoriesSubscription');

        if ($subscribed) {
            if (!$subscription) {
                $subscription = $mapper->build([
                    'user_id'     => $profile->id,
                    'category_id' => $categoryId
                ]);

                $mapper->save($subscription);
            }
        } else {
            if ($subscription) {
                $mapper->delete($subscription);
            }
        }

        return $app->generalResponse(['subscribed' => (bool) $subscribed], Application::PUT_GET_OK);
    }

    /**
     * @param Application $app
     * @param Profile $profile
     * @param $flag
     */
    public function togglePush(Application $app, Profile $profile, $flag)
    {
        $profile = $app->getCurrentProfile();
        if (!$flag) {
            $profile->disablePushNotifications();
        } else {
            $profile->enablePushNotifications();
        }

        $app->spot()->mapper('BZR\Entity\Profile')->save($profile);
    }

    /**
     * @param Application $app
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\JsonResponse
     */
    public function setMulti(Application $app, Request $request)
    {
        $profile = $app->getCurrentProfile();

        $enabled = $request->get('enabled', []);

        if (!is_array($enabled)) {
            return $app->errorResponse(Keys::ERROR_ID_NO, Keys::ERROR_ID_NO, 1, Application::NOT_FOUND_ERROR);
        }

        $enabledPush = $request->get('push');
        if (!is_null($enabledPush)) {
            $this->togglePush($app, $profile, (bool) $enabledPush);
        }

        /**
         * @var \BZR\Entity\Mapper\Categories $categoriesMapper
         */
        $categoriesMapper = $app->spot()->mapper('BZR\Entity\Categories');
        $categoriesWithoutParentIdList = $categoriesMapper->getCategoriesIdWithParent(Categories::NO_PARENT);

        $subscriptions = $profile->subscriptions;
        $mapper        = $app->spot()->mapper('BZR\Entity\UsersCategoriesSubscription');
        $exists        = [];

        foreach ($subscriptions as $subscription) {
            $categoryId = $subscription->category_id;

            if (!in_array($categoryId, $categoriesWithoutParentIdList)) {
                continue; //обрабатываются только категории у которых нет родителя
            }

            if (!in_array($categoryId, $enabled)) {
                $mapper->delete($subscription);
            } else {
                $exists[] = $categoryId;
            }
        }

        $toCreate = array_diff($enabled, $exists);

        foreach ($toCreate as $categoryId) {

            if (!is_numeric($categoryId)) {
                continue;
            }

            $subscription = $mapper->build([
                'user_id'     => $profile->id,
                'category_id' => $categoryId
            ]);

            $mapper->save($subscription);
        }

        $response = $this->getSubscriptionsResponse($app, $profile);

        return $app->generalResponse($response, Application::PUT_GET_OK);
    }

    /**
     * @param Application $app
     * @param Profile $profile
     * @return \BZR\Entity\Response\Base
     * @throws \BZR\Exception
     */
    protected function getSubscriptionsResponse(Application $app, Profile $profile)
    {
        $mapper = $app->spot()->mapper('BZR\Entity\UsersCategoriesSubscription');
        $subscriptions = $mapper->where(['user_id' => $profile->id])->with('category');
        //$subscriptions    = $profile->subscriptions->with('category');
        $categoriesMapper = $app->spot()->mapper('BZR\Entity\Categories');

        $categories = $categoriesMapper->getCategoriesWithParent(Categories::NO_PARENT);

        return $this->response('BZR\Entity\Response\Subscriptions', $app, $profile, $categories, $subscriptions);
    }
}
