<?php
/**
 * Created by PhpStorm.
 * User: Александр
 * Date: 18.07.2016
 * Time: 11:15
 */

namespace BZR\Controller;

use BZR\Application;
use BZR\Translation\Keys;
use Symfony\Component\HttpFoundation\Request;

class ImagesDelete
{
    public function imagesDelete (Application $app, $advId = false, $idsImages = false)
    {
        if ($idsImages || $advId) {
            $result = self::imagesActionDelete($app, $advId, $idsImages);
            return $app->generalResponse([$result], Application::PUT_GET_OK, []);
        }

        return $app->errorResponse(Keys::ERROR_SELECT, Keys::ERROR_SELECT, 1, Application::METHOD_ERROR);
    }

    public function imagesActionDelete ($app, $advId, $idsImages)
    {
        //если есть массив изображений, удаляем по штучно
        if ($idsImages) {
            $idImage = explode(',', $idsImages);
            $arrayIds = [];

            foreach ($idImage as $key => $value) {
                array_push($arrayIds, $value);
            }

            $mapperAdvImages = $app->spot()->mapper('BZR\Entity\AdvImages');
            $entityImage = $mapperAdvImages->where(['id' => $arrayIds, 'adv_id' => $advId])->execute();

            if($entityImage->count() > 0){
                $advId = $entityImage[0]->adv_id;
            }
        } else {
            //активно когда удаляем объявление
            $mapperAdvImages = $app->spot()->mapper('BZR\Entity\AdvImages');
            $entityImage = $mapperAdvImages->where(['adv_id' => $advId])->execute();

            if($entityImage->count() > 0){
                $advId = $entityImage[0]->adv_id;
            }
        }

        //проверяем причастность пользователя к объявлению
        if($advId){
            $getCurrentProfile = $app->getCurrentProfile();
            $mapperAdv = $app->spot()->mapper('BZR\Entity\Adv');
            $entityAdv = $mapperAdv->first(['id' => $advId, 'user' => $getCurrentProfile->id]);
        }

        //если объявление пренадлежит пользователю
        if ($entityAdv) {

            foreach ($entityImage as $image) {
                $name = $image->name;
                $dir  = $image->dir;

                //удаляем изображения
                self::deleteFileImage ($app, $name, $dir);

                //удаляем запись из БД
                $mapperAdvImages->delete(['id' => $image->id]);
            }
            return true;
        }

        return false;
    }


    public function deleteFileImage ($app, $name, $dir)
    {
        //удаляем миниатюрки
        if (file_exists($app['config']['rootUploadImages'].$dir.'/L_'.$name)) {
            unlink($app['config']['rootUploadImages'].$dir.'/L_'.$name);
        }

        //удаляем миниатюрки
        if (file_exists($app['config']['rootUploadImages'].$dir.'/M_'.$name)) {
            unlink($app['config']['rootUploadImages'].$dir.'/M_'.$name);
        }

        //удаляем миниатюрки
        if (file_exists($app['config']['rootUploadImages'].$dir.'/S_'.$name)) {
            unlink($app['config']['rootUploadImages'].$dir.'/S_'.$name);
        }

        //удаляем оригинал
        if (file_exists($app['config']['rootUploadImages'].$dir.'/'.$name)) {
            unlink($app['config']['rootUploadImages'].$dir.'/'.$name);
        }

    }
    
}