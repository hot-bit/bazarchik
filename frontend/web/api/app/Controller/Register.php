<?php

namespace BZR\Controller;

use BZR\Application;
use BZR\Arr;
use BZR\Translation\Keys;
use BZR\ErrorChecking;
use BZR\Entity\Response\User;
use BZR\Controller\Profile;
use Symfony\Component\HttpFoundation\Request;

class Register extends Base
{
    public function postRegister(Application $app, Request $request)
    {
        $mapperProfile = $app->spot()->mapper('BZR\Entity\Profile');
        $mapperUsersAuth = $app->spot()->mapper('BZR\Entity\UsersAuthMass');

        $getCurrentProfile = $app->getCurrentProfile();

        if ($getCurrentProfile) {
            //если мобильный токен и токен авторизации существует, означает то что пользователь уже авторизован, и не может быть зарегистрирован.
            return $app->errorResponse(Keys::ERROR_AUT_EX, Keys::ERROR_AUT_EX, 1, Application::NOT_FOUND_ERROR);
        } else {
            //////////ERRORS CHECKED//////////////////////////////////////
            $arrayErrorChecking = [
                'name'     => $request->get('name'),
                'email'    => $request->get("email"),//уникальное
                'phone'    => $request->get("phone"),//уникальное
                'password' => $request->get('password')
            ];

            $errors = errorChecking::errorChecking($arrayErrorChecking, $app, $mapperProfile, $corrector=true);

            //действия для регистрации после авторизации через соц. сети, когда почта уже есть.
            if(count($errors) > 0){
                $pr_errors = preg_replace ("/[^a-zA-ZА-Яа-я0-9\s]/","", $errors[0]->getPropertyPath());
            }

            //действия для регистрации после авторизации через соц. сети, когда почта уже есть.
            $entityUser = $mapperProfile->first(['email' => $request->get("email"), 'password' => '']);
            if (count($errors) == 1 and $entityUser and isset($pr_errors) and $pr_errors == 'email') {
            }elseif (count($errors) > 0) {
                return $app->errorResponseArray($errors, 1, Application::VALIDATION_ERROR);
            }
            //////////ERRORS CHECKED//////////////////////////////////////

            $arrayPG = $app->passwordGeneration($request->get('password'));

            if($entityUser){
                $entityUser->password = $arrayPG['password'];
                $entityUser->salt = $arrayPG['salt'];
                $entityUser->phone = $request->get('phone');

                $mapperProfile->update($entityUser);
                $idUser = $entityUser->id;
            } else {
                $entity = $mapperProfile->build([
                    'name'         => $request->get('name'),
                    'email'        => $request->get("email"),//уникальное
                    'phone'        => $request->get('phone'),//уникальное
                    'password'     => $arrayPG['password'],
                    'role'         => '3',
                    'salt'         => $arrayPG['salt'],
                    'last_login'     => time(),
                    'date_reg'     => time(),
                    'status'       => '0'
                ]);

                $idUser = $mapperProfile->insert($entity);

                //добавляем запись в сводную таблицу токенов, для последующего отслеживания авторизаций
                $authToken = md5(rand(1,9999));
                $app->setAuthToken($authToken);

                $entityUsersAuth = $mapperUsersAuth->build([
                    'user_id'      => $idUser,
                    'mobile_token' => $app->getMobileToken(),
                    'auth_token'   => $app->getAuthToken(),
                    'lat'          => '11',
                    'lng'          => '11',
                    'last_login'   => time(),
                ]);

                $mapperUsersAuth->insert($entityUsersAuth);
            }

            $result = Profile::getProfileUp($app, $request, $idUser, $resultTokken = true, $postAndGet = true);

            return $result;
        }
    }
}