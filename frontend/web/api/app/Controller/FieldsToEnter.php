<?php

namespace BZR\Controller;

use BZR\Application;
use BZR\Arr;
use BZR\Translation\Keys;
use \Silex\Application\TranslationTrait;
use Symfony\Component\HttpFoundation\Request;

class FieldsToEnter //extends Base
{
	static public function fieldsToEnter(Application $app, Request $request, $category = false, $giveArrayCompact = '3')
	{
		$getOptionSearch = $request->get('optionSearch');

		if($request->get('category')){
			$category = $request->get('category');
		}

		$option1 = [];
		$option2 = [];
		$optionSearch = [];

		if($category == 742){
			$option1 = ['brand', 'model','year','vin','body','doors','color','engine','volume','box','drive','wheel',
			];
			$option2 = ['st_wheel', 'salon','climat','help_secure','heating','glass','el','mem','help','hijack','airbags','media','audio','light','disc','TI'
			];
			$optionSearch = ['brand', 'model','body', 'year', 'running', 'box', 'engine', 'volume', 'drive', 'price', 'wheel',
			];
		}

		if($category == 743){
			$option1 = ['brand', 'model','year','vin','body','doors','color','engine','volume','box','drive','wheel', 'running'
			];
			$option2 = ['st_wheel', 'salon','climat','help_secure','heating','glass','el','mem','help','hijack','airbags','media','audio','light','disc','TI'
			];
			$optionSearch = ['brand', 'model','body', 'year', 'running', 'box', 'engine', 'volume', 'drive', 'price', 'wheel'
			];
		}

		if($category == 78 or $category == 79 or $category == 80 or $category == 82 or $category == 85 or $category == 86 or $category == 87 or $category == 88 or $category == 90 or $category == 91){
			$option1 = ['year', 'running', 'color', 'engine','volume','box', 'salon','climat'];
		}

		if($category == 72 or $category == 73 or $category == 74 or $category == 75 or $category == 76){
			$option1 = ['moto_brand', 'year', 'vin', 'running','color','moto_volume', 'box'];
			$optionSearch = ['moto_brand', 'year', 'running', 'box', 'moto_volume'
			];
		}

		//Шины
		if($category == 137){
			$option1 = ['diam', 'height_prof', 'width_prof'];
		}

		//Мотошины
		if($category == 138){
			$option1 = ['diam', 'width_prof', 'round', 'height_prof'];
		}

		//Диски
		if($category == 139){
			$option1 = ['brand', 'size', 'holes', 'types', 'width', 'holes_diameter', 'outfly'];
		}

		//Колёса
		if($category == 140){
			$option1 = ['disc_type', 'width_ob', 'wholes_diam', 'diam', 'wholes', 'outfly', 'width_prof', 'height_prof'];
		}

		//Колпаки
		if($category == 141){
			$option1 = ['diam'];
		}

		//Продажа квартир
		if ($category == 143 or $category == 144 or $category == 145 or $category == 146 or $category == 148 or
            $category == 149 or $category == 150 or $category == 151 or $category == 153 or $category == 154 or
            $category == 155 or $category == 156 or $category == 158 or $category == 160 or $category == 163 or
            $category == 164 or $category == 165 or $category == 166 or $category == 168 or $category == 169 or
            $category == 170 or $category == 171 or $category == 873 or $category == 874 or $category == 875) {

			$option1 = ['nobed', 'cover'];
			$optionSearch = ['price', 'nobed', 'cover'];
		}

		//Женская одежда
		if($category == 263 or $category == 264 or $category == 265 or $category == 266 or $category == 267 or $category == 268 or $category == 269 or $category == 270 or $category == 271 or $category == 272 or $category == 273 or $category == 274){
			$option1 = ['size_f'];
		}

		if($category == 277 or $category == 278 or $category == 279 or $category == 280 or $category == 281 or $category == 282 or $category == 283){
			$option1 = ['size_m'];
		}

		if($category == 289 or $category == 290 or $category == 291 or $category == 292 or $category == 293 or $category == 294 or $category == 295 or $category == 296 or $category == 299 or $category == 301 or $category == 302 or $category == 303 or $category == 304 or $category == 305 or $category == 306){
			$option1 = ['size_c'];
		}

		$array = FieldsToEnter::finishParentResult($app, $category, $option1, $option2, $optionSearch, $getOptionSearch);

		if($giveArrayCompact == '1'){
			return FieldsToEnter::collectNames($array);
		}

		if($giveArrayCompact == '2'){
			//echo "<pre>";var_dump($array);
			return $array;
		}

		if($giveArrayCompact == '3'){
			//echo "<pre>";var_dump($array);
			return $app->generalResponse($array, Application::PUT_GET_OK);
		}
	}

	static public function finishParentResult($app, $id, $option1, $option2, $optionSearch, $getOptionSearch)
	{
		//ОБЯЗАТЕЛЬНЫЕ ПАРАМЕТРЫ/////////////
		$array = [
			'mainOption' => [

				'not_email' => [
					'0' => [
						'header' => $app->trans('not_email'),
						'type' => 'checkbox',
						'checkbox' => FieldsToEnter::handlerFields($app, $event = 'not_email_checkbox')]],

				'not_skype' => [
					'0' => [
						'header'   => $app->trans('not_skype'),
						'type'     => 'checkbox',
						'checkbox' => FieldsToEnter::handlerFields($app, $event = 'not_skype_checkbox')
                    ]
                ],

				'name' => [
					'0' => [
						'header' => $app->trans('your_name'),
						'type' => 'input',
						'name' => 'name']],

				'phone' => [
					'0' => [
						'header' => $app->trans('your_phone'),
						'type' => 'input',
						'name' => 'phone']],

				/*'email' => [
					'0' => [
						'header' => $app->trans('your_email'),
						'type' => 'input',
						'name' => 'email']],*/

				'city' => [
					'0' => [
						'header' => $app->trans('your_city'),
						'type' => 'select',
						'name' => 'city']],

				'area' => [
					'0' => [
						'header' => $app->trans('your_area'),
						'type' => 'select',
						'name' => 'area']],

				'title' => [
					'0' => [
						'header' => $app->trans('text'),
						'type' => 'input',
						'name' => 'title']],

				'text' => [
					'0' => [
						'header' => $app->trans('text_adv'),
						'type' => 'textarea',
						'name' => 'text']],

				'price' => [
					'0' => [
						'header' => $app->trans('price'),
						'type' => 'input',
						'name' => 'price']],

				'photo' => [
					'0' => [
						'header' => $app->trans('photo'),
						'type' => 'file',
						'name' => 'photo']],
			],
			'option1' => [],
			'option2' => [],
			'optionSearch' => []

		];

		//ДОПОЛНИТЕЛЬНЫЕ ПАРАМЕТРЫ/////////////
		$arrayOptions = [

			'brand' => [
				'0' => [
					'header' => $app->trans('Brand'),
					'type' => 'select',
					'name' => 'brand']],

			'moto_brand' => [
				'0' => [
					'header' => $app->trans('Brand'),
					'type' => 'select',
					'name' => 'moto_brand']],

			'model' => [
				'0' => [
					'header' => $app->trans('Model'),
					'type' => 'select',
					'name' => 'model']],

			'price' => [
				'0' => [
					'header' => $app->trans('price'),
					'type' => 'input',
					'name' => 'price',
					'typeInfo' => 'number',
					'MinMaxType' => 'rangebar',
					'MinMax' => ['steps'=> '1000', 'min' => '0', 'max' => '30000']
				]],

			'year' => [
				'0' => [
					'header' => $app->trans('year'),
					'type' => 'select',
					'name' => 'year',
					'typeInfo' => 'number',
					'MinMaxType' => 'rangebar',
					'MinMax' => ['steps'=> '1', 'min' => '1960', 'max' => date('Y')],
					'year' => FieldsToEnter::handlerFields($app, $event = 'year')]],

			'vin' => [
				'0' => [
					'header' => $app->trans('vin'),
					'type' => 'input',
					'name' => 'vin']],

			'body' => [
				'0' => [
					'header' => $app->trans('Body'),
					'type' => 'radio',
					'name' => 'body',
					'MinMaxType' => 'checkbox',
					'body' => FieldsToEnter::handlerFields($app, $event = 'body')]],

			'doors' => [
				'0' => [
					'header' => $app->trans('doors'),
					'type' => 'radio',
					'name' => 'doors',
					'doors' => FieldsToEnter::handlerFields($app, $event = 'doors')]],

			'color' => [
				'0' => [
					'header' => $app->trans('color'),
					'type' => 'radio',
					'name' => 'color',
					'color' => FieldsToEnter::handlerFields($app, $event = 'color')]],

			'engine' => [
				'0' => [
					'header' => $app->trans('Engine'),
					'type' => 'radio',
					'name' => 'engine',
					'MinMaxType' => 'checkbox',
					'engine' => FieldsToEnter::handlerFields($app, $event = 'engine')]],

			'volume' => [
				'0' => [
					'header' => $app->trans('Volume'),
					'type' => 'MinMax',
					'name' => 'volume',
					'volume' => ['steps'=> '0.1', 'min' => '0.5', 'max' => '9.1'],
					'MinMaxType' => 'rangebar',
					'typeInfo' => 'number',
					'MinMax' => ['steps'=> '0.1', 'min' => '0.5', 'max' => '9.1']
				]],
			'moto_volume' => [
				'0' => [
					'header' => $app->trans('Volume'),
					'type' => 'MinMax',
					'name' => 'volume',
					'volume' => ['steps'=> '0.05', 'min' => '0.25', 'max' => '2.0'],
					'MinMaxType' => 'rangebar',
					'typeInfo' => 'number',
					'MinMax' => ['steps'=> '0.05', 'min' => '0.25', 'max' => '2.0'],
				]],

			'box' => [
				'0' => [
					'header' => $app->trans('box'),
					'type' => 'radio',
					'name' => 'box',
					'MinMaxType' => 'checkbox',
					'box' => FieldsToEnter::handlerFields($app, $event = 'box')]],

			'drive' => [
				'0' => [
					'header' => $app->trans('drive'),
					'type' => 'radio',
					'name' => 'drive',
					'MinMaxType' => 'checkbox',
					'drive' => FieldsToEnter::handlerFields($app, $event = 'drive')]],

			'wheel' => [
				'0' => [
					'header' => $app->trans('wheel'),
					'type' => 'radio',
					'name' => 'wheel',
					'MinMaxType' => 'select',
					'wheel' => FieldsToEnter::handlerFields($app, $event = 'wheel')]],

			'running' => [
				'0' => [
					'header' => $app->trans('Running'),
					'type' => 'input',
					'name' => 'running',
					'MinMaxType' => 'rangebar',
					'typeInfo' => 'number',
					'MinMax' => ['steps'=> '10000', 'min' => '0', 'max' => '200000']
				]],

			'st_wheel' => [
				'0' => [
					'header' => $app->trans('driving_wheel_assistance'),
					'type' => 'radio',
					'name' => 'st_wheel',
					'st_wheel' => FieldsToEnter::handlerFields($app, $event = 'st_wheel')]],

			'salon' => [
				'0' => [
					'header' => $app->trans('category_salon'),
					'type' => 'radio',
					'name' => 'salon',
					'salon' => FieldsToEnter::handlerFields($app, $event = 'salon')],
				'1' =>[
					'header' => $app->trans('category_salon'),
					'type' => 'checkbox',
					//'name' => 'salon_checkbox',
					'checkbox' => FieldsToEnter::handlerFields($app, $event = 'salon_checkbox')]],

			'climat' => [
				'0' => [
					'header' => $app->trans('category_climat'),
					'type' => 'radio',
					'name' => 'climat',
					'climat' => FieldsToEnter::handlerFields($app, $event = 'climat')],
				'1' => [
					'header' => $app->trans('category_climat'),
					'type' => 'checkbox',
					//'name' => 'category_climat',
					'checkbox' => FieldsToEnter::handlerFields($app, $event = 'climat_checkbox')]],

			'help_secure' => [
				'0' => [
					'header' => $app->trans('category_help_secure'),
					'type' => 'checkbox',
					'name' => 'help_secure',
					'checkbox' => FieldsToEnter::handlerFields($app, $event = 'help_secure_checkbox')]],

			'heating' => [
				'0' => [
					'header' => $app->trans('category_heating'),
					'type' => 'checkbox',
					'name' => 'heating',
					'checkbox' => FieldsToEnter::handlerFields($app, $event = 'heating_checkbox')]],

			'glass' => [
				'0' => [
					'header' => $app->trans('electric_power_windows'),
					'type' => 'select',
					'name' => 'glass',
					'glass' => FieldsToEnter::handlerFields($app, $event = 'glass')]],

			'el' => [
				'0' => [
					'header' => $app->trans('category_el'),
					'type' => 'checkbox',
					'name' => 'el',
					'checkbox' => FieldsToEnter::handlerFields($app, $event = 'el_checkbox')]],

			'mem' => [
				'0' => [
					'header' => $app->trans('category_mem'),
					'type' => 'checkbox',
					'name' => 'mem',
					'checkbox' => FieldsToEnter::handlerFields($app, $event = 'mem_checkbox')]],

			'help' => [
				'0' => [
					'header' => $app->trans('category_help'),
					'type' => 'checkbox',
					'name' => 'help',
					'checkbox' => FieldsToEnter::handlerFields($app, $event = 'help_checkbox')]],

			'hijack' => [
				'0' => [
					'header' => $app->trans('category_hijack'),
					'type' => 'checkbox',
					'name' => 'hijack',
					'checkbox' => FieldsToEnter::handlerFields($app, $event = 'hijack_checkbox')]],

			'airbags' => [
				'0' => [
					'header' => $app->trans('category_airbags'),
					'type' => 'checkbox',
					'name' => 'airbags',
					'checkbox' => FieldsToEnter::handlerFields($app, $event = 'airbags_checkbox')]],


			'media' => [
				'0' => [
					'header' => $app->trans('category_media'),
					'type' => 'checkbox',
					'name' => 'media',
					'checkbox' => FieldsToEnter::handlerFields($app, $event = 'media_checkbox')]],

			'audio' => [
				'0' => [
					'header' => $app->trans('category_media2'),
					'type' => 'radio',
					'name' => 'audio',
					'audio' => FieldsToEnter::handlerFields($app, $event = 'audio')],
				'1' => [
					'header' => $app->trans('category_media2'),
					'type' => 'checkbox',
					//'name' => 'category_media2',
					'checkbox' => FieldsToEnter::handlerFields($app, $event = 'audio_checkbox')]],

			'light' => [
				'0' => [
					'header' => $app->trans('category_light'),
					'type' => 'radio',
					'name' => 'light',
					'light' => FieldsToEnter::handlerFields($app, $event = 'light')],
				'1' => [
					'header' => $app->trans('category_light'),
					'type' => 'checkbox',
					//'name' => 'category_light',
					'checkbox' => FieldsToEnter::handlerFields($app, $event = 'light_checkbox')]],

			'disc' => [
				'0' => [
					'header' => $app->trans('Tires_and_wheels'),
					'type' => 'select',
					'name' => 'disc',
					'disc' => FieldsToEnter::handlerFields($app, $event = 'disc')]],

			'TI' => [
				'0' => [
					'header' => $app->trans('category_TI'),
					'type' => 'checkbox',
					'name' => 'TI',
					'checkbox' => FieldsToEnter::handlerFields($app, $event = 'TI_checkbox')]],

			'diam' => [
				'0' => [
					'header' => $app->trans('diam'),
					'type' => 'select',
					'name' => 'diam',
					'diam' => FieldsToEnter::handlerFields($app, $event = 'diam')]],

			'size' => [
				'0' => [
					'header' => $app->trans('diam'),
					'type' => 'select',
					'name' => 'size',
					'size' => FieldsToEnter::handlerFields($app, $event = 'diam')]],

			'height_prof' => [
				'0' => [
					'header' => $app->trans('height_prof'),
					'type' => 'select',
					'name' => 'height_prof',
					'height_prof' => FieldsToEnter::handlerFields($app, $event = 'height_prof')]],

			'width_prof' => [
				'0' => [
					'header' => $app->trans('width_prof'),
					'type' => 'select',
					'name' => 'width_prof',
					'width_prof' => FieldsToEnter::handlerFields($app, $event = 'width_prof')]],

			'round' => [
				'0' => [
					'header' => $app->trans('round'),
					'type' => 'select',
					'name' => 'round',
					'round' => FieldsToEnter::handlerFields($app, $event = 'round')]],

			'types' => [
				'0' => [
					'header' => $app->trans('category_disc_type'),
					'type' => 'select',
					'name' => 'type',
					'types' => FieldsToEnter::handlerFields($app, $event = 'type')]],

			'disc_type' => [
				'0' => [
					'header' => $app->trans('category_disc_type'),
					'type' => 'select',
					'name' => 'disc_type',
					'disc_type' => FieldsToEnter::handlerFields($app, $event = 'type')]],

			'width' => [
				'0' => [
					'header' => $app->trans('category_width'),
					'type' => 'select',
					'name' => 'width',
					'width' => FieldsToEnter::handlerFields($app, $event = 'width')]],

			'width_ob' => [
				'0' => [
					'header' => $app->trans('category_width'),
					'type' => 'select',
					'name' => 'width',
					'width_ob' => FieldsToEnter::handlerFields($app, $event = 'width')]],

			'holes_diameter' => [
				'0' => [
					'header' => $app->trans('category_holes_diameter'),
					'type' => 'select',
					'name' => 'holes_diameter',
					'holes_diameter' => FieldsToEnter::handlerFields($app, $event = 'holes_diameter')]],

			'wholes_diam' => [
				'0' => [
					'header' => $app->trans('category_holes_diameter'),
					'type' => 'select',
					'name' => 'wholes_diam',
					'wholes_diam' => FieldsToEnter::handlerFields($app, $event = 'holes_diameter')]],

			'outfly' => [
				'0' => [
					'header' => $app->trans('category_outfly'),
					'type' => 'select',
					'name' => 'outfly',
					'outfly' => FieldsToEnter::handlerFields($app, $event = 'outfly')]],

			'holes' => [
				'0' => [
					'header' => $app->trans('category_holes'),
					'type' => 'select',
					'name' => 'holes',
					'holes' => FieldsToEnter::handlerFields($app, $event = 'holes')]],

			'wholes' => [
				'0' => [
					'header' => $app->trans('category_holes'),
					'type' => 'select',
					'name' => 'wholes',
					'wholes' => FieldsToEnter::handlerFields($app, $event = 'holes')]],

			'city_param' => [
				'0' => [
					'header' => $app->trans('city'),
					'type' => 'select',
					'name' => 'city_param']],

			'area_param' => [
				'0' => [
					'header' => $app->trans('area'),
					'type' => 'select',
					'name' => 'area_param']],

			'nobed' => [
				'0' => [
					'header' => $app->trans('nobed'),
					'type' => 'select',
					'name' => 'nobed',
					'MinMaxType' => 'select',
					'nobed' => FieldsToEnter::handlerFields($app, $event = 'nobed')]],

			'cover'  => [
				'0' => [
					'header' => $app->trans('cover'),
					'type' => 'input',
					'MinMaxType' => 'rangebar',
					'typeInfo' => 'number',
					'MinMax' => ['steps'=> '2', 'min' => '12', 'max' => '500'],
					'name' => 'cover']],

			'size_f' => [
				'0' => [
					'header' => $app->trans('param_size'),
					'type' => 'select',
					'name' => 'size_f',
					'size_f' => FieldsToEnter::handlerFields($app, $event = 'size_f')]],

			'size_m' => [
				'0' => [
					'header' => $app->trans('param_size'),
					'type' => 'select',
					'name' => 'size_m',
					'size_m' => FieldsToEnter::handlerFields($app, $event = 'size_m')]],

			'size_c' => [
				'0' => [
					'header' => $app->trans('param_size'),
					'type' => 'select',
					'name' => 'size_c',
					'size_c' => FieldsToEnter::handlerFields($app, $event = 'size_c')]],


		];


		if($getOptionSearch == 'false' or $getOptionSearch == ''){

			//СОЗДАНИЕ МАССИВА/////////////
			foreach ($option1 as $value) {

				if(is_array($arrayOptions[$value])){
					$array['option1'] = array_merge($array['option1'], [$arrayOptions[$value]]);
				}
			}

			//СОЗДАНИЕ МАССИВА/////////////
			foreach ($option2 as $key => $value) {

				if(is_array($arrayOptions[$value])){
					$array['option2'] = array_merge($array['option2'], [$arrayOptions[$value]]);
				}
			}

		}

        if ($id == 143 or $id == 144 or $id == 145 or $id == 146 or $id == 148 or $id == 149 or $id == 150 or $id == 151 or $id == 153 or $id == 154 or $id == 155 or $id == 156 or
            $id == 158 or $id == 160 or $id == 163 or $id == 164 or $id == 165 or $id == 166 or $id == 168 or $id == 169 or $id == 170 or $id == 171) {

            $arrayOptions['price']['0']['MinMax'] = ['steps'=> '1000', 'min' => '0', 'max' => '1000000'];
        }

        if ($id == 144 or $id == 146 or $id == 149 or $id == 151 or $id == 154 or $id == 156 or $id == 164 or $id == 166 or $id == 169 or $id == 171) {
            $arrayOptions['price']['0']['MinMax'] = ['steps'=> '50', 'min' => '200', 'max' => '2000'];
        }

        if ($id == 873 or $id == 874 or $id == 875){
            $arrayOptions['price']['0']['MinMax'] = ['steps'=> '10', 'min' => '20', 'max' => '700'];
        }

		if($getOptionSearch == 'true'){

			$array['mainOption'] = [];

			//СОЗДАНИЕ МАССИВА/////////////
			foreach ($optionSearch as $key => $value) {

				if(is_array($arrayOptions[$value])){
					$array['optionSearch'] = array_merge($array['optionSearch'], [$arrayOptions[$value]]);
				}
			}

		}

		//echo "<pre>";var_dump($array);
		return $array;
	}

	static public function handlerFields($app, $event)
	{
		$result = [];

		if($event == 'year'){
			$result = [['none' => $app->trans('Choose_year')]];

			for ($i = date('Y'); $i >= 1960; $i--) {
				$result = array_merge($result, [[$i => $i]]);
			}
		}

		if($event == 'disc'){
			$result = [['none' => $app->trans('Tires_and_wheels_default')]];
			$r = 1;

			for ($i = 7; $i < 31; $i++) {
				$result = array_merge($result, [[$r => $i]]);
				$r++;
			}
		}

		if($event == 'diam'){
			$result = [['none' => $app->trans('default_diam')]];
			$r = 1;

			for ($i = 12; $i < 30; $i++) {
				$result = array_merge($result, [[$r => $i]]);
				$r++;
			}
		}

		if($event == 'height_prof'){
			$result = [['none' => $app->trans('default_height_prof')]];
			$r = 1;

			for ($i = 25; $i <= 95; $i+=5) {
				$result = array_merge($result, [[$r => $i]]);
				$r++;
			}
		}

		if($event == 'width_prof'){
			$result = [['none' => $app->trans('default_width_prof')]];
			$r = 1;

			for ($i = 125; $i <= 395; $i+=10) {
				$result = array_merge($result, [[$r => $i]]);
				$r++;
			}
		}

		if($event == 'holes'){
			$r = 0;
			for ($i = 3; $i <= 10; $i++) {

				if($r == 0){
					$result = [['none' => $i]];
				} else {
					$result = array_merge($result, [[$r => $i]]);
				}

				$r++;
			}
		}

		if($event == 'width'){
			$r = 0;

			for ($i = 4; $i <= 13; $i+=0.5) {

				if($r == 0){
					$result = [['none' => $i]];
				} else {
					$result = array_merge($result, [[$r => $i]]);
				}

				$r++;
			}
		}

		if($event == 'holes_diameter'){
			$result = [['none' => $app->trans('width_holes_default')]];
			$r = 1;

			for ($i = 98; $i <= 256; $i+=2) {
				$result = array_merge($result, [[$r => $i]]);
				$r++;
			}
		}

		if($event == 'outfly'){
			$result = [['none' => $app->trans('outfly_default')]];
			$r = 1;

			for ($i = -65; $i <= 186; $i++) {
				$result = array_merge($result, [[$r => $i]]);
				$r++;
			}
		}

		if($event == 'nobed'){
			$result = [['none' => $app->trans('nobed_default')]];
			$r = 1;

			for ($i = 1; $i <= 9; $i++) {
				$result = array_merge($result, [[$r => $i]]);
				$r++;
			}
		}

		if($event == 'doors'){
			$result = [
				['none' => $app->trans('Not_specify')],
				['1' => '1'],
				['2' => '2'],
				['3' => '3'],
				['4' => '4'],
				['5' => '5'],
			];
		}

		if($event == 'round'){
			$result = [
				['none' => $app->trans('param_round_1')],
				['1' => $app->trans('param_round_2')],
				['2' => $app->trans('param_round_3')],
			];
		}

		if($event == 'body'){
			$result = [
				['1'  => $app->trans('Sedan')],
				['2'  => $app->trans('Hatchback')],
				['3'  => $app->trans('Versatile')],
				['4'  => $app->trans('SUV')],
				['5'  => $app->trans('Convertible')],
				['6'  => $app->trans('Crossover')],
				['7'  => $app->trans('Coupe')],
				['8'  => $app->trans('Limousine')],
				['9'  => $app->trans('Minivan')],
				['10' => $app->trans('Pickup')],
				['11' => $app->trans('Van')],
				['12' => $app->trans('Minibus')],
			];
		}

		if($event == 'color'){
			$result = [
				['1' => 'red'],
				['2' => 'brown'],
				['3' => 'orange'],
				['4' => 'beige'],
				['5' => 'yellow'],
				['6' => 'green'],
				['7' => 'blue'],
				['8' => 'dark_blue'],
				['9' => 'violet'],
				['10' => 'purple'],
				['11' => 'pink'],
				['12' => 'white'],
				['13' => 'grey'],
				['14' => 'black'],
				['15' => 'gold'],
				['16' => 'silver'],
			];
		}

		if($event == 'engine'){
			$result = [
				['1' => $app->trans('Petrol')],
				['2' => $app->trans('Diesel')],
				['3' => $app->trans('Hybrid')],
				['4' => $app->trans('Electro')],
				['5' => $app->trans('Gas')],
			];
		}

		if($event == 'box'){
			$result = [
				['1' => $app->trans('Mechanics')],
				['2' => $app->trans('Automatic')],
				['3' => $app->trans('Robot')],
				['4' => $app->trans('CVT')],
			];
		}

		if($event == 'drive'){
			$result = [
				['1' => $app->trans('Forward')],
				['2' => $app->trans('Rear')],
				['3' => $app->trans('Full')],
			];
		}

		if($event == 'wheel'){
			$result = [
				['1' => $app->trans('Right')],
				['2' => $app->trans('Left')],
			];
		}

		if($event == 'st_wheel'){
			$result = [
				['none' => $app->trans('param_default')],
				['1' => $app->trans('param_st_1')],
				['2' => $app->trans('param_st_2')],
				['3' => $app->trans('param_st_3')],
			];
		}

		if($event == 'salon'){
			$result = [
				['none' => $app->trans('param_salon_default')],
				['1' => $app->trans('Leather')],
				['2' => $app->trans('Cloth')],
				['3' => $app->trans('Velours')],
				['4' => $app->trans('Combined')],
			];
		}

		if($event == 'salon_checkbox'){
			$result = [
				['salon_leather_wheel' => [
					'name' => 'salon_leather_wheel',
					'header' => $app->trans('param_salon_leather_wheel')]
				],
				['salon_hatch' => [
					'name' => 'salon_hatch',
					'header' => $app->trans('param_salon_hatch')]
				]
			];
		}

		if($event == 'climat'){
			$result = [
				['none' => $app->trans('param_climat_default')],
				['1' => $app->trans('param_climat_1')],
				['2' => $app->trans('param_climat_2')],
				['3' => $app->trans('param_climat_3')],
			];
		}

		if($event == 'climat_checkbox'){
			$result = [
				['climate_wheel_contr' => ['name' => 'climate_wheel_contr', 'header' => $app->trans('param_climate_wheel_contr')]],
				['climate_aterm' => ['name' => 'climate_aterm', 'header' => $app->trans('param_climate_aterm')]]
			];
		}

		if($event == 'help_secure_checkbox'){
			$result = [
				['help_secure_abs' => ['name' => 'help_secure_abs', 'header' => $app->trans('param_help_secure_abs')]],
				['help_secure_asr' => ['name' => 'help_secure_asr', 'header' => $app->trans('param_help_secure_asr')]],
				['help_secure_esp' => ['name' => 'help_secure_esp', 'header' => $app->trans('param_help_secure_esp')]],
				['help_secure_ebd' => ['name' => 'help_secure_ebd', 'header' => $app->trans('param_help_secure_ebd')]],
				['help_secure_eba' => ['name' => 'help_secure_eba', 'header' => $app->trans('param_help_secure_eba')]],
				['help_secure_eds' => ['name' => 'help_secure_eds', 'header' => $app->trans('param_help_secure_eds')]],
				['help_secure_pds' => ['name' => 'help_secure_pds', 'header' => $app->trans('param_help_secure_pds')]]
			];
		}

		if($event == 'heating_checkbox'){
			$result = [
				['heating_front_seat' => ['name' => 'heating_front_seat', 'header' => $app->trans('param_heating_front_seat')]],
				['heating_rear_seat' => ['name' => 'heating_rear_seat', 'header' => $app->trans('param_heating_rear_seat')]],
				['heating_mirrors' => ['name' => 'heating_mirrors', 'header' => $app->trans('param_heating_mirrors')]],
				['heating_rear_window' => ['name' => 'heating_rear_window', 'header' => $app->trans('param_heating_rear_window')]],
				['heating_wheel' => ['name' => 'heating_wheel', 'header' => $app->trans('param_heating_wheel')]],
			];
		}

		if($event == 'glass'){
			$result = [
				['none' => $app->trans('param_glass_defaul')],
				['1' => $app->trans('param_glass_1')],
				['2' => $app->trans('param_glass_2')],
			];
		}

		if($event == 'el_checkbox'){
			$result = [
				['el_wheel_contr' => ['name' => 'el_wheel_contr', 'header' => $app->trans('param_el_wheel_contr')]],
				['el_front_seats' => ['name' => 'el_front_seats', 'header' => $app->trans('param_el_front_seats')]],
				['el_rear_seats' => ['name' => 'el_rear_seats', 'header' => $app->trans('param_el_rear_seats')]],
				['el_mirrors' => ['name' => 'el_mirrors', 'header' => $app->trans('param_el_mirrors')]],
				['el_wheel' => ['name' => 'el_wheel', 'header' => $app->trans('param_el_wheel')]],
				['el_folding_mirrors' => ['name' => 'el_folding_mirrors', 'header' => $app->trans('param_el_folding_mirrors')]],
			];
		}

		if($event == 'mem_checkbox'){
			$result = [
				['mem_front_seats' => ['name' => 'mem_front_seats', 'header' => $app->trans('param_mem_front_seats')]],
				['mem_rear_seats' => ['name' => 'mem_rear_seats', 'header' => $app->trans('param_mem_rear_seats')]],
				['mem_mirrors' => ['name' => 'mem_mirrors', 'header' => $app->trans('param_mem_mirrors')]],
				['mem_wheel' => ['name' => 'mem_wheel', 'header' => $app->trans('param_mem_wheel')]],
			];
		}

		if($event == 'help_checkbox'){
			$result = [
				['help_jockey' => ['name' => 'help_jockey', 'header' => $app->trans('param_help_jockey')]],
				['help_rain_sensor' => ['name' => 'help_rain_sensor', 'header' => $app->trans('param_help_rain_sensor')]],
				['help_light_sensor' => ['name' => 'help_light_sensor', 'header' => $app->trans('param_help_light_sensor')]],
				['help_r_park_sensor' => ['name' => 'help_r_park_sensor', 'header' => $app->trans('param_help_r_park_sensor')]],
				['help_f_park_sensor' => ['name' => 'help_f_park_sensor', 'header' => $app->trans('param_help_f_park_sensor')]],
				['help_blind_spot' => ['name' => 'help_blind_spot', 'header' => $app->trans('param_help_blind_spot')]],
				['help_rear_camera' => ['name' => 'help_rear_camera', 'header' => $app->trans('param_help_rear_camera')]],
				['help_cruise_control' => ['name' => 'help_cruise_control', 'header' => $app->trans('param_help_cruise_control')]],
				['help_computer' => ['name' => 'help_computer', 'header' => $app->trans('param_help_computer')]],
			];
		}

		if($event == 'hijack_checkbox'){
			$result = [
				['hijack_signal' => ['name' => 'hijack_signal', 'header' => $app->trans('param_hijack_signal')]],
				['hijack_central_lock' => ['name' => 'hijack_central_lock', 'header' => $app->trans('param_hijack_central_lock')]],
				['hijack_immobi' => ['name' => 'hijack_immobi', 'header' => $app->trans('param_hijack_immobi')]],
				['hijack_sputnik' => ['name' => 'hijack_sputnik', 'header' => $app->trans('param_hijack_sputnik')]]
			];
		}

		if($event == 'airbags_checkbox'){
			$result = [
				['airbags_front' => ['name' => 'airbags_front', 'header' => $app->trans('param_airbags_front')]],
				['airbags_knee' => ['name' => 'airbags_knee', 'header' => $app->trans('param_airbags_knee')]],
				['airbags_blinds' => ['name' => 'airbags_blinds', 'header' => $app->trans('param_airbags_blinds')]],
				['airbags_front_side' => ['name' => 'airbags_front_side', 'header' => $app->trans('param_airbags_front_side')]],
				['airbags_rear_side' => ['name' => 'airbags_rear_side', 'header' => $app->trans('param_airbags_rear_side')]]
			];
		}

		if($event == 'searchIssetPhotos'){
			$result = [
				['searchIssetPhotos' => ['name' => 'searchIssetPhotos', 'header' => $app->trans('searchIssetPhotos')]],
			];
		}

		if($event == 'media_checkbox'){
			$result = [
				['media_cd' => ['name' => 'media_cd', 'header' => $app->trans('param_media_cd')]],
				['media_mp3' => ['name' => 'media_mp3', 'header' => $app->trans('param_media_mp3')]],
				['media_tv' => ['name' => 'media_tv', 'header' => $app->trans('param_media_tv')]],
				['media_radio' => ['name' => 'media_radio', 'header' => $app->trans('param_media_radio')]],
				['media_video' => ['name' => 'media_video', 'header' => $app->trans('param_media_video')]],
				['media_wheel_control' => ['name' => 'media_wheel_control', 'header' => $app->trans('param_media_wheel_control')]],
				['media_usb' => ['name' => 'media_usb', 'header' => $app->trans('param_media_usb')]],
				['media_aux' => ['name' => 'media_aux', 'header' => $app->trans('param_media_aux')]],
				['media_bluetooth' => ['name' => 'media_bluetooth', 'header' => $app->trans('param_media_bluetooth')]],
				['media_gps' => ['name' => 'media_gps', 'header' => $app->trans('param_media_gps')]],
			];
		}

		if($event == 'audio'){
			$result = [
				['none' => $app->trans('param_media_audio_default')],
				['1' => $app->trans('param_media_audio_1')],
				['2' => $app->trans('param_media_audio_2')],
				['3' => $app->trans('param_media_audio_3')],
				['4' => $app->trans('param_media_audio_4')],
			];
		}

		if($event == 'audio_checkbox'){
			$result = [
				['sub' => ['name' => 'sub', 'header' => $app->trans('param_media_sub')]],
			];
		}

		if($event == 'not_email_checkbox'){
			$result = [
				['not_email' => ['name' => 'not_email', 'header' => $app->trans('not_email')]],
			];
		}

		if($event == 'not_skype_checkbox'){
			$result = [
				['not_skype' => ['name' => 'not_skype', 'header' => $app->trans('not_skype')]],
			];
		}

		if($event == 'light'){
			$result = [
				['none' => $app->trans('param_light_default')],
				['1' => $app->trans('param_light_1')],
				['2' => $app->trans('param_light_2')],
				['3' => $app->trans('param_light_3')],
			];
		}

		if($event == 'light_checkbox'){
			$result = [
				['light_afog' => ['name' => 'light_afog', 'header' => $app->trans('param_light_afog')]],
				['light_cleaner' => ['name' => 'light_cleaner', 'header' => $app->trans('param_light_cleaner')]],
				['light_adaptive' => ['name' => 'light_adaptive', 'header' => $app->trans('param_light_adaptive')]],
			];
		}

		if($event == 'TI_checkbox'){
			$result = [
				['TI_service_book' => ['name' => 'TI_service_book', 'header' => $app->trans('param_TI_service_book')]],
				['TI_dealer' => ['name' => 'TI_dealer', 'header' => $app->trans('param_TI_dealer')]],
				['TI_warranty' => ['name' => 'TI_warranty', 'header' => $app->trans('param_TI_warranty')]],
			];
		}


		if($event == 'type'){
			$result = [
				['none' => $app->trans('type_default')],
				['1' => $app->trans('Forged')],
				['2' => $app->trans('Cast')],
				['3' => $app->trans('Stamped')],
				['4' => $app->trans('Spokes')],
				['5' => $app->trans('National')],
			];
		}

		if($event == 'size_f'){
			$result = [
				['none' => '--'],
				['1' => '35–46 (S)'],
				['2' => '37–38 (S)'],
				['3' => '38–39 (S)'],
				['4' => '40–42 (XS)'],
				['5' => '42–44 (S)'],
				['6' => '44–46 (M)'],
				['7' => '46–48 (L)'],
				['8' => '48–50 (XL)'],
				['9' => '&gt; 50 (XXL)'],
			];
		}

		if($event == 'size_m'){
			$result = [
				['none' => '--'],
				['1' => '38–39 (S)'],
				['2' => '40–41 (S)'],
				['3' => '42–43 (S)'],
				['4' => '44–45 (S)'],
				['5' => '46–48 (M)'],
				['6' => '44–46 (S)'],
				['7' => '46–48 (M)'],
				['8' => '48–50 (L)'],
				['9' => '50–52 (XL)'],
				['10' => '52–54 (XXL)'],
				['11' => '&gt; 54 (XXXL)'],
			];
		}

		if($event == 'size_c'){
			$result = [
				['none' => '--'],
				['1' => '50-56 sm (0-2 month)'],
				['2' => '62-68 sm (2-6 month)'],
				['3' => '74-80 sm (7-12 month)'],
				['4' => '86-92 sm (1-2 years)'],
				['5' => '98-104 sm (2-4 years)'],
				['6' => '122-128 sm (6-8 years)'],
				['7' => '134-140 sm (8-10 years)'],
				['8' => '146-152 sm (10-12 years)'],
			];
		}

		return $result;
	}

	static public function collectNames($arrayResultOne)
	{
		$array = [
			'input' => '',
			'select' => '',
			'radio' => '',
			'MinMax' => '',
			'checkbox' => '',
			'textarea' => '',
			'file' => '',
		];
		$count = [
			'input' => '0',
			'select' => '0',
			'radio' => '0',
			'MinMax' => '0',
			'checkbox' => '0',
			'textarea' => '0',
			'file' => '0',
		];

		foreach ($arrayResultOne as $keyOn => $valueOn) {

			if(Arr::is_array($arrayResultOne[$keyOn]) and $keyOn !== 'mainOption'){

				foreach ($arrayResultOne[$keyOn] as $keyTwo => $valueTwo) {

					foreach ($valueTwo as $keyThree => $valueThree) {

						if(isset($valueThree['type']) and $valueThree['type'] == 'checkbox'){

							foreach ($valueThree['checkbox'] as $keyCheckbox => $valueCheckbox) {
								$array[$valueThree['type']][$valueCheckbox[key($valueCheckbox)]['name']]
									= ['name' => $valueCheckbox[key($valueCheckbox)]['name'], 'header' => $valueCheckbox[key($valueCheckbox)]['header']];
								$count[$valueThree['type']]++;
							}

						} else {
							$array[$valueThree['type']][$valueThree['name']] = [
								'name' => $valueThree['name'],
								'header' => $valueThree['header']
							];
							$count[$valueTwo[$keyThree]['type']]++;
						}
					}
				}
			}
		}
		//echo"<pre>";var_dump($array);
		return $array;
	}

}