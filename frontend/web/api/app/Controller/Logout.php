<?php

namespace BZR\Controller;

use BZR\Application;
use BZR\Translation\Keys;

class Logout extends Base
{
    static public function logout(Application $app)
    {
        $mapperRow = $app->spot()->mapper('BZR\Entity\UsersAuthMass');
        $result = $mapperRow->first(['mobile_token' => $app->getMobileToken(), 'auth_token' => $app->getAuthToken()]);

        $result->auth_token = '';
        $app->setAuthToken('');

        $resultUpdate = $mapperRow->update($result);

        if($resultUpdate){
            return $app->errorResponse(Keys::SUCCESS_LOGAUT, Keys::SUCCESS_LOGAUT, 1, Application::POST_OK);
        } else {
            return $app->errorResponse(Keys::ERROR_NOT_UPDATE, Keys::ERROR_NOT_UPDATE, 1, Application::NOT_FOUND_ERROR);
        }
    }
}