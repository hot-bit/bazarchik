<?php

namespace BZR\Controller;

use BZR\Application;
use BZR\ErrorChecking;
use BZR\Entity\MessageAdv as MessageAdvEntity;
use BZR\Exception;
use Symfony\Component\HttpFoundation\Request;
use BZR\Translation\Keys;
use \Silex\Application\TranslationTrait;

class MessageAdv
{
    public function postMessageAdv(Application $app, Request $request)
    {
        $array = [];

        //////////ERRORS CHECKED//////////////////////////////////////
        $arrayErrorChecking = [
            'adv_id'         => $request->get('adv_id'),
            'name'           => $request->get('name'),
            'text'           => $request->get('text', $request->getContent()),
        ];

        $errors = ErrorChecking::errorChecking($arrayErrorChecking, $app, $mapper = false, $corrector=false);

        if (count($errors) > 0) {
            return $app->errorResponseArray($errors, 1, Application::VALIDATION_ERROR);
        }
        //////////ERRORS CHECKED//////////////////////////////////////

        //проверяем существование объявления
        $mapperAdv = $app->spot()->mapper('BZR\Entity\Adv');
        $resultAdv = $mapperAdv->first(['id' => (int)($request->get('adv_id'))]);

        if($resultAdv){
            $mapperMessageAdv = $app->spot()->mapper('BZR\Entity\MessageAdv');
            /**
             * @var MessageAdvEntity $entity
             */
            $entity = $mapperMessageAdv->build([
                'adv_id'  => $request->get('adv_id'),
                'user_id' => $resultAdv->user,
                'name'    => $request->get('name'),
                'phone'   => $request->get('phone', ''),
                'email'   => $request->get('email'),
                'text'    => $request->get('text', $request->getContent()),
                'date'    => time(),
                'status'  => 1,
            ]);

            $lastInsertId = $mapperMessageAdv->insert($entity);

            if ($lastInsertId) {
                $this->sendNotification($app, $entity->user_id);
                return $app->generalResponse([['id' => $lastInsertId]], Application::POST_OK);
            } else {
                throw new Exception(var_export($entity->errors(), true));
            }
        }

        return $app->errorResponse(Keys::ERROR_INSERT, Keys::ERROR_INSERT, 1, Application::NOT_FOUND_ERROR);
    }

    /**
     * @param Application $app
     * @param int $ownerId
     */
    protected function sendNotification(Application $app, $ownerId)
    {
        try {
            $profileMapper = $app->spot()->mapper('BZR\Entity\Profile');
            $owner         = $profileMapper->get($ownerId);

            if ($owner) {
                $messageTranslationKey = $app->config(['notifications', 'message', 'newMessage']);
                $message = $app->trans($messageTranslationKey, [], 'messages', $owner->locale);

                $app->notification()->sendToUser($owner, $message, ['type' => 'newMessage']);
            }
        } catch (\Exception $e) {
            $app->log($e->getMessage() . ' ' . $e->getCode(), 'error');
        }
    }

    public function getMessageAdv(Application $app, Request $request)
    {
        $perPage      = (int)($request->get('per-page'));//число для показов
        $pageCorrect  = (int)($request->get('page'));//страница
        $withdrawFrom = ($pageCorrect-1) * $perPage;
        $inspection = true;
        $array = [];
        $i = 0;
        $headers = [];

        //проверяем существование объявления
        $mapperMessageAdv = $app->spot()->mapper('BZR\Entity\MessageAdv');
        $getCurrentProfile = $app->getCurrentProfile();
        $enityMessageAdv = $mapperMessageAdv->where(['user_id' => $getCurrentProfile->id]);

        //ПОСТРАНИЧНАЯ НАВИГАЦИЯ
        $pageCount = 0;

        if ($perPage > 0) {
            $pageCount = $enityMessageAdv->count() / (int)($perPage);
        }

        $pageCount = ceil($pageCount);

        if(empty($pageCorrect) or empty($perPage) or $withdrawFrom < 0 or $perPage < 0 or $pageCorrect > $pageCount){

            $withdrawFrom = 0;
            $perPage      = 10;
            $pageCorrect  = 1;

            $pageCount = $enityMessageAdv->count() / $perPage;
            $pageCount = ceil($pageCount);
            $inspection = false;

            if(!$request->get('page') or !$request->get('per-page')){
                $inspection = true;
            }
        }
        //ПОСТРАНИЧНАЯ НАВИГАЦИЯ

        if($enityMessageAdv->count() > 0 && $inspection){

            $sql = "
            SELECT ma.*, a.title AS titleAdv FROM message_adv ma 
            LEFT JOIN adv a ON(ma.adv_id = a.id)
            WHERE ma.user_id = '".$getCurrentProfile->id."'
            ORDER BY date DESC
            LIMIT $withdrawFrom, $perPage
        ";
            $sqlQuery = $mapperMessageAdv->query($sql);

            foreach($sqlQuery as $keyAdv => $valueAdv) {

                $dateA = date('Ymd', $valueAdv->date);
                $dateB = date('Ymd', time());
                $date = date('d.m.Y H:i',$valueAdv->date);

                if($dateA == $dateB){
                    $date = date('H:i',$valueAdv->date);
                    $date = $app->trans('to_day_date') .' '. $date;
                }

                $array[$i] = [
                    'id'       => $valueAdv->id,
                    'adv_id'   => $valueAdv->adv_id,
                    'name'     => $valueAdv->name,
                    'phone'    => $valueAdv->phone,
                    'email'    => $valueAdv->email,
                    'text'     => $valueAdv->text,
                    'titleAdv' => $valueAdv->titleAdv,
                    'date'     => $date,
                    'status'   => $valueAdv->status,
                ];
                $i++;
            }

            //ПОСТРАНИЧНАЯ НАВИГАЦИЯ
            $headers = [
                'X-Pagination-Current-Page' => $pageCorrect,
                'X-Pagination-Page-Count'   => $pageCount,
                'X-Pagination-Per-Page'     => $perPage,
                'X-Pagination-Total-Count'  => $enityMessageAdv->count()
            ];
            //ПОСТРАНИЧНАЯ НАВИГАЦИЯ
        }
        return $app->generalResponse($array, Application::POST_OK, $headers);
    }

    public function deleteMessageAdv(Application $app, Request $request)
    {
        $mapperMessageAdv = $app->spot()->mapper('BZR\Entity\MessageAdv');
        $inspection = false;

        //проверяем существование объявления
        $resultMessageAdv = $mapperMessageAdv->first(['id' => (int)($request->get('id'))]);

        if($resultMessageAdv) {

            //удаляем сообщение
            $inspection = $mapperMessageAdv->delete(['id' => (int)($request->get('id'))]);

            if($inspection){
                return $app->generalResponse([['id' => $inspection]], Application::POST_OK);
            }
        }

        return $app->generalResponse([['id' => $inspection]], Application::POST_OK);
    }

    public function oldPostAdv(Application $app, Request $request)
    {
        $mapperMessageAdv = $app->spot()->mapper('BZR\Entity\MessageAdv');
        $inspection = false;
        $getCurrentProfile = $app->getCurrentProfile();

        //проверяем существование сообщения
        $enityMessageAdv = $mapperMessageAdv->first(['id' => (int)($request->get('id')), 'user_id' => $getCurrentProfile->id]);

        //запрос на колличество не прочитанных сообщений
        $mapperProfile = $app->spot()->mapper('BZR\Entity\Profile');
        $enityProfile = $mapperProfile->where(['id' => $getCurrentProfile->id])
            ->with(['newMessage'])
            ->execute();

        if($enityMessageAdv) {
            //ставим метку как прочитанное
            $enityMessageAdv->status = 2;
            $inspection = $mapperMessageAdv->update($enityMessageAdv);

            if($inspection){
                $mapperProfile = $app->spot()->mapper('BZR\Entity\Profile');
                $enityProfile = $mapperProfile->where(['id' => $getCurrentProfile->id])
                    ->with(['newMessage'])
                    ->execute();

                return $app->generalResponse(['id' => (int)($request->get('id')), 'newMessage' => count($enityProfile[0]->newMessage->toArray())], Application::POST_OK);
            }
        }

        return $app->generalResponse(['id' => (int)($request->get('id')), 'newMessage' => count($enityProfile[0]->newMessage->toArray())], Application::POST_OK);
    }

}