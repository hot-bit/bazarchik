<?php

namespace BZR\Controller;

use BZR\Application;
use Symfony\Component\HttpFoundation\Request;

class CheckPasswordRecoveryCode extends Base
{
    public function checkPasswordRecoveryCode(Application $app, Request $request)
    {
    	$mapperInitPasswordRecovery = $app->spot()->mapper('BZR\Entity\InitPasswordRecovery');
    	$array = [];

        $result = $mapperInitPasswordRecovery->first(['code' => $request->get('code'), "time_code >=" => time()]);

        if($result){
        	$array = [$result->id];
        }

        return $app->generalResponse($array, Application::PUT_GET_OK);
    }
}