<?php

namespace BZR\Controller;

use BZR\Application;
use BZR\SimpleImage;
use Symfony\Component\HttpFoundation\Request;

class UploadImages //extends Base
{
    public static function getMarkedImageUrl (Application $app, $dir, $filename)
    {
        $path   = $app->config('rootUploadImages') . DIRECTORY_SEPARATOR . $dir . DIRECTORY_SEPARATOR . $filename;
        $wmPath = $app->config('rootUploadImagesWm') . DIRECTORY_SEPARATOR . $dir . DIRECTORY_SEPARATOR . $filename;

        $urlPicturesWm = $app->config('urlPicturesWm');

        if (! file_exists($wmPath)) {
            if (file_exists($path)) {

                $targetDirectory = dirname($wmPath);

                if (!file_exists($targetDirectory)) {
                    @mkdir($targetDirectory, 0757, true);
                    chmod($targetDirectory, 0757);
                }

                copy($path, $wmPath);
            }
        }

        return $urlPicturesWm . $dir . DIRECTORY_SEPARATOR . $filename;
    }

	public static function uploadImages($app, $request, $idAd)
	{
		$files = $request->files->get('photo1');
		$rootUploadImages = $app['config']['rootUploadImages'].$app['config']['toDay'].'/';

		if(!is_dir($rootUploadImages)){
			@mkdir($rootUploadImages, true);
			chmod($rootUploadImages, 0757);
		}

		if(isset($files)){

			for ($i=1; $i < 10; $i++) {

				$file = $request->files->get("photo$i");

				if(isset($file)){
					$name = UploadImages::giveNameGenerate($app);
					$arrayIm = UploadImages::copyImageAndWaterMark($app, $rootUploadImages, $file, $name, $i);

					if(isset($arrayIm['l'])){
						UploadImages::insertImageBd($app, $app['config']['toDay'], $name, $idAd, $i, $arrayIm);
					}
				}
			}
		}
	}

	public static function copyImageAndWaterMark($app, $rootUploadImages, $file, $name)
	{
		$image = new SimpleImage();

		$getpathName = $file->getpathName();
		$image_info = $image->load($getpathName);
		$arrayIm = ['l' => '0', 'm' => '0', 's' => '0'];
		//вертикальное высота
		//900
		//525
		//145
		if($image_info){

			$image->resizeToWidth($app['config']['imagesPx']["L"]);
			$copyStatus = $image->save($rootUploadImages.'L_'.$name);

			if($copyStatus){

				// Загрузка штампа и фото, для которого применяется водяной знак
				$stamp = imagecreatefrompng($app['config']['stampImageWaterMark']);
				$im = imagecreatefromjpeg($rootUploadImages.'L_'.$name);

				// Установка полей для штампа и получение высоты/ширины штампа
				$marge_right = '10';
				$marge_bottom = '10';

				$sx = imagesx($stamp);
				$sy = imagesy($stamp);

				// Копирование изображения штампа на фотографию с помощью смещения края
				// и ширины фотографии для расчета позиционирования штампа.
				$copyStatus = imagecopy($im, $stamp, imagesx($im) - $sx - $marge_right, imagesy($im) - $sy - $marge_bottom, 0, 0, imagesx($stamp), imagesy($stamp));

				//освобождение памяти
				Imagejpeg($im,$rootUploadImages.'L_'.$name);
				ImageDestroy($im);
				$image->load($rootUploadImages.'L_'.$name);
				$arrayIm['l'] = '1';

				//M_
				$image->resizeToWidth($app['config']['imagesPx']["M"]);
				$copyStatus = $image->save($rootUploadImages.'M_'.$name);
				if($copyStatus) $arrayIm['m'] = '1';
				//S_
				$image->resizeToWidth($app['config']['imagesPx']["S"]);
				$copyStatus = $image->save($rootUploadImages.'S_'.$name);
				if($copyStatus) $arrayIm['s'] = '1';
				//NO PRIFIX
				$image->resizeToWidth($app['config']['imagesPx']["realName"]);
				$copyStatus = $image->save($rootUploadImages.$name);
			}
		}

		return $arrayIm;
	}

	public static function giveNameGenerate($app)
	{
		$name = $app->Random(10);
        $name = trim($name);
		$name = $name.'.jpg';
		$mapperRow = $app->spot()->mapper('BZR\Entity\AdvImages');
		$result = $mapperRow->where(['dir' => $app['config']['toDay'], 'name' => $name])->count();

		if($result > 0){
			UploadImages::giveNameGenerate($app);
		}

		return $name;
	}

	public static function insertImageBd($app, $toDay, $name, $idAd, $i, $arrayIm)
	{
		$i = ($i == 1) ? '1' : '0';
		$mapperRow = $app->spot()->mapper('BZR\Entity\AdvImages');
		$entity = $mapperRow->build([
			'adv_id' => $idAd,
			'name'   => $name,
			'dir'    => $toDay,
			'main'   => $i,
			'm'      => $arrayIm['m'],
			'l'      => $arrayIm['l'],
			's'      => $arrayIm['s'],
			'craftMobile'      => 1,
		]);
		$mapperRow->insert($entity);
	}
}