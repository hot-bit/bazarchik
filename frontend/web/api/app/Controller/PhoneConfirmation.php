<?php

namespace BZR\Controller;

use BZR\Application;
use BZR\Entity\Response\User;
use BZR\Entity\UsersPushToken;
use BZR\Exception;
use BZR\Notification\Sender;
use BZR\Translation\Keys;
use Spot\Entity;
use Symfony\Component\HttpFoundation\Request;

class PhoneConfirmation extends Base
{
    const CONFIRMATION_LIMIT = 100;

    /**
     * @param Application $app
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\JsonResponse
     */
    public function request(Application $app, Request $request)
    {
        $profile = $app->getCurrentProfile();

        if ($profile->count_sms >= self::CONFIRMATION_LIMIT) {
            return $app->errorResponse(Keys::ERROR_CONFIRMATION_SMS_LIMIT, Keys::ERROR_CONFIRMATION_SMS_LIMIT, 1, Application::FORBIDDEN_ERROR);
        }

        $phone = $profile->phone;

        if (!$phone) {
            return $app->errorResponse(Keys::ERROR_CONFIRMATION_NO_PHONE, Keys::ERROR_CONFIRMATION_NO_PHONE, 1, Application::FORBIDDEN_ERROR);
        }

        $code       = mt_rand(1000, 9999);
        $messageKey = $app->config(['sms', 'confirmationMessage']);
        $message    = $app->trans($messageKey);
        $message    = str_replace("%code%", $code, $message);

        $profile->sms_code  = $code;
        $profile->count_sms = intval($profile->count_sms) + 1;

        $app->sms()->send($phone, $message);

        $mapper = $app->spot()->mapper('BZR\Entity\Profile');

        $mapper->save($profile);

        $responseEntity = new User($profile);
        return $app->generalResponse($responseEntity->jsonSerialize($app), Application::PUT_GET_OK);
    }

    /**
     * @param Application $app
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\JsonResponse
     */
    public function confirm(Application $app, Request $request)
    {
        $profile = $app->getCurrentProfile();

        $code = $request->get('code');

        if (!$code) {
            return $app->errorResponse(Keys::ERROR_ID_NO, Keys::ERROR_ID_NO, 1, Application::NOT_FOUND_ERROR);
        }

        if ($code == $profile->sms_code) {
            $profile->success_phone = 1;
            $profile->send_sms = 1;
            $profile->sms_code = 0;

            $mapper = $app->spot()->mapper('BZR\Entity\Profile');
            $mapper->save($profile);

            $responseEntity = new User($profile);
            return $app->generalResponse($responseEntity->jsonSerialize($app), Application::PUT_GET_OK);
        } else {
            return $app->errorResponse(Keys::ERROR_CONFIRMATION_WRONG_CODE, Keys::ERROR_CONFIRMATION_WRONG_CODE, 1, Application::FORBIDDEN_ERROR);
        }
    }
}
