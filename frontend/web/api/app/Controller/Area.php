<?php

namespace BZR\Controller;

use BZR\Application;
use Symfony\Component\HttpFoundation\Request;
use BZR\Translation\Keys;

class Area //extends Base
{
    public function getArea(Application $app, Request $request)
    {
    	$mapperRow = $app->spot()->mapper('BZR\Entity\Area');
    	$city = (int)($request->get('city'));
        $array = [];
        $i = 0;

    	if(!empty($city)){
			$results = $mapperRow->where(['city' => $city]);	
    	} else {
    		$results = $mapperRow->all();
    	}

        if($results->count() > 0){
            foreach($results as $result) {
                $array[$i] = [
                    'id' => $result->id,
                    'name_ru'=> $result->name_ru,
                    'name_en'=> $result->name_en,
                    'name_gr'=> $result->name_gr,
                    'city'=> $result->city,
                    'lat'=> $result->lat,
                    'lng'=> $result->lng,
                ];
                $i++;
            }
        } else {
            return $app->generalResponse($array, Application::PUT_GET_OK);
        }
        
        return $app->generalResponse($array, Application::PUT_GET_OK);      

    }

    public static function getOneArea($app, $id)
    {
        $mapperRow = $app->spot()->mapper('BZR\Entity\Area');
        $results = $mapperRow->first(['id' => $id]);
        $name = '';

        if(isset($results->name_en)){
            $name = $results->name_en;
        }
        return $name;
    }
}