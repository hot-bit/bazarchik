<?php

namespace BZR\Controller;

use BZR\Application;
use BZR\SimpleImage;
use Symfony\Component\HttpFoundation\Request;
use BZR\Entity\Response\User;

class AvatarDownload
{
    public static function uploadAvatar(Application $app, Request $request)
    {
        $getCurrentProfile = $app->getCurrentProfile();

        if ($getCurrentProfile) {

            $file = $request->files->get('avatar');
            $mapperProfile = $app->spot()->mapper('BZR\Entity\Profile');

            $fileName = 'user_' . $getCurrentProfile->id . '.jpg';
            $directory = $app['config']['rootUploadAvatar'] . '/' . $getCurrentProfile->id . '/';
            $result = [false];

            if (!is_dir($directory)) {
                @mkdir($directory, 0757, true);
                //chmod($directory, 0757);
            }

            if (isset($file)) {
                AvatarDownload::copyImage($app, $fileName, $directory, $file);
                $result = AvatarDownload::insertAvatarBd($app, $fileName, $getCurrentProfile->id);

                if ($result == true) {

                    //$result = $mapperProfile->first(['id' => $getCurrentProfile->id]);
                    $enityProfile = $mapperProfile->where(['id' => $getCurrentProfile->id])
                        ->with(['newMessage'])
                        ->execute();
                    //дорабатываем аватар - URL
                    $mapperProfile->urlPicturesAvatar($app, $enityProfile);

                    $result = new User($enityProfile[0]);
                    $result = $result->jsonSerialize($app);
                }
            }
        }

        return $app->generalResponse($result, Application::POST_OK);
    }

    public static function copyImage($app, $fileName, $directory, $file)
    {
        $image = new SimpleImage();
        $pathName = $file->getPathname();

        if ($pathName) {
            $image_info = $image->load($pathName);
            //$name = $name.'.jpg';

            if ($image_info) {
                $image->save($directory . $fileName);

                if ($image->getWidth() < $image->getHeight()) {
                    $image->resizeToWidth($app['config']['imagesPx']["avatar"]);
                } else {
                    $image->resizeToHeight($app['config']['imagesPx']["avatar"]);
                }

                $image->save($directory . 'small_' . $fileName);

            }
        }
    }

    public static function insertAvatarBd($app, $fileName, $idProfil)
    {
        $mapperProfile = $app->spot()->mapper('BZR\Entity\Profile');
        //$name = $name.'.jpg';
        $entity = $mapperProfile->first(['id' => $idProfil]);
        $result = false;

        if ($entity) {
            $entity->avatar = $fileName;
            
            $mapperProfile->update($entity);
            $result = true;
        }

        return $result;
    }
}