<?php

namespace BZR\Controller;

use BZR\Application;
use BZR\Arr;
use BZR\Entity\Response\Adv;

use Spot\Entity\Collection;
use Spot\Mapper;
use Symfony\Component\HttpFoundation\Request;

class Ads extends Base
{
    public function getAds(Application $app, Request $request)
    {
        //получаем весь обработанные параметры
        $arrayRequestParams = $this->requestParams($app, $request);
        //echo "<pre>";var_dump($arrayRequestParams);

        //получаем ответ от БД с данными
        $arrayResult = $this->searchAdv($app, $arrayRequestParams);
        //echo "<pre>";var_dump($arrayResult);

        $resultOutput = $this->resultOutput($app, $arrayResult, $request);

        $categoryId = (int)($request->get('category_id'));

        if ($categoryId) {
            $profile = $app->getCurrentProfile();

            if ($profile) {
                $this->markRootCategoryVisited($app, $categoryId, $profile->id);
            }

        }

        return $resultOutput;
    }

    private function markRootCategoryVisited(Application $app, $categoryId, $userId)
    {
        /**
         * @var \BZR\Entity\Mapper\Categories $categoriesMapper
         */
        $categoriesMapper = $app->spot()->mapper('BZR\Entity\Categories');

        /**
         * @var \BZR\Entity\Mapper\AdsLastDateUser $categoryVisitsMapper
         */
        $categoryVisitsMapper = $app->spot()->mapper('BZR\Entity\AdsLastDateUser');

        $rootParentCategory = $categoriesMapper->getParentCategory($categoryId);

        $rootParentCategoryId = $rootParentCategory ? $rootParentCategory->id : $categoryId;
        $categoryVisitsMapper->visit($rootParentCategoryId, $userId);

    }

    public function getAdsCount(Application $app, Request $request)
    {
        $arrayRequestParams = $this->requestParams($app, $request);
        $arrayResult = $this->searchAdv($app, $arrayRequestParams);
        $count = Arr::get($arrayResult, 'entityCount', 0);

        return $app->generalResponse(['count' => $count]);
    }

    static public function resultOutput($app, $arrayResult, $request)
    {
        //Этот метод соритирует по правильному порядку(id) из массива arrayId
        //СТАТИЧЕСКИЙ потому-что Favorites работает с ним же.
        $entityAdv = $arrayResult['entityAdv'];
        $arrayId = $arrayResult['arrayId'];
        $entityCount = $arrayResult['entityCount'];
        $perPage = (int)($request->get('per-page'));//число для показов
        $pageCorrect = (int)($request->get('page'));//страница
        $PageCount = 0;
        $i = 0;
        $array = [];
        $headers = [];

        if($entityCount > 0){

            foreach($arrayId as $keyArrayId => $valueArrayId) {
                foreach($entityAdv as $keyAdv => $valueAdv) {

                    if($valueArrayId == $valueAdv->id){
                        $result = new Adv($entityAdv[$keyAdv], true);
                        $array[$i] = $result->jsonSerialize($app, $request);
                        $i++;
                    }
                }
            }

            //ПОСТРАНИЧНАЯ НАВИГАЦИЯ
            if(empty($pageCorrect) or empty($perPage) or $pageCorrect > $PageCount){
                $perPage = 10;
                $pageCorrect = 1;
            }

            if($perPage == 0){
                $perPage = 1;
            }

            $PageCount = $entityCount / $perPage;
            $PageCount = ceil($PageCount);

            $headers = [
                'X-Pagination-Current-Page' => $pageCorrect,
                'X-Pagination-Page-Count' => $PageCount,
                'X-Pagination-Per-Page' => $perPage,
                'X-Pagination-Total-Count' => $entityCount
            ];
            //ПОСТРАНИЧНАЯ НАВИГАЦИЯ

            return $app->generalResponse($array, Application::PUT_GET_OK, $headers);
        } else {
            return $app->generalResponse([], Application::PUT_GET_OK, $headers);
        }
    }

    public function getVip($app, $arrayRequestParams, $limit, $offset)
    {
        /**
         * @var Mapper $mapperRow
         */
        $mapperRow = $app->spot()->mapper('BZR\Entity\Adv');

        $categoryQuery = $arrayRequestParams['categoryQuery'];
        $perSortBd = $arrayRequestParams['perSortBd'];
        $leftJoin = $arrayRequestParams['leftJoin'];
        $sqlWhere = $arrayRequestParams['sqlWhere'];
        $like = $arrayRequestParams['like'];

        $vipCondition = "and a.vip = 1 and a.vip_start > " . (time()-3600*24*7);

        $innerJoinWhereSql = $arrayRequestParams['innerJoinWhereSql'];

        $innerJoin = '';
        $sql = '';
        $where = '';
        $groupBy = 'GROUP BY a.id';

        if(isset($arrayRequestParams['innerJoinWhereSql']['innerJoin']) and isset($arrayRequestParams['innerJoinWhereSql']['sql']) and isset($arrayRequestParams['innerJoinWhereSql']['where'])){
            $where = $innerJoinWhereSql['where'];
            $sql = $innerJoinWhereSql['sql'];
            $innerJoin = $innerJoinWhereSql['innerJoin'];
        }

        $sqlQuery = "
            SELECT a.id FROM adv a
            $leftJoin
            $innerJoin

            WHERE a.status = 2 $vipCondition
            
            $like
            $sqlWhere
            
            $categoryQuery
            $sql
            $where
            
            $groupBy

            $perSortBd

            LIMIT $offset, $limit
        ";

        $arrayIds = $mapperRow->query($sqlQuery);

        $sqlCount = "
            SELECT COUNT(id) AS countsId FROM (
                SELECT a.id AS id FROM adv a
                $leftJoin
                $innerJoin
    
                WHERE a.status = 2 $vipCondition
    
                $like
                $sqlWhere
                
                $categoryQuery
                $sql
                $where
                
                GROUP BY a.id
            ) AS temp
        ";

        $arrayIdsCount = $mapperRow->query($sqlCount);

        $arrayId = [];
        foreach ($arrayIds as $key => $value) {
            array_push($arrayId, $value->id);
        }

        /**
         * @var Collection $entityAdv
         */
        $entityAdv = $mapperRow->where(['id' => $arrayId])
            ->with(['photos','ad_params','counter'])
            ->execute();

        return [$arrayId, $entityAdv, $arrayIdsCount[0]->countsId];
    }

    public function getPremium($app, $arrayRequestParams, $limit, $offset)
    {
        /**
         * @var Mapper $mapperRow
         */
        $mapperRow = $app->spot()->mapper('BZR\Entity\Adv');

        $categoryQuery = $arrayRequestParams['categoryQuery'];
        $perSortBd = $arrayRequestParams['perSortBd'];
        $leftJoin = $arrayRequestParams['leftJoin'];
        $sqlWhere = $arrayRequestParams['sqlWhere'];
        $like = $arrayRequestParams['like'];

        $premiumCondition = "and (a.premium = 1 and prem_start > " . (time()-3600*24*7) . ")";
        $premiumCondition .= " and (a.vip != 1 or vip_start < " . (time()-3600*24*7) . ")";

        $innerJoinWhereSql = $arrayRequestParams['innerJoinWhereSql'];

        $innerJoin = '';
        $sql = '';
        $where = '';
        $groupBy = 'GROUP BY a.id';

        if(isset($arrayRequestParams['innerJoinWhereSql']['innerJoin']) and isset($arrayRequestParams['innerJoinWhereSql']['sql']) and isset($arrayRequestParams['innerJoinWhereSql']['where'])){
            $where = $innerJoinWhereSql['where'];
            $sql = $innerJoinWhereSql['sql'];
            $innerJoin = $innerJoinWhereSql['innerJoin'];
        }

        $sqlQuery = "
            SELECT a.id FROM adv a
            $leftJoin
            $innerJoin

            WHERE a.status = 2 $premiumCondition
            
            $like
            $sqlWhere
            
            $categoryQuery
            $sql
            $where
            
            $groupBy

            $perSortBd

            LIMIT $offset, $limit
        ";

        $arrayIds = $mapperRow->query($sqlQuery);

        $sqlCount = "
            SELECT COUNT(id) AS countsId FROM (
                SELECT a.id AS id FROM adv a
                $leftJoin
                $innerJoin
    
                WHERE a.status = 2 $premiumCondition
    
                $like
                $sqlWhere
                
                $categoryQuery
                $sql
                $where
                
                GROUP BY a.id
            ) AS temp
        ";

        $arrayIdsCount = $mapperRow->query($sqlCount);

        $arrayId = [];
        foreach ($arrayIds as $key => $value) {
            array_push($arrayId, $value->id);
        }

        /**
         * @var Collection $entityAdv
         */
        $entityAdv = $mapperRow->where(['id' => $arrayId])
            ->with(['photos','ad_params','counter'])
            ->execute();

        return [$arrayId, $entityAdv, $arrayIdsCount[0]->countsId];
    }

    public function getCommon($app, $arrayRequestParams, $limit, $offset)
    {
        /**
         * @var Mapper $mapperRow
         */
        $mapperRow = $app->spot()->mapper('BZR\Entity\Adv');

        $categoryQuery = $arrayRequestParams['categoryQuery'];
        $perSortBd = $arrayRequestParams['perSortBd'];
        $leftJoin = $arrayRequestParams['leftJoin'];
        $sqlWhere = $arrayRequestParams['sqlWhere'];
        $like = $arrayRequestParams['like'];

        $commonCondition = ("and (a.premium != 1 or prem_start < " . (time()-3600*24*7) . ")");
        $commonCondition .= (" and (a.vip != 1 or vip_start < " . (time()-3600*24*7) . ")");

        $innerJoinWhereSql = $arrayRequestParams['innerJoinWhereSql'];

        $innerJoin = '';
        $sql = '';
        $where = '';
        $groupBy = 'GROUP BY a.id';

        if(isset($arrayRequestParams['innerJoinWhereSql']['innerJoin']) and isset($arrayRequestParams['innerJoinWhereSql']['sql']) and isset($arrayRequestParams['innerJoinWhereSql']['where'])){
            $where = $innerJoinWhereSql['where'];
            $sql = $innerJoinWhereSql['sql'];
            $innerJoin = $innerJoinWhereSql['innerJoin'];
        }

        $sqlQuery = "
            SELECT a.id FROM adv a
            $leftJoin
            $innerJoin

            WHERE a.status = 2 $commonCondition
            
            $like
            $sqlWhere
            
            $categoryQuery
            $sql
            $where
            
            $groupBy

            $perSortBd

            LIMIT $offset, $limit
        ";

        $arrayIds = $mapperRow->query($sqlQuery);

        $sqlCount = "
            SELECT COUNT(id) AS countsId FROM (
                SELECT a.id AS id FROM adv a
                $leftJoin
                $innerJoin
    
                WHERE a.status = 2 $commonCondition
    
                $like
                $sqlWhere
                
                $categoryQuery
                $sql
                $where
                
                GROUP BY a.id
            ) AS temp
        ";

        $arrayIdsCount = $mapperRow->query($sqlCount);

        $arrayId = [];
        foreach ($arrayIds as $key => $value) {
            array_push($arrayId, $value->id);
        }

        /**
         * @var Collection $entityAdv
         */
        $entityAdv = $mapperRow->where(['id' => $arrayId])
            ->with(['photos','ad_params','counter'])
            ->execute();

        return [$arrayId, $entityAdv, $arrayIdsCount[0]->countsId];
    }


    public function premiumSelection($app, $arrayRequestParams)
    {
        $limit = $arrayRequestParams['perPage'];
        $pageSize = $limit;
        $pageNumber = max(0, $arrayRequestParams['pageNumber'] - 1);
        $itemsBeforeSelection = 0;

        $vipIdList = [];
        $premiumIdList = [];
        $commonIdList = [];

        $vipCollection = [];
        $premiumCollection = [];
        $commonCollection = [];

        $vipCount = 0;
        $premiumCount = 0;
        $commonCount = 0;

        /*$log = "";
        $log .= var_export($app['request']->query->all(), true) . "\n";
        $log .= var_export($arrayRequestParams, true) . "\n";
        $log .= "pageSize: $pageSize\n";
        $log .= "limit: $limit\n";
        $log .= "pageNumber: $pageNumber\n";
        $log .= "itemsBeforeSelection: $itemsBeforeSelection\n";*/

        if ($limit > 0) {

            $offset = ($pageNumber * $pageSize) - $itemsBeforeSelection;
            $vipLimit = $pageSize;
            list($vipIdList, $vipCollection, $vipCount) = $this->getVip($app, $arrayRequestParams, $vipLimit, $offset);

            $collectionCount = count($vipCollection);

            if ($collectionCount < $vipLimit) {
                $itemsBeforeSelection += $vipCount - $collectionCount;
            } else {
                $itemsBeforeSelection += $offset;
            }

            $limit = $limit - $collectionCount;

            /*$log .= "vip\n";
            $log .= "offset: $offset\n";
            $log .= "limit: $limit\n";
            $log .= "collectionCount: $collectionCount\n";
            $log .= "vipCount: $vipCount\n";
            $log .= "itemsBeforeSelection: $itemsBeforeSelection\n";*/
        }

        $premiumLimit = 3;

        if ($limit > 0) {
            $offset = ($pageNumber - floor($itemsBeforeSelection / $pageSize)) * $premiumLimit;
            $premiumLimit = min($premiumLimit, $limit);

            list($premiumIdList, $premiumCollection, $premiumCount) = $this->getPremium($app, $arrayRequestParams, $premiumLimit, $offset); // *, 3, 5

            $collectionCount = count($premiumCollection);

            if ($collectionCount < $premiumLimit) {
                $itemsBeforeSelection += $premiumCount - $collectionCount;
            } else {
                $itemsBeforeSelection += $offset/* + count($premiumCollection)*/; //10
            }

            $limit = $limit - $collectionCount; //6 - 3 = 3

            /*$log .= "premium\n";
            $log .= "offset: $offset\n";
            $log .= "limit: $limit\n";
            $log .= "collectionCount: $collectionCount\n";
            $log .= "premiumCount: $premiumCount\n";
            $log .= "itemsBeforeSelection: $itemsBeforeSelection\n";*/
        }

        if ($limit > 0) {
            $pageOffset = floor($itemsBeforeSelection / $pageSize);// 2 / 10 = 0
            $itemsOffset = $itemsBeforeSelection - ($pageOffset * $pageSize);// 0

            $offset = (($pageNumber - $pageOffset) * $pageSize) - $itemsOffset;// ((1 - 1) * 10) - 0 = 0
            list($commonIdList, $commonCollection, $commonCount) = $this->getCommon($app, $arrayRequestParams, $limit, $offset);

            $collectionCount = count($commonCollection);

           /* $log .= "common\n";
            $log .= "offset: $offset\n";
            $log .= "limit: $limit\n";
            $log .= "collectionCount: $collectionCount\n";
            $log .= "commonCount: $commonCount\n";
            $log .= "itemsBeforeSelection: $itemsBeforeSelection\n";*/
        }



        $collections = [$vipCollection, $premiumCollection, $commonCollection];

        $resultCollection = false;

        foreach ($collections as $collection) {
            if (count($collection) === 0) {
                continue;
            }

            if ($resultCollection) {
                $resultCollection->merge($collection, false);
            } else {
                $resultCollection = $collection;
            }
        }
        //$cnt = count($resultCollection);
        //$log .= "resultCollection: $cnt\n";


        $idList = array_merge($vipIdList, $premiumIdList, $commonIdList);

        $count = $vipCount + $premiumCount + $commonCount;

        /*$log .= "idList: " . implode(", ", $idList) . "\n";
        $log .= "count: " . $count . "\n";

        file_put_contents(__DIR__ . '/../../log/search3.log', $log . "\n\n", FILE_APPEND);


        if (isset($_GET['test'])) {
            echo $log . "\n\n";
            var_dump($vipIdList, $premiumIdList, $commonIdList); die;
        }*/

        return ['arrayId' => $idList, 'entityAdv' => $resultCollection, 'entityCount' => $count];
    }

    public function searchAdv($app, $arrayRequestParams)
    {
        /**
         * @var Mapper $mapperRow
         */
        $mapperRow = $app->spot()->mapper('BZR\Entity\Adv');

        $categoryQuery = $arrayRequestParams['categoryQuery'];
        $vip = $arrayRequestParams['vip'];
        $premium = $arrayRequestParams['premium'];
        $perSortBd = $arrayRequestParams['perSortBd'];
        $withdrawFrom = $arrayRequestParams['withdrawFrom'];
        $perPage = $arrayRequestParams['perPage'];
        $leftJoin = $arrayRequestParams['leftJoin'];
        $sqlWhere = $arrayRequestParams['sqlWhere'];
        $like = $arrayRequestParams['like'];
        $premiumFirst = $arrayRequestParams['premiumFirst'];
        $pageNumber = $arrayRequestParams['pageNumber'];

        if ($premiumFirst) {
            return $this->premiumSelection($app, $arrayRequestParams);
        }

        $premiumAdvListCount = 0;
        $premiumAdvList = [];
        $premiumPerPageLimit = 3;

        if ($premiumFirst) {

            $premiumRequestParams = $arrayRequestParams;
            $premiumRequestParams['premiumFirst'] = false;
            $premiumRequestParams['premium'] = 1;
            $premiumRequestParams['perPage'] = $premiumPerPageLimit;
            $premiumRequestParams['withdrawFrom'] = max(0, ($pageNumber - 1)) * $premiumPerPageLimit;
            $premiumRequestParams['perSortBd'] = " ORDER BY prem_start DESC ";

            $tempPremiumAdvList = $this->searchAdv($app, $premiumRequestParams);
            $premiumAdvList = $tempPremiumAdvList['entityAdv'];
            $premiumAdvListCount = $tempPremiumAdvList['entityCount'];
        }

        //$limit = $perPage - count($premiumAdvList);

        if (count($premiumAdvList) != $premiumPerPageLimit && count($premiumAdvList) > 0) { //последняя страница на которой есть премиум объявления
            $limit  = $perPage - count($premiumAdvList);
            $offset = ($perPage - $premiumPerPageLimit) * max(0, ($pageNumber - 1));
        } elseif (count($premiumAdvList) == $premiumPerPageLimit) { //одна из страниц с полным набором премиум объявлений
            $limit  = $perPage - $premiumPerPageLimit;
            $offset = ($perPage - $premiumPerPageLimit) * max(0, ($pageNumber - 1));
        } else { //страница без премиум объявлений
            $limit  = $perPage;
            $offset = ($perPage) * max(0, ($pageNumber - 1)) - $premiumAdvListCount;
        }

        $innerJoinWhereSql = $arrayRequestParams['innerJoinWhereSql'];

        $innerJoin = '';
        $sql = '';
        $where = '';
        $groupBy = 'GROUP BY a.id';

        if(isset($arrayRequestParams['innerJoinWhereSql']['innerJoin']) and isset($arrayRequestParams['innerJoinWhereSql']['sql']) and isset($arrayRequestParams['innerJoinWhereSql']['where'])){
            $where = $innerJoinWhereSql['where'];
            $sql = $innerJoinWhereSql['sql'];
            $innerJoin = $innerJoinWhereSql['innerJoin'];
            //$groupBy = 'GROUP BY a.id';
        }

        $vipCondition = $vip ? ("and a.vip = 1 and a.vip_start > " . (time()-3600*24*7)) : "";
        $premiumCondition = $premium ? ("and a.premium = 1 and prem_start > " . (time()-3600*24*7)) : "";

        if ($premiumFirst) {
            $premiumCondition = ("and (a.premium <> 1 and prem_start < " . (time()-3600*24*7) . ")");
        }

        $sqlQuery = "
            SELECT a.id FROM adv a
            $leftJoin
            $innerJoin

            WHERE a.status = 2 $vipCondition $premiumCondition
            
            $like
            $sqlWhere
            
            $categoryQuery
            $sql
            $where
            
            $groupBy

            $perSortBd

            LIMIT $offset, $limit
        ";
        //file_put_contents(__DIR__ . '/../../log/search2.log', var_export($arrayRequestParams, true) . "\n\n", FILE_APPEND);
        //file_put_contents(__DIR__ . '/../../log/search2.log', $sqlQuery . "\n\n", FILE_APPEND);

        //echo $sqlQuery;
        $arrayIds = $mapperRow->query($sqlQuery);
        //echo "<pre>";var_dump($arrayIds);

        $sqlCount = "
            SELECT COUNT(id) AS countsId FROM (
                SELECT a.id AS id FROM adv a
                $leftJoin
                $innerJoin
    
                WHERE a.status = 2 $vipCondition $premiumCondition
    
                $like
                $sqlWhere
                
                $categoryQuery
                $sql
                $where
                
                GROUP BY a.id
            ) AS temp
        ";

        $arrayIdsCount = $mapperRow->query($sqlCount);
        //echo "<pre>$sqlCount";var_dump($arrayIdsCount);

        $arrayId = [];
        foreach ($arrayIds as $key => $value) {
            array_push($arrayId, $value->id);
        }
        //echo"<pre>";var_dump($arrayId);

        /**
         * @var Collection $entityAdv
         */
        $entityAdv = $mapperRow->where(['id' => $arrayId])
            ->with(['photos','ad_params','counter'])
            ->execute();

        if (count($premiumAdvList)) {
            $entityAdv->merge($premiumAdvList);
            foreach ($premiumAdvList as $premiumAdv) {
                array_unshift($arrayId, $premiumAdv->id);
            }
        }

        //echo"<pre>";var_dump($entityAdv->photos);
        return ['arrayId' => $arrayId, 'entityAdv' => $entityAdv, 'entityCount' => $arrayIdsCount[0]->countsId];
    }

    /**
     * @param Application $app
     * @param $request
     * @return array
     */
    public function requestParams($app, $request)
    {
        $pageCorrect = $pageNumber = (int)($request->get('page'));//страница
        $perPage = (int)($request->get('per-page'));//сколько на странице
        $withdrawFrom = ($pageCorrect-1) * $perPage;//действующая страница

        $perSort   = $request->get('sort');
        $sortOrder = ($request->get('sort-order') === 'asc') ? 'ASC' : 'DESC';

        $photoIsset = $request->get('photoIsset');//только с фото
        $category_id = (int)($request->get('category_id'));//категория
        $vip = $request->get('vip');
        $premium = $request->get('premium');
        $categoryQuery = '';//формируется запрос с категориями
        $paramsSearch = $request->get('paramsSearch');
        $paramsTitle = $request->get('paramsTitle');//исли слово для поиска введено
        $city = $request->get('city');//город дял поиска
        $area = $request->get('area');//район для поиска

        $premiumFirst = $request->get('premiumFirst');//район для поиска

        $onlyNew = $request->get('new', 'false') === 'true';
        $byTitle = $request->get('by_title', 'false') === 'true';

        $like = '';
        $sqlWhere = '';
        $leftJoin = '';
        $innerJoinWhereSql = [];

        $perSortBd = "ORDER BY a.date $sortOrder, a.id $sortOrder";//по умолчанию сортирует по дате добавления

        if ($city) {
            $sqlWhere .= " AND a.city = $city ";
        }

        if ($area) {
            $sqlWhere .= " AND a.area = $area ";
        }

        if ($onlyNew) {
            $sqlWhere .= " AND a.is_new = 1 ";
        }

        if ($paramsTitle) {

            $words = explode(' ', $paramsTitle);

            $searchText  = [];
            $searchTitle = [];
            $connection  = $app->spot()->getConnection();

            foreach ($words as $word) {
                $word = $connection->quote("%$word%");
                $searchTitle[] = " a.title LIKE $word";

                if(!$byTitle){
                    $searchText[] = " a.text_rdy LIKE $word";
                }
            }

            $searchText = implode(' AND ', $searchText);
            $searchTitle = implode(' AND ', $searchTitle);

            $parts = [];
            if ($searchText) {
                $parts[] = "($searchText)";
            }

            if ($searchTitle) {
                $parts[] = "($searchTitle)";
            }

            $like .= " AND (" . implode(" OR ", $parts) . ") ";
        }

        if(empty($pageCorrect) or empty($perPage) or $withdrawFrom < 0 or $perPage < 0){
            $withdrawFrom = 0;
            $perPage = 10;
        }

        if ($vip == "true") {
            $vip = 1;
        } else {
            $vip = 0;
        }

        if ($premium == "true") {
            $premium = 1;
        } else {
            $premium = 0;
        }

        if ($perSort == 'popular'){
            $leftJoin .= ' LEFT JOIN adv_counter ac ON (a.id = ac.id) ';
            $perSortBd = " ORDER BY ac.total $sortOrder, a.id DESC ";
        }

        if ($perSort == 'price') {
            $perSortBd = " ORDER BY a.price $sortOrder, a.id DESC ";
        }

        if ($perSort == 'rand') {
            $perSortBd = " ORDER BY RAND() ";
        }

        if(isset($photoIsset) and !empty($photoIsset)){
            $leftJoin .= ' LEFT JOIN adv_images as ai ON (a.id = ai.adv_id) ';
            $sqlWhere .= ' AND NOT ISNULL(ai.id) ';
        }

        //получаем категории
        if($category_id){
            $allCategories = Categories::allCategories($app, $category_id);
            //переводим в строку
            $categoryText = $this->categoryText($allCategories);
            $categoryQuery = " and a.category IN ($categoryText)";
        }

        //формируем запрос для вывода по параметрам
        if($paramsSearch){
            $innerJoinWhereSql = $this->paramsSearch($paramsSearch);
        }

        $premiumFirst = ($premiumFirst === "true");

        //отдаём массив
        $array = [
            'categoryQuery' => $categoryQuery,
            'vip' => $vip,
            'like' => $like,
            'premium' => $premium,
            'perSortBd' => $perSortBd,
            'withdrawFrom' => $withdrawFrom,
            'perPage' => $perPage,
            'leftJoin' => $leftJoin,
            'innerJoinWhereSql' => $innerJoinWhereSql,
            'sqlWhere' => $sqlWhere,
            'premiumFirst' => $premiumFirst,
            'pageNumber' => $pageNumber,
        ];

        return $array;
    }

    public function categoryText($categortList)
    {
        $text = '';
        foreach ($categortList as $key => $value) {

            if(!empty($key)){
                $text .= "$value ,";
            }
        }

        $text = substr($text, 0, -1);
        return $text;
    }


    public function paramsSearch($paramsSearch)
    {
        //недвижка
        //$paramsSearch = 'paramCity:2,paramArea:28,nobed:2,cover:14|486,price:0|200,';

        //авто
        //$paramsSearch = 'brand:1,model:1,body:1;2;3;,year:1960|2107,running:0|185000,box:1;2;,engine:1;2;,volume:0|185000,drive:1;2;,engine:1;2;,price:0|185000,wheel:1,';

        $explodeParams = explode(',', $paramsSearch);
        $countParams = count($explodeParams);
        $where = '';
        $innerJoin = '';
        $sql = '';
        $s = 0; //исчисляемое

        $arrayAdvParams = ['paramCity' => 'select', 'paramArea' => 'select', 'nobed' => 'select', 'cover' => 'MinMax', 'body' => 'select', 'brand' => 'select', 'model' => 'select', 'year' => 'MinMax', 'running' => 'MinMax', 'box' => 'select', 'engine' => 'select', 'volume' => 'MinMax', 'drive' => 'select', 'wheel' => 'select'];
        $arrayAdv = ['city' => 'select', 'area' => 'select', 'price' => 'MinMax'];

        $fakeParams = [
            'moto_brand' => 'brand'
        ];

        //if($countParams > 0){

            for ($i=0; $i < $countParams; $i++) {
                $bdLitter = '';
                $explodeOneParam = explode(':', $explodeParams[$i]);

                $paramName = '';
                $value = '';

                if (isset($explodeOneParam[0])) {
                    $paramName = $explodeOneParam[0];
                }

                if (isset($explodeOneParam[1])) {
                    $value = $explodeOneParam[1];
                }

                if (isset($fakeParams[$paramName])) {
                    $paramName = $fakeParams[$paramName];
                }

                //определяем причстной к таблицам
                if(isset($arrayAdv[$paramName])){
                    $bdLitter = "a";
                }

                if(isset($arrayAdvParams[$paramName])){
                    $bdLitter = 'ap';
                }

                //смотрим если есть MinMax
                $countMinMax = substr_count($value, '|');

                if($countMinMax > 0 and !empty($bdLitter)){
                    $explodeMinMax = explode('|', $value);

                    if($bdLitter == 'a'){
                        $sql .= " and $bdLitter.$paramName >= $explodeMinMax[0] and $bdLitter.$paramName <= $explodeMinMax[1] ";
                    }

                    if($bdLitter == 'ap'){
                        $innerJoin .= " INNER JOIN adv_params p$s ON a.id = p$s.adv_id ";
                        $where .= " AND (p$s.name = '$paramName' AND p$s.value >= '$explodeMinMax[0]' AND p$s.value <= '$explodeMinMax[1]' ) ";
                    }
                    $s++;
                }

                //смотрим если есть вложенные опции (;)
                $countParentOption= substr_count($value, ';');

                if ($countParentOption > 0 and !empty($bdLitter)) {

                    $explodeOption = explode(';', $value);
                    $or = '';
                    $orSimbol = ' OR ';

                    for ($r=0; $r < $countParentOption; $r++) {

                        $r_p = $r + 1;
                        if($r_p == $countParentOption){
                            $orSimbol = '';
                        }

                        $or .= " p$s.name = '$paramName' and p$s.value = '$explodeOption[$r]' $orSimbol ";

                    }

                    $innerJoin .= " INNER JOIN adv_params p$s ON a.id = p$s.adv_id ";

                    $where .= " AND ( $or ) ";
                    $s++;

                }

                //если просто значение
                if ($countMinMax == 0 and $countParentOption == 0 and !empty($bdLitter)) {

                    if($bdLitter == 'a'){
                        $sql .= " and $bdLitter.$paramName = $value ";
                    }

                    if($bdLitter == 'ap'){

                        if($paramName == 'paramCity'){
                            $paramName = 'city';
                        }

                        if($paramName == 'paramArea'){
                            $paramName = 'area';
                        }

                        $innerJoin .= " INNER JOIN adv_params p$s ON a.id = p$s.adv_id ";
                        $where .= " AND (p$s.name = '$paramName' AND p$s.value = '$value' ) ";
                    }
                    $s++;
                }
            }
        //}

        return ['where' => $where, 'innerJoin' => $innerJoin, 'sql' => $sql];
    }

}