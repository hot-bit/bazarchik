<?php

namespace BZR\Controller;

use BZR\Application;
use Symfony\Component\HttpFoundation\Request;
use BZR\Entity\Response\News as NewsResponse;

class News extends Base
{
    /**
     * @param Application $app
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\JsonResponse
     */
    public function get(Application $app, Request $request)
    {
        $limit = max(1, intval($request->get('per-page', 10)));
        $page  = max(1, intval($request->get('page', 1)));

        /**
         * @var \BZR\Entity\Mapper\News $mapper
         */
        $mapper = $app->spot()->mapper('BZR\Entity\News');
        $count = $mapper->getCount();
        $list   = $mapper->getPage($page, $limit);
        $result = [];

        foreach ($list as $news) {
            $result[] = new NewsResponse($news);
        }

        $headers = [
            'X-Pagination-Current-Page' => $page,
            'X-Pagination-Page-Count'   => ceil($count / $limit),
            'X-Pagination-Per-Page'     => $limit,
            'X-Pagination-Total-Count'  => $count,
        ];

        return $app->generalResponse($result, Application::PUT_GET_OK, $headers);
    }
}
