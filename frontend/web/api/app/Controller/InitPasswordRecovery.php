<?php

namespace BZR\Controller;

use BZR\Application;
use BZR\Translation\Keys;
use BZR\ErrorChecking;
use Symfony\Component\HttpFoundation\Request;

class InitPasswordRecovery extends Base
{
    public function initPasswordRecovery(Application $app, Request $request)
    {
        $mapperRow = $app->spot()->mapper('BZR\Entity\Profile');

        //////////ERRORS CHECKED//////////////////////////////////////
        $arrayErrorChecking = [
            'email'    => $request->get('email')
        ];

        $errors = ErrorChecking::errorChecking($arrayErrorChecking, $app, $mapperRow, $corrector=false);

        if (count($errors) > 0) {
            return $app->errorResponseArray($errors, 1, Application::VALIDATION_ERROR);
        }
        //////////ERRORS CHECKED//////////////////////////////////////

        $result = $mapperRow->first(['email' => $request->get('email')]);

        if ($result) {

            $code = $app->Random($count = 5);
            $mapperRow = $app->spot()->mapper('BZR\Entity\InitPasswordRecovery');

            $entity = $mapperRow->build([
                'user_id'    => $result->id,
                'code'       => $code,
                'time_code'  => time() + $app['config']['initPasswordRecovery']['timeWait']
            ]);

            $result = $mapperRow->insert($entity);

            if($result){
                $headers= "MIME-Version: 1.0\r\n";
                $headers .= "Content-type: text/html; charset=UTF-8\r\n";

                $headers .= 'From: www.bazar-cy.com' . "\r\n" .
                    'Reply-To: www.bazar-cy.com' . "\r\n" .
                    'X-Mailer: PHP/' . phpversion();

                $fullText = "
                    <html>
                        <head></head>
                        <body>
                            <table>
                                <tr>
                                    <td>".$app['config']['initPasswordRecovery']['text']." <span style='color:green;'>".$code."</span></td>
                                </tr>
                            </table>
                        </body>
                    </html>";


                //$request->get('email')
                if(mail($request->get('email'), $app['config']['initPasswordRecovery']['header_text'], $fullText, $headers)){
                    return $app->errorResponse(Keys::SUCCESS_EMAIL_SENT, Keys::SUCCESS_EMAIL_SENT, 1, Application::PUT_GET_OK);

                } else {
                    return $app->errorResponse(Keys::ERROR_EMAIL_SENT, Keys::ERROR_EMAIL_SENT, 1, Application::SERVER_ERROR);
                }

            } else {
                return $app->errorResponse(Keys::ERROR_INSERT, Keys::ERROR_INSERT, 1, Application::NOT_FOUND_ERROR);
            }

        } else {
            return $app->errorResponse(Keys::ERROR_NO_EMAIL, Keys::ERROR_NO_EMAIL, 1, Application::NOT_FOUND_ERROR);
        }

    }
}