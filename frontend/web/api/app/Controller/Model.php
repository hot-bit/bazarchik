<?php

namespace BZR\Controller;

use BZR\Application;
use BZR\Translation\Keys;
use Symfony\Component\HttpFoundation\Request;

class Model //extends Base
{
    public function getModel(Application $app, Request $request)
    {
        $mapperRow = $app->spot()->mapper('BZR\Entity\Model');
        $results = $mapperRow->all();
        $brand = (int)($request->get('brand'));
        $array = [];
        $i = 0;

        if(!empty($brand)){
            $results = $mapperRow->where(['brand_id' => $brand]);
        } else {
            $results = $mapperRow->all();
        }

        if($results->count() > 0){
            foreach($results as $result) {
                $array[$i] = [
                    'id' => $result->id,
                    'brand_id' => $result->brand_id,
                    'name'=> $result->name,
                    'count_used'=> $result->count_used,
                    'count_new'=> $result->count_new,
                    'count_rent'=> $result->count_rent,
                ];
                $i++;
            }
        } else {
            return $app->errorResponse(Keys::ERROR_SELECT, Keys::ERROR_SELECT, 1, Application::NOT_FOUND_ERROR);
        }

        return $app->generalResponse($array, Application::PUT_GET_OK);
    }

    static public function getOneModel($app, $id)
    {
        $mapperRow = $app->spot()->mapper('BZR\Entity\Model');
        $results = $mapperRow->first(['id' => $id]);
        return $results->name;
    }
}