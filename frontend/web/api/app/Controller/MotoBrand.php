<?php

namespace BZR\Controller;

use BZR\Application;
use BZR\Translation\Keys;
use Symfony\Component\HttpFoundation\Request;

class MotoBrand //extends Base
{
    public function getMotoBrand(Application $app, Request $request)
    {
        $mapperRow = $app->spot()->mapper('BZR\Entity\MotoBrand');
        $results = $mapperRow->all();
        $array = [];
        $i = 0;

        if($results->count() > 0){
            foreach($results as $result) {
                $array[$i] = [
                    'id'    => $result->id,
                    'name'  => $result->name,
                    'count' => $result->count,
                ];
                $i++;
            }
        } else {
            return $app->errorResponse(Keys::ERROR_SELECT, Keys::ERROR_SELECT, 1, Application::NOT_FOUND_ERROR);
        }

        return $app->generalResponse($array, Application::PUT_GET_OK);
    }

    static public function getOneMotoBrand($app, $id)
    {
        $mapperRow = $app->spot()->mapper('BZR\Entity\MotoBrand');
        $results = $mapperRow->first(['id' => $id]);
        return $results->name;
    }
}