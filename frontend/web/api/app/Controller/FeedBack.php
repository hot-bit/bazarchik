<?php

namespace BZR\Controller;

use BZR\Application;
use BZR\Arr;
use BZR\ErrorChecking;
use BZR\Translation\Keys;
use Symfony\Component\HttpFoundation\Request;

class FeedBack
{
    public function postFeedBack(Application $app, Request $request)
    {
    	//////////ERRORS CHECKED//////////////////////////////////////
        $arrayErrorChecking = [
                'phone'          => $request->get('phone'),
                'name'           => $request->get('name'),
                'email'          => $request->get('email'),
                'text'           => $request->get('text', $request->getContent()),
                //'adv_id'         => $request->get('adv_id'),
        ];
      
        $errors = ErrorChecking::errorChecking($arrayErrorChecking, $app, $mapperAdv = false, $corrector=false);

        if (count($errors) > 0) {           
            return $app->errorResponseArray($errors, 1, Application::VALIDATION_ERROR);
        }
        //////////ERRORS CHECKED//////////////////////////////////////

        $headers= "MIME-Version: 1.0\r\n";
        $headers .= "Content-type: text/html; charset=UTF-8\r\n";

        $headers .= 'From: www.bazar-cy.com' . "\r\n" .
    		'Reply-To: www.bazar-cy.com' . "\r\n" .
    		'X-Mailer: PHP/' . phpversion();

       	$body = "
       		<html>
       			<head></head>
       			<body>
       				<table>
       					<tr>
							<td>
								<p>name: ".$request->get('name')."</p>
							</td>
						</tr>
						<tr>
							<td>
								<p>phone: ".$request->get('phone')."</p>
							</td>
						</tr>
						<tr>
							<td>
								<p>email: ".$request->get('email')."</p>
							</td>
						</tr>
						<tr>
							<td>
								<p>text: ".$request->get('text')."</p>
							</td>
						</tr>
					</table>
				</body>
			</html>";

        if(mail($app['config']['postFeedBack']['email_FeedBack'], $app['config']['postFeedBack']['title']/*.$request->get('adv_id')*/, $body , $headers)){

            return $app->errorResponse(Keys::SUCCESS_EMAIL_SENT, Keys::SUCCESS_EMAIL_SENT, 1, Application::PUT_GET_OK);
        } else {
            return $app->errorResponse(Keys::ERROR_EMAIL_SENT, Keys::ERROR_EMAIL_SENT, 1, 500);
        }
    }
}