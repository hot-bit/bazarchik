<?php

namespace BZR\Controller;

use BZR\Application;
use BZR\Arr;
use BZR\Translation\Keys;
use BZR\Entity\Response\Adv;
use Symfony\Component\HttpFoundation\Request;

class MyAdv extends Base
{
    public function getMyAdv(Application $app, Request $request)
    {
        //добавляем id пользователя к объявления, делаем причастность.
        $mapperRow = $app->spot()->mapper('BZR\Entity\Adv');
        $getCurrentProfile = $app->getCurrentProfile();

        $perPage      = (int)($request->get('per-page'));//число для показов
        $pageCorrect  = (int)($request->get('page'));//страница
        $withdrawFrom = ($pageCorrect-1) * $perPage;
        $inspection = true;
        $headers = [];
        $array = [];
        $i = 0;

        $enityAdv = $mapperRow->where(['status' => [1, 2, 4], 'user' => $getCurrentProfile->id])
            ->execute();

        //echo"<pre>";var_dump($enityAdv);

        //ПОСТРАНИЧНАЯ НАВИГАЦИЯ
        $pageCount = 0;

        if ($perPage > 0) {
            $pageCount = $enityAdv->count() / (int)($perPage);
        }

        $pageCount = ceil($pageCount);

        if(empty($pageCorrect) or empty($perPage) or $withdrawFrom < 0 or $perPage < 0 or $pageCorrect > $pageCount){

            $inspection = false;

            if(!$request->get('page') or !$request->get('per-page')){
                $inspection = true;
            }
        }
        //ПОСТРАНИЧНАЯ НАВИГАЦИЯ

        if($enityAdv->count() > 0 && $inspection){

            $entityAdv = $mapperRow->where(['user' => $getCurrentProfile->id, 'status' => [1, 2,4]])
            ->with(['photos','ad_params','counter'])
            ->limit($perPage,$withdrawFrom)
            ->order(['date_create' => 'DESC'])
            ->execute();

            foreach($entityAdv as $keyAdv => $valueAdv) {
                $result = new Adv($entityAdv[$keyAdv]);
                $array[$i] = $result->jsonSerialize($app, $request);
                $i++;
            }

            $headers = [
                'X-Pagination-Current-Page' => $pageCorrect,
                'X-Pagination-Page-Count' => $pageCount,
                'X-Pagination-Per-Page' => $perPage,
                'X-Pagination-Total-Count' => $enityAdv->count(),
            ];

            return $app->generalResponse($array, Application::PUT_GET_OK, $headers); 

        } else {
            return $app->generalResponse([], Application::PUT_GET_OK, $header = []);
        }

    }

}
