<?php

namespace BZR\Controller;

use BZR\Application;
use BZR\Translation\Keys;
use Symfony\Component\HttpFoundation\Request;

class City //extends Base
{
    public function getCity(Application $app, Request $request)
    {
        $mapperRow = $app->spot()->mapper('BZR\Entity\City');
        $results = $mapperRow->all();
        $array = [];
        $i = 0;

        if($results->count() > 0){
            foreach($results as $result) {
                $array[$i] = [
                    'id' => $result->id,
                    'name_ru'=> $result->name_ru,
                    'name_en'=> $result->name_en,
                    'name_gr'=> $result->name_gr,
                    'lat'=> $result->lat,
                    'lng'=> $result->lng,
                ];
                $i++;
            }
        } else {
            return $app->errorResponse(Keys::ERROR_SELECT, Keys::ERROR_SELECT, 1, Application::NOT_FOUND_ERROR);
        }

        return $app->generalResponse($array, Application::PUT_GET_OK);
    }

    static public function getOneCity($app, $id)
    {
        $mapperRow = $app->spot()->mapper('BZR\Entity\City');
        $results = $mapperRow->first(['id' => $id]);
        $name = '';

        if(isset($results->name_en)){
            $name = $results->name_en;
        }
        return $name;
    }
}