<?php

namespace BZR\Controller;

use BZR\Application;
use BZR\Exception;
use BZR\Translation\Keys;
use BZR\Arr;
use BZR\ErrorChecking;
use BZR\Entity\Response\User;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Validator\Constraints as Assert;

class Profile extends Base
{
    public static function getProfile(Application $app, Request $request)
    {
        $avatar = '';
        $getCurrentProfile = $app->getCurrentProfile();

        $mapperProfile = $app->spot()->mapper('BZR\Entity\Profile');
        $enityProfile = $mapperProfile->where(['id' => $getCurrentProfile->id])
            ->with(['newMessage'])
            ->execute();
        
        if ($enityProfile->count() > 0) {
            //дорабатываем аватар - URL
            $mapperProfile->urlPicturesAvatar($app, $enityProfile);

            $enityProfile = new User($enityProfile[0]);

            return $app->generalResponse($enityProfile->jsonSerialize($app), Application::PUT_GET_OK);
        } else {
            return $app->errorResponse(Keys::ERROR_USER_ID, Keys::ERROR_USER_ID, 1, Application::NOT_FOUND_ERROR);
        }
    }

    public static function postProfile(Application $app, Request $request)
    {
        $getCurrentProfile = $app->getCurrentProfile();

        if ($getCurrentProfile) {
            $mapperProfile = $app->spot()->mapper('BZR\Entity\Profile');

            //////////ERRORS CHECKED//////////////////////////////////////
            $email = $request->get('email');
            $arrayErrorChecking = [
                'email'    => $email,
                'name'     => $request->get("name"),
                'b_day'    => $request->get("bday"),
                'b_month'  => $request->get("bmonth"),
                'b_year'   => $request->get("byear"),
                'phone'    => $request->get('phone'),
                'type'     => $request->get('account_type'),
                'city'     => $request->get('city'),
                'area'     => $request->get('area'),
            ];

            $errors = errorChecking::errorChecking($arrayErrorChecking, $app, $mapperProfile, $corrector=false);

            if (count($errors) > 0) {
                return $app->errorResponseArray($errors, 1, Application::VALIDATION_ERROR);
            }

            $entityCount = $mapperProfile->all()->where(['email' => $email, 'id !=' => $getCurrentProfile->id])->count();
            if ($entityCount > 0) {
                $constraint_array = ['email' => new Assert\Callback(ErrorChecking::formingError($app, $mapperProfile, 'This email address is already using'))];
                $constraint = new Assert\Collection($constraint_array);
                $errors = $app['validator']->validate(['email' => $email], $constraint);
            }

            if (count($errors) > 0) {
                return $app->errorResponseArray($errors, 1, Application::VALIDATION_ERROR);
            }
            //////////ERRORS CHECKED//////////////////////////////////////



            $getCurrentProfile->email   = $email;
            $getCurrentProfile->name    = $request->get('name');
            $getCurrentProfile->b_day   = $request->get("bday");
            $getCurrentProfile->b_month = $request->get("bmonth");
            $getCurrentProfile->b_year  = $request->get('byear');
            $getCurrentProfile->city    = $request->get('city');
            $getCurrentProfile->area    = $request->get('area');

            $phone = $request->get('phone');

            if ($phone !== $getCurrentProfile->phone) {
                $getCurrentProfile->success_phone = 0;
            }

            $getCurrentProfile->phone = $phone;

            $skype = $request->get('skype', null);
            if (!is_null($skype)) {
                $getCurrentProfile->skype = $skype;
            }
            //file_put_contents(__DIR__ . '/../../log/pr.log', "email: " . var_export($email, true). "\n", FILE_APPEND);
            //file_put_contents(__DIR__ . '/../../log/pr.log', "req: " . var_export($request->get('send_sms', null), true). "\n", FILE_APPEND);
            $sendSms = $request->get('send_sms', null);
            if (!is_null($sendSms)) {
                $getCurrentProfile->send_sms = $sendSms;
            }

            //переменая для бизнесс аккаунта
            //if ($request->get('website')) {
                $getCurrentProfile->website = $request->get('website', '');
            //}

            //переменая для бизнесс аккаунта
            //if ($request->get('company')) {
                $getCurrentProfile->company = $request->get('company', '');
            //}

            //переменая для бизнесс аккаунта
            if ($request->get('lat') and $request->get('lng')) {
                $getCurrentProfile->lat = $request->get('lat');
                $getCurrentProfile->lng = $request->get('lng');
            }

            //тип аккаунта
            if ($request->get('account_type') or $request->get('account_type') == 0) {
                $getCurrentProfile->type = $request->get('account_type');
            }

            $mapperProfile->update($getCurrentProfile);

            $mapperProfile = $app->spot()->mapper('BZR\Entity\Profile');
            $enityProfile = $mapperProfile->where(['id' => $getCurrentProfile->id])
                ->with(['newMessage'])
                ->execute();

            //дорабатываем аватар - URL
            $mapperProfile->urlPicturesAvatar($app, $enityProfile);
            $getCurrentProfile = new User($enityProfile[0]);
            //file_put_contents(__DIR__ . '/../../log/pr.log', "res: " . var_export($getCurrentProfile->jsonSerialize($app), true). "\n", FILE_APPEND);
            return $app->generalResponse($getCurrentProfile->jsonSerialize($app), Application::POST_OK);
        } else {
            return $app->errorResponse(Keys::ERROR_ID_NO, Keys::ERROR_ID_NO, 1, Application::NOT_FOUND_ERROR);
        }
    }

    public static function getProfileUp($app, $request, $id, $resultTokken = false, $postAndGet = false)
    {
        $mapperProfile = $app->spot()->mapper('BZR\Entity\Profile');
        $enityProfile = $mapperProfile->where(['id' => $id])
            ->with(['newMessage'])
            ->execute();

        if ($enityProfile->count() > 0) {
            //дорабатываем аватар - URL
            $mapperProfile->urlPicturesAvatar($app, $enityProfile);

            $result = new User($enityProfile[0]);
            $result = $result->jsonSerialize($app);

            if($resultTokken){
                $result = Arr::unshift($result, 'auth_token', $app->getAuthToken());
            }

            if($postAndGet == true){
                $status = Application::POST_OK;
            } else {
                $status = Application::PUT_GET_OK;
            }

            return $app->generalResponse($result, $status);
        } else{
            return $app->errorResponse(Keys::ERROR_USER_ID, Keys::ERROR_USER_ID, 1, Application::NOT_FOUND_ERROR);
        }
    }

    public static function newPasswordRecovery(Application $app, Request $request)
    {
        $mapperInitPasswordRecovery = $app->spot()->mapper('BZR\Entity\InitPasswordRecovery');
        $mapperProfile = $app->spot()->mapper('BZR\Entity\Profile');

        //////////ERRORS CHECKED//////////////////////////////////////
        $arrayErrorChecking = [
            'id'        => $request->get('id'),
            'password'    => $request->get('password')
        ];

        $errors = ErrorChecking::errorChecking($arrayErrorChecking, $app, $mapperProfile, $corrector=false);

        if (count($errors) > 0) {
            return $app->errorResponseArray($errors, 1, Application::VALIDATION_ERROR);
        }
        //////////ERRORS CHECKED//////////////////////////////////////

        $result = $mapperInitPasswordRecovery->first(['id' => $request->get('id')]);

        if($result){
            $resultProfile = $mapperProfile->first(['id' => $result->user_id]);

            if($resultProfile){
                $arrayPG = $app->passwordGeneration($request->get('password'));
                $authToken = md5(rand(1,9999));
                $app->setAuthToken($authToken);

                $resultProfile->password = $arrayPG['password'];
                $resultProfile->salt = $arrayPG['salt'];

                $result = $mapperProfile->update($resultProfile);

                if($result){
                    $array = [$result];
                } else {
                    $array = [];
                }

                return $app->generalResponse($array, Application::POST_OK);
            }
        }

        return $app->errorResponse(Keys::ERROR_ID_NO, Keys::ERROR_ID_NO, 1, Application::NOT_FOUND_ERROR);
    }

    public static function newPassword(Application $app, Request $request)
    {
        $mapperRow = $app->spot()->mapper('BZR\Entity\Profile');
        $getCurrentProfile = $app->getCurrentProfile();

        if ($getCurrentProfile) {
            //////////ERRORS CHECKED//////////////////////////////////////
            $arrayErrorChecking = [
                'password'    => $request->get('password')
            ];

            $errors = errorChecking::errorChecking($arrayErrorChecking, $app, $mapperRow, $corrector=false);

            if (count($errors) > 0) {
                return $app->errorResponseArray($errors, 1, Application::VALIDATION_ERROR);
            }
            //////////ERRORS CHECKED//////////////////////////////////////

            $arrayPG = $app->passwordGeneration($request->get('password'));

            $getCurrentProfile->password = $arrayPG['password'];
            $getCurrentProfile->salt = $arrayPG['salt'];

            $mapperRow->update($getCurrentProfile);

            $result = Profile::getProfileUp($app, $request, $getCurrentProfile->id, $resultTokken = true, $postAndGet = true);

            return $result;

        } else {
            return $app->errorResponse(Keys::ERROR_ID_NO, Keys::ERROR_ID_NO, 1, Application::NOT_FOUND_ERROR);
        }

    }


}