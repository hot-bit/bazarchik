<?php

namespace BZR\Controller;

use BZR\Application;
use BZR\Arr;
use BZR\ErrorChecking;
use BZR\Translation\Keys;
use BZR\Controller\Profile;
use Facebook\GraphNodes\Birthday;
use Symfony\Component\HttpFoundation\Request;
use Facebook\Facebook;
use Facebook\Exceptions\FacebookResponseException;
use Facebook\Exceptions\FacebookSDKException;

class Login extends Base
{
    public function social(Application $app, Request $request)
    {
        $getCurrentProfile = $app->getCurrentProfile();

        if ($getCurrentProfile) {
            //если мобильный токен и токен авторизации существует, означает то что пользователь уже авторизован, и не может быть авторизован повторно.
            return $app->errorResponse(Keys::ERROR_AUT_EX, Keys::ERROR_AUT_EX, 1, Application::NOT_FOUND_ERROR);
        } else {
            //существует ли авторизационнай токен из социальной сети, который является главным инструментом для взаимодействия последующих скриптов
            //$app['session']->set('token', 'ya29.CjIGA6k93EpEMa8I2Mo94aeWGTMwBZhsTktY5xhUMH8Tr4zcxlaK4KcR6YHC-QGF3Ajbuw');
            $token = $request->get('token');
            //$token = $app['session']->get('token'); //тестовый вариант, для проверки. Токен должен быть занесён в сессию.

            if (empty($token)) {
                return $app->errorResponse(Keys::ERROR_TOKEN_EMPTY, Keys::ERROR_TOKEN_EMPTY, 1, Application::NOT_FOUND_ERROR);
            }

            $type  = $request->get('type');

            if ($type === "fb") {
                return $this->loginFacebook($app, $request, $token);
            }
            if ($type === "gp") {
                return $this->loginGooglePlus($app, $request, $token);
            }

            return $app->errorResponse('Test error', 'not implemented yet', 100, 500);
        }
    }

    protected function loginGooglePlus(Application $app, Request $request, $token)
    {
        $arrayConfig = $app->config('google_plus');
        $client_id = $arrayConfig['client_id'];
        $client_secret = $arrayConfig['client_secret'];
        $application_name = $arrayConfig['application_name'];

        $client = new \Google_Client();
        $client->setApplicationName($application_name);
        $client->setClientId($client_id);
        $client->setClientSecret($client_secret);
        $client->setRedirectUri('postmessage');

        $plus = new \Google_Service_Plus($client);

        $params['access_token'] = $token;
        $arrayResultSocial = json_decode(file_get_contents('https://www.googleapis.com/oauth2/v1/userinfo' . '?' . urldecode(http_build_query($params))), true);

        //print_r($arrayResultSocial);
        return $this->generalAuthorization($app, $request, $arrayResultSocial);
    }

    protected function loginFacebook(Application $app, Request $request, $token)
    {
        $fb = new Facebook($app->config('facebook'));

        try {
            $response = $fb->get('/me?fields=email,name,birthday,picture', $token);
            $arrayResultSocial = $response->getGraphUser();
        } catch(FacebookResponseException $e) {
            // When Graph returns an error
            return $app->errorResponse(Keys::ERROR_AUT_FB, Keys::ERROR_AUT_FB, 1, Application::NOT_FOUND_ERROR);
        } catch(FacebookSDKException $e) {
            // When validation fails or other local issues
            return $app->errorResponse(Keys::ERROR_AUT_FB, Keys::ERROR_AUT_FB, 1, Application::NOT_FOUND_ERROR);
        }

        //print_r($arrayResultSocial);
        return $this->generalAuthorization($app, $request, $arrayResultSocial);
    }

    public function generalAuthorization($app, $request, $arrayResultSocial)
    {
        $type = $request->get('type');
        $idUser = $arrayResultSocial['id'];

        if ($type === "fb") {
            $bdRowId = 'fb_id';
        }

        if ($type === "gp") {
            $bdRowId = 'g_id';
        }

        //1 проверка FB
        //2 проверка Google Plus
        //обрабатываем переменные

        if(isset($arrayResultSocial['picture']['url']) and $type == "fb"){
            $avatar = $arrayResultSocial['picture']['url'];
        }elseif (isset($arrayResultSocial['picture']) and $type == "gp") {
            $avatar = $arrayResultSocial['picture'];
        } else {
            $avatar = '';
        }

        if(isset($arrayResultSocial['name']) and $type == "fb"){
            $name = $arrayResultSocial['name'];
        }elseif (isset($arrayResultSocial['name']) and $type == "gp") {
            $name = $arrayResultSocial['name'];
        } else {
            $name = '';
        }

        if(isset($arrayResultSocial['email']) and $type == "fb"){
            $email = $arrayResultSocial['email'];
        }elseif (isset($arrayResultSocial['email']) and !empty($arrayResultSocial['email']) and $type == "gp") {
            $email = $arrayResultSocial['email'];
        } else {
            $email = '';
        }

        if(isset($arrayResultSocial['birthday'])){

            if ($type == 'fb') {
                $birthDateString = $arrayResultSocial['birthday'] instanceof Birthday ? $arrayResultSocial['birthday']->format('m/d/Y') : $arrayResultSocial['birthday'];
                $birthdate = explode('/', $birthDateString);// MM/DD/YYY дата рождения
                $b_day = (int)($birthdate[1]);
                $b_month = (int)($birthdate[0]);
                $b_year = (int)($birthdate[2]);
            } else {
                $birthdate = explode('-', $arrayResultSocial['birthday']);// YYYY-MM-DD дата рождения
                //если год пустой значит у человека закрыта часть информации - настройки приватности, а если части нету, то это уже не корректная информация и мы обнуляем.
                if((int)($birthdate[0])){
                    $b_year = (int)($birthdate[0]);
                    $b_month = (int)($birthdate[1]);
                    $b_day = (int)($birthdate[2]);
                } else {
                    $b_day = '';
                    $b_month = '';
                    $b_year = '';
                }
            }
        } else {
            $b_day = '';
            $b_month = '';
            $b_year = '';
        }

        //Проверка email(а) на существование в БД, если он существует то это означает что вход происходит с другого устройства и нужно не регистрировать а просто авторизоваться. EMAIL - уникальное поле и оно не повторяеться, прописанно в самой БД.
        //$email = '222@mail.ru';//Строка для теста, потому-что у приложения нет прав для выборки email.

        if(!empty($email)){
            $mapperRow = $app->spot()->mapper('BZR\Entity\Profile');
            $entity = $mapperRow->first(['email' => $email]);

            if ($entity) {
                //проверяем существует ли закреплённый id соц. сети за аккаунтом если нет ЗАКРЕПЛЯЕМ АККАУНТ за ID из соц сети.
                if(!empty($entity->$bdRowId)){
                    return Login::actionsSuccessfulAuthorization($app, $request, $entity);
                } else {
                    $entity->$bdRowId = $idUser;
                    $entity->name = $name;
                    $entity->success_email = 1;
                    $mapperRow->update($entity);
                    return Login::actionsSuccessfulAuthorization($app, $request, $entity);
                }
            } else {
                $array = $mapperRow->build([
                    'name'          => $name,
                    'email'         => $email,
                    'b_day'         => $b_day,
                    'b_month'       => $b_month,
                    'b_year'        => $b_year,
                    'avatar'        => $avatar,
                    'status'        => 1,
                    'role'          => 3,
                    'date_reg'      => time(),
                    'success_email' => 1,
                    $bdRowId        => $idUser
                ]);

                $result = $mapperRow->insert($array);

                if($result){
                    $entity = $mapperRow->first(['id' => $result]);

                    return Login::actionsSuccessfulAuthorization($app, $request, $entity);
                }

                return $app->errorResponse(Keys::ERROR_INSERT, Keys::ERROR_INSERT, 1, Application::NOT_FOUND_ERROR, $headers = []);
            }

        } else {
            return $app->errorResponse(Keys::ERROR_AUT_EMAIL, Keys::ERROR_AUT_EMAIL, 1, Application::NOT_FOUND_ERROR, $headers = []);
        }

    }

    public function login(Application $app, Request $request)
    {
        $mapperRow = $app->spot()->mapper('BZR\Entity\Profile');
        $getCurrentProfile = $app->getCurrentProfile();

        if ($getCurrentProfile) {
            //если мобильный токен и токен авторизации существует, означает то что пользователь уже авторизован, и не может быть авторизован повторно.
            return $app->errorResponse(Keys::ERROR_AUT_EX, Keys::ERROR_AUT_EX, 1, Application::NOT_FOUND_ERROR);
        } else {
            //////////ERRORS CHECKED//////////////////////////////////////
            $arrayErrorChecking = [
                'email'    => $request->get('login'),//уникальное поле
                'password' => $request->get('password')
            ];

            $errors = errorChecking::errorChecking($arrayErrorChecking, $app, $mapperRow, $corrector=false);

            if (count($errors) > 0) {
                return $app->errorResponseArray($errors, 1, Application::VALIDATION_ERROR);
            }
            //////////ERRORS CHECKED//////////////////////////////////////

            $arrayPG = $app->passwordGenerationLogin($request->get('password'), $request->get('login'));

            $entity = $mapperRow->first(['email' => $request->get('login'), 'password' => $arrayPG['password']]);

            if ($entity) {
                return $this->actionsSuccessfulAuthorization($app, $request, $entity);
            } else {
                return $app->errorResponse(Keys::ERROR_AUT, Keys::ERROR_AUT, 1, Application::NOT_FOUND_ERROR, $headers = []);
            }
        }
    }

    public function actionsSuccessfulAuthorization($app, $request, $entity)
    {
        $mapperProfile = $app->spot()->mapper('BZR\Entity\Profile');
        $entity->last_login = time();
        $mapperProfile->update($entity);

        //создаём мобильный токен если его нет
        $mapperUsersAuth = $app->spot()->mapper('BZR\Entity\UsersAuthMass');

        $entityUsersAuth = $mapperUsersAuth->first(['user_id' => $entity->id, 'mobile_token' => $app->getMobileToken()]);

        if ($entityUsersAuth) {
            $authToken = md5(rand(1,9999));
            $app->setAuthToken($authToken);

            $entityUsersAuth->auth_token = $authToken;
            $mapperUsersAuth->update($entityUsersAuth);
        } else {

            $authToken = md5(rand(1,9999));
            $app->setAuthToken($authToken);

            $entityUsersAuth = $mapperUsersAuth->build([
                'user_id'      => $entity->id,
                'mobile_token' => $app->getMobileToken(),
                'auth_token'   => $app->getAuthToken(),
                'lat'          => '0',
                'lng'          => '0',
                'last_login'   => time(),
            ]);

            $mapperUsersAuth->insert($entityUsersAuth);
        }

        $result = Profile::getProfileUp($app, $request, $entity->id, $resultTokken = true, $postAndGet = true);

        return $result;
    }
}