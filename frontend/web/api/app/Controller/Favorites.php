<?php

namespace BZR\Controller;

use BZR\Application;
use BZR\Arr;
use BZR\Controller\Ads;
use BZR\Translation\Keys;
use Symfony\Component\HttpFoundation\Request;

class Favorites
{
    public function postFavorites(Application $app, Request $request)
    { 
    	$mapperFavorites = $app->spot()->mapper('BZR\Entity\Favorites');
    	$mapperAdv = $app->spot()->mapper('BZR\Entity\Adv');
    	$getCurrentProfile = $app->getCurrentProfile();
    	$adv_id = (int)($request->get("adv_id"));

    	if($getCurrentProfile and isset($adv_id) and !empty($adv_id)){

    		$resultAdv = $mapperAdv->first(['id' => $adv_id]);
    		$results = $mapperFavorites->first(['user_id' => $getCurrentProfile->id, 'adv_id' => $adv_id]);

    		if($results and $resultAdv){
    			return $app->generalResponse([$adv_id], Application::PUT_GET_OK, $header = []);
    			
    		} elseif (!$results and $resultAdv) {
    			$entityFavorites = $mapperFavorites->build([
	                'user_id'         => $getCurrentProfile->id,
                    'adv_id'          => $adv_id,
                    'favorite_date'   => time(),
            	]);
    
	            $mapperFavorites->insert($entityFavorites);
	    		return $app->generalResponse([$adv_id], Application::PUT_GET_OK, $header = []);

    		} else {
    			return $app->errorResponse(Keys::ERROR_SELECT, Keys::ERROR_SELECT, 1, Application::METHOD_ERROR, $headers = []);
    		}
    	}
    }

    public function getFavorites(Application $app, Request $request)
    {
        $getCurrentProfile = $app->getCurrentProfile();

        $perPage      = (int)($request->get('per-page'));//число для показов
        $pageCorrect  = (int)($request->get('page'));//страница
        $withdrawFrom = ($pageCorrect-1) * $perPage;
        $inspection = true;

        $mapperFavorites = $app->spot()->mapper('BZR\Entity\Favorites');
        $entityFavorites = $mapperFavorites->where(['user_id' => $getCurrentProfile->id]);

        //ПОСТРАНИЧНАЯ НАВИГАЦИЯ
        $pageCount = 0;

        if ($perPage > 0) {
            $pageCount = $entityFavorites->count() / (int)($perPage);
        }

        $pageCount = ceil($pageCount);

        if(empty($pageCorrect) or empty($perPage) or $withdrawFrom < 0 or $perPage < 0 or $pageCorrect > $pageCount){

            $inspection = false;

            if(!$request->get('page') or !$request->get('per-page')){
                $inspection = true;
            }
        }
        //ПОСТРАНИЧНАЯ НАВИГАЦИЯ

        if($entityFavorites->count() > 0 && $inspection){
            //список с adv_id объявлений
            $mapperAdv = $app->spot()->mapper('BZR\Entity\Adv');

            $sqlQuery = "
                SELECT f.adv_id FROM favorite f
                LEFT JOIN adv a ON(f.adv_id = a.id)
                WHERE a.status = 2 AND f.user_id = ". $getCurrentProfile->id. "
                ORDER BY f.favorite_date DESC
                LIMIT $withdrawFrom, $perPage
            ";
            $arrayIds = $mapperAdv->query($sqlQuery);

            $arrayId = [];
            foreach ($arrayIds as $key => $value) {
                array_push($arrayId, $value->adv_id);
            }

            $entityAdv = $mapperAdv->where(['id' => $arrayId])
                ->with(['photos','ad_params','counter'])
                ->execute();

            return Ads::resultOutput($app, ['arrayId' => $arrayId, 'entityAdv' => $entityAdv, 'entityCount' => $entityFavorites->count()], $request);

        } else {
            return $app->generalResponse([], Application::PUT_GET_OK, $header = []);
        }

    }

    public function delFavorites(Application $app, Request $request)
    {
        $mapperFavorites = $app->spot()->mapper('BZR\Entity\Favorites');
        $getCurrentProfile = $app->getCurrentProfile();

        if($getCurrentProfile){
            $entityFavorites = $mapperFavorites->first(['user_id' => $getCurrentProfile->id, 'adv_id' => (int)($request->get('adv_id'))]);

            if($entityFavorites){
                //список с adv_id объявлений
                $mapperFavorites->delete(['user_id' => $getCurrentProfile->id, 'adv_id' => (int)($request->get('adv_id'))]);

                return $app->generalResponse([(int)($request->get('adv_id'))], Application::PUT_GET_OK, $header = []);
            } else {
                return $app->generalResponse([], Application::PUT_GET_OK, $header = []);
            }
        }
    }


}