<?php

namespace BZR;

use Symfony\Component\Debug\Exception\FlattenException;
use Symfony\Component\HttpKernel\Event\GetResponseForExceptionEvent;


class ExceptionHandler extends \Silex\ExceptionHandler
{
    public function onSilexError(GetResponseForExceptionEvent $event)
    {
        if (!$this->enabled) {
            return;
        }

        $exception = $event->getException();
        if (!$exception instanceof FlattenException) {
            $exception = FlattenException::create($exception);
        }

        $response = ErrorResponse::create(
            new ErrorData("Server error", $exception->getMessage(), $exception->getCode()),
            Application::UNKNOWN_ERROR
        );

        $event->setResponse($response);
    }
}
