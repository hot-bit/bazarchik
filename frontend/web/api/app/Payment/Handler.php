<?php
namespace BZR\Payment;


use PayPal\Api\Payment;
use Silex\Application;

class Handler
{
    const EXPECTED_CURRENCY = "EUR";
    const PAYMENT_STATE     = "approved";
    const TRANSACTION_STATE = "completed";

    private $app;

    public function __construct(Application $app)
    {
        $this->app = $app;
    }

    /**
     * @param $paymentId
     * @return mixed
     * @throws Exception
     * @see https://developer.paypal.com/webapps/developer/docs/integration/mobile/verify-mobile-payment/
     * @see http://paypal.github.io/PayPal-PHP-SDK/sample/doc/payments/GetPayment.html
     */
    public function getPaymentAmount($paymentId)
    {
        try {
            $payment = Payment::get($paymentId, $this->app['paypal.context']);
        } catch (\Exception $e) {
            throw new Exception("No payment details", 0, $e);
        }

        $state = $payment->getState();
        if ($state !== self::PAYMENT_STATE) {
            throw new Exception("Wrong payment state: \"$state\"", 0);
        }

        $total = 0.0;

        foreach ($payment->getTransactions() as $transaction) {
            $amount = $transaction->getAmount();
            $total += floatval($amount->getTotal());

            $currency = $amount->getCurrency();
            if ($currency !== self::EXPECTED_CURRENCY) {
                throw new Exception("Wrong transaction currency: \"$currency\"", 0);
            }

            foreach($transaction->getRelatedResources() as $resource) {
                $transactionState = $resource->getSale()->getState();

                if ($transactionState !== self::TRANSACTION_STATE) {
                    throw new Exception("Wrong transaction state: \"$transactionState\"", 0);
                }
            }
        }

        return $total;
    }
}