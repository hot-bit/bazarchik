<?php

namespace BZR;

use BZR\Application\NotificationTrait;
use BZR\Application\PaymentTrait;
use BZR\Application\SmsTrait;
use BZR\Application\SpotTrait;
use \Silex\Application\TranslationTrait;
use BZR\Translation\Keys;
use Symfony\Component\HttpFoundation\JsonResponse;

class Application extends \Silex\Application
{
    use TranslationTrait;
    use SpotTrait;
    use PaymentTrait;
    use NotificationTrait;
    use SmsTrait;

    const SERVER_ERROR     = 500;
    const VALIDATION_ERROR = 422;
    const NOT_FOUND_ERROR  = 404;
    const METHOD_ERROR     = 405;
    const FORBIDDEN_ERROR  = 403;
    const AUTH_ERROR       = 401;
    const PUT_GET_OK       = 200;
    const POST_OK          = 201;
    const DELETE_OK        = 204;

    const LOCALE_EN = 'En-en';
    const LOCALE_RU = 'Ru-ru';
    const LOCALE_GR = 'El-gr';

    /**
     * @var string
     */
    protected $mobileToken;
    protected $authToken;
    protected $geoPositionHeaders;
    protected $currentProfile;
    protected $favoriteUserAdv;

    /**
     * @return string
     */
    public function getMobileToken()
    {
        return $this->mobileToken;
    }

    public function getAuthToken()
    {
        return $this->authToken;
    }

    public function getGeoPosition()
    {
        return $this->geoPositionHeaders;
    }

    /**
     * @param string $token
     */
    public function setMobileToken($token)
    {
        $this->mobileToken = $token;
    }

    public function setAuthToken($token)
    {
        $this->authToken = $token;
    }

    public function setGeoPosition($geoPositionHeaders)
    {
        $this->geoPositionHeaders = $geoPositionHeaders;
    }

    /**
     * @param bool $force
     * @return \BZR\Entity\Profile|false
     */
    public function getCurrentProfile($force = false)
    {
        if (is_null($this->currentProfile) || $force) {
            $identityMapper = $this->spot()->mapper('BZR\Entity\UsersAuthMass');
            $identity = $identityMapper->first(['mobile_token' => $this->getMobileToken(), 'auth_token' => $this->getAuthToken()]);

            if ($identity) {
                $profileMapper = $this->spot()->mapper('BZR\Entity\Profile');
                $profile = $profileMapper->first(['id' => $identity->user_id]);

                if ($profile) {
                    $this->currentProfile = $profile;
                } else {
                    $this->currentProfile = false;
                }
            } else {
                $this->currentProfile = false;
            }
        }

        return $this->currentProfile;
    }

    public function getFavoriteUserAdv()
    {
        $getCurrentProfile = $this->getCurrentProfile();

        if ($getCurrentProfile) {
            $mapperFavorites = $this->spot()->mapper('BZR\Entity\Favorites');
            $entityFavorites = $mapperFavorites->where(['user_id' => $getCurrentProfile->id])->execute();

            if ($entityFavorites) {
                $this->favoriteUserAdv = $entityFavorites;
            } else {
                $this->favoriteUserAdv = false;
            }
        }

        return $this->favoriteUserAdv;
    }

    public function getAvatar($user_id)
    {
        $mapperProfile= $this->spot()->mapper('BZR\Entity\Profile');
        $avatar = null;

        $resultAvatar = $mapperProfile->first(['id' => $user_id]);

        if ($resultAvatar) {
            $avatar = $resultAvatar->avatar;
        }

        return $avatar;
    }

    public function geoPosition($geoPositionHeaders)
    {
        $mapperUsersAuthMass = $this->spot()->mapper('BZR\Entity\UsersAuthMass');

        if(!empty($geoPositionHeaders)){
            $geoPositionHeaders = explode(';', $geoPositionHeaders);

            $result = $mapperUsersAuthMass->first(['mobile_token' => $this->getMobileToken(), 'auth_token' => $this->getAuthToken(), 'lat' => $geoPositionHeaders[0], 'lng' => $geoPositionHeaders[1]]);

            if (!$result and !empty($geoPositionHeaders[0]) and !empty($geoPositionHeaders[1])) {
                $result = $mapperUsersAuthMass->first(['mobile_token' => $this->getMobileToken(), 'auth_token' => $this->getAuthToken()]);
                $result->lat = $geoPositionHeaders[0];
                $result->lng = $geoPositionHeaders[1];

                $resultUpdate = $mapperUsersAuthMass->update($result);

                if(!$resultUpdate){
                    return $this->errorResponse(Keys::ERROR_NOT_UPDATE, Keys::ERROR_NOT_UPDATE, 1, Application::NOT_FOUND_ERROR);
                }
            }
        }
    }

    public function Random($count = 10/*, $type = 1*/)
    {
        $chars = '1234567890ZYXWVUTSRQPONMLKJIHGFEDCBAzyxwvutsrqponmlkjihgfedcba';
        //if ($type == 2)
            //$chars .= '!"№;%:?*()_+=-~/\<>,.[]{}';
        $code = "";
        $clen = strlen($chars) - 1;

        while (strlen($code) < $count)
            $code .= $chars[mt_rand(0, $clen)];

        return $code;
    }

    public function passwordGeneration($password)
    {
        $salt = $this->Random(32);
        $password = sha1(sha1($password.$salt).$this['config']['secret_key']);

        return ['salt' => $salt, 'password' => $password];
    }

    public function passwordGenerationLogin($password, $email)
    {
        $mapperRow = $this->spot()->mapper('BZR\Entity\Profile');
        $result = $mapperRow->first(['email' => $email]);
        $salt = '';

        if($result){
            $salt = $result->salt;
        }

        $password = sha1(sha1($password.$salt).$this['config']['secret_key']);
        return ['salt' => $salt, 'password' => $password];
    }

    /**
     * @param null $key
     * @param null $default
     * @return mixed
     */
    public function config($key = null, $default = null)
    {
        if (is_null($key)) {
            return $this['config'];
        } else {
            return Arr::path($this['config'], $key, $default);
        }
    }

    public function giveTotalView($id)
    {
        $mapperRow = $this->spot()->mapper('BZR\Entity\AdvCounter');
        $result = $mapperRow->first(['id' => $id]);

        if($result){
            $total = $result->total + $result->today;
        } else {
            $total = 0;
        }

        return $total;
    } 
    
    public function generalResponse($result, $status = 200, $header = [])
    {
        $headers = [
            'MobileToken' => $this->getMobileToken(),
            'Geo-Position' => $this->getGeoPosition(),
            'locale' => $this["locale"]
        ];

        $headers = Arr::merge($headers, $header);
        return new JsonResponse($result, $status, $headers);
    }
    
    public function errorResponse($name, $message, $code, $status = 200, array $headers = [])
    {
        $headers = [
            'MobileToken' => $this->getMobileToken(),
            'Geo-Position' => $this->getGeoPosition(),
            'locale' => $this["locale"]
        ];

        $data = [
            'field'   => $name,
            'message' => $this->trans($message),
            'code'    => $code,
            //'status'  => $status,
        ];

        return new JsonResponse([$data], $status, $headers);
    }

    public function errorResponseArray($errors, $code, $status = 500, array $headers = [])
    {
        $arrayErrors = [];
        $i = 0;

        foreach ($errors as $error) {
            $name = preg_replace ("/[^a-zA-ZА-Яа-я0-9\s]/","", $error->getPropertyPath());

            $arrayErrors[$i] = [
            'field'    => $name,
            'message'  => ucfirst($name).'. '.$this->trans($error->getMessage())
            ];

            $i++;
        }

        $headers = [
            'MobileToken' => $this->getMobileToken(),
            'Geo-Position' => $this->getGeoPosition(),
            'locale' => $this["locale"]
        ];

        return new JsonResponse($arrayErrors, $status, $headers);
    }

    public function log($message, $level = 'info')
    {
        $logName = date("Y-m-d") . '.' . $level . '.log';
        $log     = "[" . date("Y-m-d H:i:s") . "] " . $message . "\n";
        file_put_contents($this->config('logDir') . '/' . $logName, $log, FILE_APPEND);
    }

}