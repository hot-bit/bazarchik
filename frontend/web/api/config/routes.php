<?php

/**
 * @var BZR\Application $app
 */

$app->before(function (\Symfony\Component\HttpFoundation\Request $request, \BZR\Application $app) {
    $mobileToken = $request->headers->get('MobileToken');
    $locale = $request->headers->get('locale');
    $geoPositionHeaders = $request->headers->get('Geo-Position');
    $authToken = $request->get('auth_token');

    $app->setMobileToken($mobileToken);
    $app->setAuthToken($authToken);
    $app->setGeoPosition($geoPositionHeaders);
    $app["locale"] = $locale;

    if (!empty($mobileToken) and !empty($authToken)) {
        //проверяем авторизацию
        $currentProfile = $app->getCurrentProfile();

        if ($currentProfile == false) {
            return $app->errorResponse(BZR\Translation\Keys::TOKEN_MISSING, BZR\Translation\Keys::TOKEN_MISSING, 1, \BZR\Application::AUTH_ERROR);
        }
        //обновление позиции пользователя
        $app->geoPosition($geoPositionHeaders);

        $profileMapper = $app->spot()->mapper('BZR\Entity\Profile');
        $currentProfile->locale = $locale;
        $profileMapper->save($currentProfile);
    }

});

$autResolution = function (\Symfony\Component\HttpFoundation\Request $request, \BZR\Application $app) {
    $getCurrentProfile = $app->getCurrentProfile();

    if ($getCurrentProfile == false) {
        return $app->errorResponse(BZR\Translation\Keys::TOKEN_MISSING, BZR\Translation\Keys::TOKEN_MISSING, 1, \BZR\Application::FORBIDDEN_ERROR);
    }

};

$app->post('/loginSocial', "\\BZR\\Controller\\Login::social");
$app->post('/login', "\\BZR\\Controller\\Login::login");
$app->post('/register', "\\BZR\\Controller\\Register::postRegister");

$app->get('/ad', "\\BZR\\Controller\\MainAdv::getAdv");
$app->get('/categories', "\\BZR\\Controller\\Categories::getCategories");
$app->get('/ads', "\\BZR\\Controller\\Ads::getAds");
$app->get('/ads/count', "\\BZR\\Controller\\Ads::getAdsCount");
$app->post('/initPasswordRecovery', "\\BZR\\Controller\\InitPasswordRecovery::initPasswordRecovery");
$app->get('/checkPasswordRecoveryCode', "\\BZR\\Controller\\CheckPasswordRecoveryCode::checkPasswordRecoveryCode");
$app->post('/newPasswordRecovery', "\\BZR\\Controller\\Profile::newPasswordRecovery");

$app->post('/logout', "\\BZR\\Controller\\Logout::logout")
->before($autResolution);
$app->post('/createAd', "\\BZR\\Controller\\MainAdv::postCreatAdv")
->before($autResolution);
$app->post('/editAd/{adv_id}', "\\BZR\\Controller\\MainAdv::postEditAdv")
->before($autResolution);
$app->get('/profile', "\\BZR\\Controller\\Profile::getProfile")->before($autResolution);
$app->post('/profile', "\\BZR\\Controller\\Profile::postProfile")->before($autResolution);

$app->post('/UploadImages', "\\BZR\\Controller\\UploadImages::uploadImages");
$app->get('/UploadImages', "\\BZR\\Controller\\UploadImages::uploadImages");

$app->get('/city', "\\BZR\\Controller\\City::getCity");
$app->get('/area', "\\BZR\\Controller\\Area::getArea");
$app->get('/brand', "\\BZR\\Controller\\Brand::getBrand");
$app->get('/model', "\\BZR\\Controller\\Model::getModel");


$app->get('/fieldsToEnter', "\\BZR\\Controller\\FieldsToEnter::fieldsToEnter");
$app->get('/paramFields', "\\BZR\\Controller\\ParamFields::paramFields");
$app->post('/newPassword', "\\BZR\\Controller\\Profile::newPassword")
->before($autResolution);

$app->get('/motoBrand', "\\BZR\\Controller\\MotoBrand::getMotoBrand");

$app->post('/makePayment', "\\BZR\\Controller\\Payment::pay")->before($autResolution);
$app->post('/upgradeAd', "\\BZR\\Controller\\Payment::buy")->before($autResolution);
$app->post('/upAdForFree', "\\BZR\\Controller\\Payment::upForFree")->before($autResolution);
$app->get('/priceList', "\\BZR\\Controller\\Payment::priceList")->before($autResolution);
$app->get('/myTransactions', "\\BZR\\Controller\\Payment::myTransactions")->before($autResolution);

$app->get('/user/subscriptions/isSubscribed', "\\BZR\\Controller\\Subscriptions::isSubscribed")->before($autResolution);
$app->get('/user/subscriptions/get', "\\BZR\\Controller\\Subscriptions::get")->before($autResolution);
$app->post('/user/subscriptions/set', "\\BZR\\Controller\\Subscriptions::set")->before($autResolution);
$app->post('/user/subscriptions/setMulti', "\\BZR\\Controller\\Subscriptions::setMulti")->before($autResolution);

$app->post('/user/phoneConfirmation/request', "\\BZR\\Controller\\PhoneConfirmation::request")->before($autResolution);
$app->post('/user/phoneConfirmation/confirm', "\\BZR\\Controller\\PhoneConfirmation::confirm")->before($autResolution);

$app->post('/user/emailConfirmation/request', "\\BZR\\Controller\\EmailConfirmation::request")->before($autResolution);
$app->post('/user/emailConfirmation/confirm', "\\BZR\\Controller\\EmailConfirmation::confirm")->before($autResolution);

//$app->post('/push/toggle', "\\BZR\\Controller\\PushNotifications::toggle")->before($autResolution);
$app->post('/push/addToken', "\\BZR\\Controller\\PushNotifications::addToken")->before($autResolution);

$app->post('/report/send', "\\BZR\\Controller\\Reports::sendReport");

$app->get('/MyAdv', "\\BZR\\Controller\\MyAdv::getMyAdv")
->before($autResolution);

$app->post('/FavoriteAdd', "\\BZR\\Controller\\Favorites::postFavorites")
->before($autResolution);
$app->get('/FavoriteGet', "\\BZR\\Controller\\Favorites::getFavorites")
    ->before($autResolution);
$app->post('/FavoriteDel', "\\BZR\\Controller\\Favorites::delFavorites")
    ->before($autResolution);

$app->post('/FeedBack', "\\BZR\\Controller\\FeedBack::postFeedBack");

$app->post('/ImagesDelete', "\\BZR\\Controller\\ImagesDelete::imagesDelete")->before($autResolution);

$app->post('/DeleteAdv', "\\BZR\\Controller\\MainAdv::deleteAdv")->before($autResolution);

$app->post('/FeedBackAdministration', "\\BZR\\Controller\\FeedBackAdministration::postFeedBackAdministration");

$app->get('/OneCategories', "\\BZR\\Controller\\Categories::oneCategories");


$app->get('/test', "\\BZR\\Controller\\PushNotifications::test");


$app->post('/AvatarDownload', "\\BZR\\Controller\\AvatarDownload::uploadAvatar");


$app->post('/MessageAdv', "\\BZR\\Controller\\MessageAdv::postMessageAdv");
$app->get('/MessageAdv', "\\BZR\\Controller\\MessageAdv::getMessageAdv")->before($autResolution);
$app->get('/DeleteMessageAdv', "\\BZR\\Controller\\MessageAdv::deleteMessageAdv");

$app->post('/StoppedAdv', "\\BZR\\Controller\\MainAdv::stoppedAdv")->before($autResolution);

$app->post('/RestoreAdv', "\\BZR\\Controller\\MainAdv::restoreAdv")->before($autResolution);

$app->post('/OldPostAdv', "\\BZR\\Controller\\MessageAdv::oldPostAdv")->before($autResolution);

$app->get('/news', "\\BZR\\Controller\\News::get");


