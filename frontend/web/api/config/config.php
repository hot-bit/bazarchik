<?php
/*define('DB_HOST',     'localhost');
define('DB_USER',     'bazarcy_dbadmin');
define('DB_PASSWORD', 'awNWT3KNLZwc');
mysql://bazarcy_dbadmin:awNWT3KNLZwc@localhost/bazarcy_main?charset=utf8
define('DB_NAME',   'bazarcy_main');*/

return [
    'debug' => false,
    'dsn' => 'mysql://deprog99_user_fs:fshj432gjhcYUGFE^$$#vrhgacfsd@localhost/deprog99_apibaza?charset=utf8',
    'facebook' => [
        /*'app_id'                => '1029583313777565',
        'app_secret'            => '9ccea5bbd3dc50cfad74a49c6d10b4eb',
        'default_graph_version' => 'v2.5',*/

        /*'app_id'                => '744638115676222',
        'app_secret'            => '1fe581e68ea32955f9abec374631a994',
        'default_graph_version' => 'v2.6',*/

        'app_id'                => '998179396929762',
        'app_secret'            => '58386bc523c037774ae5c3d6c63c80ea',
        'default_graph_version' => 'v2.6',

    ],
    'secret_key'          => '7.,F-YG_{<0HVlKq5yH)uC*b}}F~6?/p',
    'rootUploadImages'    => '/home/deprog99/public_html/api/api/upload/wm/',
    'rootUploadImagesWm'  => '/home/deprog99/public_html/api/api/upload/wm/',
    'rootUploadAvatar'    => '/home/deprog99/public_html/api/api/upload/avatar/',
    'stampImageWaterMark' => '/home/deprog99/public_html/api/api/upload/bazar_logo_sm.png',
    'toDay'    => date('Y_m_d'),
    'simmilar' => '4',
    'google_plus' => [
        'client_id'          => '423574760807-1dvj22igdh526pd5tu0l6rbu0hd06a96.apps.googleusercontent.com',
        'client_secret'      => 'nGlWzKca25VsfsW6_YTVARS3',
        'application_name'   => 'GoogleAUT',
    ],
    'urlPictures'            => 'https://www.bazar-cy.com/uploads/images/',
    'urlPicturesWm'          => 'https://www.bazar-cy.com/uploads/wm/',
    'urlPicturesTest'        => 'http://api.deprog.ru/api/upload/wm/',
    'urlPicturesAvatar'      => 'http://api.deprog.ru/api/upload/avatar/',
    'rootIconMainCategories' => 'http://api.deprog.ru/api/upload/icons/',
    'imagesPx'   => [
        'L'        => 1200,
        'M'        => 700,
        'S'        => 230,
        'realName' => 640,
        'avatar'   => 300,
    ],

    'validation' => [
        'name'          => ['length' => 5],
        'phone'         => ['length' => 5],
        'b_day'         => ['length' => 1],
        'b_month'       => ['length' => 1],
        'b_year'        => ['length' => 1],
        'skype'         => ['length' => 5],
        'type'          => ['length' => 1],
        'city'          => ['length' => 1],
        'area'          => ['length' => 1],
        'avatar'        => ['length' => 5],
        'password'      => ['length' => 5],
        'category'      => ['length' => 1],
        'title'         => ['length' => 5],
        'text'          => ['length' => 1],
        'price'         => ['length' => 2],
        'adv_id'        => ['length' => 1],
        'id'            => ['length' => 1],
        'account_type'  => ['length' => 1],
        'code'          => ['length' => 32],
        'mimeTypeImage' => [
            'maxSize'   => 200000,
            'mimeTypes' => ['image/png', 'image/jpeg']
        ],

    ],
    'validationCorrector' => [
        'email'     => ['This email address is already using'],
        'phone'     => ['This phone is already using'],
    ],
    'initPasswordRecovery' => [
        'text'         => "Your personal code to restore password :",
        'header_text'  => 'Recover password',
        'timeWait'  => 600,
    ],
    'postFeedBack' => [
        'title'           => "Cyprus bazar: request from your adv #",
        'email_FeedBack'  => 'killme005@mail.ru',
    ],
    'postFeedBackAdministration' => [
        'title'           => "Cyprus bazar: administration missage",
        'email_FeedBack'  => 'killme005@mail.ru',
    ],
    "paypal" => [
        "clientId"     => "AZIZg4MmioQtB7LIuH_ZEEd96z_kvO-EszhlML2_liC3zuHHYPIrSHjBc8BhVdX7WuxzAe-QMEQucSm7",
        "clientSecret" => "EJy5k60QQzOkE0JdQ_L8qP7GnsmTeZXUnAK1p27mgL0DNmt_pBhvHUcipET3TFTk7jiXHhbAnb7hbkr2"
    ],
    "price" => [
        'color' => 2,
        'up' => 1,
        'vip' => 4,
        'premium' => 5
    ],
    'notifications' => [
        'apnsCertificatePath' => __DIR__ . '/testCyprusBazar.pem',
        'apnsPassPhrase' => 'example',
        'gcnApiToken' => 'AIzaSyCEpVqX-uSFxPoSxllMfuG2A4JCO8g782A',
        'message' => [
            'newAdvert'          => 'newAdvertNotification',
            'changeAdvertStatus' => 'changeStatusNotification',
            'newMessage'         => 'newMessageNotification',
        ]
    ],
    'sms' => [
        'commtor' => [
            'url'         => 'http://www.commtor.com/webapi/AccountApi/PostMessage',
            'authKey'     => 'x459klSSS34saTa29',
            'header'      => 'CyprusBazar',
            'countryCode' => 357
            //'postData' => 'AuthKey=&Telephone=%phone%&Header=&Body=%message%'
        ],
        'confirmationMessage' => 'phoneConfirmationMessage'
    ],
    'email' => [
        'confirmationMessage' => 'emailConfirmationMessage',
        'confirmationMessageSubject' => 'emailConfirmationMessageSubject',
        'confirmationMessageHeaders' => "MIME-Version: 1.0\r\n"
            . "Content-type: text/html; charset=UTF-8\r\n"
            . "From: website@bazar-cy.com\r\n"
            . "Reply-To: www.bazar-cy.com\r\n"
            . "X-Mailer: PHP/" . phpversion()
    ],
    'site' => [
        'advUrl' => 'https://www.bazar-cy.com/%lang%/adv/%url%_%id%/',
        'langs'  => [
            'En-en' => 'en',
            'Ru-ru' => 'ru',
            'El-gr' => 'gr',
        ]
    ],
    'logDir' => __DIR__ . '/../log/'
];