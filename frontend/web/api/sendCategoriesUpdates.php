<?php

require_once __DIR__ . '/bootstrap.php';

/**
 * @var \BZR\Entity\Mapper\UsersCategoriesSubscription $subscriptionMapper
 * @var \BZR\Entity\Mapper\Categories $categoriesMapper
 */
$updatesMapper      = $app->spot()->mapper('BZR\Entity\CategoryUpdate');
$categoriesMapper   = $app->spot()->mapper('BZR\Entity\Categories');
$subscriptionMapper = $app->spot()->mapper('BZR\Entity\UsersCategoriesSubscription');

/**
 * @var \BZR\Entity\CategoryUpdate[] $updates
 */
$updates = $updatesMapper->where(['count >' => 0])->execute();

foreach ($updates as $update) {
    /**
     * @var \BZR\Entity\Categories $category
     */
    $category = $categoriesMapper->get($update->category_id);

    if ($category) {
        $categoryId     = $category->id;
        $parentCategory = $categoriesMapper->getParentCategory($category);

        $userIdList = $subscriptionMapper->getSubscribedUserIdList($category->id, $parentCategory ? $parentCategory->id : false);

        foreach ($userIdList as $locale => $list) {
            $categoryName = $category->getTranslatedName($locale);
            $messageTranslationKey = $app->config(['notifications', 'message', 'newAdvert']);

            $message = str_replace(
                ["%category%", "%count%"],
                [$categoryName, $update->count],
                $app->trans($messageTranslationKey, [], 'messages', $locale)
            );

            try {
                $app->notification()->sendToUsers($list, $message, ['categoryId' => $category->id, 'type' => 'updateCategory']);
            } catch (\Exception $e) {
                //TODO logging
            }
        }
    }

    $update->clear();
    $updatesMapper->save($update);
}

