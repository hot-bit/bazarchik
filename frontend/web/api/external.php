<?php

require_once __DIR__ . '/bootstrap.php';

class ApiGateway
{
    /**
     * @var \BZR\Application
     */
    protected $app;

    /**
     * ApiGateway constructor.
     * @param \BZR\Application $app
     */
    public function __construct(\BZR\Application $app)
    {
        $this->app = $app;
        $app->boot();
    }

    /**
     * @param int $advId
     * @param int $userId
     * @param int $status
     */
    public function changeAdvStatus($advId, $userId, $status)
    {
        $app = $this->app;
        try {
            $profileMapper = $app->spot()->mapper('\BZR\Entity\Profile');
            $user = $profileMapper->get($userId);

            if ($user) {
                $messageTranslationKey = $app->config(['notifications', 'message', 'changeAdvertStatus']);
                $message = $app->trans($messageTranslationKey, [], 'messages', $user->locale);

                $app->notification()->sendToUsers([$user->id], $message, ['advId' => $advId, 'type' => 'changeStatus']);
            }
        }
        catch (\Exception $e) {
            //TODO logging
        }
    }

    /**
     * @param int $categoryId
     * @param int $userId
     */
    public function addNewAdv($categoryId, $userId)
    {
        $app = $this->app;
        try {
            /**
             * @var \BZR\Entity\Mapper\Categories $categoriesMapper
             */
            $categoriesMapper = $app->spot()->mapper('\BZR\Entity\Categories');
            $categoriesMapper->updateCategory($categoryId);
        } catch (\Exception $e) {
            //TODO logging
        }
    }

    public function logAdvUpgrade($type, $advId)
    {
        $app = $this->app;
        try {
            $paymentsMapper = $app->spot()->mapper('\BZR\Entity\Payments');
            $keyTypeTranslationKey = $paymentsMapper->getTypeTranslationKey($type);

            $advMapper = $app->spot()->mapper('\BZR\Entity\Adv');
            $enityAdv = $advMapper->first(['id' => $advId]);

            $paymentsEntity = $paymentsMapper->build([
                'user_id'      => $enityAdv->user,
                'type_payment' => $keyTypeTranslationKey,
                'id_payment'   => $app->Random(),
                'price'        => $app['config'][$type],
                'date'         => time()
            ]);

            $paymentsMapper->insert($paymentsEntity);

        } catch (\Exception $e) {
            //TODO logging
        }
    }
}

global $API_GATEWAY;
$API_GATEWAY = new ApiGateway($app);