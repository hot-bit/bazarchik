var Vue = require('vue/dist/vue.min');
var VueResource = require('vue-resource');
var VueI18n  = require('vue-i18n');
var checkbox = require('./components/checkbox.vue');
var radioButton = require('./components/radio-button.vue');
var multiRow = require('./components/multi-row.vue');
var favorite = require('./components/favorite.vue');
var carBody = require('./components/car-body.vue');
var carColor = require('./components/car-color.vue');
var SendSmsCode = require('./components/send-sms-code.vue');
var UploadImages = require('./components/upload-images.vue');
var SmsLogin = require('./components/sms-login.vue');
var AdminAdvTitle = require('./components/admin/adv-title.vue');
var AdminModerateAd = require('./components/admin/moderate-ad.vue');
var ProfileAddress = require('./components/profile-address.vue');
var ShopMap = require('./components/shop-map.vue');
var Chat = require('./components/chat.vue');
const VueGoogleMap = require('vue2-google-maps');

Vue.use(VueResource);
Vue.use(VueGoogleMap, {
    load: {
        key: 'AIzaSyAacC73p-D8Cf0qOscGIcvty5AwvtITejM',
        // libraries: 'places', // This is required if you use the Autocomplete plugin
        // OR: libraries: 'places,drawing'
        // OR: libraries: 'places,drawing,visualization'
        // (as you require)
    }
});


new Vue({
    el: '.vue-block',
    data: function () {
       return {
           login_type: 0,
           user_type: 0,
           favorite_count: 0,
           adminAdvTitle: '',
           adminAdvUrl: '',
       }
    },
    components: {
        'checkbox': checkbox,
        'radio-button': radioButton,
        'multi-row': multiRow,
        'favorite': favorite,
        'car-body': carBody,
        'car-color': carColor,
        // 'google-map': VueGoogleMap.Map,
        'send-sms-code':SendSmsCode,
        'upload-images':UploadImages,
        'sms-login':SmsLogin,
        'admin-adv-title':AdminAdvTitle,
        'profile-address':ProfileAddress,
        'admin-moderate-ad':AdminModerateAd,
        'shop-map':ShopMap,
        'chat':Chat
    },
    methods: {
        setGlobalUserType: function (id) {
            this.user_type = id;
        },
        incrementFavorite: function () {
            this.favorite_count++;
        },
        decrementFavorite: function () {
            this.favorite_count--;
        },
        totalFavorite: function () {
            this.$resource('/ajax/favorite-total-count/').get().then(function(response) {
                this.favorite_count = response.data;
            }, function(error) {
                // ошибка
            })
        }

    },
    created: function () {
        this.totalFavorite()
    }
});