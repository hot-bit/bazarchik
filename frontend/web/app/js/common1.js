$(function () {

    // var count = document.getElementsByClassName('svg').length;

    var c = 0;
    jQuery('img.svg').each(function () {
        // if(c == count) return;
        var $img = jQuery(this);
        var imgID = $img.attr('id');
        var imgClass = $img.attr('class');
        var imgURL = $img.attr('src');

        jQuery.get(imgURL, function (data) {
            // Get the SVG tag, ignore the rest
            var $svg = jQuery(data).find('svg');

            // Add replaced image's ID to the new SVG
            if (typeof imgID !== 'undefined') {
                $svg = $svg.attr('id', imgID);
            }
            // Add replaced image's classes to the new SVG
            if (typeof imgClass !== 'undefined') {
                $svg = $svg.attr('class', imgClass + ' replaced-svg');
            }

            // Remove any invalid XML tags as per http://validator.w3.org
            $svg = $svg.removeAttr('xmlns:a');

            // Replace image with new SVG
            $img.replaceWith($svg);
        }, 'xml');
        c++;
    });


// Подключение jQuery Form Style
    $(function () {

        $('select.styled').styler();
        $('select.styled2').styler();
        // $('input, select').styler();
        $('input.checkbox-dstr').styler('destroy');
        $('select.sl-search').styler('destroy');
        $('input.sl-chb').styler('destroy');
        $('input.sl-chb__category').styler('destroy');
        $('input.sl-chb__active').styler('destroy');
        $('.transmision-field input').styler('destroy');
        $('.color-field input').styler('destroy');
        $('input.sec-parametr__radio').styler('destroy');
        $('input.param-cb').styler('destroy');
        $('input#iorange').styler('destroy');
        $('input.purchase-cb').styler('destroy');
        $('input#setting-lc').styler('destroy');
        $('input#setting-business').styler('destroy');
        $('input.register-checkbox').styler('destroy');
        $('#prefix-mobile').styler('destroy');
        $('.mobile-dropdown__absolute select').styler('destroy');
        $('#show-email').styler('destroy');
        $('#show-skype').styler('destroy');


    });


    $('.search-bottom__left label').click(function (e) {

        if ($('input', this).is(':checked')) {
            $(this).css('color', '#0f0f0f');
        } else {
            $(this).css('color', '#c1c1c1');
        }

    });

// Ширина для DropDown
    if (document.getElementsByClassName('home_body')[0]) {
        var FindCategoryWidth = document.getElementsByClassName('find-category')[0].clientWidth;
        var FindCategoryDropDown = document.getElementsByClassName('find-dropdown__content');

        FindCategoryWidth = FindCategoryWidth - 80;
        for (var i = 0; i < FindCategoryDropDown.length; i++) {
            FindCategoryDropDown[i].style.width = FindCategoryWidth + 'px';
        }

        var blocks = document.querySelectorAll('.find-category__large .find-category__dropdown');
        for (var j = 0; j < blocks.length; j++) {
            blocks[j].style.width = FindCategoryWidth + 'px';
        }

        var blocksSmall = document.querySelectorAll('.find-category__small .find-category__dropdown');
        for (var s = 0; s < blocksSmall.length; s++) {
            blocksSmall[s].style.width = FindCategoryWidth + 'px';
        }

    }


//  Вывод категории для отдельных страниц
    $('.find-category__item').on('click', function () {
        var to_close = $(this).children('.find-category__dropdown');
        to_close.toggleClass('hidden');

        $(document).mouseup(function (e) { // событие клика по веб-документу
            $('.find-category__dropdown').addClass('hidden');
        });

    });


// Style to placeholder
    if (document.body.clientWidth < 640) {
        var placeholder = document.getElementsByClassName('search_input')[0];
        placeholder.placeholder = 'Я ищу';
    }


// Вывод городов и категорий
    var TextCategory = document.getElementById('text-category');
    TextCity = document.getElementById('cityList');


    $('input.sl-chb__category').on('click', function () {
        $('#categorySelected').html($('input.sl-chb__category:checked').val());
        if (TextCategory.innerHTML == 'Был выбран undefined') {
            TextCategory.innerHTML = 'Выберите категорию';
        }
    });

    $('input.sl-chb').on('click', function () {
        $('#cityList').html($('input:checked').val());
        if (TextCity.innerHTML == 'Был выбран undefined') {
            TextCity.innerHTML = 'Выберите город';
        }
        var arr = $('input.sl-chb:checked').map(function () {
            return this.value;
        }).get();
        if (arr.length >= 2) {
            $('#cityList').html('Выбрано городов : ' + arr.length);
        }
    });


// Показ ЛК для больших экранов
    $('.auth-login__yes').on('click', function () {
        if ($(this).attr('opened') == 0) {
            var $loginUser = $('.login-user'); // сохраним попап в переменную

            if ($loginUser.data('loading')) return; // если уже загружается, ничего не делаем
            $loginUser
                .data('loading', true) // указываем, что попап загружается
                .show("slide", {direction: "left"}, 1000, function () {
                    $(this).data('loading', false) // попап загрузился
                });
            $(this).attr('opened', '1');
        }
    });


// END показ меню для больших экранов

// Показ меню для средних экранов


    // $('.mobile-burger').on('click', function () {
    //
    //     console.log('burger');
    //
    //     var $loginUser = $('.login-user'); // сохраним попап в переменную
    //
    //     if ($loginUser.data('loading')) return; // если уже загружается, ничего не делаем
    //
    //     $loginUser
    //         .data('loading', true) // указываем, что попап загружается
    //         .show("slide", {direction: "left"}, 1000, function () {
    //             $(this).data('loading', false) // попап загрузился
    //         });
    // });

    $(document).mouseup(function (e) { // событие клика по веб-документу
        var div = $(".login-user"); // тут указываем ID элемента
        if (div.data('loading')) return; // если загружается, ничего не делаем

        if (!div.is(e.target) // если клик был не по нашему блоку
            && div.has(e.target).length === 0) { // и не по его дочерним элементам
            div.hide("slide", {direction: "left"}, 1000);
        }
        if (div.is(':hidden')) {
            $('.auth-login__yes').attr('opened', '0')
        }
    });

// END показ меню

// Убираем чекбоксы

    $(document).mouseup(function (e) { // событие клика по веб-документу
        var div = $("#checkboxes"); // тут указываем ID элемента
        if (!div.is(e.target) // если клик был не по нашему блоку
            && div.has(e.target).length === 0) { // и не по его дочерним элементам
            div.hide();
        }
    });


// END чекбосквы

// Выдача класса для категорий 
    /*
    $(document).on('click', 'li.ads-category__link', function() {
      $('li.ads-category__link').each(function(idx, elem) {
        if($(elem).has('active')) {
          $(elem).removeClass('active');
        }
      });
        $(this).addClass('active');
        var CategorySubmenu = $(this).parent().parent().next();
        CategorySubmenu.css('display', 'block');
    });

    $(document).on('click', 'li.category-submenu__link', function() {
      $('li.category-submenu__link').each(function(idx, elem) {
        if($(elem).has('active')) {
          $(elem).removeClass('active');
        }
      });
        $(this).addClass('active');
        var SubmenuDrpd = $(this).parent().parent().next();
        SubmenuDrpd.css('display', 'block');
    });


    $(document).on('click', 'li.submenu-drpd__link', function() {
      $('li.submenu-drpd__link').each(function(idx, elem) {
        if($(elem).has('active')) {
          $(elem).removeClass('active');
        }
      });
        $(this).addClass('active');
        var DrpdLast = $(this).parent().parent().next();
        DrpdLast.css('display', 'block')

    });


    $(document).on('click', 'li.drpd-last__link', function() {
      $('li.drpd-last__link').each(function(idx, elem) {
        if($(elem).has('active')) {
          $(elem).removeClass('active');
        }
      });
      $(this).addClass('active');
    });
    */

// Background-Color for body car 


    $('.transmision-field input').on('click', function () {
        $('.transmision-field').css('background-color', '#fff');
        $(this).parent().css('background-color', '#74a90f');
        $(this).parent().siblings('.transmission-field__title').css('color', '#74a90f');
    });

// Выбор цвет

    $('.color-field input').on('click', function () {
        $('.color-field').css('opacity', '0.2');
        $(this).parent().css('opacity', '1');
    });

// Input type range
    $("#iorange").ionRangeSlider({
        min: 0,
        max: 6,
        step: 0.1
    });

    $('#rangedouble').ionRangeSlider({
        type: 'double',
        min: 0,
        max: 6,
        step: 0.1
    });
//  END type range


    $('.add-detailes__title').on('click', function () {
        $('.add-detailes').toggleClass('hidden');
        $('.ads-detailes__open').toggleClass('hidden');
        $('.ads-detailes__closed').toggleClass('hidden');
    });


    $('.sec-parametr__title').on('click', function () {
        $('.secundar-parametrs').toggleClass('hidden');
        $('.sec-parametr__open').toggleClass('hidden');
        $('.sec-parametr__closed').toggleClass('hidden');
    });


    $('.open-subcategory').on('click', function () {
        $('.sub-category__all').toggleClass('hidden');
        $('.subcategory__open').toggleClass('hidden');
        $('.subcategory__closed  ').toggleClass('hidden');
        $('.sub-category__preview').toggleClass('hidden');
    });


    $('.faq-question').on('click', function () {
        $(this).next().toggle('slow');
        $(this).children('.faq-question__closed').toggleClass('hidden');
        $(this).children('.faq-question__opened').toggleClass('hidden');
    });

    $('.find-price__title').on('click', function () {
        $('.find-price__fields').toggleClass('hidden');
    });


    $('.newads-bottom__like').on('click', function () {
        $(this).toggleClass('active');
        if ($(this).hasClass('active')) {
            $(this).children().attr('src', 'img/heart-galery.svg')
        }
        else {
            $(this).children().attr('src', 'img/likestroke.svg')
        }
    });


    $('.greenStyle').change(function () {
        if ($(this).text() != null) {
            $(this).css('border-color', '#669708');
            $(this).prev().css('color', '#669708')
        }
    });


// Запись количество
    $('input.sl-chb__active').on('click', function () {
        var arr = $('input.sl-chb__active:checked').map(function () {
            return this.value;
        }).get();
        $('#cityListActive').html(arr[0]);
        if (arr.length >= 2) {
            $('#cityListActive').html('Выбрано городов : ' + arr.length);
        }
        if (arr.length == 0) {
            $('#cityListActive').html('Все города');
        }
    });

    $('.catalog-to__list').on('click', function () {
        $('.principaly-newads__container').addClass('listen');
    });

    $('.catalog-to__block').on('click', function () {
        $('.principaly-newads__container').removeClass('listen');
    });

    $('.filter-for__md').on('click', function () {
        $('.sidebar-bottom__small').show("slide", {direction: "right"});
        $('.sidebar-small__title').css('display', 'block');
    });


    $('.close-sidebar__small').on('click', function () {
        $('.sidebar-bottom__small').hide("slide", {direction: "right"});
        $('.login-user').hide("slide", {direction: "left"}, 1000);
    });

    var mySwiper = new Swiper('.swiper-container', {
        // Optional parameters
        loop: true,
        slidesPerView: 6,
        spaceBetween: 10
    });


    var PrinSwiper = new Swiper('.newsads-sliders', {
        loop: true,
        slidesPerView: 4,
        spaceBetween: 60,
        nextButton: '.newsads-right',
        prevButton: '.newsads-left',
        breakpoints: {
            992: {
                slidesPerView: 2,
                spaceBetween: 50
            },
            640: {
                slidesPerView: 1
            },
        }
    });


    if ($(window).width() < 995 && document.getElementsByClassName('home_body')[0]) {
        $('.principaly-newads__content').addClass('swiper-wrapper');
        $('.principaly-newads__container').addClass('swiper-slide');
        var mySwiperPrincipal = new Swiper('.slider-small', {
            loop: true,
            spaceBetween: 80,
            nextButton: '.right-arrow',
            prevButton: '.left-arrow',
            slidesPerView: 2,
            breakpoints: {
                640: {
                    slidesPerView: 1
                },

            }
        });
    }


// Показ доп. параметров
    $('.detailes-all__title').on('click', function () {
        $(this).children('.detailes-all__opened').toggleClass('hidden');
        $(this).children('.detailes-all__closed').toggleClass('hidden');
        $(this).siblings('.item-detailes__other').toggle('slow');
    });


// $('.item-contacts__title').on('click',function(){
//   $(this).next().toggle('slow');
//   $(this).parent().parent().css('top', (isOpen ? '70%' : 0)); // В зависимости от состояния задаем позицию
// })

    var daOpen = false; // Задаем переменную в которой будем хранить состояние

    $('.item-contacts__title').on('click', function () {
        $(this).next().toggle('slow');
        $(this).parent().parent().css('top', (daOpen ? '90%' : 0));
        daOpen = !daOpen; // Меняем состояние на противоположное
    });


    $('.ads__actions').on('click', function () {
        $(this).next().toggle('slow');
    });


// Кнопка подробнее

    $('.purchase-about').on('click', function () {
        $(this).next().toggle('slow'),
            $(this).children('.purchase-about__opened').toggleClass('hidden');
        $(this).children('.purchase-about__closed').toggleClass('hidden');
    });

// Добавляем адресс
    $('.add-address').on('click', function () {
        $('.setting-adress__inputs').append('<input type="text" class="greenStyle" placeholder="" name="setting-address">')
    });


    var swip = new Swiper('.photo-general__content', {
        loop: true,
        nextButton: '.newsads-right',
        prevButton: '.newsads-left'
    });


    var SwipThumb = new Swiper('.swiper-for-thumbs', {
        slidesPerView: 7,
        spaceBetween: 5
    });


    var windowsHeight = $(window).height();
    var bodyHeight = $('body').height();

    if (bodyHeight < windowsHeight) {
        $('body').css('min-height', '100vh');
        $('footer').addClass('to-bottom');
    }

    $('.filter-mobile .sort').on('click', function () {
        $(this).parent().toggleClass('active');
    })

});


