$('.sub-category__more-button').click(function(){
    $('.sub-category').addClass('active');
    return false;
});$('.sub-category__less-button').click(function(){
    $('.sub-category').removeClass('active');
    return false;
});


$('#catalog-filter-form').on('change', 'input, select', function () {
   reloadFilter();
});

function reloadFilter(){
    var form = $('#catalog-filter-form');
    $.post(form.attr('filter-request'), form.serialize(), function (o) {
        // var o = JSON.parse(ret);
        $('.sidebar-find-button').text(o.count);

        var config = {
            range: {
                min: o.min,
                max: o.max
            }
        };

        var slider = document.getElementById('price-range');
        slider.noUiSlider.updateOptions(config);
    });
}