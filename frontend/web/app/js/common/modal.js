/**
 * Created by York on 27.09.2017.
 */

$(document).ready(function () {
    if (window.location.hash.indexOf("#modal-")) {
        $(window.location.hash).show();
    }

    $('[data-sideup]').magnificPopup({
        type: 'inline',
        mainClass: 'remove-overlay',
        showCloseBtn: false
    });
    $('[data-modal-close]').click(function(){
        $.magnificPopup.instance.close()
    });
});


$('body').on('click', '[data-modal]', function () {
    var modal = $(this).attr('data-modal');

    $('#' + modal).show();
});

$('body').on('click', '[data-modal-close]', function () {
    var modal = $(this).parents('.modal').attr('id');

    $('.modal').hide();
});

$('.open-popup-link').magnificPopup({
    type:'inline',
    midClick: true // Allow opening popup on middle mouse click. Always set it to true if you don't provide alternative source in href.
});

$('[data-gallery]').magnificPopup({
    type: 'image',
    delegate: '[data-image]',
    gallery: {
        enabled: true,
        preload: [0,1]
    }
});
$('[data-popup]').magnificPopup({
    type: 'inline',
    midClick: true // Allow opening popup on middle mouse click. Always set it to true if you don't provide alternative source in href.
});


$(document).mouseup(function (e) { // событие клика по веб-документу
    var div = $(".modal"); // тут указываем ID элемента
    if (!div.is(e.target) // если клик был не по нашему блоку
        && div.has(e.target).length === 0) { // и не по его дочерним элементам
        div.hide(); // скрываем его
    }
});
