/**
 * Created by York on 28.09.2017.
 */

$(document).ready(function(){

// Настройка fileUploader
    var dist_path = $('#dist_path').val();
    var file = $('.uploader').fileuploader({
        addMore: true,
        // theme: 'One-button',
        thumbnails: {
            // thumbnails list HTML {String, Function}
            // example: '<ul></ul>'
            // example: function(options) { return '<ul></ul>'; }
            box: '<div class="fileuploader-items">' +
            '<ul class="fileuploader-items-list"></ul>' +
            '</div>',

            // append thumbnails list to selector {null, String, jQuery Object}
            // example: 'body'
            // example: $('body')
            boxAppendTo: null,

            // thumbnails item HTML {String, Function}
            // example: '<li>${name}</li>'
            // example: function(item) { return '<li>' + item.name + '</li>'; }
            item: '<li class="fileuploader-item">' +
                '<div class="columns">' +
                    '<div class="column-thumbnail">${image}</div>' +
                    '<div class="column-title">' +
                        '<div title="${name}">${name}</div>' +
                        '<span>${size2}</span>' +
                    '</div>' +
                    '<div class="column-actions">' +
                        '<div class="star-upload"><img src="'+dist_path+'/img/star.svg" class="svg star"></div>' +
                        '<div class="turnleft"><img src="'+dist_path+'/img/turnleft.svg" class="svg turnleft"></div>' +
                        '<div class="turnright"><img src="'+dist_path+'/img/turnright.svg" class="svg turnright"></div>' +
                        '<div class="remoove"><a class="fileuploader-action fileuploader-action-remove" title="Remove">&times;<i></i></a></div>' +
                    '</div>' +
                '</div>' +
                '<div class="progress-bar2">${progressBar}<span></span></div>' +
            '</li>',
            canvasImage: {
                width: 200,
                height: 165
            },

        },
        listInput: true,
        enableApi: true,
        upload: {
            // url: '/frontend/web/adv/upload-image',
            // enctype: 'text/plain',
            beforeSend: function(item, listEl, parentEl, newInputEl, inputEl) {
                // example:
                // here you can extend the upload data
                item.upload.data.new_data_attribute = 'nice';

                // example:
                // here you can create upload headers
                // item.upload.headers = {
                //     "Authorization": "Basic " + btoa(username + ":" + password)
                // };

                return true;
            },
        },

        captions: {
            button: function(options) { return $('.upload-photo__container').attr('data-file-uploader-button'); },
        }
    });

    var api = $.fileuploader.getInstance($('.uploader'));
// Загрузка изображений
    $('.btn_upload_images').click(function () {
        api.uploadStart();
    });


// Добавление класса для звезды фотографии

    $(document).on('click', 'img.star' ,function(){
        $(this).attr('src', dist_path+'/img/star-active.svg')
    })



// Прокрутка фотографии

    // var ForCurentDeg = 0;
    //
    // $(document).on('click', 'img.turnright' ,function(){
    //     ForCurentDeg += 90;
    //     var ImageCanvas = $(this).parent().parent().siblings('.column-thumbnail').children().children();
    //     var RotateRight = 'rotate' + '(' +ForCurentDeg + 'deg' + ')';
    //     ImageCanvas.css('transform',RotateRight);
    // })
    //
    // $(document).on('click', 'img.turnleft' ,function(){
    //     ForCurentDeg -= 90;
    //     var ImageCanvas = $(this).parent().parent().siblings('.column-thumbnail').children().children();
    //     var RotateLeft = 'rotate' + '(' +ForCurentDeg + 'deg' + ')';
    //     ImageCanvas.css('transform',RotateLeft);
    // })
// END прокрутка фотографии
});
