/**
 * Created by York on 19.09.2017.
 */

$(document).ready(function(){

    moment.locale($('#language').val());
    $('[data-time]').each(function(){
        var time = $(this).attr('data-time');
        $(this).text(moment.unix(time).fromNow());
    });
});
