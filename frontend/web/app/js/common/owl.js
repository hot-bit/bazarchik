
$(document).ready(function(){

    var nav_icons = [
        '<div class="icon ion-ios-arrow-left"></div>',
        '<div class="icon ion-ios-arrow-right"></div>',
    ];
    $('.owl-carousel-image').owlCarousel({
        thumbs: true,
        nav: false,
        loop: false,
        nav_icons: ['', ''],
        thumbsPrerendered: true,
        items: 1,
    });

    $('.owl-carousel-catalog').owlCarousel({
        loop: true,
        items: 4,
        nav: true,
        navText: nav_icons,
        responsive:{
            0 : {
                items: 1,
            },
            480: {
                items: 2,
            },
            800: {
                items: 3
            },
            1200: {
                items: 4
            }
        }
    });
});