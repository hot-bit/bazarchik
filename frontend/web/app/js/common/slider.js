$(function () {
    // import noUiSlider from 'nouislider';

    var sliders = document.getElementsByClassName('range-slider');

    for ( var i = 0; i < sliders.length; i++ ) {
        var self = sliders[i];

        var startMin = parseInt($(self).attr('data-value_from'));
        var startMax = parseInt($(self).attr('data-value_to'));
        var rangeMin = parseInt($(self).attr('data-min'));
        var rangeMax = parseInt($(self).attr('data-max'));

        var inputMin = document.getElementById($(self).attr('data-f1'));
        var inputMax = document.getElementById($(self).attr('data-f2'));

        var start = [startMin, startMax];
        var connect = true;
        var step = parseInt($(self).attr('data-step'));

        noUiSlider.create(self, {
            start: start,
            step:step,
            connect: connect,
            range: {
                min: rangeMin,
                max: rangeMax
            }
        });

        self.noUiSlider.on('update', function ( values, handle ) {
            var val = parseInt(values[handle]);
            if ( handle ) {
                 $(inputMax).val(val);
            }else {
                 $(inputMin).val(val);

            }
        });

        inputMin.addEventListener('change', function () {
            self.noUiSlider.set([this.value, null]);
        });
        inputMax.addEventListener('change', function () {
            self.noUiSlider.set([null, this.value]);
        });
    }



    // });


});