// // Попап окна для категории
$('.find-category__item').on('click', function () {
    var to_close = $(this).next('.find-category__dropdown');
    to_close.toggleClass('hidden');

});
// Закрыть попап окно для категории
$(document).mouseup(function (e) { // событие клика по веб-документу
    var ForDropDown = $('.find-category__dropdown').parent().parent();
    $('.find-category__dropdown').addClass('hidden');
});

// Ширина для DropDown
if (document.getElementsByClassName('home_body')[0]) {
    var FindCategoryWidth = document.getElementsByClassName('find-category')[0].clientWidth;
    var FindCategoryDropDown = document.getElementsByClassName('find-dropdown__content');

    FindCategoryWidth = FindCategoryWidth - 80;
    for (var i = 0; i < FindCategoryDropDown.length; i++) {
        FindCategoryDropDown[i].style.width = FindCategoryWidth + 'px';
    }

    var blocks = document.querySelectorAll('.find-category__large .find-category__dropdown');
    for (var j = 0; j < blocks.length; j++) {
        blocks[j].style.width = FindCategoryWidth + 'px';
    }

    var blocksSmall = document.querySelectorAll('.find-category__small .find-category__dropdown');
    for (var s = 0; s < blocksSmall.length; s++) {
        blocksSmall[s].style.width = FindCategoryWidth + 'px';
    }

}