$(document).ready(function () {
    $('.SelectBox').SumoSelect({
        okCancelInMulti: false,
        placeholder: 'Select Here',
        captionFormat: '{0} Selected',
        noMatch : 'No matches for "{0}"',
        captionFormatAllSelected: 'All',
    });
    $('.SelectBox-search').SumoSelect({
        okCancelInMulti: false,
        search: true,
        searchText: 'Enter here',
        placeholder: 'Select Here',
        captionFormat: '{0} Selected',
        noMatch : 'No matches for "{0}"',
        captionFormatAllSelected: 'All',
    });

    $('.SelectBox-noborder').SumoSelect({
        okCancelInMulti: false,
        search: true,
        searchText: 'Enter here',
        placeholder: 'Select Here',
        captionFormat: '{0} Selected',
        noMatch : 'No matches for "{0}"',
        captionFormatAllSelected: 'All',
    });

    $('select').SumoSelect({
        okCancelInMulti: false,
        placeholder: 'Select Here',
        captionFormat: '{0} Selected',
        noMatch : 'No matches for "{0}"',
        captionFormatAllSelected: 'All',
    });
});