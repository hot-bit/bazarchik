/**
 * Created by York on 28.09.2017.
 */

$('[data-toggle]').click(function () {

    var attr = $(this).attr('data-toggle');

    $('#'+attr).toggleClass('active');

});