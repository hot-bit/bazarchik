<?php

/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */
/* @var $model \common\models\LoginForm */

use common\models\CMS;
use kosv\ulogin\widget\UloginWidget;
use yii\helpers\Html;
use yii\bootstrap\ActiveForm;

$this->title = Yii::t('app', 'Login');
Yii::$app->breadcrumbs->Items([
    $this->title
]);
?>
<section class="content site-login">

    <div class="row">
        <div class="col-sm-6 col-sm-push-3">
            <h1><?= Html::encode($this->title) ?></h1>

            <div>
                <a v-show="login_type == 0" class="link"
                   @click="login_type = 1"><?= Yii::t('app', 'Login with phone') ?></a>
                <a v-show="login_type == 1" class="link"
                   @click="login_type = 0"><?= Yii::t('app', 'Login with email') ?></a>
<!--                --><?php //echo UloginWidget::widget([
//                'options' => [
//                    'display' => 'panel',
//                    'fields' => ['first_name', 'last_name', 'email', 'photo'],
//                    'providers' => ['google', 'facebook'],
////                    'callback' => 'authCallback', //Ваш js callback, который будет вызыватся для отправки данных в контроллер
//                    'redirect_uri' => \yii\helpers\Url::base('https').\yii\helpers\Url::to(['site/login-soc']) //При использовании callback, нужно поставить пустую строку
//                ],
//            ]);?>

                <div>
                    <a href="#modal-login" class="link" data-sideup><?=Yii::t('app', 'Entrance through social networks');?></a>
                </div>
            </div>

            <div v-show="login_type == 0" id="email-area">

                <?php $model = new \common\models\LoginForm();
                $form = ActiveForm::begin([
                    'id' => 'login-form',
                    'enableAjaxValidation' => true,
                    'action' => ['site/login'],
                    'validationUrl' => ['site/validate-login'],
                ]); ?>


                <?= $form->field($model, 'email')->textInput(['autofocus' => true]) ?>

                <?= $form->field($model, 'password')->passwordInput()->label(Yii::t('app', 'Password'). ' '. Html::a(Yii::t('app', 'Forgot password?'), ['site/request-password-reset'], ['class' => 'margin-left'])) ?>

                <div class="row">
                    <div class="col-sm-6"><?= Html::submitButton('<i class="icon icon-user" style="line-height: 17px; font-size: 21px;"></i>' . Yii::t('app', 'Login'), ['class' => 'btn btn--green btn--icon', 'enableClientValidation' => false]) ?></div>
                    <div class="col-sm-6" style="text-align: right"><a href="#modal-register" data-sideup class="btn btn--orange"><?=Yii::t('app', 'Registration');?> </a></div>
                </div>


                <?php ActiveForm::end(); ?>

            </div>
            <div v-show="login_type == 1" id="phone-area">

                <sms-login
                        codes='<?= json_encode(Yii::$app->params['sms_phone_codes']) ?>'
                        text_again="<?=Yii::t('app', 'Send code again');?>"
                        text_again_in="<?=Yii::t('app', 'Send code again in');?>"
                        text_send_code="<?=Yii::t('app', 'Send code');?>"
                        text_error="<?= Yii::t('app', 'Incorrect code'); ?>"
                        text_accept_code="<?=Yii::t('app', 'Accept code');?> "></sms-login>
            </div>
        </div>
    </div>
</section>
