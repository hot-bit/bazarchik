<?php
/**
 * Created by PhpStorm.
 * User: York
 * Date: 03.11.2017
 * Time: 17:36
 *
 * @var $this \yii\web\View
 * @var $model \common\models\Adv[]
 */

\Yii::$app->breadcrumbs->Items([
    Yii::t('app', 'Favorite')
]);
?>


<section class="content">

    <h1><?=Yii::t('app', 'Favorite');?> </h1>


    <div class="row">

        <?foreach ($model as $item):?>
            <div class="col-sm-3">
                <?=$this->render('//adv/catalog/_catalog-item!new', ['item' => $item])?>
            </div>
        <?endforeach;?>
    </div>
</section>
