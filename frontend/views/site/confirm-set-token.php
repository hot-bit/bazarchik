<?php
/**
 * Created by PhpStorm.
 * User: York
 * Date: 06.01.2018
 * Time: 11:53
 */
/* @var $this yii\web\View */

use yii\widgets\ActiveForm;


$this->title = Yii::t('app', 'Confirm email address');
Yii::$app->breadcrumbs->Items([ $this->title]);

?>

<section class="content">


    <div class="col-sm-6 col-sm-push-3">
        <h1 class="margin-bottom-large"><?= $this->title?></h1>
        <?php $form = ActiveForm::begin([
            'id' => 'confirm-form'
        ]); ?>

        <?=$form->field($model, 'token')?>
        <div class="form-group">
            <?= \yii\helpers\Html::submitInput(Yii::t('app', 'Confirm'), ['class' => 'btn btn--green'])?>
        </div>
        <?php ActiveForm::end(); ?>

    </div>
    <div class="clearfix"></div>


</section>
