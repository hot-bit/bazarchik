<?php

/* @var $this yii\web\View */
/* @var $last_news \common\models\News[] */
use common\models\CMS;
use yii\helpers\Html;

$this->title = 'Cyprus BAZAR';


?>



<?=$this->render('//common/main_category', compact('categories'))?>



<section class="newads-pricipaly">

    <div class="newads-principaly__title">
        <h2><?=Yii::t('app', 'New ads')?></h2>
        <a href="<?=\yii\helpers\Url::to(['adv/index'])?>"><span><?=Yii::t('app', 'All Ads')?></span> <img src="<?=Yii::getAlias('@dist')?>/img/more.svg" class='svg' alt="<?=Yii::t('app', 'All Ads')?>"> </a>
    </div>

    <div class="row hidden-mobile">
        <?foreach ($last_adv as $item):?>
            <div class="col-sm-3">
                <?=$this->render('//adv/catalog/_catalog-item!new', ['item' => $item])?>
            </div>
        <?endforeach;?>
    </div>

    <div class=" show-mobile owl-carousel owl-carousel-catalog catalog-items__main" style="height: 300px;">
        <?foreach ($last_adv as $item):?>
            <?=$this->render('//adv/catalog/_catalog-item!new', ['item' => $item])?>
        <?endforeach;?>
    </div>

</section>

<section class="index-image">
    <!--Index image Content  -->
    <div class="index-image__content">
        <!--Index image title  -->
        <div class="index-image__title">
            <h2><?=Yii::t('app', 'Sell faster')?></h2>
        </div>
        <!-- End Index image tilte  -->

        <!-- Index image Paragraph  -->
        <div class="index-image__paragraph">
            <p><?=\common\models\CMS::Block('Sell faster text')?></p>
        </div>
        <!-- End index image paragraph  -->

        <!-- Index Image button  -->
        <div class="index-image__button">
            <button class="btn btn--orange">
                <a href="<?=\yii\helpers\Url::to(['adv/add'])?>">
                    <span class="add-plus"><img src="<?=Yii::getAlias('@dist')?>/img/add.svg" alt="AddAds" class="svg"></span>
                    <span><?=Yii::t('app', 'Add adv')?></span>
                </a>
            </button>
        </div>
        <!-- End Index Image button  -->
    </div>
    <!--End Image Content  -->
</section>

<section class="about-us apps">


    <div class="about-us__bottom">

        <div class="about-bottom__left">

            <div class="bottom-left__text">
                <p>
                    <?=\common\models\CMS::Block('Main footer text')?>
                </p>
            </div>
        </div>


        <div class="about-bottom__right">

            <div class="bottom-right__apple">
                <a href="<?=Yii::$app->params['ios_app']?>" target="_blank">
                    <div class="about-btm__icon"><img src="<?=Yii::getAlias('@dist')?>/img/apple.png" alt="Apple Icon"></div>
                    <div class="about-apple__text"><?=Yii::t('app', "Upload <br /> in {name}", ['name' => 'Apple Store'])?></div>
                </a>
            </div>


            <div class="bottom-right__market">
                <a href="<?=Yii::$app->params['android_app']?>" target="_blank">
                    <div class="about-btm__icon"><img src="<?=Yii::getAlias('@dist')?>/img/market.png" alt="Google PlayMarket"></div>
                    <div class="about-market__text"><?=Yii::t('app', "Upload <br /> in {name}", ['name' => 'Google Play'])?></div>
                </a>
            </div>
        </div>

    </div>

</section>

<section class="news">

    <div class="sectionews-title">

        <h2><?=Yii::t('app', 'News and articles')?></h2>

        <a href="<?=\yii\helpers\Url::to(['news/index']) ?>"><span><?=Yii::t('app', 'All news')?></span><img src="<?=Yii::getAlias('@dist')?>/img/more.svg" class='svg' alt="<?=Yii::t('app', 'All news')?>"></a>

    </div>
    <!-- News Content  -->
    <div class="news-content">

        <?foreach ($last_news as $item):?>

            <!-- News Container  -->
            <div class="news-container">
                <a class='news-container__link' href="<?=$item->_link ?>">
                    <div class="news-container__image"><img src="<?=$item->path ?>" alt=""></div>

                    <div class="news-container__href"><?=$item->title?></div>

                    <div class="news-container__date" data-time="<?=$item->date ?>"></div>
                </a>
            </div>
            <!-- End News Container  -->
        <?endforeach;?>

    </div>
    <!-- End News Content  -->

</section>

<section class="often-search">

    <div class="often-search__title">
        <h2><?=Yii::t('app', 'Popular request')?></h2>
    </div>

    <div class="often-search__content">

        <?=\frontend\widgets\cms\PopularSearch::widget()?>

    </div>

</section>

<section class="about-us">
    <div class="about-us__title">

        <h2><?=Yii::t('app', 'About us')?></h2>

    </div>

    <div class="about-us__content">

        <div class="about-us__container">
            <p><?=\common\models\CMS::Block('About us 1')?></p>
        </div>


        <div class="about-us__container">
            <p><?=\common\models\CMS::Block('About us 2')?></p>
        </div>


        <div class="about-us__container">
            <p><?=\common\models\CMS::Block('About us 3')?> </p>
        </div>


        <div class="about-us__container">
            <p><?=\common\models\CMS::Block('About us 4')?></p>
        </div>


    </div>
</section>


