<?php
/**
 * Created by PhpStorm.
 * User: York
 * Date: 24.03.2018
 * Time: 16:25
 */

$title = Yii::t('app', 'The site is now being updated');
?>
<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title><?= $title?></title>
    <link rel="stylesheet" href="//cdnjs.cloudflare.com/ajax/libs/flexboxgrid/6.3.1/flexboxgrid.min.css" type="text/css" >
    <link rel="stylesheet" href="/dist/css/main.min.css">
</head>
<body>
    <div class="row center-md center-sm margin-top">
        <div class="col-xs-12 col-sm-6 col-md-4">
            <h1> <?= $title?></h1>
            <p><?=Yii::t('app', 'We are preparing a new functional for you and soon it will be available.');?> </p>
        </div>
    </div>
    <div class="row center-md center-sm">
        <div class="col-xs-12 col-sm-8 col-md-8">
            <img src="/img/image.png" alt="" style="width: 100%">
        </div>
    </div>
</body>
</html>