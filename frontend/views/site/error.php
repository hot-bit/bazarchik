<?php

/* @var $this yii\web\View */
/* @var $name string */
/* @var $message string */
/* @var $exception Exception */

use yii\helpers\Html;

$this->title = $name;

$categories = \common\models\Categories::find()->where(['parent' => 0])->andWhere(['status' => true])->orderBy('position')->all();

?>
<section class="content site-error">

    <h1><?= Html::encode($this->title) ?></h1>
    <h3><?= nl2br(Html::encode($message)) ?></h3>
    
    <?=Yii::t('app', 'Use search or one of category below');?>

    <div class="row">
        <? $i = 0; foreach($categories as $category):?>
            <?= $i != 0 &&  $i % 6 == 0 ? '</div><div class="row">' : ''?>
            <div class="col-sm-4 col-md-2 col-xs-12">

                <h4><a href="<?= $category->link?>"><?= $category->name?></a></h4>

                <ul>
                    <?foreach($category->childs as $item):?>
                        <li><a href="<?= $item->link?>"><?= $item->name?></a></li>
                    <?endforeach?>
                </ul>
            </div>
        <?$i++; endforeach?>
    </div>

</section>
