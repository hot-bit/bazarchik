<?php

/* @var $this \yii\web\View */

/* @var $content string */

use common\models\CMS;
use frontend\assets\AppAsset;
use yii\helpers\Html;
//Yii::$app->seo->test();
AppAsset::register($this);
//\mimicreative\react\ReactAsset::register($this);

$body_class = '';
if (Yii::$app->controller->id == 'site' && Yii::$app->controller->action->id == 'index')
    $body_class = 'home_body';
elseif (Yii::$app->controller->id == 'adv' && Yii::$app->controller->action->id == 'get')
    $body_class = 'new-item__body';

?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">
<head>
    <meta name="description" content="<?= Html::encode(Yii::$app->seo->description) ?>">

    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">

    <meta property="og:image" content="<?= Yii::$app->seo->image ?>">
    <link rel="shortcut icon" href="<?= Yii::getAlias('@dist') ?>/favicon.ico" type="image/x-icon">

    <!-- Chrome, Firefox OS and Opera -->
    <meta name="theme-color" content="#000">
    <!-- Windows Phone -->
    <meta name="msapplication-navbutton-color" content="#000">
    <!-- iOS Safari -->
    <meta name="apple-mobile-web-app-status-bar-style" content="#000">


    <!--    <script src="//ulogin.ru/js/ulogin.js"></script>-->
    <meta charset="<?= Yii::$app->charset ?>">
    <?= Html::csrfMetaTags() ?>
    <?= Yii::$app->seo->head?>
    <!-- Minified version of `es6-promise-auto` below. -->
    <?php $this->head() ?>
    <!--[if IE]>
    <link rel="stylesheet" type="text/css" href="/css/ie-fix.css" />
    <![endif]-->
    <link rel="stylesheet" type="text/css" href="/css/ie-10.css" />
    <script src="https://cdn.jsdelivr.net/npm/es6-promise@4/dist/es6-promise.min.js"></script>
    <script src="https://cdn.jsdelivr.net/npm/es6-promise@4/dist/es6-promise.auto.min.js"></script>

    <!-- Google Tag Manager -->
    <noscript><iframe src="//www.googletagmanager.com/ns.html?id=GTM-P92BHP"
                      height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
    <script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
                new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
            j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
            '//www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
        })(window,document,'script','dataLayer','GTM-P92BHP');</script>
    <!-- End Google Tag Manager -->

</head>
<body class='<?= $body_class ?> body'>
<?php $this->beginBody() ?>
<? if (Yii::$app->user->can('ADMIN_PANEL')): ?>
    <?= $this->render('//common/admin_line') ?>
<? endif; ?>
<div class="vue-block">

    <?= $this->render('//common/header') ?>


    <?= $this->render('//common/top_search') ?>

    <div class="content-block">

        <?= Yii::$app->breadcrumbs->run() ?>

        <section class="alert-wrapper"><?= \common\widgets\Alert::widget() ?></section>

        <?= $content ?>

    </div>


    <?= $this->render('//common/modals') ?>
    <?= $this->render('//common/footer') ?>

    <?= Html::hiddenInput('dist_path', Yii::getAlias('@dist'), ['id' => 'dist_path']); ?>
    <?= Html::hiddenInput('language', Yii::$app->language, ['id' => 'language']); ?>

</div>

<?php $this->endBody() ?>
</body>
</html>
<?php $this->endPage() ?>
