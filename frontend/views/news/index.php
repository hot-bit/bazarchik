<?php
/**
 * Created by PhpStorm.
 * User: yorks
 * Date: 31.12.2017
 * Time: 21:26
 */
/* @var $this yii\web\View */
/* @var $model \common\models\News[]  */
/* @var $pages \yii\data\Pagination */

$this->title = Yii::t('app', 'News');
Yii::$app->breadcrumbs->Items([
    $this->title
]);

?>


<section class="content">
    <h1><?= $this->title?></h1>

    <div class="news-content__page">
        <?foreach($model as $item):?>
            <?=$this->render('_row', compact('item')) ?>
        <?endforeach?>
    </div>

    <div>
        <?=\frontend\widgets\LinkPager::widget([
            'pagination' => $pages
        ]) ?>
    </div>
</section>
