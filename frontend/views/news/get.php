<?php
/**
 * Created by PhpStorm.
 * User: yorks
 * Date: 31.12.2017
 * Time: 21:39
 */
/* @var $this yii\web\View */
/* @var $model \common\models\News */
/* @var $next_news \common\models\News */
/* @var $prev_news \common\models\News */

$this->title = $model->title;
Yii::$app->breadcrumbs->Items([
    \yii\helpers\Url::to(['news/index']) => Yii::t('app', 'News'),
    $this->title
]);

?>

<section class="content">
    <h1> <?=$this->title ?></h1>

    <div class="one-news__date" data-time="<?=$model->date ?>"></div>
    <div class="one-news__content">
        <?=$model->text ?>
    </div>
    <div class="one-news__pagination">

        <?if($prev_news):?>
            <div class="one-news__prev">
                <a href="<?=$prev_news->_link ?>"><?= Yii::t('app', 'Previous news')?></a>
            </div>
        <?endif?>

        <?if($next_news):?>
            <div class="one-news__next">
                <a href="<?=$next_news->_link ?>"><?= Yii::t('app', 'Next news')?></a>
            </div>
        <?endif?>

    </div>
</section>
