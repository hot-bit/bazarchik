<?php
/**
 * Created by PhpStorm.
 * User: yorks
 * Date: 31.12.2017
 * Time: 21:30
 */
/* @var $this yii\web\View */
/* @var $item \common\models\News */

?>

<div class="news-container">

    <div class="news-container__href"><a href="<?=$item->_link ?>"><?= $item->title?></a></div>

    <div class="news-container__date" data-time="<?=$item->date ?>"></div>

</div>