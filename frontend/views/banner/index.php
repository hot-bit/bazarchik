<?php
/**
 * Created by PhpStorm.
 * User: yorks
 * Date: 31.01.2018
 * Time: 0:48
 */
/* @var $this yii\web\View */
/* @var $model \common\models\Banner */

$this->title = Yii::t('app', 'Create banner');
Yii::$app->breadcrumbs->Items([
    $this->title
]);
?>

<section class="content">
    <h1><?= $this->title ?></h1>

    <div class="row">
        <div class="col-sm-6">
            <?php $form = yii\widgets\ActiveForm::begin([
                'id' => 'banner-create',
            ]); ?>
                <?= $form->field($model, 'name') ?>
                <?= $form->field($model, 'phone') ?>
                <?= $form->field($model, 'email') ?>
                <?= $form->field($model, 'link') ?>
                <?= $form->field($model, 'place')->dropDownList(\common\models\Banner::placeList(), ['prompt' => '-- Choose a place --']) ?>
                <?= $form->field($model, 'imageField')->fileInput() ?>
            
            
                <div class="form-group">
                    <?= \yii\helpers\Html::submitButton(Yii::t('app', 'Apply'), ['class' => 'btn btn--green'])?>
                </div>
            <?php yii\widgets\ActiveForm::end(); ?>
        </div>
        <div class="col-sm-6">

            <?= \common\models\CMS::Block('Create banner') ?>
        </div>
    </div>

</section>

