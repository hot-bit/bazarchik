<?php
/**
 * Created by PhpStorm.
 * User: York
 * Date: 28.09.2017
 * Time: 22:42
 *
 * @var $model \frontend\models\CatalogFilter
 * @var $form \yii\widgets\ActiveForm
 */

$area_list = [];

if($model->city_id)
    $area_list = \common\models\Area::map($model->city_id);
?>


<?= $form->field($model, 'city_id')->dropDownList(\common\models\City::map(), [
    'prompt' => Yii::t('app', 'All city'),
    'class' => 'SelectBox',
]) ?>
<?= $form->field($model, 'area_id')->dropDownList($area_list, [
    'prompt' => Yii::t('app', 'All area'),
    'class' => 'SelectBox',
]) ?>
<?= $form->field($model, 'bedrooms')->dropDownList(\common\models\map\Bedrooms::Map(), [
    'prompt' => Yii::t('app', 'All bedrooms'),
    'class' => 'SelectBox',
]) ?>
