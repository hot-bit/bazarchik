<?php
/**
 * Created by PhpStorm.
 * User: York
 * Date: 28.09.2017
 * Time: 22:44
 *
 * @var \yii\widgets\ActiveForm $form
 */

?>


<div class="row">
    <div class="col-sm-3">
        <?=$form->field($model, 'diam')->dropDownList(\common\models\map\Tires::Diameter())?>
    </div>
    <div class="col-sm-3">
        <?=$form->field($model, 'width_prof')->dropDownList(\common\models\map\Tires::Width())?>
    </div>
    <div class="col-sm-3">
        <?=$form->field($model, 'height_prof')->dropDownList(\common\models\map\Tires::Height())?>
    </div>
</div>