<?php
/**
 * Created by PhpStorm.
 * User: York
 * Date: 28.09.2017
 * Time: 22:45
 */

?>

<div class="row">

    <div class="col-sm-3">
        <?=$form->field($model, 'diam')->dropDownList(\common\models\map\MotoTire::Diameter())?>
    </div>
    <div class="col-sm-3">
        <?=$form->field($model, 'round')->dropDownList(\common\models\map\MotoTire::Round())?>
    </div>
    <div class="col-sm-3">
        <?=$form->field($model, 'width_prof')->dropDownList(\common\models\map\MotoTire::Width())?>
    </div>
    <div class="col-sm-3">
        <?=$form->field($model, 'height_prof')->dropDownList(\common\models\map\MotoTire::Height())?>
    </div>
</div>