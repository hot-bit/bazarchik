<?php
/**
 * Created by PhpStorm.
 * User: York
 * Date: 28.09.2017
 * Time: 22:42
 *
 * @var $id integer
 * @var $form ActiveForm
 * @var $model \common\models\forms\ParamsForm
 */

use common\models\map\CarParams;
use yii\widgets\ActiveForm;

//var_dump($_POST);
?>

<div class="row">
    <div class="col-sm-3">
        <?= $form->field($model, 'brand')->dropDownList(\common\models\Brand::map(), [
            'onchange' => "
             $.ajax({
                type     :'POST',
                cache    : false,
                url  : '" . \yii\helpers\Url::to(['ajax/car-models']) . "',
                data: {id: $(this).val(), model:'ParamsForm', dest:'common-models-forms'},
                success  : function(response) {
                    $('.field-paramsform-model .SumoSelect').remove();
                    $('.field-paramsform-model').append(response.data);  
                    $('#paramsform-model').SumoSelect();
                }
            });  
            return false;",
        ]) ?>
    </div>
    <div class="col-sm-3">
        <?= $form->field($model, 'model')->dropDownList(\common\models\Model::map($model->brand), ['prompt' => Yii::t('app', 'Select model')]) ?>
    </div>
    <div class="col-sm-3">
        <?= $form->field($model, 'year')->textInput() ?>
    </div>
    <div class="col-sm-3">
        <?= $form->field($model, 'vin')->textInput() ?>
    </div>
    <div class="col-sm-3">
        <?= $form->field($model, 'volume')->textInput() ?>
    </div>
</div>
<?if(!isset($new) || !$new):?>
    <div class="row">
        <div class="col-sm-3"><?= $form->field($model, 'running')->textInput() ?></div>
    </div>
<?endif?>


<car-body active="<?= $model->body ?>" name="ParamsForm[body]"><?= $model->attributeLabels()['body'] ?></car-body>

<?= $form->field($model, 'doors')->widget(\frontend\widgets\cms\RadioGroupField::className(), [
    'items' => [Yii::t('app', 'Not specify'), 2, 3, 4, 5],
]) ?>

<?= $form->field($model, 'color')->widget(\frontend\widgets\cms\CarColorField::className(), [
    'items' => \common\models\map\CarColor::Map()
]) ?>

<?= $form->field($model, 'engine')->widget(\frontend\widgets\cms\RadioGroupField::className(), [
    'items' => \common\models\map\CarEngine::Map(),
    'empty' => 1
]) ?>

<?= $form->field($model, 'box')->widget(\frontend\widgets\cms\RadioGroupField::className(), [
    'items' => \common\models\map\CarBox::Map(),
    'empty' => 1
]) ?>

<?= $form->field($model, 'drive')->widget(\frontend\widgets\cms\RadioGroupField::className(), [
    'items' => \common\models\map\CarDrive::Map(),
    'empty' => 1
]) ?>

<?= $form->field($model, 'wheel')->widget(\frontend\widgets\cms\RadioGroupField::className(), [
    'items' => \common\models\map\CarWheel::Map(),
    'empty' => 1
]) ?>

<div>
    <div class="row">
        <div class="col-sm-6">
            <?= $form->field($model, 'st_wheel')->widget(\frontend\widgets\cms\RadioList::className(), ['items' => CarParams::WheelAssist()]) ?>
        </div>
        <div class="col-sm-6">
            <div class="form-group">
                <?= $form->field($model, 'salon')->widget(\frontend\widgets\cms\RadioList::className(), ['items' => CarParams::Saloon(), 'group' => false]) ?>
                <?= $form->field($model, 'salon_leather_wheel')->widget(\frontend\widgets\cms\Checkbox::className()) ?>
                <?= $form->field($model, 'salon_hatch')->widget(\frontend\widgets\cms\Checkbox::className()) ?>
            </div>
        </div>
        <div class="col-sm-6">
            <div class="form-group">
                <?= $form->field($model, 'climat')->widget(\frontend\widgets\cms\RadioList::className(), ['items' => CarParams::Climate(), 'group' => false]) ?>
                <?= $form->field($model, 'climate_wheel_contr')->widget(\frontend\widgets\cms\Checkbox::className()) ?>
                <?= $form->field($model, 'climate_aterm')->widget(\frontend\widgets\cms\Checkbox::className()) ?>
            </div>
        </div>
        <div class="col-sm-6">
            <div class="form-group">
                <label><?= $model->getAttributeLabel('active_save') ?></label>
                <?= $form->field($model, 'help_secure_abs')->widget(\frontend\widgets\cms\Checkbox::className()) ?>
                <?= $form->field($model, 'help_secure_asr')->widget(\frontend\widgets\cms\Checkbox::className()) ?>
                <?= $form->field($model, 'help_secure_esp')->widget(\frontend\widgets\cms\Checkbox::className()) ?>
                <?= $form->field($model, 'help_secure_ebd')->widget(\frontend\widgets\cms\Checkbox::className()) ?>
                <?= $form->field($model, 'help_secure_eba')->widget(\frontend\widgets\cms\Checkbox::className()) ?>
                <?= $form->field($model, 'help_secure_eds')->widget(\frontend\widgets\cms\Checkbox::className()) ?>
                <?= $form->field($model, 'help_secure_pds')->widget(\frontend\widgets\cms\Checkbox::className()) ?>
            </div>
        </div>
        <div class="col-sm-6">
            <div class="form-group">
                <label><?= $model->getAttributeLabel('heating') ?></label>
                <?= $form->field($model, 'heating_front_seat')->widget(\frontend\widgets\cms\Checkbox::className()) ?>
                <?= $form->field($model, 'heating_rear_seat')->widget(\frontend\widgets\cms\Checkbox::className()) ?>
                <?= $form->field($model, 'heating_mirrors')->widget(\frontend\widgets\cms\Checkbox::className()) ?>
                <?= $form->field($model, 'heating_rear_window')->widget(\frontend\widgets\cms\Checkbox::className()) ?>
                <?= $form->field($model, 'heating_wheel')->widget(\frontend\widgets\cms\Checkbox::className()) ?>
            </div>
        </div>
        <div class="col-sm-6">
            <div class="form-group">
                <label><?= $model->getAttributeLabel('active_save') ?></label>
                <?= $form->field($model, 'el_front_seats')->widget(\frontend\widgets\cms\Checkbox::className()) ?>
                <?= $form->field($model, 'el_rear_seats')->widget(\frontend\widgets\cms\Checkbox::className()) ?>
                <?= $form->field($model, 'el_mirrors')->widget(\frontend\widgets\cms\Checkbox::className()) ?>
                <?= $form->field($model, 'el_wheel')->widget(\frontend\widgets\cms\Checkbox::className()) ?>
                <?= $form->field($model, 'el_folding_mirrors')->widget(\frontend\widgets\cms\Checkbox::className()) ?>
            </div>
        </div>
        <div class="col-sm-6">
            <div class="form-group">
                <label><?= $model->getAttributeLabel('mem') ?></label>
                <?= $form->field($model, 'mem_front_seats')->widget(\frontend\widgets\cms\Checkbox::className()) ?>
                <?= $form->field($model, 'mem_rear_seats')->widget(\frontend\widgets\cms\Checkbox::className()) ?>
                <?= $form->field($model, 'mem_mirrors')->widget(\frontend\widgets\cms\Checkbox::className()) ?>
                <?= $form->field($model, 'mem_wheel')->widget(\frontend\widgets\cms\Checkbox::className()) ?>
            </div>
        </div>
        <div class="col-sm-6">
            <?= $form->field($model, 'glass')->widget(\frontend\widgets\cms\RadioList::className(), ['items' => CarParams::ElWindow()]) ?>
        </div>
        <div class="col-sm-6">
            <div class="form-group">
                <?= $form->field($model, 'audio')->widget(\frontend\widgets\cms\RadioList::className(), ['items' => CarParams::Speakers(), 'group' => false]) ?>
                <?= $form->field($model, 'sub')->widget(\frontend\widgets\cms\Checkbox::className()) ?>
            </div>
        </div>
        <div class="col-sm-6">
            <div class="form-group">
                <label><?= $model->getAttributeLabel('help') ?></label>
                <?= $form->field($model, 'help_jockey')->widget(\frontend\widgets\cms\Checkbox::className()) ?>
                <?= $form->field($model, 'help_rain_sensor')->widget(\frontend\widgets\cms\Checkbox::className()) ?>
                <?= $form->field($model, 'help_light_sensor')->widget(\frontend\widgets\cms\Checkbox::className()) ?>
                <?= $form->field($model, 'help_r_park_sensor')->widget(\frontend\widgets\cms\Checkbox::className()) ?>
                <?= $form->field($model, 'help_f_park_sensor')->widget(\frontend\widgets\cms\Checkbox::className()) ?>
                <?= $form->field($model, 'help_blind_spot')->widget(\frontend\widgets\cms\Checkbox::className()) ?>
                <?= $form->field($model, 'help_rear_camera')->widget(\frontend\widgets\cms\Checkbox::className()) ?>
                <?= $form->field($model, 'help_cruise_control')->widget(\frontend\widgets\cms\Checkbox::className()) ?>
                <?= $form->field($model, 'help_computer')->widget(\frontend\widgets\cms\Checkbox::className()) ?>
            </div>
        </div>
        <div class="col-sm-6">
            <div class="form-group">
                <label><?= $model->getAttributeLabel('hijack') ?></label>
                <?= $form->field($model, 'hijack_signal')->widget(\frontend\widgets\cms\Checkbox::className()) ?>
                <?= $form->field($model, 'hijack_central_lock')->widget(\frontend\widgets\cms\Checkbox::className()) ?>
                <?= $form->field($model, 'hijack_immobi')->widget(\frontend\widgets\cms\Checkbox::className()) ?>
                <?= $form->field($model, 'hijack_sputnik')->widget(\frontend\widgets\cms\Checkbox::className()) ?>
            </div>
        </div>
        <div class="col-sm-6">
            <div class="form-group">
                <label><?= $model->getAttributeLabel('airbags') ?></label>
                <?= $form->field($model, 'airbags_front')->widget(\frontend\widgets\cms\Checkbox::className()) ?>
                <?= $form->field($model, 'airbags_knee')->widget(\frontend\widgets\cms\Checkbox::className()) ?>
                <?= $form->field($model, 'airbags_blinds')->widget(\frontend\widgets\cms\Checkbox::className()) ?>
                <?= $form->field($model, 'airbags_front_side')->widget(\frontend\widgets\cms\Checkbox::className()) ?>
                <?= $form->field($model, 'airbags_rear_side')->widget(\frontend\widgets\cms\Checkbox::className()) ?>
            </div>
        </div>
        <div class="col-sm-6">
            <div class="form-group">
                <label><?= $model->getAttributeLabel('media') ?></label>
                <?= $form->field($model, 'media_cd')->widget(\frontend\widgets\cms\Checkbox::className()) ?>
                <?= $form->field($model, 'media_mp3')->widget(\frontend\widgets\cms\Checkbox::className()) ?>
                <?= $form->field($model, 'media_radio')->widget(\frontend\widgets\cms\Checkbox::className()) ?>
                <?= $form->field($model, 'media_tv')->widget(\frontend\widgets\cms\Checkbox::className()) ?>
                <?= $form->field($model, 'media_video')->widget(\frontend\widgets\cms\Checkbox::className()) ?>
                <?= $form->field($model, 'media_wheel_control')->widget(\frontend\widgets\cms\Checkbox::className()) ?>
                <?= $form->field($model, 'media_usb')->widget(\frontend\widgets\cms\Checkbox::className()) ?>
                <?= $form->field($model, 'media_aux')->widget(\frontend\widgets\cms\Checkbox::className()) ?>
                <?= $form->field($model, 'media_bluetooth')->widget(\frontend\widgets\cms\Checkbox::className()) ?>
                <?= $form->field($model, 'media_gps')->widget(\frontend\widgets\cms\Checkbox::className()) ?>
            </div>
        </div>
        <div class="col-sm-6">
            <div class="form-group">
                <?= $form->field($model, 'light')->widget(\frontend\widgets\cms\RadioList::className(), ['items' => CarParams::Headlight(), 'group' => false]) ?>
                <?= $form->field($model, 'light_afog')->widget(\frontend\widgets\cms\Checkbox::className()) ?>
                <?= $form->field($model, 'light_cleaner')->widget(\frontend\widgets\cms\Checkbox::className()) ?>
                <?= $form->field($model, 'light_adaptive')->widget(\frontend\widgets\cms\Checkbox::className()) ?>
            </div>
        </div>
        <div class="col-sm-6">
            <div class="form-group">
                <label><?= $model->getAttributeLabel('TI') ?></label>
                <?= $form->field($model, 'TI_service_book')->widget(\frontend\widgets\cms\Checkbox::className()) ?>
                <?= $form->field($model, 'TI_dealer')->widget(\frontend\widgets\cms\Checkbox::className()) ?>
                <?= $form->field($model, 'TI_warranty')->widget(\frontend\widgets\cms\Checkbox::className()) ?>
            </div>
        </div>
    </div>
</div>

