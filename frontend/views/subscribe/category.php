<?php
/**
 * Created by PhpStorm.
 * User: York
 * Date: 28.12.2017
 * Time: 17:42
 */

use yii\widgets\ActiveForm;

/* @var $this \yii\web\View */
/* @var $category \common\models\Categories  */
/* @var $model \common\models\Subscribe */

if(!Yii::$app->user->isGuest){
    $model->email = Yii::$app->user->identity->email;
    $model->name = Yii::$app->user->identity->name;
}

?>

<section class="content">
    <div class="row">
        <div class="col-sm-12">
            <h1><?=Yii::t('app', 'Subscribe to category');?></h1>
            <h4><?= $model->category_name?></h4>
        </div>
    </div>

    <div class="row">
        <div class="col-sm-6">
            <?php $form = ActiveForm::begin([
                'id' => 'subscribe-form'
            ]); ?>
                <?= $form->errorSummary($model)?>
                <?= \yii\helpers\Html::activeHiddenInput($model, 'category_id')?>
                <?=$form->field($model, 'name')?>
                <?=$form->field($model, 'email')?>

            <div>
                <?= \yii\helpers\Html::submitButton(Yii::t('app', 'Subscribe'), [
                    'class' => 'btn btn--green'
                ])?>
            </div>
            <?php ActiveForm::end(); ?>
        </div>
    </div>

</section>