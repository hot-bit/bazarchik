<?php
/**
 * Created by PhpStorm.
 * User: York
 * Date: 27.09.2017
 * Time: 15:38
 */

use common\models\CMS;
use yii\helpers\Html;
use yii\widgets\ActiveForm;


$model = new \common\models\forms\SearchForm();
if(isset($_GET['SearchForm']))
    $model->load($_GET);


$categories = \yii\helpers\ArrayHelper::map(\common\models\Categories::find()->where('parent = 0')->all(), 'id', 'name');

$today = \common\models\Adv::find()->where(['>', 'date_create', strtotime('-1 day')])->count();
$total = \common\models\Adv::find()->count();

?>

<section class="search">
    <?php
    $form = ActiveForm::begin([
        'id' => 'top-search',
        'options' => ['class' => 'search-form'],
        'action' => ['adv/search'],
        'method' => 'get',
    ]); ?>

        <div class="search-top">

            <div class="search-top__left">

                <?=Html::activeTextInput($model, 'search', [
                    'class' => 'search_input',
                    'placeholder' => Yii::t('app', '{today} ads today, total {total}', ['today' => $today, 'total' => $total])
                ])?>

            </div>


            <div class="search-top__right">

                <div class="find-ads">
                    <button class='btn btn--orange'>
                        <i class="icon-search"></i>
                        <span class='find-text'><?= Yii::t('app', 'Find ads') ?></span>
                    </button>
                </div>

            </div>

        </div>


        <div class="search-bottom">

            <div class="search-bottom__left">

                <div class="seach-select">

                    <?=Html::activeCheckbox($model, 'in_title', [
                        'class' => 'checkbox-dstr'
                    ])?>
                    <?=Html::activeCheckbox($model, 'photo', [
                        'class' => 'checkbox-dstr'
                    ])?>
                    <?=Html::activeCheckbox($model, 'only_new', [
                        'class' => 'checkbox-dstr'
                    ])?>
                </div>
            </div>


            <div class="search-bottom__right">
                <div class="search-right__content">

                    <div class="search-right__content-item selectbox__noborder">
                        <?=Html::activeDropDownList($model, 'city_id', \common\models\City::map(), [
                            'placeholder' =>  Yii::t('app', 'All cities'),
                            'class' => 'SelectBox',
                            'multiple' => true
                        ])?>
                    </div>
                    <div class="search-right__content-item selectbox__noborder">
                        <?=Html::activeDropDownList($model, 'category_id', $categories, [
                            'prompt' => Yii::t('app', 'All categories'),
                            'class' => 'SelectBox'
                        ])?>
                    </div>
                </div>
            </div>
        </div>
    <?php ActiveForm::end(); ?>

</section>
