<?php
/**
 * Created by PhpStorm.
 * User: York
 * Date: 15.09.2017
 * Time: 12:33
 */
use common\models\CMS;

/**
 * @var $this \yii\web\View
 * @var $catalog \frontend\models\Catalog
 */
$back_link = \yii\helpers\Url::home();

if($catalog->category_model){
    if($catalog->category_model->parent == 0)
        $back_link = \yii\helpers\Url::to(['adv/index']);
    else{
        $back_link = $catalog->category_model->_parent->link;
    }
}
$count_adv = $catalog->count_ads > 999 ? '999+'  : $catalog->count_ads;

$subscribe_url = ['subscribe/category', 'id' => $catalog->category_model->id];
if($catalog->subcategory_type){
    if($catalog->subcategory_type == 'brand')
        $subscribe_url['brand'] = $catalog->subcategory_model->id;
    if($catalog->subcategory_type == 'model')
        $subscribe_url['car_model'] = $catalog->subcategory_model->id;
}

?>


<div class="catalog-sidebar">

    <div class="catalog-sidebar__top">


        <div class="sidebar-top__out">
            <a href="<?=$back_link?>">
                <div class="sidebar-out__icon">
                    <img src="<?=Yii::getAlias('@dist')?>/img/sidebarOut.svg" alt="SidebarOut" class="svg">
                </div>

                <div class="sidebar-out__text">
                    <span><?=Yii::t('app', 'Back')?></span>
                </div>
            </a>
        </div>

        <div class="sidebar-top__left">
            <div class="sidebar-top__find">

                <h2><?=Yii::t('app', 'Found {count} ads', ['count' => $count_adv])?></h2>

            </div>

            <?if($catalog->category_model):?>
                <div class="sidebar-top__subscribe">
                    <div class="subscribe-icon">
                        <?= CMS::svg('rss', ['class' => 'icon--gray'])?>
                    </div>
                    <div class="susbcribe-text">
                        <a href="<?=\yii\helpers\Url::to($subscribe_url)?>">
                            <?=Yii::t('app', 'Subscribe to category')?>
                        </a>
                    </div>
                </div>
            <?endif;?>
        </div>


    </div>
    <div id="search-modal" class="white-popup mfp-hide mfp-right">
        <h2 class="modal-title"><?=Yii::t('app', 'Search');?> </h2>

        <div class="modal-close" data-modal-close>
            <?= \common\models\CMS::svg('menuclose', ['class' => 'icon--gray', 'size' => '30'])?>
        </div>

        <div class="catalog-sidebar__bottom">
            <?=$this->render('//adv/catalog/_category_search', ['category' => $catalog->category_model, 'catalog' => $catalog])?>
        </div>
    </div>
    <div class="catalog-sidebar__category">
        <div class="find-category__content">
            <?=$this->render('//common/_category_icon_grid')?>
        </div>
    </div>


</div>
