<?php
/**
 * Created by PhpStorm.
 * User: York
 * Date: 14.09.2017
 * Time: 14:33
 */

use common\models\CMS;
?>

<section class="find-category">
    <div class="find-category__title">
        <h2><?=Yii::t('app', 'Find by category')?></h2>
    </div>

    <!-- Категории для больших экранов -->
    <div class="find-category__large">

        <?=$this->render('//common/_category_full');?>

    </div>
    <!-- END Категории для больших экранов  -->


    <!-- Категории для средних экранов  -->
    <div class="find-category__small">

        <?=$this->render('//common/_category_full');?>
    </div>
    <!--END Категории для маленьких экранов  -->

</section>
