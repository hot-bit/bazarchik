<?php
/**
 * Created by PhpStorm.
 * User: York
 * Date: 13.12.2017
 * Time: 15:34
 *
 * @var \yii\web\View $this
 */

$menu = [
    [
        'name' => 'Admin panel',
        'icon' => 'ion-ios-locked-outline',
        'link' => '/backend/web',
    ],
    [
        'name' => 'Moderate ads',
        'icon' => 'ion-ios-box-outline',
        'link' => '/backend/web/adv',
    ],
//    [
//        'name' => 'Admin menus',
//        'icon' => 'ion-ios-more-outline',
//        'link' => '',
//    ],
    [
        'name' => Yii::t('app', 'Add translates'),
        'icon' => 'ion-ios-reload',
        'link' => ['system/translates'],
    ],
];

?>
<link rel="stylesheet" href="http://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css">
<div class="admin-panel">
    <ul>
        <?foreach($menu as $item):?>
            <li><a href="<?= \yii\helpers\Url::to($item['link'])?>"><i class="<?= $item['icon']?>"></i> <span><?= $item['name']?></span></a></li>
        <?endforeach?>
    </ul>
</div>
<div style="height: 30px"></div>