<?php
/**
 * Created by PhpStorm.
 * User: yorks
 * Date: 26.01.2018
 * Time: 0:55
 */
/* @var $this yii\web\View */

?>

<div class="footer-text">

    <p><?= Yii::$app->seo->text_bottom ?></p>

</div>

<footer>
    <div class="footer">
        <section class="padding-bottom-remove content ">
            <div class="row">
                <div class="col-sm-4 footer__block">
                    <a href="<?= Yii::$app->request->home ?>">
                        <img src="<?= Yii::getAlias('@dist') ?>/img/logo_white.png" alt="">
                    </a>
                </div>
                <div class="col-sm-4 footer__block footer__block-center">

                    <?= \yii\widgets\Menu::widget([
                        'items' => [
                            [
                                'label' => Yii::t('app', 'Show all ads'),
                                'url' => ['adv/index'],
                            ],
                            [
                                'label' => Yii::t('app', 'About us'),
                                'url' => ['page/get', 'url' => 'about_us'],
                            ],
                            [
                                'label' => Yii::t('app', 'Help desk'),
                                'url' => ['faq/index'],
                            ],
                        ]
                    ]) ?>
                </div>
                <div class="col-sm-4 footer__block footer__block-right">
                    <a href="<?= \yii\helpers\Url::to(['adv/add']) ?>"
                       class="btn btn--green"><?= Yii::t('app', 'POST AN AD') ?></a>
                </div>
            </div>
        </section>
    </div>
    <div class="footer__bottom">
        <section class="content padding-bottom-remove">
            <div class="footer__bottom-left">
                <div align="center" alt="Click to Verify" title="Click to Verify" style="border-width: 0px; background-image: url(&quot;https://tracedseals.starfieldtech.com/siteseal/images/siteseal_base_lite.gif&quot;); background-repeat: no-repeat; cursor: pointer; width: 132px; height: 31px;"><div style="position: relative; top: 20px; left: 12px; width: 106px; line-height: 7.5pt;"><div style="color: rgb(106, 156, 186); font-weight: bold; font-size: 6pt;">2016-05-17</div></div></div>
            </div>
            <p class="footer__bottom-right">
                <?= \common\models\CMS::Block('Footer') ?>
            </p>

        </section>
    </div>
</footer>