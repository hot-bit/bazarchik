<?php
/**
 * Created by PhpStorm.
 * User: York
 * Date: 28.12.2017
 * Time: 12:29
 */

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this \yii\web\View */

?>

<div class="white-popup  mfp-hide mfp-left" id="modal-register">

    <div class="modal-close" data-modal-close>
        <?= \common\models\CMS::svg('menuclose', ['class' => 'icon--gray', 'size' => '30'])?>
    </div>


    <h3 class="margin-top-large">
        <?= Yii::t('app', 'Registration') ?>
    </h3>

    <div class="catalog-bottom__social">

        <div class="catalog-social__container">
            <div id="uLogin_7c3b1995" data-uloginid="7c3b1995"></div>
        </div>

    </div>

    <?php $model = new \frontend\models\SignupForm();
    $form = ActiveForm::begin([
        'id' => 'register-auth',
        'enableAjaxValidation' => true,
        'action' => ['site/signup'],
        'validationUrl' => ['site/validate-signup'],
    ]); ?>

    <?= $form->field($model, 'email')->textInput(['autofocus' => true]) ?>


    <checkbox name="SignupForm[rule]"
              checked="<?= $model->rule ?>"><?= Yii::t('app', 'I have read and agree  <a href="{rules}">the service rules</a>', ['rules' => \yii\helpers\Url::to(['page/get', 'url' => 'terms_and_conditions'])]) ?></checkbox>

    <checkbox name="SignupForm[subscribe]"
              checked="<?= $model->subscribe ?>"><?= Yii::t('app', 'I want to receive newsletters') ?></checkbox>

    <?= Html::submitButton(Yii::t('app', 'Registration'), ['class' => 'btn btn--green register-go']) ?>

    <?php ActiveForm::end(); ?>

    <?// = $this->render('//common/_category_full'); ?>

</div>