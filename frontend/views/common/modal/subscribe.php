<?php
/**
 * Created by PhpStorm.
 * User: York
 * Date: 28.12.2017
 * Time: 12:32
 */
/* @var $this \yii\web\View */

?>

<div class="login-user login-user__access">
    <div class="close-sidebar__small"  data-modal-close>
        <img src="<?= Yii::getAlias('@dist') ?>/img/closePopupIcon.svg" alt="" class="svg">
    </div>

    <div class="add-ads">

        <button class="add-ads  btn btn--green">
            <a href="add.html">
                <span class="add-plus"><img src="<?= Yii::getAlias('@dist') ?>/img/add.svg" alt=""
                                            class="svg"></span>
                <span>Подать объявления</span>
            </a>
        </button>

    </div>


    <div class="login-acces__title">
        Получить доступ
        ко всем материалам сайта
    </div>

    <form class="login-acces">

        <div class="login-acces__field">

            <label>Ваше имя</label>
            <input type='text' class='greenStyle' placeholder='Имя'>

        </div>


        <div class="login-acces__field  login-acces__mobile">

            <label>Мобильный телефон</label>
            <div class="mobile-dropdown__relative">
                <input type='text' class='greenStyle'>
                <div class="mobile-dropdown__absolute">
                    <select name="prefix" id="prefix-mobile">
                        <option value="+357">+357</option>
                        <option value="+44">+44</option>
                    </select>
                </div>
            </div>
        </div>


        <div class="login-acces__field">

            <label>Код потверждения</label>
            <input type='text' placeholder='Введите SMS-код'>

        </div>

        <button class='btn btn--green'>

  <span class='button-send__icon'>
    <svg
        xmlns="http://www.w3.org/2000/svg"
        width="24px" height="15px">
      <path fill-rule="evenodd" fill="rgb(255, 255, 255)"
            d="M22.960,1.732 L10.232,14.460 C9.842,14.850 9.208,14.850 8.818,14.460 L1.040,6.682 C0.649,6.291 0.649,5.658 1.040,5.267 C1.430,4.877 2.063,4.877 2.454,5.267 L9.525,12.338 L21.546,0.318 C21.936,-0.073 22.570,-0.073 22.960,0.318 C23.351,0.708 23.351,1.341 22.960,1.732 Z"/>
    </svg>
  </span>

            <span class="button-send__text">Готово</span>

        </button>

    </form>

    <?= $this->render('//common/_category_full'); ?>
</div>