<?php
/**
 * Created by PhpStorm.
 * User: York
 * Date: 28.12.2017
 * Time: 12:29
 */
/* @var $this \yii\web\View */

?>

<? if (!Yii::$app->user->isGuest): ?>
    <div class="white-popup mfp-hide mfp-left login-user" id="modal-user">

        <div class="exit-login">
            <a href="<?= \yii\helpers\Url::to(['site/logout']) ?>">
                <div class="exit-login__icon">
                    <?= \common\models\CMS::svg('back', ['class' => 'icon--gray', 'size' => '30'])?>
                </div>

                <div class="exit-login__text">
                    <span><?= Yii::t('app', 'Logout') ?></span>
                </div>
            </a>
        </div>

        <div class="modal-close" data-modal-close>
            <?= \common\models\CMS::svg('menuclose', ['class' => 'icon--gray', 'size' => '30'])?>
        </div>

        <div class="add-ads">

            <button class="add-ads btn btn--green">
                <a href="add.html">
                    <span class="add-plus">
                        <img src="<?= Yii::getAlias('@dist') ?>/img/add.svg" alt="AddAds" class="svg">
                    </span>
                    <span>Подать объявления</span>
                </a>
            </button>

        </div>

        <div class="row">
            <div class="col-sm-5">
                <?if(Yii::$app->user->identity->avatar):?>
                    <?= \yii\helpers\Html::img(Yii::$app->user->identity->thumb, ['style' => ''])?>
                <?endif?>
            </div>
            <div class="col-sm-7">
                <h4>
                    <?= Yii::$app->user->identity->name ?>
                </h4>

                <div class="user-balls">
                    <? $money = Yii::$app->user->identity->money ?>
                    <span class="number-balls"><?= $money == 0 ? Yii::t('app', 'no') : $money ?> </span><?= Yii::t('app', 'bonuses') ?>
                </div>
            </div>
        </div>

        <div class="user-links">

            <? foreach (common\models\Menu::ProfileMenu() as $menu): if(isset($menu['hide']) && $menu['hide']) continue; ?>
                <div class="user-link">
                    <a href="<?= \yii\helpers\Url::to($menu['link']) ?>">
                        <div class="user-link__icon">
                            <img src="<?= Yii::getAlias('@dist') ?>/img/<?= $menu['icon'] ?>" alt="<?= $menu['name'] ?>"
                                 class="svg">
                        </div>
                        <div class="user-link__href"><?= $menu['name'] ?></div>
                    </a>
                </div>
            <? endforeach; ?>


        </div>

        <?= $this->render('//common/_category_full'); ?>

    </div>
<? endif; ?>