<?php
/**
 * Created by PhpStorm.
 * User: York
 * Date: 28.12.2017
 * Time: 12:29
 */

use kosv\ulogin\widget\UloginWidget;
use yii\helpers\Html;
use common\models\CMS;
use yii\widgets\ActiveForm;

/* @var $this \yii\web\View */

?>


<!-- Login -->
<div class="white-popup mfp-hide mfp-left" id="modal-login">

    <div class="modal-close" data-modal-close>
        <?= \common\models\CMS::svg('menuclose', ['class' => 'icon--gray', 'size' => '30'])?>
    </div>

    <h3 class="margin-top-large">
        <?= Yii::t('app', 'Login to your account') ?>
    </h3>

    <div class="catalog-bottom__social">

        <div class="catalog-social__container tes">
            <?php
            echo UloginWidget::widget([
                'options' => [
                    'display' => 'panel',
                    'fields' => ['first_name', 'last_name', 'email', 'photo'],
                    'providers' => ['google', 'facebook'],
//                    'callback' => 'authCallback', //Ваш js callback, который будет вызыватся для отправки данных в контроллер
                    'redirect_uri' => \yii\helpers\Url::base('https').\yii\helpers\Url::to(['site/login-soc']) //При использовании callback, нужно поставить пустую строку
                ],
            ]);
            ?>
        </div>

    </div>

        <div>
            <a v-show="login_type == 0" class="link"
               @click="login_type = 1"><?= Yii::t('app', 'Login with phone') ?></a>
            <a v-show="login_type == 1" class="link"
               @click="login_type = 0"><?= Yii::t('app', 'Login with email') ?></a>
        </div>

        <div v-show="login_type == 0" id="email-area">

            <?php $model = new \common\models\LoginForm();
            $form = ActiveForm::begin([
                'id' => 'login-form',
                'enableAjaxValidation' => true,
                'action' => ['site/login'],
                'validationUrl' => ['site/validate-login'],
            ]); ?>


            <?= $form->field($model, 'email')->textInput(['autofocus' => true]) ?>

            <?= $form->field($model, 'password')->passwordInput() ?>

            <div class="login-go">
                <div class="login-go__left">
                    <?= Html::submitButton(CMS::svg('user', ['icon--white']) . Yii::t('app', 'Login'), ['class' => 'btn btn--green btn--icon', 'enableClientValidation' => false]) ?>
                </div>

            <div class="login-go__right">
                <a href="<?= \yii\helpers\Url::to(['site/request-password-reset']) ?>"
                   style="width: 100px;"><?= Yii::t('app', 'Forgot password?') ?></a>
            </div>
        </div>


            <?php ActiveForm::end(); ?>

        </div>
        <div v-show="login_type == 1" id="phone-area">

            <sms-login
                    codes='<?= json_encode(Yii::$app->params['sms_phone_codes']) ?>'
                    text_again="<?=Yii::t('app', 'Send code again');?>"
                    text_again_in="<?=Yii::t('app', 'Send code again in');?>"
                    text_send_code="<?=Yii::t('app', 'Send code');?>"
                    text_error="<?= Yii::t('app', 'Incorrect code'); ?>"
                    text_accept_code="<?=Yii::t('app', 'Accept code');?> "></sms-login>
        </div>

        <div class="login-registr">

        <span><?= Yii::t('app', 'Doesn\'t have profile?') ?></span>
            <a href="#modal-register" data-sideup><?= Yii::t('app', 'Registration') ?></a>
    </div>

    <? //= $this->render('//common/_category_full'); ?>


    </div>

    <!-- Registration -->