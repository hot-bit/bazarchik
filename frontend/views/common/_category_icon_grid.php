<?php
/**
 * Created by PhpStorm.
 * User: York
 * Date: 15.09.2017
 * Time: 12:27
 */

use common\models\CMS;

$model = \common\models\Categories::find()->where('level = 1')->orderBy('position')->all();
?>

<?foreach($model as $item):?>
    <a href="<?=$item->link?>" class="find-grid">
        <div class=" category-item">

            <div class="category-item__icon <?= $item->color?>">
                <i class="icon-<?= $item->icon?>"></i>
            </div>
            <div class="category-item__text">
                <?=$item->name?>
            </div>
        </div>
    </a>
<?endforeach;?>
