<?php
/**
 * Created by PhpStorm.
 * User: York
 * Date: 15.09.2017
 * Time: 17:48
 */


use common\models\Categories;
use common\models\CMS;

$categories = Categories::Tree();

?>

<!-- Контейнер для всех категории -->
<div class="find-category__content">
    <div class="grid-find__large">
        <? $i = 0; foreach($categories[0] as $item):?>
        <?if($i != 0 && $i % 4 == 0):?></div><div class="grid-find__large"><?endif?>
            <!-- Категория №<?=$i?>-->
            <div class="find-grid <?=$item->color?>">
                <div class="find-category__item category-item">

                    <div class="category-item__icon <?= $item->color?>">
                        <i class="icon-<?= $item->icon?>"></i>
                    </div>
                    <div class="category-item__text">
                        <?=$item->name?>
                    </div>
                </div>
                <!-- DropDown для категория №<?=$i?> -->
                <div class="find-category__dropdown hidden">
                    <div class="find-dropdown__content">
                        <?if(isset($categories[$item->id])):?>
                            <ul>
                                <li>
                                    <a href="<?= $item->link ?>">
                                        <span class="title"><?= Yii::t('app', 'Show all') ?></span>
                                        <span class="number"><?=Yii::t('app', 'total')?> <?=$item->count_all?></span>
                                    </a>
                                </li>

                                <? $c = 0;
                                /**
                                 * @var $cat Categories
                                 */
                                foreach($categories[$item->id] as $cat):?>
                                    <li>
                                        <a href="<?=$cat->link?>">
                                            <span class="title"><?=$cat->name_en?></span>
                                            <span class="number"><?=Yii::t('app', 'total')?> <?=$cat->count_all?></span>
                                        </a>
                                    </li>
                                <? $c++; endforeach;?>
                            </ul>
                        <?endif?>
                    </div>
                </div>
                <!-- END DropDown для категория №<?=$i?>  -->
            </div>
            <!-- END категория -->
        <? $i++; endforeach;?>
    </div>
</div>
<!-- END Контейнер для всех категории   -->
