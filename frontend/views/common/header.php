<?php
/**
 * Created by PhpStorm.
 * User: York
 * Date: 26.01.2018
 * Time: 11:45
 */

use common\models\CMS;
use yii\helpers\Html;

?>

<header class="header">
    <div class="header-content__left">
        <div class="header-logo">
            <a href="<?= Yii::$app->request->home ?>">
                <img src="<?= Yii::getAlias('@dist') ?>/img/logo.png" alt="Cyprus BAZAR">
            </a>
        </div>

        <div class="header-free__ads">
            <a href="<?= Yii::$app->request->home ?>">
                <span><?= Yii::t('app', 'Free<br />ads') ?> </span>
            </a>
        </div>

        <div class="header-icon__apple">
            <a href="<?= Yii::$app->params['ios_app'] ?>" target="_blank">
                <img src="<?= Yii::getAlias('@dist') ?>/img/apple.svg" alt="Apple Icon" class="svg">
            </a>
        </div>

        <div class="header-icon__market">
            <a href="<?= Yii::$app->params['android_app'] ?>" target="_blank">
                <img src="<?= Yii::getAlias('@dist') ?>/img/market-header.svg" alt="Market SVG" class="svg">
            </a>
        </div>

        <div class="header-language selectbox__noborder">
            <?php
            $url = $_SERVER['REQUEST_URI'];
            $url = substr($url, 3);
            ?>
            <?= Html::dropDownList('lang', Yii::$app->request->lngCode, \yii\helpers\ArrayHelper::map(\common\models\Lang::find()->all(), 'url', 'name'), ['id' => 'current_language',
                'onchange' => 'window.location = "/"+$(this).val()+"' . $url . '"',
            ]) ?>
        </div>
    </div>

    <div class="header-content__right">
        <?if(!Yii::$app->user->isGuest):?>
            <div class="margin-right-large"><a href="<?= \yii\helpers\Url::to(['chat/index'])?>" class="ion-chatboxes link-gray-orange <?= Yii::$app->user->identity->hasUnreadChat ? 'text-orange' : ''?>" style="font-size: 22pt"></a></div>
        <?endif?>

        <div class="header-likes">
            <a href="<?= \yii\helpers\Url::to(['site/favorite']) ?>" class="link-icon link-gray-orange link-no-line">
                <i class="icon icon-like text-icon "></i>
                <span v-show="favorite_count != 0">{{ favorite_count }}</span>
            </a>
        </div>

        <div class="auth">
            <div>
                <? if (!Yii::$app->user->isGuest): ?>
                    <a href="#modal-user" data-sideup class="link-icon link-gray-orange">
                        <i class="icon icon-user text-icon"></i>
                        <span><?= Yii::$app->user->identity->name ?></span>
                    </a>
                <? else: ?>
                    <a href="#modal-login" data-sideup class="link-icon link-gray-orange">
                        <i class="icon icon-user text-icon"></i>
                        <span><?= Yii::t('app', 'Login') ?></span>
                    </a>
                <? endif; ?>
            </div>
            <? if (Yii::$app->user->isGuest): ?>
                <div class="auth-login__no hidden">
                    <a href="#"><?= Yii::t('app', 'Login') ?></a>
                </div>
            <? endif; ?>
        </div>

        <div class="add-ads">
            <a class="add-ads btn btn--green" href="<?= \yii\helpers\Url::to(['adv/add']) ?>">
                <i class="icon-add"></i>
                <span><?= Yii::t('app', 'Add adv') ?></span>
            </a>
        </div>

        <a class="mobile-burger" data-sideup href="#<?= Yii::$app->user->isGuest ? 'modal-login' : 'modal-user' ?>">
            <?= CMS::svg('menu', ['class' => 'icon--gray', 'size' => 33]) ?>
        </a>
    </div>
</header>
