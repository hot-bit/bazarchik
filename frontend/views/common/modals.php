<?php
/**
 * Created by PhpStorm.
 * User: York
 * Date: 15.09.2017
 * Time: 18:02
 */
/* @var $this yii\web\View */

use common\models\CMS;
use yii\bootstrap\ActiveForm;
use yii\helpers\Html;

?>



<?= $this->render('//common/modal/portfolio')?>
<? if (Yii::$app->user->isGuest): ?>

    <?= $this->render('//common/modal/login')?>
    <?= $this->render('//common/modal/registration')?>
<? endif; ?>

<?//= $this->render('//common/modal/subscribe')?>