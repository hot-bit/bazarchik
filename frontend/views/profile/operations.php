<?php
/**
 * Created by PhpStorm.
 * User: York
 * Date: 04.11.2017
 * Time: 12:37
 */

use yii\grid\GridView;
use yii\widgets\Pjax;

/* @var $this \yii\web\View */

Yii::$app->breadcrumbs->Items([
    Yii::t('app', 'Operations')
]);
?>

<section class="content">
    <h1><?=Yii::t('app', 'Operations');?> </h1>


    <?php Pjax::begin(); ?>
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'tableOptions' => [
            'class' => 'table table-striped'
        ],
        'pager' => [
            'prevPageLabel' => false,
            'nextPageLabel' => false
        ],
        'columns' => [
            'id_adv',
            'date:date',
            'f_price',
            'f_items',
            'f_status',
            'token'

        ],
    ]); ?>
    <?php Pjax::end(); ?>


</section>