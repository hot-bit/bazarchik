<?php
/**
 * Created by PhpStorm.
 * User: York
 * Date: 31.12.2017
 * Time: 10:24
 *
 * @var $this \yii\web\View
 * @var $model \common\models\Adv[]
 */

$this->title = Yii::t('app', 'My ads');
Yii::$app->breadcrumbs->Items([
    $this->title
]);

$statusList = [ 
    \common\models\Adv::STATUS_BLOCK => Yii::t('app', 'Banned'),
    \common\models\Adv::STATUS_MODERATE => Yii::t('app', 'On moderate'),
    \common\models\Adv::STATUS_ACTIVE => Yii::t('app', 'Active'),
    \common\models\Adv::STATUS_PAUSE => Yii::t('app', 'Paused'),
    \common\models\Adv::STATUS_STOP => Yii::t('app', 'Stopped'),
    \common\models\Adv::STATUS_REMOVED => Yii::t('app', 'Removed')
];
$activeStatus = null;
if(isset($_GET['status'])) $activeStatus = $_GET['status'];

?>

<section class="content">

    <h1><?= $this->title; ?> </h1>

    <div class="row">
        <div class="col-sm-9"></div>
        <div class="col-sm-3">
            <?= \yii\helpers\Html::dropDownList('statusList', $activeStatus, $statusList, [
                'prompt' => Yii::t('app', 'All'),
                'onchange' => "
                    console.log($(this).val())
                    if($(this).val() == '') {
                        window.location = '".\yii\helpers\Url::to(['profile/ads'])."';
                        return true;
                    } 
                    window.location = '".\yii\helpers\Url::to(['profile/ads', 'status' => ''])."'+$(this).val()                    
                "
            ])?>
        </div>
    </div>


    <div class="my-ads__content">

        <?if($model):?>
            <? foreach ($model as $item): ?>
                <?= $this->render('//adv/catalog/_catalog-item-list', ['item' => $item, 'isMyAds' => true]) ?>
            <? endforeach; ?>
        <?else:?>
            <h3><?=Yii::t('app', 'Nothing found');?> </h3>
        <?endif?>

    </div>
</section>

