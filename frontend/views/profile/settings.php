<?php
/**
 * Created by PhpStorm.
 * User: York
 * Date: 30.10.2017
 * Time: 9:42
 */


/* @var $model \common\models\User
 * @var $this \yii\web\View
 */

use common\models\CMS;
use yii\helpers\Html;
use yii\widgets\ActiveForm;

Yii::$app->breadcrumbs->Items([
    Yii::t('app', 'Profile settings')
]);

$phone_hint = '';
if ($model->success_phone)
    $phone_hint = '<i class="success-input"></i>';
elseif ($model->phone_code == '357')
    $phone_hint = \yii\helpers\Html::a(Yii::t('app', 'Approve my phone'), '#confirm-phone', [
        'data-popup' => '',
        'onclick' => "return false;"
    ]);


//Yii::$app->session->setFlash('success', 'This is the message');
?>



<section class="settings">

    <div class="settings-title">
        <h2><?=Yii::t('app', 'Profile settings');?> </h2>
    </div>


    <div class="">

        <?php $form = ActiveForm::begin([
            'id' => 'profile-form',
            'options' => [
                'enctype' => 'multipart/form-data'
            ]
        ]); ?>
        <div class="general-fields">
            <?=Html::errorSummary($model)?>

            <div class="row">
                <div class="col-sm-6">
                    <div class="row">
                        <div class="col-sm-6">
                            <?= $form->field($model, 'name')->textInput() ?>
                        </div>
                        <div class="col-sm-6">
                            <?= $form->field($model, 'phone')->widget(\frontend\widgets\bazar\PhoneField::className(), [
                                'wrap_class' => 'form-group form-group--small general-field__container',
                                'placeholder' => Yii::t('app', 'Your phone'),
                            ])
                                ->hint($phone_hint) ?>
                        </div>
                    </div>
                </div>
                <div class="col-sm-6">
                    <div class="row">
                        <div class="col-sm-4">
                            <?= $form->field($model, 'b_day')->dropDownList(CMS::dayList()) ?>
                        </div>
                        <div class="col-sm-4">
                            <?= $form->field($model, 'b_month')->dropDownList(CMS::monthList()) ?>
                        </div>
                        <div class="col-sm-4">
                            <?= $form->field($model, 'b_year')->dropDownList(CMS::yearList()) ?>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-sm-6">
                    <?= $form->field($model, 'email')->textInput(['disabled' => $model->success_email ? true : false])
                        ->hint($model->success_email ? '<i class="success-input"></i>' : \yii\helpers\Html::a(Yii::t('app', 'Approve my email'), ['confirm-email'])) ?>
                </div>
                <div class="col-sm-6">
                    <div class="row">
                        <div class="col-sm-6">
                            <?= $form->field($model, 'skype')->textInput() ?>
                        </div>
                        <div class="col-sm-6">
                            <radio-button
                                @user_type="setGlobalUserType"
                                items_url="<?=\yii\helpers\Url::to(['ajax/get-user-type-list'])?>"
                                name="User[is_business]"
                                active="<?=$model->is_business ? 1 : 0?>"
                            ><?=Yii::t('app', "Account type")?></radio-button>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-sm-12">
                    <?=$form->field($model, 'langs_arr')->checkboxList(['en' => Yii::t('app', 'English'), 'el' => Yii::t('app', 'Greek'), 'ru' => Yii::t('app', 'Russian')], ['class' => 'checkbox-list'])?>
                </div>
            </div>

            <div class="row" v-show="user_type > 0">
                <div class="col-sm-4">
                    <?= $form->field($model, 'company')->textInput() ?>
                </div>
                <div class="col-sm-4">
                    <?= $form->field($model, 'website')->textInput() ?>
                </div>
                <div class="col-sm-4">
                    <?= $form->field($model, 'business_link')->textInput() ?>
                </div>
            </div>
            <div v-show="user_type > 0">
                <profile-address
                        text_add_address="<?= Yii::t('app', 'Add address') ?>"
                        name="User[address_array]"
                        endpoint="<?=\yii\helpers\Url::to(['ajax/load-profile-address'])?>"
                ></profile-address>
            </div>

            <div v-show="user_type > 0">
                <?= $form->field($model, 'company_description')->textarea() ?>
            </div>

            <div class="row">
                <div class="col-sm-3">
                    <?= $form->field($model, 'city')->dropDownList(\common\models\City::map()) ?>
                </div>
                <div class="col-sm-3">
                    <?= $form->field($model, 'area')->dropDownList(\common\models\Area::map($model->city)) ?>
                </div>
            </div>


            <div class="row">
                <div class="col-sm-3">
                    <?= $form->field($model, 'old_password')->passwordInput() ?>
                </div>
                <div class="col-sm-3">
                    <?= $form->field($model, 'new_password')->passwordInput() ?>
                </div>
                <div class="col-sm-3">
                    <?= $form->field($model, 'new_password_repeat')->passwordInput() ?>
                </div>
            </div>

            <?= $form->field($model, 'imageFile')->widget(\common\widgets\cms\ImageField::className(), ['deleteFile' => \yii\helpers\Url::to(['profile/delete-ava'])]) ?>
            <?= Html::submitButton($model->isNewRecord ? Yii::t('app', 'Create') : Yii::t('app', 'Update'), ['class' => 'btn btn--green']) ?>
        </div>

        <?php ActiveForm::end(); ?>
    </div>

    <div id="confirm-phone" class="white-popup mfp-hide">
        <h3><?=Yii::t('app', 'Enter code from SMS');?> </h3>

        <send-sms-code
                text_again="<?=Yii::t('app', 'Send code again');?>"
                text_again_in="<?=Yii::t('app', 'Send code again in');?>"
                text_send_code="<?=Yii::t('app', 'Send code');?>"
                text_error="<?= Yii::t('app', 'Incorrect code'); ?>"
                text_accept_code="<?=Yii::t('app', 'Accept code');?> "></send-sms-code>
    </div>
</section>

