<?php
/**
 * Created by PhpStorm.
 * User: York
 * Date: 02.01.2018
 * Time: 15:32
 */

use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model  \common\models\Adv */

$this->title = Yii::t('app', 'Adv parameters');

Yii::$app->breadcrumbs->Items([
    \yii\helpers\Url::to(['ads']) => Yii::t('app', 'My ads'),
    \yii\helpers\Url::to(['edit-adv', 'id' => $model->id]) => $model->title,
    $this->title
]);

$filter = $model->_category->filter;
?>

<section class="content">
    <h1><?= $this->title?></h1>

    <?php $form = ActiveForm::begin([
        'id' => 'params-adv-form'
    ]); ?>

    <?=$this->render('//filters/add-adv/'.$filter, ['id' => $model->id, 'form' => $form, 'model' => $params])?>
    <?= \yii\helpers\Html::submitButton(Yii::t('app', 'Save'), ['class' => 'btn btn--green']) ?>
    <?php ActiveForm::end(); ?>
</section>
