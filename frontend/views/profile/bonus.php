<?php
/**
 * Created by PhpStorm.
 * User: York
 * Date: 31.12.2017
 * Time: 10:46
 */

/* @var $this yii\web\View */
/* @var $model \common\models\Adv  */

?>

<section class="content">

    <h1><?=Yii::t('app', 'Coupons');?> </h1>

    <div class="row">

        <?foreach ($model as $item):?>
            <div class="col-sm-3">
                <?=$this->render('//adv/catalog/_catalog-item!new', ['item' => $item])?>
            </div>
        <?endforeach;?>
    </div>
</section>
