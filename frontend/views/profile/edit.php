<?php
/**
 * Created by PhpStorm.
 * User: York
 * Date: 01.01.2018
 * Time: 17:04
 */

/* @var $this yii\web\View */
/* @var $model \common\models\Adv */

$this->title = $model->title;

Yii::$app->breadcrumbs->Items([
    \yii\helpers\Url::to(['ads']) => Yii::t('app', 'My ads'),
    $this->title
]);

$model->text = strip_tags($model->text);

?>

<section class="content">
    <h1><?= $this->title ?></h1>

    <?php $form = yii\widgets\ActiveForm::begin([
        'id' => 'edit-adv-form',
    ]); ?>

    <?= $form->field($model, 'title')->textInput(['id' => 'a-title']) ?>

    <?= $form->field($model, 'text')->textarea(['id' => 'a-text']) ?>

    <div class="row">
        <div class="col-sm-3">
            <?= $form->field($model, 'price') ?>
        </div>
    </div>

    <div class="form-group">
        <label class="control-label"><?=Yii::t('app', 'Images');?></label>
        <upload-images adv_id="<?= $model->id?>"  text_upload="<?=Yii::t('app', 'Upload images');?> "></upload-images>
    </div>


    <? if ($model->_category->has_filter): ?>
        <div class="form-group">
            <a href="<?= \yii\helpers\Url::to(['edit-params-adv', 'id' => $model->id])?>"><?=Yii::t('app', 'Edit parameters');?> </a>
        </div>
    <? endif ?>

    <div class="form-group">
        <?= \yii\helpers\Html::submitInput(Yii::t('app', 'Save'), ['class' => 'btn btn--green'])?>
    </div>


    <?php yii\widgets\ActiveForm::end(); ?>

</section>
