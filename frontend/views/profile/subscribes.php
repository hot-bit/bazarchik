<?php
/**
 * Created by PhpStorm.
 * User: York
 * Date: 04.11.2017
 * Time: 12:38
 */

use yii\grid\GridView;
use yii\widgets\Pjax;

/* @var $this \yii\web\View */

Yii::$app->breadcrumbs->Items([
    Yii::t('app', 'Subscribes')
]);
?>

<section class="content">
    <h1><?=Yii::t('app', 'Subscribes');?> </h1>

    <?php Pjax::begin(); ?>
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'tableOptions' => [
            'class' => 'table table-striped'
        ],
        'columns' => [
            'date:date',
            'category_name',
            'f_status',
            'action:html'

        ],
    ]); ?>
    <?php Pjax::end(); ?>

</section>