<?php
/**
 * Created by PhpStorm.
 * User: York
 * Date: 24.11.2017
 * Time: 12:34
 *
 * @var $this \yii\web\View
 * @var $model \common\models\FaqCategory[]
 */

Yii::$app->breadcrumbs->Items([
    Yii::t('app', 'Breadcrumbs')
]);

?>


<section class="content">

    <h1><?=Yii::t('app', 'FAQ');?> </h1>


    <div class="faq">
        <?foreach ($model as $item):?>
            <div class="faq-slogan"><?=$item->name?></div>
            <ul>
                <?foreach ($item->_items as $faq):?>
                    <li>
                        <a href="<?=\yii\helpers\Url::to(['faq/get', 'link' => $faq->link])?>" class=""><?=$faq->title?></a>
                    </li>
                <?endforeach?>
            </ul>
        <?endforeach;?>
    </div>

</section>
