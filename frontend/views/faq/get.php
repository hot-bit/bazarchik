<?php
/**
 * Created by PhpStorm.
 * User: York
 * Date: 24.11.2017
 * Time: 12:34
 *
 * @var $this \yii\web\View
 * @var $model \common\models\Faq
 */


Yii::$app->breadcrumbs->Items([
    \yii\helpers\Url::to(['faq/index']) => Yii::t('app', 'Breadcrumbs'),
    $model->title
]);

?>


<section class="content">

    <h1><?=$model->title;?> </h1>


    <div class="faq-text">
        <?=$model->text?>
    </div>

</section>



