<?php
/**
 * Created by PhpStorm.
 * User: York
 * Date: 11.01.2018
 * Time: 12:31
 */

use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $adv \common\models\Adv */
/* @var $model \frontend\models\CouponForm */

$this->title = Yii::t('app', 'Buy coupon');
Yii::$app->breadcrumbs->Items([
    $this->title
]);
?>


<section class="content">
    <h1><?= $this->title?></h1>

    <?php $form = ActiveForm::begin([
        'id' => 'coupon-form'
    ]); ?>

    <?= $form->errorSummary($model)?>

    <div class="row">
        <div class="col-sm-6">
            <table class="table">
                <tr>
                    <td><b><?=Yii::t('app', 'Price');?></b> </td>
                    <td>&euro; <?= $model->price?></td>
                </tr>
                <tr>
                    <td><b><?=Yii::t('app', 'Available');?></b> </td>
                    <td><?= $model->available?></td>
                </tr>
                <tr>
                    <td><b><?=Yii::t('app', 'Your email');?></b> </td>
                    <td><?= $model->email?></td>
                </tr>
                <tr>
                    <td><b><?=Yii::t('app', 'Your phone');?></b> </td>
                    <td><?= $model->f_phone?></td>
                </tr>
            </table>
            <?=$form->field($model, 'rules')->checkbox(['label' => \yii\helpers\Html::a(Yii::t('app', 'Coupon exchange terms'), ['page/get', 'url' => 'coupon-exchange-terms'])])?>

            <?= \yii\helpers\Html::submitButton(Yii::t('app', 'Buy coupon'), ['class' => 'btn btn--green'])?>
            <br>

            <small class="text text-green"><?=Yii::t('app', 'Available bounuses');?> &euro; <?= Yii::$app->user->identity->money?> </small>

        </div>
    </div>



    <?php ActiveForm::end(); ?>

</section>



