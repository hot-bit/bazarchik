<?php
/**
 * Created by PhpStorm.
 * User: York
 * Date: 14.09.2017
 * Time: 20:51
 */

use common\models\CMS;

$item_params = '';
if($item->is_vip && !isset($no_vip))
    $item_params .= ' vip-ads';
if($item->is_prem)
    $item_params .= ' premium-container';
if($item->is_color)
    $item_params .= ' highlighting-ads';

?>


<div class="principaly-newads__container <?=$item_params?>">

    <div class="principaly-newads__image">
        <a href="<?=$item->link?>">
            <img src="<?=$item->main_image?>">

            <?if($item->some_photo):?>
                <div class="ads-image__more">
                    <?=CMS::svg('multiplyphotos', ['class' => 'icon--white'])?>
                </div>
            <?endif;?>

            <?if($item->is_vip && !isset($no_vip)):?>
                <div class="vip-ads__title">vip</div>
            <?endif?>

            <?if($item->is_color):?>
                <div class="highlighting-gradient"></div>
            <?endif;?>
        </a>
    </div>

    <div class="principlay-newads__all">

        <div class="principaly-newads__title"><h4><a title="<?=$item->title?>" href="<?=$item->link?>"><?=$item->title?></a></h4></div>
        <div class="principaly-newads__info">
            <?if($item->_city):?>
                <div class="principaly-info__city"><?=$item->_city->name?></div>
            <?endif;?>
            <div class="principaly-info__date"><?=Yii::$app->formatter->asRelativeTime($item->date_create)?></div>
        </div>
        <div class="pricinaply-newads__bottom">
            <div class="newads-bottom__price"><?=$item->f_price?></div>
            <?= \frontend\widgets\Favorite::widget([
                'is_active' => Yii::$app->favorite->isActive($item->id),
                'item_id' => $item->id
            ])?>
        </div>
    </div>
</div>
