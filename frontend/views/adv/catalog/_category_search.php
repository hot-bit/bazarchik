<?php
/**
 * Created by PhpStorm.
 * User: York
 * Date: 15.09.2017
 * Time: 12:31
 */

use common\models\Categories;
use common\models\CMS;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;

/**
 * @var $this \yii\web\View
 * @var $category \common\models\Categories
 * @var $catalog \frontend\models\Catalog
 */

$model = $catalog->filter;

$category_arr = [
    'id' => [],
    'options' => []
];

//if(in_array($catalog->subcategory_type, ['brand', 'model']))
//    $category->filter = 'auto';
if($model->_category){
    foreach ($model->_category->stairs_models as $item) {
        $category_arr['id'][] = $item->id;
        $category_arr['options'][] = ArrayHelper::map(Categories::find()->where(['parent' => $item->parent])->andWhere(['header' => 0])->all(), 'id', 'name');
    }
    $category_arr['id'][] = '';
    $category_arr['options'][] = ArrayHelper::map(Categories::find()->where(['parent' => $model->_category->id])->andWhere(['header' => 0])->all(), 'id', 'name');

}
else{
    $category_arr['id'][] = 0;
    $category_arr['options'][] = ArrayHelper::map(Categories::find()->where(['parent' => 0])->andWhere(['header' => 0])->all(), 'id', 'name');
}

$this->registerJs("
    $('.category-wrap').on('change' ,'.category-select', function(){
        var level = parseInt($(this).parents('.category-area').attr('data-level'));
        var val = $(this).val();
        
        $.post('" . \yii\helpers\Url::to(['ajax/get-category-select']) . "', {parent:val}, function(o){ 
            if(o.select)
                $('.category-area[data-level=\"'+(level+1)+'\"]').html(o.select); 
            else
                $('.category-area[data-level=\"'+(level+1)+'\"]').html('')
            
            $('.filter-area').html('');
                
            $('#adv-category').val(val)
            for(var i = level+2; i <= 4; i++ ){
                $('.category-area[data-level=\"'+i+'\"]').html('')
            }
            $('select').SumoSelect(); 
        })
        
    });
");

?>


<? $form = \yii\widgets\ActiveForm::begin([
    'method' => 'GET',
    'id' => 'catalog-filter-form',
    'action' => ['adv/search', 'cat' => $catalog->category],
    'options' => [
        'class' => 'find-ads',
        'filter-request' => \yii\helpers\Url::to(['ajax/filter-request'])
    ],
    'fieldConfig' => [
        'options' => [
            'class' => 'form-group form-group--no-margin general-field__container',
        ],
    ]
]); ?>

    <div class="form-group form-group--no-margin general-field__container field-catalogfilter-price_from has-success">
        <label for="catalogfilter-price_from" class="control-label"><?= Yii::t('app', 'Price'); ?> </label>
        <div>
            <?= Html::activeTextInput($model, 'price_from', ['style' => 'width: 45%;', 'placeholder' => Yii::t('app', 'from')]) ?>
            -
            <?= Html::activeTextInput($model, 'price_to', ['style' => 'width: 45%;', 'placeholder' => Yii::t('app', 'to')]) ?>

            <div id="price-range" class="range-slider"
                 data-min="<?= $catalog->min_price ?>"
                 data-max="<?= $catalog->max_price ?>"
                 data-value_from="<?= $model->price_from ?>"
                 data-value_to="<?= $model->price_to ?>"
                 data-f1="catalogfilter-price_from"
                 data-f2="catalogfilter-price_to"
                 data-step="1"
            ></div>
        </div>
        <div class="clearfix"></div>
    </div>
<?= Html::activeHiddenInput($model, 'category_id', ['id' => 'adv-category']) ?>

    <div class="form-group category-wrap margin-top">
        <label for="" class="control-label"><?= Yii::t('app', 'Category'); ?> </label>

        <div class="category-area" data-level="1">
            <? if ($category_arr['options'][0]): ?>
                <?= Html::dropDownList('category', $category_arr['id'][0], $category_arr['options'][0], ['class' => 'form-control category-select', 'prompt' => Yii::t('app', '-- none --')]) ?>
            <? endif ?>
        </div>
        <div class="category-area" data-level="2">
            <? if ($category_arr['options'][1]): ?>
                <?= Html::dropDownList('category', $category_arr['id'][1], $category_arr['options'][1], ['class' => 'form-control category-select', 'prompt' => Yii::t('app', '-- none --')]) ?>
            <? endif ?>
        </div>
        <div class="category-area" data-level="3">
            <? if ($category_arr['options'][2]): ?>
                <?= Html::dropDownList('category', $category_arr['id'][2], $category_arr['options'][2], ['class' => 'form-control category-select', 'prompt' => Yii::t('app', '-- none --')]) ?>
            <? endif ?>
        </div>
        <div class="category-area" data-level="4">
            <? if ($category_arr['options'][3]): ?>
                <?= Html::dropDownList('category', $category_arr['id'][3], $category_arr['options'][3], ['class' => 'form-control category-select', 'prompt' => Yii::t('app', '-- none --')]) ?>
            <? endif ?>
        </div>
    </div>


<? if ($category->filter != 'prop'): ?>
    <?= $form->field($model, 'city_id')->dropDownList(\common\models\City::map(), ['class' => 'SelectBox', 'multiple' => true, 'placeholder' => Yii::t('app', 'All cities')]) ?>
<? endif ?>


    <div class="filter-area">
        <? if ($model->_category && $model->_category->filter): ?>
            <?= $this->render('//filters/search/' . $model->_category->filter, compact('form', 'model')) ?>
        <? endif; ?>
    </div>


    <div class="show-all__ads">

        <?= Html::submitButton('
            <span class="button-icon">
            ' . CMS::svg('search', ['class' => 'icon--white', 'size' => '20']) . '
                </span><span class="button-text sidebar-find-button"> ' . Yii::t('app', 'Show {count} ads', ['count' => $catalog->count_ads > 99 ? '99+' : $catalog->count_ads]) . '</span>') ?>

    </div>

<? \yii\widgets\ActiveForm::end(); ?>