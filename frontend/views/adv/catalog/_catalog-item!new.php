<?php
/**
 * Created by PhpStorm.
 * User: York
 * Date: 08.11.2017
 * Time: 18:11
 *
 * @var  $item \common\models\Adv
 * @var $catalog_view boolean
 */
if(!isset($catalog_view)) $catalog_view = false;

?>

<div class="catalog-item <?= $catalog_view ? 'catalog-view' : '' ?> <?= $item->is_color ? 'catalog-item__color' : ''?> <?= $item->is_prem ? 'catalog-item__prem' : ''?>">
    <div class="catalog-item__image">
        <a href="<?=$item->link?>">
            <img src="<?=$item->main_image?>">


            <?if(count($item->images) > 1):?>
                <?=\common\models\CMS::svg('multiplyphotos', ['class' => 'icon--white'])?>
            <?endif;?>

            <?if($item->is_vip && !isset($no_vip)):?>
                <div class="catalog-item__vip">vip</div>
            <?endif?>
            <?if($item->is_color):?>
                <div class="catalog-item__color-gradient"></div>
            <?endif;?>
        </a>
    </div>

    <a href="<?=$item->link?>" class="catalog-item__title" title="<?=$item->title?>">
        <?=$item->title?>
    </a>
    <div class="catalog-item__params">
        <div class="catalog-item__params-city"><?=$item->_city->name?></div>
        <div class="catalog-item__params-date" data-time="<?= $item->date_create?>"></div>
        <div class="catalog-item__params-price"><?=$item->f_price?></div>
        <div class="catalog-item__params-favorite">

            <?= \frontend\widgets\Favorite::widget([
                'is_active' => Yii::$app->favorite->isActive($item->id),
                'item_id' => $item->id,
            ])?>
        </div>
    </div>
</div>
