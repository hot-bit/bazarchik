<?php
/**
 * Created by PhpStorm.
 * User: York
 * Date: 15.12.2017
 * Time: 16:40
 *
 * @var \frontend\models\Catalog $catalog
 * @var \common\models\Categories $item
 * @var int $i
 */
?>

<li class="<?= $i > 7 ? 'sub-category__extra' : ''?> sub-category__item col-sm-3 col-xs-6">
    <a class="sub-category__title" href="<?=$item->link?>"><?=$item->name?></a>
    <div class="sub-category__info">
        <?=Yii::t('app', 'total')?> <?=$item->count_all?>
    </div>
</li>
