<?php
/**
 * Created by PhpStorm.
 * User: yorks
 * Date: 31.12.2017
 * Time: 23:34
 */

use common\models\Adv;

/* @var $this yii\web\View */
/* @var $item \common\models\Adv */
/* @var $isMyAds boolean */

$status_color = '';
if ($item->status == Adv::STATUS_ACTIVE) $status_color = 'honeydew';
elseif (in_array($item->status, [Adv::STATUS_BLOCK, Adv::STATUS_REMOVED])) $status_color = 'lavenderblush';
elseif (in_array($item->status, [Adv::STATUS_MODERATE, Adv::STATUS_PAUSE, Adv::STATUS_STOP])) $status_color = 'floralwhite';

?>

<div class="row margin-bottom" style="background: <?= $status_color ?>; padding: 15px 0">
    <div class="col-sm-3">
        <a href="<?= $item->link ?>">
            <img src="<?= $item->main_image ?>" style="width: 100%;" alt="">
        </a>
    </div>
    <div class="col-sm-7">
        <div class="row">
            <div class="col-sm-12" style="height: 40px;">
                <h4 class="margin-top-small" style="height: 24px; overflow: hidden">
                    <a href="<?= $item->link ?>" class="link link-black link-no-line link-large link-hover-green">
                        <?= $item->title ?>
                    </a>
                </h4>
            </div>
        </div>
        <div class="row">
            <div class="col-sm-6">
                <div class="text-meta"><?= $item->_city->name ?></div>
            </div>
            <div class="col-sm-6 text-right">
                <div class="text-meta" data-time="<?= $item->date_create ?>"></div>
            </div>
        </div>
        <div class="margin-top text-green text-large"><?= $item->f_price ?></div>

        <? if ($isMyAds): ?>
            <div class="text-bottom" style="height: 85px;">

                <? if ($item->status != Adv::STATUS_REMOVED): ?>
                    <a class="link link-gray link-medium link-no-line margin-right"
                       href="<?= \yii\helpers\Url::to(['edit-adv', 'id' => $item->id]) ?>">
                        <i class="ion-android-create"></i>
                        <?= Yii::t('app', 'Edit'); ?>
                    </a>
                <? endif ?>
                <? if ($item->status == Adv::STATUS_ACTIVE): ?>
                    <a class="link link-gray link-medium link-no-line margin-right"
                       href="<?= \yii\helpers\Url::to(['stop-adv', 'id' => $item->id]) ?>">
                        <i class="ion-android-search"></i>
                        <?= Yii::t('app', 'Remove from search'); ?>
                    </a>
                <? endif ?>
                <? if ($item->status == Adv::STATUS_PAUSE): ?>
                    <a class="link link-gray link-medium link-no-line margin-right"
                       href="<?= \yii\helpers\Url::to(['start-adv', 'id' => $item->id]) ?>">
                        <i class="ion-android-search"></i>
                        <?= Yii::t('app', 'Return to search'); ?>
                    </a>
                <? endif ?>
                <? if ($item->status == Adv::STATUS_STOP): ?>
                    <a class="link link-gray link-medium link-no-line margin-right"
                       href="<?= \yii\helpers\Url::to(['start-adv', 'id' => $item->id]) ?>">
                        <i class="ion-android-search"></i>
                        <?= Yii::t('app', 'Post again'); ?>
                    </a>
                <? endif ?>

                <? if ($item->status != Adv::STATUS_REMOVED): ?>
                    <a class="link link-gray link-medium link-no-line margin-right"
                       href="<?= \yii\helpers\Url::to(['delete-adv', 'id' => $item->id]) ?>">
                        <i class="ion-android-close"></i>
                        <?= Yii::t('app', 'Remove'); ?>
                    </a>
                <? else: ?>
                    <a class="link link-gray link-medium link-no-line margin-right"
                       href="<?= \yii\helpers\Url::to(['start-adv', 'id' => $item->id]) ?>">
                        <i class="ion-android-search"></i>
                        <?= Yii::t('app', 'Restore'); ?>
                    </a>
                <? endif ?>
                <? if ($item->status == Adv::STATUS_ACTIVE): ?>
                    <a class="link link-medium link-hover-green link-no-line margin-right"
                       href="<?= \yii\helpers\Url::to(['adv/improve', 'id' => $item->id]) ?>">
                        <i class="ion-android-star-outline"></i>
                        <?= Yii::t('app', 'Improve ad'); ?>
                    </a>
                <? endif ?>

            </div>
        <? endif ?>
    </div>
    <div class="col-sm-2">
        <div>
            <?= Yii::t('app', 'Adv status'); ?> : <?= $item->status_name ?>
        </div>
        <? if ($item->status == Adv::STATUS_ACTIVE): ?>
            <div>
                <? if ($item->can_free_rise): ?>
                    <a href="<?= \yii\helpers\Url::to(['adv/free-rise', 'id' => $item->id]) ?>"><?= Yii::t('app', 'Rise in catalog for FREE'); ?> </a>
                <? else: ?>
                    <?= Yii::t('app', 'Free up: {day}', ['day' => $item->free_rise_in]); ?>
                <? endif ?>
            </div>
        <? endif ?>
        <div>
            <label><?= Yii::t('app', 'Improvements'); ?> </label> <br>
            <? if ($item->improve_list): ?>
                <?= implode(', ', $item->improve_list) ?>
            <? else: ?>
                <?= Yii::t('app', 'None'); ?>
            <? endif ?>
        </div>
    </div>
</div>
