<?php
/**
 * Created by PhpStorm.
 * User: York
 * Date: 15.12.2017
 * Time: 16:40
 *
 * @var \frontend\models\Catalog $catalog
 */

$top_categories = $catalog->children_categories;

if($catalog->subcategory_model){
    if($catalog->subcategory_type == 'brand')
        $top_categories = $catalog->subcategory_model->_models;
}
elseif($catalog->category_model && $catalog->category_model->id == 743)
    $top_categories = \common\models\Brand::find()->orderBy('count_used desc')->all();



?>

<?if($top_categories):?>
    <div class=" row">
        <ul class="sub-category">
            <?$i = 1; foreach ($top_categories as $item): if($item->count_all == 0) continue; ?>

                <?if($i == 8):?>
                    <li class="sub-category__more-button col-sm-3 col-xs-6">
                        <a href="#"><?=Yii::t('app', 'Show more');?>&nbsp;
                            <i class="ion-ios-arrow-down"></i>
                        </a>
                    </li>
                <?endif?>
                <?= $this->render('_catalog-subcategories-item', compact('item', 'catalog', 'i'))?>
            <?$i++; endforeach;?>

            <?if($i > 8):?>
                <li class="sub-category__less-button col-sm-3 col-xs-6">
                    <a href="#"><?=Yii::t('app', 'Show less');?>&nbsp;
                        <i class="ion-ios-arrow-up"></i>
                    </a>
                </li>
            <?endif?>
        </ul>
    </div>
<?endif;?>

