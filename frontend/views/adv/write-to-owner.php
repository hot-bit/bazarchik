<?php
/**
 * Created by PhpStorm.
 * User: York
 * Date: 14.11.2017
 * Time: 16:23
 *
 * @var $model \frontend\models\WriteToOwnerForm
 */

use yii\widgets\ActiveForm;


if($model->shop_id)
    Yii::$app->breadcrumbs->Items([
        $model->_shop->shop_link => $model->_shop->shop_name,
        Yii::t('app', 'Write to manager')
    ]);
else
    Yii::$app->breadcrumbs->Items([
        $model->_adv->link => $model->_adv->title,
        Yii::t('app', 'Write to owner')
    ]);

?>

<section class="content">

    <?php $form = ActiveForm::begin([
        'id' => 'write-to-owner-form'
    ]); ?>

    <h1><?= Yii::t('app', $model->shop_id ? 'Write a massage to the manager' : 'Write a message to the owner'); ?></h1>

    <?=\yii\helpers\Html::errorSummary($model)?>

    <h3><?= Yii::t('app', 'Subject'); ?>: <?=$model->_adv->title?></h3>
    <?=$form->field($model, 'name')->textInput() ?>
    <?=$form->field($model, 'phone')->textInput()?>
    <?=$form->field($model, 'email')->textInput()?>
    <?=$form->field($model, 'text')->textarea()?>

    <?=\yii\helpers\Html::submitButton(Yii::t('app', 'Send message'), ['class' => 'btn btn--green'])?>
    <?php ActiveForm::end(); ?>

</section>
