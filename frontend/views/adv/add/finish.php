<?php
/**
 * Created by PhpStorm.
 * User: York
 * Date: 06.11.2017
 * Time: 14:05
 *
 * @var $model \common\models\Adv
 * @var $this \yii\web\View
 */

Yii::$app->breadcrumbs->Items([
    Yii::t('app', 'Add adv')
])
?>


<section class="newadd">
    <div class="newadd-title">
        <h1><?=Yii::t('app', 'Your advertisement has been successfully posted');?> </h1>
    </div>

    <div class="row">
        <?=Yii::t('app', $this->render('_finish_text'), [
            'user_name' => $model->name,
            'user_email' => $model->email,
            'adv_id' => $model->id, 
            'adv_title' => $model->title
        ]);?>
    </div>

    <div class="clearbox" style="display: block"></div>
</section>
