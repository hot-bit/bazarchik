<?php
/**
 * Created by PhpStorm.
 * User: York
 * Date: 08.09.2017
 * Time: 16:06
 */
use common\models\CMS;
use yii\widgets\ActiveForm;

/* @var $model \frontend\models\AddAdvForm*/
?>

<div class="newadd-content add-detailes content">
    <!-- Form add  -->

    <div class="add-general__title">
        <h3><?= Yii::t('app',  'Category') ?></h3>
    </div>
    <?php $form = ActiveForm::begin([
        'id' => 'add-ad-form',
        'options' => ['class' => 'newadd-form'],
        'fieldConfig' => [
            'options' => [
                'class' => 'form-group general-field__container',
            ],
        ]
    ]); ?>


    <?=$form->errorSummary($model)?>


    <?=\frontend\widgets\bazar\SelectAdvCategory::widget(['model' => $model])?>


    <a href="<?= \yii\helpers\Url::to(['adv/add', 'step' => $_GET['step'] - 1, 'id' => $model->id])?>" class="btn btn--green"><i class="ion-ios-arrow-left"></i> <?= Yii::t('app', 'Back')?></a>

    <?=\yii\helpers\Html::submitButton(Yii::t('app', 'Next').' <i class="ion-ios-arrow-right"></i>', ['class' => 'btn btn--green'])?>


    <?php ActiveForm::end(); ?>
</div>
