<?php
/**
 * Created by PhpStorm.
 * User: York
 * Date: 08.09.2017
 * Time: 16:06
 */
use common\models\CMS;
use yii\widgets\ActiveForm;


/* @var $model \frontend\models\AddAdvForm*/
/* @var $this \yii\web\View */
?>




<div class="add-general content">

    <div class="add-general__title">
        <h3><?= Yii::t('app', 'Photo') ?></h3>
    </div>

    <?php $form = ActiveForm::begin([
        'id' => 'add-ad-form',
        'options' => ['class' => 'newadd-form', 'enctype' => 'multipart/form-data'],
        'fieldConfig' => [
            'options' => [
                'class' => 'form-group  general-field__container',
            ],
        ]
    ]);
    ?>


    <upload-images adv_id="<?= $model->id?>" text_upload="<?=Yii::t('app', 'Upload images');?> "></upload-images>


    <div class="clearfix"></div>
    <div class="margin-top">
        <a href="<?= \yii\helpers\Url::to(['adv/add', 'step' => $_GET['step'] - 1, 'id' => $model->id])?>" class="btn btn--green"><i class="ion-ios-arrow-left"></i> <?= Yii::t('app', 'Back')?></a>
        <a href="<?= \yii\helpers\Url::to(['adv/add', 'step' => $_GET['step'] + 1, 'id' => $model->id])?>" class="btn btn--green"><?= Yii::t('app', 'Next')?> <i class="ion-ios-arrow-right"></i></a>

    </div>
    <div class="clearfix" style="display: block"></div>
    <?php ActiveForm::end(); ?>

</div>
