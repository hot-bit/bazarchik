<?php
/**
 * Created by PhpStorm.
 * User: York
 * Date: 06.11.2017
 * Time: 13:39
 */

use yii\widgets\ActiveForm;

/* @var $this \yii\web\View */
/* @var $model \frontend\models\AddAdvForm */

$ad = $model->_adv;

$data = [
    Yii::t('app', 'Your name') => $ad->name,
    Yii::t('app', 'E-mail') =>  $ad->email,
    Yii::t('app', 'Phone') => $ad->phone,
    Yii::t('app', 'City') => $ad->_city->name,
    Yii::t('app', 'Title') => $ad->title,
    Yii::t('app', 'Price') => $ad->f_price,
    Yii::t('app', 'Category') => $ad->category_stair
];

?>

<div class="newadd-content add-detailes content">

    <div>
        <?=\common\models\CMS::Block('Add ad finish')?>
        <hr>
        <table>
            <?foreach ($data as $key => $val):?>
                <tr>
                    <td style="width: 100px"><b><?=$key?></b></td>
                    <td><?=$val?></td>
                </tr>
            <?endforeach;?>
        </table>
        <hr>
        <b><?=Yii::t('app', 'Text');?></b>: <?=$ad->text?>
        <hr>

        <?if($ad->images):?>
            <?foreach ($ad->images as $img):?>
                <img src="<?= $img->Path('catalog') ?>" alt="">
            <?endforeach?>

            <hr>
        <?endif;?>
        
    </div>
    <?php $form = ActiveForm::begin([
        'id' => 'submit-ad',
        'action' => ['adv/add-finish']
    ]); ?>
    <?=\yii\helpers\Html::activeHiddenInput($ad, 'id')?>

    <a href="<?= \yii\helpers\Url::to(['adv/add', 'step' => $_GET['step'] - 1, 'id' => $model->id])?>" class="btn btn--green"><i class="ion-ios-arrow-left"></i> <?= Yii::t('app', 'Back')?></a>
    <?=\yii\helpers\Html::submitButton(Yii::t('app', 'Post ad').' <i class="ion-ios-arrow-right"></i>', ['class' => 'btn btn--green'])?>

    <?php ActiveForm::end(); ?>

</div>