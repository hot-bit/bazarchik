<?php
/**
 * Created by PhpStorm.
 * User: York
 * Date: 08.09.2017
 * Time: 16:06
 */
use common\models\CMS;
use yii\widgets\ActiveForm;


/* @var $model \frontend\models\AddAdvForm */
/* @var $this \yii\web\View */

?>

<div class="add-general content newadd-content">

    <div class="add-general__title">
        <h3><?= Yii::t('app', 'Adv information') ?></h3>
    </div>

    <?php $form = ActiveForm::begin([
        'id' => 'add-ad-form',
        'options' => ['class' => 'newadd-form'],
        'fieldConfig' => [
            'options' => [
                'class' => 'form-group ',
            ],
        ]
    ]); ?>

        <div class="add-fields row">

            <div class="col-xs-12">
                <?= $form->field($model, 'title')->textInput(['placeholder' => Yii::t('app', 'Title')]) ?>
            </div>
            <div class="col-xs-12">
                <?= $form->field($model, 'text')->textArea() ?>
            </div>
            <div class="col-xs-12">
                <?= $form->field($model, 'price', ['options' => ['class' => 'form-group form-group--small general-field__container']])
                    ->textInput(['placeholder' => Yii::t('app', 'Price')]) ?>
            </div>


            <div class="col-xs-12">

                <a href="<?= \yii\helpers\Url::to(['adv/add', 'step' => $_GET['step'] - 1, 'id' => $model->id])?>" class="btn btn--green"><i class="ion-ios-arrow-left"></i> <?= Yii::t('app', 'Back')?></a>
                <?=\yii\helpers\Html::submitButton(Yii::t('app', 'Next').' <i class="ion-ios-arrow-right"></i>', ['class' => 'btn btn--green'])?>

            </div>

        </div>


    <?php ActiveForm::end(); ?>

</div>

