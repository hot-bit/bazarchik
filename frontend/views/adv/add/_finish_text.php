<?php
/**
 * Created by PhpStorm.
 * User: York
 * Date: 06.11.2017
 * Time: 16:10
 */

?>

<div class="col-md-12">
    <p style="font-size: 14px;">Dear, {user_name} ({user_email})</p>
    <p style="font-size: 14px;">Thank you for using our free services.</br>
        Your advertisement ID:<a href="http://www.bazar-cy.com/adv/{adv_id}/">{adv_id}</a> Title: <a href="http://www.bazar-cy.com/adv/{adv_id}/">{adv_title}</a> has been successfully posted on our website.</p>
    <p style="font-size: 14px;">To change the price or any other details about your item: you need to be a registered user.</br>
        If you are not registered you could do it here - <a href="http://www.bazar-cy.com/registr/">Registaration</a>.</br>
        If you are already a member of CYPRUS BAZAR please visit your <a href="http://www.bazar-cy.com/profile/">Profile</a> to manage your ads.</p>
</div>
<div class="col-md-12">
    <h3>Want to sell your item faster? Make your advert efficient with our options:</h3>
    <div style="padding: 0 0 10px">
        <a style="font-size:16px; margin-left: 40px; padding:0 0 3px 30px; background: rgba(0, 0, 0, 0) url('/assets/image/sprites/spritesheet.png') no-repeat scroll -5px -1575px;" href="/adv/improve?item_id={adv_id}&service=4">Raise searching</a></br>
        <a style="font-size:16px; margin-left: 40px; padding:0 0 3px 30px; background: rgba(0, 0, 0, 0) url('/assets/image/sprites/spritesheet.png') no-repeat scroll -5px -1189px;" href="/adv/improve?item_id={adv_id}&service=3">Highlight</a></br>
        <a style="font-size:16px; margin-left: 40px; padding:0 0 3px 30px; background: rgba(0, 0, 0, 0) url('/assets/image/sprites/spritesheet.png') no-repeat scroll -5px -1313px;" href="/adv/improve?item_id={adv_id}&service=2">Premium accommodation</a></br>
        <a style="font-size:16px; margin-left: 40px; padding:0 0 3px 30px; background: rgba(0, 0, 0, 0) url('/assets/image/sprites/spritesheet.png') no-repeat scroll -5px -1701px;" href="/adv/improve?item_id={adv_id}&service=1">Make it VIP</a></br>
    </div>
</div>
<div class="col-md-12" style="clear: left;">
    <p style="font-size: 14px;">After you sell your item, visit your <a href="http://www.bazar-cy.com/profile/">Profile</a> to mark your item as 'SOLD'. </br>By marking your item as 'SOLD',
        we remove your advertising from search directory - to avoid receive calls after you sold your item.</p>
    <p style="font-size: 14px;">Any other request you might have, send us an email and our support team will be pleased to assist you.</p>
    <p style="font-size: 14px;">Kind regards, CYPRUS BAZAR.</p>
</div>
