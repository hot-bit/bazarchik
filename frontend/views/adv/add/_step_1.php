<?php
/**
 * Created by PhpStorm.
 * User: York
 * Date: 08.09.2017
 * Time: 16:06
 */
use common\models\CMS;
use yii\widgets\ActiveForm;

/* @var $model \frontend\models\AddAdvForm*/
?>

<div class="newadd-content add-general">
    <!-- Form add  -->

    <div class="add-general__title">
        <h3><?= Yii::t('app',  'Personal data') ?></h3>
    </div>
    <?php $form = ActiveForm::begin([
        'id' => 'add-ad-form',
        'options' => ['class' => 'newadd-form'],
        'fieldConfig' => [
            'options' => [
                'class' => 'form-group form-group--small general-field__container',
            ],
        ]
    ]); ?>

    <div class="general-fields">
        <div class="row">
            <div class="col-sm-12">
                <?= $form->field($model, 'name')->textInput(['placeholder' => Yii::t('app', 'Your name')]) ?>
                <?= $form->field($model, 'phone')->widget(\frontend\widgets\bazar\PhoneField::className(), [
                    'wrap_class' => 'form-group form-group--small general-field__container',
                    'placeholder' => Yii::t('app', 'Your phone')
                ])?>
                <?= $form->field($model, 'email')->textInput(['placeholder' => 'example@gmail.com']) ?>
            </div>
        </div>
        <div class="row">
            <div class="col-sm-12">
                <?= $form->field($model, 'city')->dropDownList(\common\models\City::map(), [
                    'onchange' => "
                     $.ajax({
                        type     :'POST',
                        cache    : false,
                        url  : '" . \yii\helpers\Url::to(['ajax/area-by-city']) . "',
                        data: {id: $(this).val(), model:'AddAdvForm', dest:'frontend-models'},
                        success  : function(response) {
                            $('.field-addadvform-area .SumoSelect').remove();
                            $('.field-addadvform-area').append(response.data);  
                            $('#addadvform-area').SumoSelect();
                        }
                    });  
                    return false;",
                ]) ?>
                <?= $form->field($model, 'area')->dropDownList(\common\models\Area::map($model->city), ['prompt' => Yii::t('app', 'Select area')]) ?>
            </div>
        </div>
    </div>
    <?=\yii\helpers\Html::submitButton(Yii::t('app', 'Next').' <i class="ion-ios-arrow-right"></i>', ['class' => 'btn btn--green'])?>


    <?php ActiveForm::end(); ?>
</div>
