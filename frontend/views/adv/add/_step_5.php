<?php
/**
 * Created by PhpStorm.
 * User: York
 * Date: 08.09.2017
 * Time: 16:06
 *
 * @var $model \frontend\models\AddAdvForm
 * @var $this \yii\web\View
 * @var $params \common\models\forms\ParamsForm
 *
 */

use yii\widgets\ActiveForm;


$filter = $model->_adv->_category->filter;

?>



<div class="add-general content">

    <h3><?=Yii::t('app', 'Filters');?> </h3>

    <div>
        <?php $form = ActiveForm::begin([
            'id' => 'auto-form'
        ]); ?>

            <?=$this->render('//filters/add-adv/'.$filter, ['id' => $model->id, 'form' => $form, 'model' => $params])?>


            <a href="<?= \yii\helpers\Url::to(['adv/add', 'step' => $_GET['step'] - 1, 'id' => $model->id])?>" class="btn btn--green"><i class="ion-ios-arrow-left"></i> <?= Yii::t('app', 'Back')?></a>
        <?=\yii\helpers\Html::submitButton(Yii::t('app', 'Next').' <i class="ion-ios-arrow-right"></i>', ['class' => 'btn btn--green'])?>
        <?php ActiveForm::end(); ?>
    </div>

</div>