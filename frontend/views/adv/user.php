<?php
/**
 * Created by PhpStorm.
 * User: York
 * Date: 29.01.2018
 * Time: 13:04
 */
/* @var $this yii\web\View */
/* @var $pager \yii\data\Pagination */
/* @var $model \common\models\Adv[] */
/* @var $user \common\models\User */

$this->title = Yii::t('app', 'Ads by {userName}', ['userName' => $user->name]);
Yii::$app->breadcrumbs->Items([
    $this->title
]);
?>

<section class="content">
    <h1><?= $this->title?></h1>
    <?if($model):?>
        <div class="row margin-top">
            <?foreach($model as $item):?>
                <div class="col-sm-3">
                    <?=$this->render('//adv/catalog/_catalog-item!new', compact('item', 'no_vip', 'catalog_view'))?>
                </div>
            <?endforeach?>
        </div>

        <div class="catalog-pagination">
            <?=\frontend\widgets\LinkPager::widget([
                'pagination' => $pager,
                'hideOnSinglePage' => true,
                'firstPageLabel' => Yii::t('app', 'First page'),
                'lastPageLabel' => Yii::t('app', 'Last page'),
                'disabledPageCssClass' => 'hidden',
            ]);?>
        </div>
    <?endif?>
</section>



