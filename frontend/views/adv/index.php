<?php
/* @var $this yii\web\View
 * @var $catalog \frontend\models\Catalog
 */

use common\models\CMS;
use frontend\widgets\LinkPager;

$catalog_view = true;

?>

<section class="partner">

    <?= \frontend\widgets\Shop::widget([])?>

</section>

<!-- Catalog page 1-->
<section class="content">
    <h1>
        <?php
        if($catalog->search_model->search)
            echo Yii::t('app', 'Search').': '.$catalog->search_model->search;
        elseif(Yii::$app->seo->h1)
            echo Yii::$app->seo->h1;
        elseif ($catalog->subcategory_model)
            echo $catalog->subcategory_model->f_name;
        elseif ($catalog->category_model)
            echo $catalog->category_model->name;
        else
            echo Yii::t('app', 'Catalog');
        ?>
    </h1>

    <?= $this->render('catalog/_catalog-subcategories', ['catalog' => $catalog])?>

</section>

<section class="catalog-all">
    <div class="row">
        <div class="col-sm-3">
            <?=$this->render('//common/sidebar', compact('catalog'))?>
        </div>
        <div class="col-sm-9">

            <div class="row">
                <div class="col-sm-6">

                    <div class="catalog-header">
                        <div class="catalog-header__sort">

                            <div class="show-mobile">

                                <a href="#search-modal" data-sideup class="btn btn--green btn--full margin-bottom">
                                    <?= CMS::svg('filter', ['class' => 'icon--white', 'size' => 30])?>
                                    <span><?=Yii::t('app', 'Apply filter');?> </span>
                                </a>
                            </div>
                            <div class="selectbox__noborder">
                                <?= \yii\helpers\Html::dropDownList('date', $catalog->sort, \frontend\models\Catalog::DateSort(), [
                                    'prompt' => Yii::t('app', 'By date'),
                                    'class' => 'SelectBox',
                                    'onchange' => '
                                        window.location = \''.$catalog->SortUrl('').'\'+$(this).val();
                                    ',
                                ])?>
                            </div>
                            <div class="selectbox__noborder">
                                <?= \yii\helpers\Html::dropDownList('price', $catalog->sort, \frontend\models\Catalog::PriceSort(), [
                                    'prompt' => Yii::t('app', 'By price'),
                                    'class' => 'SelectBox',
                                    'onchange' => '
                                        window.location = \''.$catalog->SortUrl('').'\'+$(this).val();
                                    ',
                                ])?>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-sm-6">
                    <?if($catalog->pagination):?>
                        <div class="catalog-page">
                            <?=Yii::t('app', 'Page')?>
                            <span class="catalog-page__curent"><?=$catalog->page?></span>
                            <?=Yii::t('app', 'of')?>
                            <span class="catalog-page__total"><?=$catalog->pagination->getPageCount()?></span>
                        </div>
                    <?endif;?>
                </div>
            </div>


            <div class="catalog">
                <?if($catalog->vip):?>
                    <?foreach ($catalog->vip as $item):?>
                        <?=$this->render('//adv/catalog/_catalog-item!new', compact('item', 'catalog_view'))?>
                    <?endforeach;?>
                <?endif;?>
                <?if($catalog->premium):?>
                    <?foreach ($catalog->premium as $item): $no_vip = true;?>
                        <?=$this->render('//adv/catalog/_catalog-item!new', compact('item', 'no_vip', 'catalog_view'))?>
                    <?endforeach;?>
                <?endif;?>
                <?if($catalog->ads):?>
                    <?foreach ($catalog->ads as $item): $no_vip = true; ?>
                        <?=$this->render('//adv/catalog/_catalog-item!new', compact('item', 'no_vip', 'catalog_view'))?>
                    <?endforeach;?>
                <?else:?>
                    <?=CMS::Block('Nothing found')?>
                <?endif;?>
            </div>
            <div class="row catalog-pagination">
                <div class="col-sm-4"><?= $this->render('//common/share_icons', ['align' => 'left'])?></div>
                <?if($catalog->pagination):?>
                    <div class="col-sm-8">
                        <div class="">
                            <?=LinkPager::widget([
                                'pagination' => $catalog->pagination,
                                'hideOnSinglePage' => true,
                                'firstPageLabel' => Yii::t('app', 'First page'),
                                'lastPageLabel' => Yii::t('app', 'Last page'),
                                'disabledPageCssClass' => 'hidden',
                            ]);?>
                        </div>
                    </div>
                <?endif;?>
            </div>

            <div class="row">

                <?= \frontend\widgets\Banner::widget([
                    'place' => [\common\models\Banner::PLACE_ALL, \common\models\Banner::PLACE_CATALOG],
                    'max_size' => 240,
                    'count' => 3,
                    'block_class' => 'col-sm-4 text-center'
                ])?>
            </div>
        </div>
    </div>


</section>
