<?php
/**
 * Created by PhpStorm.
 * User: York
 * Date: 09.11.2017
 * Time: 12:46
 *
 * @var $model \frontend\models\ImproveAdForm
 * @var $this \yii\web\View
 * @var $adv \common\models\Adv
 */

use yii\widgets\ActiveForm;

Yii::$app->breadcrumbs->Items([
    [
        'url' => $adv->link,
        'label' => $adv->title
    ],
    Yii::t('app', 'Improve ad')
]);
$prices = Yii::$app->params['service_price'];



$this->registerJs("
    build();
    $('.purchase-cb').change(function(){
        build();
    });
    
    function build(){
        var total_summ = 0;
        $('.purchase-cb').each(function(el){
            if($(this).is(':checked')){
                total_summ += parseInt($(this).attr('data-price')); 
            }
        });
        
        $('.total_price').text(total_summ);
    }
");

$items = [
    [
        'name' => 'vip',
        'title' => Yii::t('app', 'Make it VIP'),
        'text' => '<p>Эта зона расположена наверху каталога/результатов поиска сайта (пример на картинке слева). </p>
                        <p>Объявление, которое вы продвигаете, будет рандомно показываться 14 дней наверху каталога/результатов
                            поиска сайта по категории, соответствующей объявлению.</p>',
        'image' => 'pay_vip_small.jpg',
    ],
    [
        'name' => 'prem',
        'title' => Yii::t('app', 'Make it Premium'),
        'text' => '<p>Эта зона расположена наверху каталога/результатов поиска сайта (пример на картинке слева). </p>
                    <p>Объявление, которое вы продвигаете, будет рандомно показываться 14 дней наверху каталога/результатов
                        поиска сайта по категории, соответствующей объявлению.</p>',
        'image' => 'pay_prem_small.jpg',
    ],
    [
        'name' => 'color',
        'title' => Yii::t('app', 'Highlight the ad'),
        'text' => '<p>Эта зона расположена наверху каталога/результатов поиска сайта (пример на картинке слева). </p>
                    <p>Объявление, которое вы продвигаете, будет рандомно показываться 14 дней наверху каталога/результатов
                        поиска сайта по категории, соответствующей объявлению.</p>',
        'image' => 'pay_color_small.jpg',
    ],
    [
        'name' => 'up',
        'title' => Yii::t('app', 'Raise in catalog and search results'),
        'text' => '<p>Эта зона расположена наверху каталога/результатов поиска сайта (пример на картинке слева). </p>
                    <p>Объявление, которое вы продвигаете, будет рандомно показываться 14 дней наверху каталога/результатов
                        поиска сайта по категории, соответствующей объявлению.</p>',
        'image' => 'pay_up_small.jpg',
    ],

];



?>


<!--suppress ALL -->
<section class="purchase">

    <div class="purchase-title">
        <h1><?=$adv->title?> №:<?=$adv->id?></h1>
    </div>

    <?php $form = ActiveForm::begin([
        'id' => 'improve-ad'
    ])  ; ?>
        <?=\yii\helpers\Html::activeHiddenInput($model, 'id')?>
        <?=\yii\helpers\Html::errorSummary($model)?>
        <?foreach ($items as $item):
            $disabled = false;
            switch ($item['name']){
                case 'up': break;
                case 'vip': $disabled = $adv->is_vip ? true : false; break;
                case 'color': $disabled = $adv->is_color ? true : false;  break;
                case 'prem': $disabled = $adv->is_prem ? true : false;  break;

            }

            ?>
            <div class="purchase-container" id="improve_<?= $item['name']?>">
                <div class="purchase-title <?= $disabled ? 'improve-disabled' : ''?>">
                    <?if( $disabled):?>
                            <?=\yii\helpers\Html::checkbox($item['name'], true, ['class' => 'purchase-cb' , 'data-price' => 0, 'disabled' => $disabled])?>
                    <?else:?>
                        <?=\yii\helpers\Html::activeCheckbox($model, $item['name'], ['class' => 'purchase-cb', 'id' => 'purchase_'.$item['name'], 'data-price' => $prices[$item['name']], 'label' => false, 'disabled' => $disabled])?>

                    <?endif?>
                    <label for="purchase_<?=$item['name']?>"><span></span> <?=$item['title']?></label>
                </div>

                <div class="purchase-price">
                    €<?=$prices[$item['name']]?>
                </div>

                <div class="purchase-about" data-toggle="improve_<?= $item['name']?>">
                    <div class="purchase-about__text"><?=Yii::t('app', 'Details');?> </div>
                    <div class="purchase-about__opened">
                        <?= \common\models\CMS::svg('arrowdown', ['class' => 'icon--gray'])?>
                    </div>
                    <div class="purchase-about__closed">
                        <?= \common\models\CMS::svg('arrowup', ['class' => 'icon--gray'])?>
                    </div>

                </div>

                <div class="purchase-dropdown">
                    <div class="purchase-dropdown__left">
                        <img src="<?=Yii::getAlias('@dist')?>/img/improve/<?=$item['image']?>">
                    </div>
                    <div class="purchase-dropdown__right">
                        <?=$item['text']?>
                    </div>
                </div>
            </div>
        <?endforeach;?>



        <?if($adv->is_owner):?>

            <div class="purchase-action">
                <div class="purchase-action__total">
                    <span><?=Yii::t('app', 'Total');?>:</span> <span>€<span class="total_price"></span></span>
                </div>

                <div class="purchase-action__right">

                    <a href="<?=$adv->link?>" class="link link-black link-medium link-no-line margin-right-large">
                        <?=\common\models\CMS::svg('arrowleft', ['size' => '13px']) ?>
                        <?= Yii::t('app', 'Go back')?>
                    </a>
                    <?if($adv->can_free_rise):?>
                        <button class="link link-medium link-green link-no-line margin-right-large" type="submit" name="ImproveAdForm[type]" value="free" class="type_free">
                            <?=\common\models\CMS::svg('freeweek', ['class' => 'icon-green']) ?>
                            <?=Yii::t('app', 'Rise in catalog for FREE');?>
                        </button>
                    <?endif?>
                    <?if(!Yii::$app->user->isGuest && Yii::$app->user->identity->can_use_bonus):?>

                        <button class="link link-medium link-no-line margin-right-large" type="submit" name="ImproveAdForm[type]" value="bonus">
                            <?=\common\models\CMS::svg('bonus', ['class' => 'icon--orange']) ?>
                            <?=Yii::t('app', 'Pay with bonuses');?> <br>
                            <small>(<?= Yii::t('app', 'account balance')?> <?=Yii::$app->user->identity->money ?>)</small>
                        </button>
                    <?endif;?>
                    <button class="link link-medium link-no-line" type="submit" name="ImproveAdForm[type]" value="paypal">
                        <?=\common\models\CMS::svg('paypal', ['class' => 'icon--orange']) ?>
                        <?=Yii::t('app', 'Pay with PayPal');?>
                    </button>
                </div>
            </div>
        <?endif?>
    <?php ActiveForm::end(); ?>


</section>