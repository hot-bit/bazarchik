<?php
/**
 * Created by PhpStorm.
 * User: York
 * Date: 15.09.2017
 * Time: 12:44
 *
 * @var $model \common\models\Adv
 * @var $similar \common\models\Adv[]
 * @var $this \yii\web\View
 */

use yii\helpers\Html;

$owner = false;
if ($model->has_user)
    $owner = $model->_owner;

$breadcrumbs = [];

foreach ($model->_category->stairs_models as $cat)
    $breadcrumbs[$cat->link] = $cat->name;

if($model->category == \common\models\Categories::CARS){
    $breadcrumbs[$model->car_brand->link] = $model->car_brand->name;
    $breadcrumbs[$model->car_model->link] = $model->car_model->name;
}


$breadcrumbs[] = $model->title;

Yii::$app->breadcrumbs->Items($breadcrumbs);
Yii::$app->seo->setAdv($model);
?>


<section class="content">
    <h1><?=$model->title?></h1>

    <div class="row">
        <div class="col-md-10 col-sm-12">
            <ul class="a-item__head">
                <li><?=$model->_city->name?></li>
                <li data-time="<?= $model->date_create?>"></li>
                <li><?=Yii::t('app', '{today} views today, total {total}', ['today' => $model->views_today, 'total' => $model->views_total]);?> </li>
                <li>№ <?=$model->id?></li>
            </ul>
        </div>
        <div class="col-md-2 col-sm-12">
            <?= $this->render('//common/share_icons', ['align' => 'right'])?>
        </div>
    </div>
    <div class="row">
        <div class="col-md-6 padding-right-40">
            <div class="new-item__photo a-item__image">
                <!--Owl Carousel Image  -->
                <div class="owl-carousel owl-carousel-image" data-slider-id="1" data-gallery>
                    <?if($model->images):?>
                        <?foreach ($model->images as $image):?>
                            <div class="">
                                <a data-image href="<?= $image->Watermark('big') ?>" class="a-item-image-horisontal">
                                    <span class="a-item-image-cover"
                                          style="background-image: url('<?= $image->Path('big') ?>')"></span>
                                    <div class="a-item-image-vertical">
                                        <img src="<?= $image->Watermark('item') ?>" alt="">
                                    </div>
                                </a>
                            </div>
                        <?endforeach;?>
                    <?else:?>

                        <div class="">
                            <img src="<?= Yii::getAlias('@dist')?>/img/noimage.jpg" alt="">
                        </div>
                    <?endif;?>
                </div>
                <!-- End Owl Carousel Image  -->

                <!--Owl Carousel Thumbs  -->
                <div class="owl-thumbs__container swiper-for-thumbs">
                    <div class="owl-thumbs  swiper-wrapper" data-slider-id="1">
                        <?foreach ($model->images as $image):?>
                            <div class="owl-thumb-item  swipe-slide"><img src="<?= $image->Path('thumb') ?>" alt=""
                                                                          style="height: 60px"></div>
                        <?endforeach;?>
                    </div>
                </div>
                <!-- ENd owl Carousel Thumbs  -->

            </div>

        </div>
        <div class="col-md-6 padding-left-40 a-item__right">
            <div class="row">
                <div class="col-sm-6 padding-right-40">
                    <div class="a-item__price rect">
                        <?=$model->f_price?>
                    </div>

                    <?if($model->is_coupon && !Yii::$app->user->isGuest):?>
                        <a href="<?= \yii\helpers\Url::to(['coupon', 'id' => $model->id])?>" class="btn btn--green"><?=Yii::t('app', 'Get coupon');?> </a>
                    <?endif?>
                    <div class="a-item__params">
                        <div class="a-item__params-field">
                            <span><?=Yii::t('app', 'Category');?> </span>
                            <?=$model->_category->name?>
                        </div>
                        <div class="a-item__params-field">
                            <span><?=Yii::t('app', 'City');?> </span>
                            <?=$model->_city->name?>
                        </div>

                        <?=$this->render('get/_top_params', compact('model'))?>
                    </div>
                </div>
                <div class="col-sm-6 padding-left-40">
                    <div class="a-item__contacts" data-toggle="phone-block" class="link-icon">
                        <i class="icon text-icon icon-phone-stroke"></i>
<!--                        --><?//=\common\models\CMS::svg('phone-stroke', ['class' => 'icon--white', 'size' => '24'])?>
                        <?=Yii::t('app', 'Show contacts');?>
                    </div>

                    <div class="a-item__contacts-content" id="phone-block">
                        <a href="javascript:;" style="position: absolute;font-size: 40px;top: -10px;   right: 10px;" onclick="$('#phone-block').removeClass('active')">
                            <i class="ion-ios-close-empty"></i>
                        </a>
                        <?= $this->render('get/contacts-content', compact('owner', 'model')) ?>
                    </div>

                    <?if($model->_owner):?>
                        <div class="a-item__link">
                            <a href="<?=\yii\helpers\Url::to(['adv/user', 'id' => $model->_owner->id])?>" class="link-icon" target="_blank">
                                <i class="icon text-icon icon-user"></i>
<!--                                --><?//=\common\models\CMS::svg('user', ['class' => 'icon--gray'])?>
                                <?=Yii::t('app', 'All user\'s ads');?>
                            </a>
                        </div>
                    <?endif?>
                    <?if($model->has_user):?>
                        <div class="a-item__link">
                            <a href="<?=\yii\helpers\Url::to(['chat/index', 'id' => $model->user])?>" class="link-icon">
                                <i class="icon text-icon ion-chatboxes"></i>
                                <!--                            --><?//=\common\models\CMS::svg('mail', ['class' => 'icon--gray'])?>
                                <?=Yii::t('app', 'Chat with owner');?>
                            </a>
                        </div>
                    <?endif?>
                    <div class="a-item__link">
                        <a href="<?=\yii\helpers\Url::to(['adv/write-to-owner', 'id' => $model->id])?>" class="link-icon">
                            <i class="icon text-icon icon-mail"></i>
<!--                            --><?//=\common\models\CMS::svg('mail', ['class' => 'icon--gray'])?>
                            <?=Yii::t('app', 'Write to owner');?>
                        </a>
                    </div>
                    <div class="a-item__link">
                        <?= \frontend\widgets\Favorite::widget([
                            'is_active' => Yii::$app->favorite->isActive($model->id),
                            'item_id' => $model->id,
                            'text' => Yii::t('app', 'Favorite')
                        ])?>

                    </div>
                    <div class="a-item__link">
                        <a href="javascript:" data-toggle="report-block" class="link-icon">
                            <i class="icon text-icon icon-spam"></i>
<!--                            --><?//=\common\models\CMS::svg('spam', ['class' => 'icon--gray'])?>
                            <?=Yii::t('app', 'Report');?>
                        </a>

                        <div class="a-item__contacts-content" id="report-block">
                            <a href="javascript:;" style="position: absolute;font-size: 40px;top: -10px;   right: 10px;" onclick="$('#report-block').removeClass('active')">
                                <i class="ion-ios-close-empty"></i>
                            </a>
                            <?=Html::a(Yii::t('app', 'Product is sold'), ['adv/report', 'id' => $model->id, 'report' => 1])?>
                            <br>
                            <?=Html::a(Yii::t('app', 'Incorrect price'), ['adv/report', 'id' => $model->id, 'report' => 2])?>
                            <br>
                            <?=Html::a(Yii::t('app', 'Can\'t call'), ['adv/report', 'id' => $model->id, 'report' => 3])?>
                            <br>
                            <?=Html::a(Yii::t('app', 'Contacts and links in desc'), ['adv/report', 'id' => $model->id, 'report' => 4])?>
                            <br>
                            <?=Html::a(Yii::t('app', 'Other reason'), ['adv/report', 'id' => $model->id, 'report' => 5])?>
                        </div>
                    </div>
                    <?if($model->is_owner):?>
                        <div class="a-item__link a-item__link-orange">
                            <a href="<?=\yii\helpers\Url::to(['adv/improve', 'id' => $model->id])?>" class="link-icon">
                                <i class="icon text-icon icon-star"></i>
<!--                                --><?//=\common\models\CMS::svg('star', ['class' => 'icon--orange'])?>
                                <?=Yii::t('app', 'Improve ad');?>
                            </a>
                        </div>
                    <?endif?>
                    <?= \frontend\widgets\Banner::widget([
                        'place' => [\common\models\Banner::PLACE_ALL, \common\models\Banner::PLACE_ADV],
                        'max_size' => 240
                    ])?>
                </div>
            </div>
        </div>
    </div>

    <div class="a-item__content">
        <?=$model->text_rdy?>
    </div>

    <div class="a-item__params">

        <div class="row">
            <?=$this->render('get/_bottom_params', compact('model'))?>
        </div>


    </div>
    
    <div class="row">
        <div class="col-md-4">
            <?if($model->prevAd):?>
                <?= Html::a('&larr; '.Yii::t('app', 'Previous ad'), $model->prevAd->link)?>
            <?endif?>
        </div>
        <div class="col-md-4 text-center"><?= Html::a(Yii::t('app', 'Return to search'), $model->_category->link)?></div>
        <div class="col-md-4 text-right">
            <?if($model->nextAd):?>
                <?= Html::a(Yii::t('app', 'Next ad').' &rarr;', $model->nextAd->link)?>
            <?endif?>
        </div>
    </div>

    <div class="a-item__similar">
        <h2><?=Yii::t('app', 'Similar ads');?> </h2>

        <div class=" owl-carousel owl-carousel-catalog">

            <?foreach ($similar as $item):?>
                <?=$this->render('//adv/catalog/_catalog-item!new', compact('item'))?>
            <?endforeach;?>



        </div>

    </div>


    <div class="a-item__mobile">
        <div class="a-item__mobile-footer">
            <a class="a-item__mobile-item" data-sideup href="#mobile-phone-block">
                <?=\common\models\CMS::svg('phone-stroke', ['class' => 'icon--gray'])?>
                <span>
                    <?=Yii::t('app', 'Show contacts');?>
                </span>
            </a>
            <a class="a-item__mobile-item" href="#">
                <?=\common\models\CMS::svg('likestroke', ['class' => 'icon--gray'])?>
                <span>
                    <?=Yii::t('app', 'Add to favorite');?>
                </span>
            </a>

            <div class=" a-item__contacts-content_mobile white-popup mfp-hide" id="mobile-phone-block">
                <?= $this->render('get/contacts-content', compact('owner', 'model')) ?>
            </div>
        </div>
    </div>
</section>

