<?php
/**
 * Created by PhpStorm.
 * User: yorks
 * Date: 25.01.2018
 * Time: 23:48
 */

?>
<div class="a-item__contacts-content_name">
    <?= $model->name ?>
</div>
<? if ($owner): ?>
    <div class="a-item__contacts-content_date">
        <?= Yii::t('app', 'Registred {date}', ['date' => date('d F Y', $owner->date_reg)]) ?>
    </div>
<? endif ?>


<div class="a-item__params">
    <div class="a-item__params-field">
        <span><?= Yii::t('app', 'Phone'); ?> </span>
        <a href="tel:<?= $model->phone ?>">
            <?= $model->phone ?>
        </a>
    </div>
    <? if ($owner): ?>
        <?if($owner->skype):?>
            <div class="a-item__params-field">
                <span>Skype</span>
                <a href="skype:<?= $owner->skype ?>"><?= $owner->skype ?></a>
            </div>
        <?endif?>
        <? if ($owner->website): ?>
            <div class="a-item__params-field">
                <span><?= Yii::t('app', 'Website') ?></span>
                <a href="<?= $owner->f_website ?>" rel="nofollow" noindex target="_blank"><?= $owner->website ?></a>
            </div>
        <? endif ?>
    <? endif ?>
</div>
