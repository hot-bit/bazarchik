<?php
/**
 * Created by PhpStorm.
 * User: York
 * Date: 21.09.2017
 * Time: 19:12
 */
use common\models\CMS;

?>

<section class="newads">

    <div class="newads-title">
        <h2><?=Yii::t('app', 'Similar ads')?></h2>

        <a href="<?=\yii\helpers\Url::to(['adv/index'])?>"><span><?=Yii::t('app', 'Back to catalog')?></span></a>

    </div>


    <div class="newsads-sliders">

        <div class="newsads-slidess swiper-wrapper">

            <?foreach ($similar as $item):?>
                <?=$this->render('//adv/get/_similar-item', ['item' => $item])?>
            <?endforeach;?>

        </div>


        <div class="newsads-left">
            <?=CMS::svg('bigarrowleft', ['class' => 'icon--gray'])?>
        </div>

        <div class="newsads-right">
            <?=CMS::svg('bigarrowright', ['class' => 'icon--gray'])?>
        </div>

    </div>


</section>
