<?php
/**
 * Created by PhpStorm.
 * User: York
 * Date: 15.09.2017
 * Time: 17:15
 */

?>


<div class="newsads-slidev swiper-slide">
    <div class="newsads-image"><img src="<?=$item->main_image?>" alt="Изображение квартиры"></div>
    <div class="newsads-title"><h4><a href="<?=$item->link?>"><?=$item->title?></a></h4></div>

    <div class="newsads-btm">
        <div class="newsads-price"><span class='price'><?=$item->f_price?></span></div>
        <div class="newads-bottom__like">
        </div>
    </div>

</div>