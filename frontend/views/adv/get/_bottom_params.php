<?php
/**
 * Created by PhpStorm.
 * User: York
 * Date: 23.11.2017
 * Time: 13:59
 *
 * @var $model \common\models\Adv
 */
/* @var $this yii\web\View */
$params = $model->_params;
$not_use = Yii::$app->params['adv_top_params'];
$limit = 7;

$this->registerCss("
.a-item__params-area .toggle-param{display: none}
.a-item__params-area .hide-options{display: none; float: right;}
.a-item__params-area .expand-options{ float: right;}
.a-item__params-area.active .toggle-param{display: block}
.a-item__params-area.active .expand-options{display: none}
.a-item__params-area.active .hide-options{display: block}

");
$this->registerJs("
    $('.expand-options a').click(function(){
        $('.a-item__params-area').addClass('active')
        return false;
    });
    $('.hide-options a').click(function(){
        $('.a-item__params-area').removeClass('active')
        return false;
    });
");
?>

<div class="a-item__params-area">
    <? $i = 1; foreach ($params as $item): if(in_array($item->name, $not_use) || !$item->val) continue;?>
        <div class="col-sm-3 <?= $i > $limit ? 'toggle-param' : ''?>">
            <div class="a-item__params-field">
                <span><?=Yii::t('app', $item->name)?></span>
                <?=$item->val?>
            </div>
        </div>
    <?$i++; endforeach;?>
    <?if(count($params) > 7):?>
        <div class="col-sm-3 expand-options">
            <div class="a-item__params-field">
                <a href="#"><?=Yii::t('app', 'Expand options');?> </a>
            </div>
        </div>
        <div class="col-sm-3 hide-options">
            <div class="a-item__params-field">
                <a href="#"><?=Yii::t('app', 'Hide options');?> </a>
            </div>
        </div>
    <?endif?>
</div>
