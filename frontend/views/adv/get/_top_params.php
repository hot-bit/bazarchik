<?php
/**
 * Created by PhpStorm.
 * User: York
 * Date: 23.11.2017
 * Time: 13:58
 *
 * @var $model \common\models\Adv
 * @var $this \yii\web\View
 */

$params = $model->_params;

$used = Yii::$app->params['adv_top_params'];

$used_models = [];

foreach ($params as $item){
    if(!in_array($item->name, $used)) continue;
    $used_models[$item->name] = $item;
}
?>

<?foreach ($used as $use):
    if(!isset($used_models[$use])) continue;
    $item = $used_models[$use];
    ?>
    <div class="a-item__params-field">
        <span><?=Yii::t('app', $item->name)?></span>
        <?=$item->val?>
    </div>
<?endforeach;?>