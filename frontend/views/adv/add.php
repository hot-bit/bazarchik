<?php
/**
 * Created by PhpStorm.
 * User: York
 * Date: 04.09.2017
 * Time: 14:29
 *
 * @var $model \frontend\models\AddAdvForm
 * @var $step string
 */

Yii::$app->breadcrumbs->Items([
    Yii::t('app', 'Add adv')
]);
?>

<section class="newadd">
    <div class="newadd-title">
        <h1><?= Yii::t('app', 'Add adv') ?></h1>
    </div>

    <?=\frontend\widgets\bazar\AddAdvBreadcrumbs::widget([
        'model' => $model
    ])?>

    <?=$this->render('add/_step_'.$step, compact('model', 'params'));?>
</section>