<?php
/**
 * Created by PhpStorm.
 * User: York
 * Date: 28.11.2017
 * Time: 12:11
 */
/* @var $this \yii\web\View
 * @var $model \common\models\User
 * @var $ads \common\models\Adv
 * @var $pages \yii\data\Pagination
 */
Yii::$app->breadcrumbs->Items([
    $model->shop_name
]);


?>

<section class="content">
    <div class="row">
        <div class="col-sm-12">
            <h1><?= $model->shop_name?></h1>
        </div>
    </div>
    <div class="row">
        <div class="col-sm-3 sidebar">

            <?if($model->avatar != ''):?>
                <div class="sidebar__block">
                    <?=\yii\helpers\Html::img($model->thumb)?>
                </div>
            <?endif;?>

            <div class="sidebar__block">
                <?=\common\models\CMS::svg('phonesmall', [])?>
                <?=$model->phone?>
            </div>
            <?if($model->address_arr):?>
                <ul class="sidebar__block">
                    <?foreach($model->address_array as $item): if(!isset($item['name'])) continue;?>
                        <li>
                            <?= $item['name']?>
                        </li>
                    <?endforeach?>
                </ul>
            <?endif;?>

            <div class="sidebar__block">
                <?= $model->company_description?>
            </div>

            
            <div class="sidebar__smallblock">
                <?= \common\models\CMS::svg('mail', ['class' => 'icon--orange'])?>
                <a href="<?= \yii\helpers\Url::to(['shop/write-to-owner', 'shop' => $model->business_link])?>"><?=Yii::t('app', 'Write to manager');?></a>

            </div>
            <?if($model->website != ''):?>
                <div class="sidebar__smallblock">
                    <?= \common\models\CMS::svg('website', ['class' => 'icon--orange'])?>
                    <a href="<?= $model->f_website?>" target="_blank" noindex="noindex" nofollow="nofollow"><?= Yii::t('app', 'Store site')?></a>
                </div>
            <?endif;?>
            <div class="sidebar__smallblock">
                <?= \common\models\CMS::svg('place')?>
                <?=Yii::t('app', 'Address on map');?>
                <shop-map id="<?= $model->id?>"></shop-map>
            </div>



        </div>
        <div class="col-sm-9">

            <div class="row">
                <?foreach($ads as $item):?>
                    <div class="col-sm-4">
                        <?= $this->render('//adv/catalog/_catalog-item!new', ['item' => $item])?>
                    </div>
                <?endforeach?>
            </div>


        </div>
    </div>
</section>