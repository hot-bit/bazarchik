<?php
/**
 * Created by PhpStorm.
 * User: York
 * Date: 13.02.2018
 * Time: 17:22
 */

/* @var $this yii\web\View */
?>
<section class="content">
    <h1><?=Yii::t('app', 'Chat');?> </h1>

    <chat user_id="<?= $_GET['id']?>"
        :msg="{not_found:'<?=Yii::t('app', 'Nothing found');?>', rec_message:'<?= Yii::t('app', 'Recient messages')?>', rec_user:'<?=Yii::t('app', 'Recient users');?>', send:'<?=Yii::t('app', 'Send');?>'}"
    ></chat>

</section>
