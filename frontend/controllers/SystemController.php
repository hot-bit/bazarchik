<?php
/**
 * Created by PhpStorm.
 * User: York
 * Date: 02.12.2017
 * Time: 16:38
 */

namespace frontend\controllers;


use common\models\CMS;
use common\models\Translate;
use yii\web\Controller;

class SystemController extends Controller {

    public function behaviors(){
        return parent::behaviors();
    }
    
    public function actionTranslates(){
        $file_path = $_SERVER['DOCUMENT_ROOT'].'/translate.txt';
        $data_file = file_get_contents($file_path);

        $data = json_decode($data_file, true);

        $langs = [1, 2, 3];

        foreach ($data as $category => $data2){
            foreach ($data2 as $message => $val){
                $model = Translate::findByMessage($category, $message);

                if(!$model){
                    foreach ($langs as $lng){
                        if($category == 'yii') continue;
                        $item = new Translate();
                        $item->category = $category;
                        $item->message = $message;
                        $item->language_id = $lng;
                        if($lng == 1)
                            $item->value = $message;

                        $item->save();
                    }
                }
            }
        }
    }


    public function actionTestSms( ){
        $phone = '+79041415353';

        CMS::SendSms($phone, 'тест', true);
    }
}