<?php
/**
 * Created by PhpStorm.
 * User: York
 * Date: 30.10.2017
 * Time: 9:38
 */

namespace frontend\controllers;


use common\models\Adv;
use common\models\CMS;
use common\models\Coupon;
use common\models\forms\ParamsForm;
use common\models\Orders;
use common\models\Subscribe;
use common\models\User;
use http\Url;
use Yii;
use yii\data\ActiveDataProvider;
use yii\filters\AccessControl;
use yii\web\Response;
use yii\web\UnauthorizedHttpException;
use yii\widgets\ActiveForm;

class ProfileController extends Controller
{

    public function behaviors(){
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'actions' => ['*'],
                        'allow' => false,
                    ],
                    [
                        'actions' => ['settings', 'operations', 'subscribes', 'ads', 'bonus', 'edit-adv', 'delete-adv', 'start-adv', 'stop-adv', 'edit-params-adv', 'confirm-email', 'delete-ava'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                    [
                        'actions' => ['subscribe-start', 'subscribe-stop', 'subscribe-delete'],
                        'allow' => true,
                        'matchCallback' => function($rule, $action) {
                            $id = \Yii::$app->request->get('id', false);
                            if(!$id) return false;

                            $model = Subscribe::findOne($id);
                            if($model->email == Yii::$app->user->identity->email || $model->user_id == \Yii::$app->user->id)
                                return true;
                            return false;

                        }
                    ]
                ],
            ],
        ];
    }

    /**
     * Мои объявления
     *
     * @return string
     */
    public function actionAds($status = ''){
        /** @var User $user */
        $user = Yii::$app->user->identity;
        Yii::$app->user->returnUrl = \yii\helpers\Url::to(['profile/ads']);
        $q = Adv::find()
            ->where(['owner' => Yii::$app->user->id]);
        if($user->success_email)
            $q->orWhere(['email' => Yii::$app->user->identity->email]);
        if($user->success_phone)
            $q->orWhere(['phone' => Yii::$app->user->identity->phone]);


        if($status != '')
            $q->andWhere(['status' => $status]);
        $model = $q->orderBy('date DESC')->all();

        return $this->render('ads', compact('model'));
    }

    /**
     * Настройки профиля
     *
     * @return array|string|Response
     */
    public function actionSettings(){
        $model = User::findOne(\Yii::$app->user->id);
        $model->setScenario('update');
        Yii::$app->user->setReturnUrl(['profile/settings']);

        if (Yii::$app->request->isAjax && $model->load(Yii::$app->request->post())) {
            Yii::$app->response->format = Response::FORMAT_JSON;
            return ActiveForm::validate($model);
        }

        if ($model->load(\Yii::$app->request->post()) && $model->save()) {
            \Yii::$app->session->setFlash('success', Yii::t('app', 'Data has been saved'));
            return $this->refresh();
        }

        return $this->render('settings', compact('model'));
    }

    /**
     * Купоны
     *
     * @return string
     */
    public function actionBonus(){
        $model = Coupon::findFreeAds();

        return $this->render('bonus', compact('model'));
    }

    /**
     * Последние операции
     *
     * @return string
     */
    public function actionOperations(){

        $dataProvider = new ActiveDataProvider([
            'query' => Orders::my(),
        ]);

        return $this->render('operations', compact('dataProvider'));
    }

    /**
     * Подписки
     *
     * @return string
     */
    public function actionSubscribes(){
        \Yii::$app->getUser()->setReturnUrl(['profile/subscribes']);
        $dataProvider = new ActiveDataProvider([
            'query' => Subscribe::my(),
        ]);
        return $this->render('subscribes', compact('dataProvider'));
    }

    /**
     * Запустить подписку
     *
     * @param $id
     */
    public function actionSubscribeStart($id){
        $model = Subscribe::findOne($id);
        $model->status = 1;
        $model->save();
        \Yii::$app->session->setFlash('success', Yii::t('app', 'Your subscription has been launched'));
        $this->goBack();
    }

    /**
     * Остановить подписку
     *
     * @param $id
     */
    public function actionSubscribeStop($id){
        $model = Subscribe::findOne($id);
        $model->status = 0;
        $model->save();
        \Yii::$app->session->setFlash('success', Yii::t('app', 'Your subscription has been stopped'));
        $this->goBack();
    }

    /**
     * Удалить подписку
     *
     * @param $id
     * @throws \Exception
     * @throws \Throwable
     * @throws \yii\db\StaleObjectException
     */
    public function actionSubscribeDelete($id){
        Subscribe::findOne($id)->delete();
        \Yii::$app->session->setFlash('success', Yii::t('app', 'Your subscription has been deleted'));
        $this->goBack();
    }

    /**
     * Остановить объявление
     *
     * @param $id
     * @return Response
     * @throws UnauthorizedHttpException
     */
    public function actionStopAdv($id){
        $adv = $this->loadAdv($id);

        $adv->status = Adv::STATUS_PAUSE;
        $adv->save();
        Yii::$app->session->setFlash('success', Yii::t('app', 'Your ad succefully stopped'));

        return $this->goBack();
    }

    /**
     * Запустить объявление
     *
     * @param $id
     * @return Response
     * @throws UnauthorizedHttpException
     */
    public function actionStartAdv($id){
        $adv = $this->loadAdv($id);

        $adv->notice_time = time();
        if($adv->status == Adv::STATUS_STOP)
            $adv->date = time();

        Yii::$app->session->setFlash('success', Yii::t('app', 'Your ad succefully started'));
        $adv->status = Adv::STATUS_ACTIVE;
        $adv->save();

        return $this->goBack();
    }

    /**
     * Удалить объявление
     *
     * @param $id
     * @return Response
     * @throws UnauthorizedHttpException
     * @throws \Exception
     * @throws \Throwable
     */
    public function actionDeleteAdv($id){
        $model = $this->loadAdv($id);

        $model->status = Adv::STATUS_REMOVED;
        $model->save();
        $model->notice_time = time();
        Yii::$app->session->setFlash('success', Yii::t('app', 'Your ad will be removed in {days} days', ['days' => Yii::$app->params['remove_in_day']]));

        return $this->goBack();
    }

    /**
     * Редактирование объявлений
     *
     * @param $id
     * @return string|Response
     * @throws UnauthorizedHttpException
     */
    public function actionEditAdv($id){
        $model = $this->loadAdv($id);

        $model->status = Adv::STATUS_MODERATE;
        if($model->load(Yii::$app->request->post()) && $model->save()){
            Yii::$app->session->setFlash('success', Yii::t('app', 'Adv has been saved'));
            return $this->refresh();
        }

        return $this->render('edit', compact('model'));
    }


    /**
     * Редактировать параметры обхявления
     *
     * @param $id
     * @return string|Response
     * @throws UnauthorizedHttpException
     */
    public function actionEditParamsAdv($id){
        $model = $this->loadAdv($id);
        $params = new ParamsForm();
        $params->id = $model->id;
        $params->loadModel();

        if($params->load(Yii::$app->request->post()) && $params->upload($model->id)){
            $model->status = Adv::STATUS_MODERATE;
            $model->save();

            Yii::$app->session->setFlash('success', Yii::t('app', 'Parameters successfully updated'));
            return $this->refresh();
        }

        return $this->render('edit-params', compact('model', 'params'));
    }


    /**
     * Загрузить объявление
     *
     * @param $id
     * @return null|Adv
     * @throws UnauthorizedHttpException
     */
    private function loadAdv($id){
        $model = Adv::findOne($id);
        if (!$model->is_owner)
            throw new UnauthorizedHttpException(Yii::t('app', 'You are not authorized to perform this action'));

        return $model;
    }

    public function actionConfirmEmail(){
        /** @var User $user */
        $user = Yii::$app->user->identity;
        $user->hash = Yii::$app->getSecurity()->generateRandomString();
        $user->save();

        $sendEmail = Yii::$app->mailer
            ->compose(
                ['html' => 'confirm_profile/profile-confirm-html', 'text' => 'confirm_profile/profile-confirm-text'],
                ['user' => $user]
            )
            ->setTo($user->email)
            ->setFrom([Yii::$app->params['support_email'] => 'Cyprus Bazar'])
            ->setSubject(Yii::t('app', 'Confirm your email address on Cyprus Bazar'))
            ->send();

        if($sendEmail){
            Yii::$app->session->setFlash('success', Yii::t('app', 'Check your email and confirm your email address'));
            return $this->goBack();
        }
    }

    public function actionDeleteAva(){
        $user = User::findOne(Yii::$app->user->id);
        $user->deleteImage();
        Yii::$app->session->addFlash('success', Yii::t('app', 'Your avatar has been removed'));
        return $this->redirect(['settings']);
    }

}