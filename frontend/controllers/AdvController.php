<?php

namespace frontend\controllers;

use common\models\Adv;
use common\models\Categories;
use common\models\forms\ParamsForm;
use common\models\Reports;
use common\models\forms\SearchForm;
use common\models\SearchRequest;
use common\models\User;
use frontend\models\AddAdvForm;
use frontend\models\Catalog;
use frontend\models\CouponForm;
use frontend\models\ImproveAdForm;
use frontend\models\WriteToOwnerForm;
use Yii;
use yii\data\Pagination;
use yii\filters\AccessControl;
use yii\helpers\Json;
use yii\web\BadRequestHttpException;
use yii\web\HttpException;
use yii\web\NotFoundHttpException;
use yii\web\Request;
use yii\web\Response;
use yii\web\UnauthorizedHttpException;
use yii\widgets\ActiveForm;

class AdvController extends Controller{

    public function behaviors(){
        return [
            'access' => [
                'class' => AccessControl::className(),
                'only' => ['add', 'add-finish'],
                'rules' => [
                    [
                        'actions' => ['add', 'add-finish', 'free-rise', 'coupon'],
                        'allow' => true,
                        'roles' => ['@']
                    ]

                ],
            ],
        ];
    }


    /**
     * Каталог
     *
     * @param bool $cat
     * @param int $page
     * @param string $sort
     * @param bool $search
     * @return string
     */
    public function actionIndex($cat = false, $page = 1, $sort = 'date desc', $search = false){
        $catalog = new Catalog($cat, $page, $sort, $search);
        Yii::$app->user->setReturnUrl($_SERVER['REQUEST_URI']);

        return $this->render('index', compact('catalog'));
    }

    /**
     * Поиск
     *
     * @param bool $cat
     * @param int $page
     * @param string $sort
     * @return string
     */
    public function actionSearch($cat = false, $page = 1, $sort = 'date desc'){
        return $this->actionIndex($cat, $page, $sort, true);
    }


    /**
     * Юесплатное подняте объявления
     *
     * @param $id
     * @return Response
     */
    public function actionFreeRise($id){
        $model = Adv::findOne($id);

        if(!$model){
            Yii::$app->session->setFlash('error', Yii::t('app', 'Adv not found'));
            return $this->redirect(Yii::$app->request->referrer);
        }
        elseif(!$model->is_owner){
            Yii::$app->session->setFlash('error', Yii::t('app', 'You are not authorized to perform this action'));
            return $this->redirect(Yii::$app->request->referrer);
        }
        elseif(!$model->can_free_rise){
            Yii::$app->session->setFlash('error', Yii::t('app', 'You can\'t rise adv at now'));
            return $this->redirect(Yii::$app->request->referrer);
        }
        elseif ($model && $model->can_free_rise && $model->is_owner){
            $model->rise();
            Yii::$app->session->setFlash('success', Yii::t('app', 'You are succefuly rise adv'));
            return $this->redirect(Yii::$app->request->referrer);
        }
        else{
            Yii::$app->session->setFlash('error', Yii::t('app', 'Error'));
            return $this->redirect(Yii::$app->request->referrer);
        }
    }

    /**
     * Добавить объявление
     *
     * @param int $step
     * @param bool $id
     * @return string|\yii\web\Response
     */
    public function actionAdd($step = 1, $id = false){
        $model = new AddAdvForm();

        if($step == 1 && !\Yii::$app->user->isGuest)
            $model->SetUser();

        if($id) $model->LoadAdv($id);

        if($step == 5 && !$model->_adv->_category->has_filter)
            return $this->redirect(['adv/add', 'step' => 'finish', 'id' => $model->id]);
        if($step == 6)
            return $this->redirect(['adv/add', 'step' => 'finish', 'id' => $model->id]);



        $model->setScenario('step_'.$step);

        $model->step = $step;
        $params = new ParamsForm();
        $params->step = $step;


        if($model->upload() && $params->upload($id)){
            // Костыль для прохождения тестов пропустить загрузку изображений так как там ошибка
            if($model->tests) $step = 4;
            return $this->redirect(['adv/add', 'step' => $step + 1, 'id' => $model->id]);
        }

        if($step == 5){
            $params->id = $id;
            $params->loadModel();
        }

        if (Yii::$app->request->isAjax && $model->load(Yii::$app->request->post())) {
            Yii::$app->response->format = Response::FORMAT_JSON;
            return ActiveForm::validate($model);
        }

        return $this->render('add', [
            'model' => $model,
            'step'  => $step,
            'params' => $params
        ]);
    }

    /**
     * Финальный текст при добавлении
     *
     * @return string
     * @throws NotFoundHttpException
     */
    public function actionAddFinish(){
        $form = \Yii::$app->request->post('Adv', false);
        if(!$form || !isset($form['id'])) throw new NotFoundHttpException();
        $id = $form['id'];

        $model = Adv::findOne($id);
        $model->status = Adv::STATUS_MODERATE;
        $model->save();

        return $this->render('add/finish', compact('model'));
    }

    /**
     * Карточка объявления
     *
     * @param null $url
     * @param $id
     * @return string
     * @throws NotFoundHttpException
     */
    public function actionGet($url = null, $id){

        $model = Adv::findOne($id);
        if(!$model) throw new NotFoundHttpException(Yii::t('app', 'Adv not found'));
        $model->addViews();

        Yii::$app->user->setReturnUrl($model->link);

        $similar = Adv::find()->where(['category' => $model->category])->andWhere("id <> :id", [':id' => $model->id])->limit(12)->all();

        return $this->render('get', compact('model', 'similar'));

    }

    /**
     * Улучшить объявление
     *
     * @param $id
     * @return string|\yii\web\Response
     */
    public function actionImprove($id){
        $model = new ImproveAdForm();
        $model->id = $id;
        $adv = Adv::findOne($id);

        if(!$adv->is_owner) Yii::$app->session->setFlash('warning', Yii::t('app', 'You can\'t improve this adv. You are not owner. Please, login to owner account'));

        if($model->load(Yii::$app->request->post()) && $model->validate() && $model->save()){
            return $this->refresh();
        }

        return $this->render('improve', ['model' => $model, 'adv' => $adv]);
    }

    public function actionCoupon($id){

        $model = new CouponForm();
        if(!$model->SetAdv($id)) throw new NotFoundHttpException();
        $model->SetUser();

        if($model->load(Yii::$app->request->post()) && $model->validate() && $model->buy()){
            return $this->goHome();
        }

        return $this->render('coupon', compact('model', 'adv'));
    }


    /**
     * Отправить репорт
     *
     * @param $id
     * @param $report
     * @return \yii\web\Response
     */
    public function actionReport($id, $report){

        $model = new Reports();
        $model->adv_id = $id;
        $model->text = Reports::$texts[$report];
        if($model->save())
            Yii::$app->session->setFlash('success', Yii::t('app', 'Your request has been sent'));
        else{
            Yii::$app->session->setFlash('error', Yii::t('app', 'Request failed'));
        }

        return $this->goBack();
    }

    /**
     * Написать владельцу объявления
     *
     * @param $id
     * @return string
     * @throws NotFoundHttpException
     */
    public function actionWriteToOwner($id){
        $model = new WriteToOwnerForm();
        $model->adv_id = $id;

        if(!$model->_adv) throw new NotFoundHttpException(Yii::t('app', 'Adv not found'));

        if($model->load(\Yii::$app->request->post()) && $model->validate() && $model->save()){
            Yii::$app->session->setFlash('success', Yii::t('app', 'Your request has been sent'));
            return $this->goBack();
        }

        return $this->render('write-to-owner', compact('model'));
    }

    public function actionUser($id){

        $user = User::findOne($id);
        $q = Adv::find()
            ->where(['user' => $user->id]);

        if($user->success_email)
            $q->orWhere(['email' => $user->email]);

        $count_q = clone $q;
        $count = $count_q->count();

        $pager = new Pagination(['totalCount' => $count]);
        $pager->defaultPageSize = 12;
        $model = $q->offset($pager->offset)->limit($pager->limit)->all();

        return $this->render('user', compact('model', 'pager', 'no_vip', 'catalog_view', 'user'));
    }

}
