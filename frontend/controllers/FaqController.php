<?php
/**
 * Created by PhpStorm.
 * User: York
 * Date: 24.11.2017
 * Time: 12:33
 */

namespace frontend\controllers;


use common\models\Faq;
use common\models\FaqCategory;

class FaqController extends Controller {

    public function behaviors(){
        return parent::behaviors();
    }
    
    public function actionIndex(){

        $model = FaqCategory::find()->all();

        return $this->render('index', compact('model'));
    }

    public function actionGet($url){
        $model = Faq::findByUrl($url);

        return $this->render('get', compact('model'));
    }
}