<?php
/**
 * Created by PhpStorm.
 * User: York
 * Date: 28.12.2017
 * Time: 17:37
 */

namespace frontend\controllers;


use common\models\Brand;
use common\models\Categories;
use common\models\Model;
use common\models\Subscribe;
use Yii;
use yii\web\Controller;
use yii\web\NotFoundHttpException;

class SubscribeController extends Controller{

    /**
     * Подписать пользователя на категорию
     *
     * @param $id
     * @param bool $car_model
     * @param bool $brand
     * @return string|\yii\web\Response
     * @throws NotFoundHttpException
     */
    public function actionCategory($id, $car_model = false, $brand = false){
        $category = Categories::findOne($id);
        if(!$category) throw new NotFoundHttpException(Yii::t('app', 'Category not found'));
        $model = new Subscribe();

        if($brand){
            $brandModel = Brand::findOne($brand);
            if(!$brandModel) throw new NotFoundHttpException(Yii::t('app', 'Brand not found'));
            $model->car_brand = $brand;
        }
        if($car_model){
            $modelModel = Model::findOne($car_model);
            if(!$modelModel) throw new NotFoundHttpException(Yii::t('app', 'Model not found'));
            $model->car_model = $car_model;
            $model->car_brand = $modelModel->brand_id;
        }

        $model->category_id = $category->id;
        if($model->load(Yii::$app->request->post()) && $model->save()){
            Yii::$app->session->setFlash('success', Yii::t('app', 'Check your email and approve your subscribe'));
            return $this->goBack();
        }
        return $this->render('category', compact('model', 'category'));
    }

    // TODO Подтверждение подписки
}