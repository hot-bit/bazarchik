<?php
/**
 * Created by PhpStorm.
 * User: York
 * Date: 30.10.2017
 * Time: 9:46
 */

namespace frontend\controllers;


use common\models\User;
use frontend\models\WriteToOwnerForm;
use Yii;
use yii\data\Pagination;
use yii\web\BadRequestHttpException;
use yii\web\Controller;
use yii\web\NotFoundHttpException;

class ShopController extends Controller {

    public function behaviors(){
        return parent::behaviors();
    }
    
    public function actionGet($url = false, $id = false){
        if(!$url && !$id) throw new BadRequestHttpException(Yii::t('app', 'Not enough params'));

        if($url) $model = User::findByCompany($url);
        else $model = User::findOne($id);

        if(!$model) throw new NotFoundHttpException(Yii::t('app', 'Page not found'));

        Yii::$app->user->setReturnUrl($model->shop_link);

        $query = $model->get_ads();
        $countQuery = clone $query;
        $count = $countQuery->count();
        $pages = new Pagination(['totalCount' => $count]);
        $pages->setPageSize(12);
        $ads = $query->offset($pages->offset)->limit($pages->limit)->all();
        return $this->render('get', compact('model', 'pages', 'ads'));
    }

    public function actionWriteToOwner($shop){
        $model = new WriteToOwnerForm();
        $model->shop_id = $shop;

        if(!$model->_shop) throw new NotFoundHttpException(Yii::t('app', 'Shop not found'));

        if($model->load(\Yii::$app->request->post()) && $model->validate() && $model->save()){
            Yii::$app->session->setFlash('success', Yii::t('app', 'Your request has been sent'));
            return $this->goBack();
        }

        return $this->render('//adv/write-to-owner', compact('model'));
    }
}