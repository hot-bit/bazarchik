<?php
/**
 * Created by PhpStorm.
 * User: yorks
 * Date: 31.12.2017
 * Time: 21:14
 */

namespace frontend\controllers;


use common\models\News;
use yii\data\Pagination;

class NewsController extends Controller
{

    public function actionIndex($page = 1){
        $q = News::find()->where(['status' => News::STATUS_ACTIVE]);

        $totalQuery = clone $q;
        $totalCount = $totalQuery->count();
        $pages = new Pagination();
        $pages->defaultPageSize = 12;
        $pages->totalCount = $totalCount;

        $model = $q->orderBy('date DESC')->limit($pages->limit)->offset($pages->offset)->all();
        return $this->render('index', compact('model', 'pages'));
    }

    public function actionGet($id, $url){
        $model = News::findOne($id);
        $next_news = News::find()->where(['<', 'id', $model->id])->orderBy('id DESC')->one();
        $prev_news = News::find()->where(['>', 'id', $model->id])->one();

        return $this->render('get', compact('model', 'next_news', 'prev_news'));

    }
}