<?php
/**
 * Created by PhpStorm.
 * User: York
 * Date: 13.02.2018
 * Time: 17:21
 */

namespace frontend\controllers;


use common\models\Chat;
use common\models\ChatMessage;
use common\models\ChatView;
use common\models\CMS;
use common\models\User;
use Yii;
use yii\filters\AccessControl;
use yii\web\BadRequestHttpException;
use yii\web\Controller;
use yii\web\Response;

class ChatController extends Controller
{
    public function behaviors(){
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'actions' => ['index', 'user-list', 'message-list', 'send-message'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
        ];
    }


    /** Чат
     * @param bool $id
     * @return string
     * @throws BadRequestHttpException
     */
    public function actionIndex($id = false){
        if($id){
            $user = User::findOne($id);
            if(!$user) throw new BadRequestHttpException(Yii::t('app', 'Bad request'));
        }

        return $this->render('index');
    }

    /** Получить список пользователей
     * @return array
     */
    public function actionUserList(){
        \Yii::$app->response->format = Response::FORMAT_JSON;

        $model = Chat::findByUser();
        return CMS::apiExport($model, ['partnerName', 'partnerId', 'partnerAvatar', 'hasUnread'], []);
    }

    /** Список собщений по пользователю
     * @param $id
     * @return array
     */
    public function actionMessageList($id){
        \Yii::$app->response->format = Response::FORMAT_JSON;

        $model = Chat::findByUsers(\Yii::$app->user->id, $id);
        if(!$model) return [];

        $items = $model->chatMessages;
        ChatView::ViewAll($id);

        return CMS::apiExport($items, ['userName'], []);
    }

    /**
     * Добавить сообщение
     */
    public function actionSendMessage(){
        \Yii::$app->response->format = Response::FORMAT_JSON;
        $userId = $_POST['userId'];
        $message = $_POST['message'];

        $chatModel = Chat::findByUsers(\Yii::$app->user->id, $userId);

        if(!$chatModel){
            $chatModel = new Chat();
            $chatModel->user_1 = \Yii::$app->user->id;
            $chatModel->user_2 = $userId;
            $chatModel->save();
        }

        $chatModel->createMessage($message);

    }
}