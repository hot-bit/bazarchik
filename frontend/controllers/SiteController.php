<?php
namespace frontend\controllers;

use common\models\Adv;
use common\models\News;
use common\models\User;
use frontend\models\ConfirmCodeForm;
use Yii;
use yii\base\InvalidParamException;
use yii\web\BadRequestHttpException;
use yii\filters\VerbFilter;
use yii\filters\AccessControl;
use common\models\LoginForm;
use frontend\models\PasswordResetRequestForm;
use frontend\models\ResetPasswordForm;
use frontend\models\SignupForm;
use frontend\models\ContactForm;
use common\models\Categories;
use yii\web\NotAcceptableHttpException;
use yii\web\NotFoundHttpException;
use yii\web\Response;
use yii\widgets\ActiveForm;

/**
 * Site controller
 */
class SiteController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'only' => ['logout', 'signup', 'validate-login'],
                'rules' => [
                    [
                        'actions' => ['signup', 'login', 'validate-login', 'login-soc', 'shut-down'],
                        'allow' => true,
                        'roles' => ['?'],
                    ],
                    [
                        'actions' => ['logout'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
            'captcha' => [
                'class' => 'yii\captcha\CaptchaAction',
                'fixedVerifyCode' => YII_ENV_TEST ? 'testme' : null,
            ],
        ];
    }

    public function actionShutDown(){
        $this->layout = false;

        return $this->render('shutdown');
    }

    public function actionFavorite(){
        $items_id = Yii::$app->favorite->items();
        $model = Adv::find()->where(['id' => $items_id])->orderBy('date DESC')->all();

        return $this->render('favorite', compact('model'));
    }

    /**
     * Displays homepage.
     *
     * @return mixed
     */
    public function actionIndex(){

        if($_SERVER['REQUEST_URI'] == '/en/')
            return $this->redirect('/', 301);

        $last_adv = Adv::findActive()->orderBy('date DESC')->limit(20)->all();
        $last_news = News::find()->where(['status' => News::STATUS_ACTIVE])->orderBy('date DESC')->limit(2)->all();
        return $this->render('index', compact( 'last_adv', 'last_news', 'categories'));
    }

    /**
     * Logs in a user.
     *
     * @return mixed
     */
    public function actionLogin()
    {
        if(!Yii::$app->user->isGuest) return $this->goBack(['site/index']);

        $model = new LoginForm();
        if ($model->load(Yii::$app->request->post()) && $model->login()) {
            \Yii::$app->response->format = Response::FORMAT_JSON;
            return $this->goBack();
        } else {
            return $this->render('login', compact('model'));
        }


    }

    public function actionValidateLogin(){

        if (Yii::$app->request->isAjax) {
            Yii::$app->response->format = Response::FORMAT_JSON;

            $model = new LoginForm(Yii::$app->getRequest()->getBodyParams()['LoginForm']);

            if (!$model->validate()) {
                return ActiveForm::validate($model);
            }
            else
                return [];
        }
    }

    /**
     * Logs out the current user.
     *
     * @return mixed
     */
    public function actionLogout()
    {
        Yii::$app->user->logout();

        return $this->goBack(['site/index']);
    }

    /**
     * Displays contact page.
     *
     * @return mixed
     */
    public function actionContact()
    {
        $model = new ContactForm();
        if ($model->load(Yii::$app->request->post()) && $model->validate()) {
            if ($model->sendEmail(Yii::$app->params['adminEmail'])) {
                Yii::$app->session->setFlash('success', 'Thank you for contacting us. We will respond to you as soon as possible.');
            } else {
                Yii::$app->session->setFlash('error', 'There was an error sending your message.');
            }

            return $this->refresh();
        } else {
            return $this->render('contact', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Signs user up.
     *
     * @return mixed
     * @throws \yii\base\Exception
     */
    public function actionSignup()
    {
        $model = new SignupForm();

        if ($model->load(Yii::$app->request->post()) && $model->signup()) {
//            \Yii::$app->response->format = Response::FORMAT_JSON;
            $model->sendEmail();
            Yii::$app->session->setFlash('success', Yii::t('app', 'Check your email and approve your registration'));
            return $this->goBack();
        } else {
            return $this->render('signup', compact('model'));
        }

    }

    public function actionValidateSignup()
    {
        if (Yii::$app->request->isAjax) {
            Yii::$app->response->format = Response::FORMAT_JSON;

            $model = new SignupForm(Yii::$app->getRequest()->getBodyParams()['SignupForm']);

            if (!$model->validate()) {
                return ActiveForm::validate($model);
            }
            else
                return [];
        }
    }

    /**
     * Requests password reset.
     *
     * @return mixed
     */
    public function actionRequestPasswordReset()
    {
        $model = new PasswordResetRequestForm();
        if ($model->load(Yii::$app->request->post()) && $model->validate()) {
            if ($model->sendEmail()) {
                Yii::$app->session->setFlash('success', 'Check your email for further instructions.');

                return $this->goHome();
            } else {
                Yii::$app->session->setFlash('error', 'Sorry, we are unable to reset password for the provided email address.');
            }
        }

        return $this->render('requestPasswordResetToken', [
            'model' => $model,
        ]);
    }

    /**
     * Resets password.
     *
     * @param string $token
     * @return mixed
     * @throws BadRequestHttpException
     */
    public function actionResetPassword($token)
    {
        try {
            $model = new ResetPasswordForm($token);
        } catch (InvalidParamException $e) {
            throw new BadRequestHttpException($e->getMessage());
        }

        if ($model->load(Yii::$app->request->post()) && $model->validate() && $model->resetPassword()) {
            Yii::$app->session->setFlash('success', 'New password saved.');

            return $this->goHome();
        }

        return $this->render('resetPassword', [
            'model' => $model,
        ]);
    }

    /**
     * Подтверждение регистрации
     *
     * @param bool $token
     * @return string|Response
     * @throws NotFoundHttpException
     * @throws \yii\base\Exception
     */
    public function actionConfirm($token = false){

        if($token){
            $model = User::find()->where(['hash' => $token])->one();
            if(!$model) throw new NotFoundHttpException(Yii::t('app', 'User not found'));

            if($model->password != null){
                $model->hash = '';
                $model->success_email = true;
                $model->save();
                Yii::$app->session->setFlash('success', Yii::t('app', 'Your email address successfully confirmed'));
                return $this->redirect(['profile/settings']);
            }

            $model->setScenario('set_password');
            if($model->load(Yii::$app->request->post()) && $model->validate()){
                $model->setPassword($model->new_password);
                $model->hash = '';
                $model->success_email = true;
                $model->save();
                Yii::$app->user->login($model);
                Yii::$app->session->setFlash('warning', Yii::t('app', 'Please, fill your profile'));
                return $this->redirect(['profile/settings']);
            }
            return $this->render('confirm-set-password', compact('model'));
        }
        elseif (!$token){
            $model = new ConfirmCodeForm();

            if($model->load(Yii::$app->request->post()) && $model->validate())
                return $this->redirect(['confirm', 'token' => $model->token]);

            return $this->render('confirm-set-token', compact('model'));
        }
    }

    public function actionLoginSoc(){
        $s = file_get_contents('http://ulogin.ru/token.php?token=' . $_POST['token'] . '&host=' . $_SERVER['HTTP_HOST']);
        $user = json_decode($s, true);
        //$user['network'] - соц. сеть, через которую авторизовался пользователь
        //$user['identity'] - уникальная строка определяющая конкретного пользователя соц. сети
        //$user['first_name'] - имя пользователя
        //$user['last_name'] - фамилия пользователя

        $model = User::findByEmail($user['email']);

        if(!$model) {
            $model = new User();
            $model->email = $user['email'];
            $model->name  = $user['first_name'].' '.$user['last_name'];
            $model->success_email = true;
            $model->save();
        }
        Yii::$app->user->login($model);

        return $this->goBack();


    }

    public function actionTest(){

//        $this->layout = 'empty';
//        $user = User::findOne(7);
//        $this->layout = '@common/mail/layouts/html';
//        $body = $this->renderPartial('@common/mail/registration-confirm-html', compact('user'));
//        $ret = $this->render('@common/mail/_email_html', ['meta' => false, 'text' => $body, 'title' => Yii::t('app', 'Confirm registration')]);

//        return $ret;

        return $this->render('test');

    }
}
