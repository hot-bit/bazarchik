<?php
/**
 * Created by PhpStorm.
 * User: York
 * Date: 11.01.2018
 * Time: 13:04
 */

namespace frontend\controllers;


use common\models\Pages;
use Yii;
use yii\web\NotFoundHttpException;

class PageController extends Controller
{

    public function behaviors()
    {
        return parent::behaviors();
    }

    public function actionGet($url)
    {
        $model = Pages::findByUrl($url);
        if(!$model) throw new NotFoundHttpException(Yii::t('app', 'Page not found'));

        return $this->render('get', compact('model'));
    }
}