<?php
/**
 * Created by PhpStorm.
 * User: yorks
 * Date: 31.01.2018
 * Time: 0:30
 */

namespace frontend\controllers;


use common\models\Banner;
use Yii;
use yii\web\Controller;

class BannerController extends Controller
{
    public function actionIndex()
    {
        $model = new Banner();
        if($model->load(\Yii::$app->request->post()) && $model->save()){
            \Yii::$app->session->setFlash('success', Yii::t('app', 'Your request has been added'));
            return $this->goHome();
        }

        return $this->render('index', compact('model'));
    }
}