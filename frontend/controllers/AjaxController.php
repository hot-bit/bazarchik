<?php
/**
 * Created by PhpStorm.
 * User: York
 * Date: 28.09.2017
 * Time: 13:42
 */

namespace frontend\controllers;


use common\models\Adv;
use common\models\AdvImages;
use common\models\Area;
use common\models\Brand;
use common\models\Categories;
use common\models\CMS;
use common\models\map\CarBodyType;
use common\models\Model;
use common\models\User;
use frontend\models\CatalogFilter;
use frontend\models\SearchForm;
use common\models\forms\ParamsForm;
use Yii;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\imagine\Image;
use yii\web\BadRequestHttpException;
use yii\web\NotFoundHttpException;
use yii\web\Response;
use yii\web\UnauthorizedHttpException;
use yii\web\UploadedFile;

class AjaxController extends Controller
{
    public function beforeAction($action)
    {
        if (Yii::$app->request->isAjax) {

            Yii::$app->response->format = Response::FORMAT_JSON;
            return parent::beforeAction($action);
        }

        throw new BadRequestHttpException();
    }

    /**
     * Получить категории по id родителя
     *
     * @param $id
     * @return array
     */
    public function actionCategoryByParent($id){

        $categories = Categories::find()->where(['parent' => $id])->andWhere('header = 0')->all();
        if(!$categories) return false;

        $ret = Html::tag('li', Html::tag('span', Yii::t('app', 'Back')), ['class' => 'ads-category__link select-category__link-back']);
        foreach ($categories as $item)
            $ret .= Html::tag('li', $item->name, ['data-id' => $item->id, 'class' => 'ads-category__link']);
        $ret = Html::tag('ul', $ret);

        return ['data' => $ret];
    }

    /**
     * Список районов
     * $_POST['id'] - id города
     *
     * @return array
     */
    public function actionAreaByCity(){
        $form_name = $_POST['model'];
        $form_class = '\\'.str_replace('-', '\\', $_POST['dest']).'\\'.$form_name;
        $model = new $form_class();

        $id = $_POST['id'];

        $data = Area::map($id);
        $field = Html::activeDropDownList($model, 'area', $data, ['class' => 'SelectBox-search', 'prompt' => Yii::t('app', 'Select area')]);

        return ['data' => $field];
    }

    /**
     * Получить список моделей мащин
     * %_POST['id'] - id бренда машины
     *
     * @return array
     */
    public function actionCarModels(){
        $form_name = $_POST['model'];
        $form_class = '\\'.str_replace('-', '\\', $_POST['dest']).'\\'.$form_name;
        $model = new $form_class();

        $id = $_POST['id'];
        $data  = Model::map($id);

        $field = Html::activeDropDownList($model, 'model', $data, ['class' => 'SelectBox-search', 'prompt' => Yii::t('app', 'Select model')]);

        return ['data' => $field];
    }

    /**
     * Добавить изображение
     *
     * @return array
     * @throws UnauthorizedHttpException
     */
    public function actionUploadImage(){
        $image = UploadedFile::getInstanceByName('file');
        if($image){
            $item_id = $_POST['item_id'];

            $adv = Adv::findOne($item_id);
            if (!$adv->is_owner && !Yii::$app->user->can('ADV_UPDATE')) throw new UnauthorizedHttpException();

            $host = $_SERVER['DOCUMENT_ROOT'];
            $path = Yii::getAlias('@adv').'/'.$item_id.'/';
            if(!is_dir($host.$path)) mkdir($host.$path, 0777, true);

            $name = CMS::GenerateImageName().'.jpg';
            $image->saveAs($host.$path.$name);

            $model = new AdvImages();
            $model->adv_id = $item_id;
            $model->name = $name;
            $model->dir = $item_id;
            $model->save();

            return [
                'id' => $model->id,
                'path' => $path.$model->name,
                'isMain' => $model->main,
            ];
        }
        return [
            'success' => false
        ];
    }

    /**
     * Вращать изображение
     *
     * @param $id
     * @param $direction
     * @throws UnauthorizedHttpException
     */
    public function actionImageRotate($id, $direction){
        $model = AdvImages::findOne($id);
        if (!$model->_adv->is_owner && !Yii::$app->user->can('ADV_UPDATE')) throw new UnauthorizedHttpException();

        if($direction == 'left')
            $model->RotateLeft();
        if ($direction == 'right')
            $model->RotateRight();
    }

    /**
     * Сделать изображение главным
     *
     * @param $id
     * @throws UnauthorizedHttpException
     */
    public function actionImageSetMain($id){
        $model = AdvImages::findOne($id);
        if (!$model->_adv->is_owner && !Yii::$app->user->can('ADV_UPDATE')) throw new UnauthorizedHttpException();

        $images = AdvImages::find()->where(['adv_id' => $model->adv_id])->all();
        foreach ($images as $image){
            $image->main = 0;
            $image->save();
        }

        $model->main = 1;
        $model->save();
    }

    /**
     * Удалить изображение при добавлении объявления
     *
     * @param $id
     * @return array
     * @throws UnauthorizedHttpException
     * @throws \Exception
     * @throws \Throwable
     * @throws \yii\db\StaleObjectException
     */
    public function actionDeleteImage($id){
        $model = AdvImages::findOne($id);
        if (!$model->_adv->is_owner && !Yii::$app->user->can('ADV_UPDATE')) throw new UnauthorizedHttpException();

        if($model->delete())
            return ['success' => true];
        return ['success' => false];
    }

    /**
     * Получить список добавленных изображений
     * используется при добавлении и редактировании объявления
     *
     * @param $id
     * @return array
     * @throws UnauthorizedHttpException
     */
    public function actionLoadImages($id){
        $model = Adv::findOne($id);

        if (!$model->is_owner && !Yii::$app->user->can('ADV_UPDATE')) throw new UnauthorizedHttpException();
        $host = Yii::getAlias('@host');
        $path = Yii::getAlias('@adv').'/'.$model->id.'/';

        $arr = [];
        foreach ($model->images as $image){
            $arr[] = [
                'name' => $image->name,
                'id' => $image->id,
                'size' => @filesize($host . $path . $image->name),
                'thumbnailUrl' => $path.$image->name,
                'isMain' => $image->main
            ];
        }
        return $arr;
    }

    /**
     * Получить список типов аккаунта
     *
     * @return array
     */
    public function actionGetUserTypeList(){
        return[
            ['id' => 0, 'name' => Yii::t('app', "Personal"),],
            ['id' => 1, 'name' => Yii::t('app', "Business"),],
        ];
    }

    /**
     * Возвращает адресов пользователя
     *
     * @return array
     * @throws UnauthorizedHttpException
     */
    public function actionGetUserAddress(){
        if(Yii::$app->user->isGuest) throw new UnauthorizedHttpException();
        /** @var User $user */
        $user = Yii::$app->user->identity;

        return $user->address_array;
    }

    /**
     * Добавить/Удалить объявление в избранное
     *
     * @param $id
     * @return bool
     */
    public function actionFavoriteChangeActive($id){
        Yii::$app->favorite->change($id);
        return true;
    }

    /**
     * Общее кол-во избранных объявлений
     *
     * @return mixed
     */
    public function actionFavoriteTotalCount(){
        return  Yii::$app->favorite->count;
    }

    /**
     * Список типов кузова
     *
     * @return array
     */
    public function actionCarBodyList(){
        return CarBodyType::Full();
    }

    /**
     * Фильтры в сайдбере
     * Отдаем кол-во найденых объявлений и максимальную и минимальную цену
     *
     * @return array
     */
    public function actionFilterRequest(){

        $ret = [];

        $filter = new CatalogFilter();

        $query = Adv::find()->where(['adv.status' => Adv::STATUS_ACTIVE]);
        if($filter->load(Yii::$app->request->post())){
            $price_from = $filter->price_from;
            $price_to = $filter->price_to;
            $filter->price_from = null;
            $filter->price_to = null;
            $query = $filter->setFilters($query);
            $query->andWhere(['adv.category' => $filter->get_category()->ChildrenIdList()]);

            $min_q = clone $query;
            $max_q = clone $query;
            $min_model = $min_q->orderBy('adv.price')->one();
            $max_model = $max_q->orderBy('adv.price DESC')->one();
            $ret['min'] = (int)$min_model->price;
            $ret['max'] = (int)$max_model->price;

            $query_total = clone $query;
            $query_total->andWhere(['>=', 'adv.price', $price_from]);
            $query_total->andWhere(['<=', 'adv.price', $price_to]);

            $model_total = $query_total->count();

            $total = Yii::t('app', 'Show {count} ads', ['count' => $model_total > 99 ? '99+' : $model_total]);
            $ret['count'] = $total;

            return $ret;
        }
    }

    /**
     * Отправить смс код
     *
     * @return array
     * @throws UnauthorizedHttpException
     */
    public function actionSendSmsCode(){
        if(Yii::$app->user->isGuest) throw new UnauthorizedHttpException();
        CMS::SendSmsCode(Yii::$app->user->identity);

        return [
            'status' => true
        ];
    }

    /**
     * Подтвердить телефонный номер
     *
     * @param $code
     * @return bool
     * @throws UnauthorizedHttpException
     */
    public function actionAcceptSmsCode($code)
    {
        if (Yii::$app->user->isGuest) throw new UnauthorizedHttpException();

        $model = Yii::$app->user->identity;

        if ($model->sms_code == $code) {
            $model->sms_code = '';

            Yii::$app->user->identity->setSuccessPhone();

            return ['status' => true];
        }
        $model->phone_try--;
        $model->save();
        return ['status' => false, 'try' => $model->phone_try];

        return true;
    }

    /**
     * Отправить код для логина
     *
     * @param $phone
     * @param $phone_code
     * @return array
     * @throws UnauthorizedHttpException
     */
    public function actionSendLoginCode($phone, $phone_code){
        if (!Yii::$app->user->isGuest) throw new UnauthorizedHttpException();

        $model = User::find()->where(['phone_code' => $phone_code])->andWhere(['phone' => $phone])->one();
        if(!$model)
            return [
                'status' => false,
                'error' => Yii::t('app', 'Incorrect phone')
            ];


        CMS::SendSmsCode($model);
        return [
            'status' => true
        ];
    }

    /**
     * Проверить код из смс для логина
     *
     * @param $code
     * @param $phone_code
     * @param $phone
     * @return array
     * @throws UnauthorizedHttpException
     */
    public function actionAcceptLoginCode($code, $phone_code, $phone)
    {
        if (!Yii::$app->user->isGuest) throw new UnauthorizedHttpException();
        $model = User::find()->where(['phone_code' => $phone_code])->andWhere(['phone' => $phone])->one();
        if (!$model)
            return [
                'status' => false,
                'error' => Yii::t('app', 'Incorrect phone')
            ];

        if ($model->sms_code == $code) {
            $model->sms_code = '';
            if (!$model->success_phone) $model->success_phone = true;
            $model->save();
            Yii::$app->user->login($model);
            return ['status' => true];
        }
        $model->phone_try--;
        $model->save();
        return ['status' => false, 'try' => $model->phone_try];
    }

    /**
     * Получить селект для категории
     *
     * @return array
     * @throws NotFoundHttpException
     */
    public function actionGetCategorySelect(){
        $category = Categories::findOne($_POST['parent']);
        if(!$category) throw new NotFoundHttpException();
        $arr = ArrayHelper::map(Categories::find()->where(['header' => 0])->andWhere(['parent' => $_POST['parent']])->all(), 'id', 'name');

        $ret = [];
        if($arr)
            $ret['select'] = Html::dropDownList('category', '', $arr, ['class' => 'category-select', 'prompt' => Yii::t('app', '-- none --')]);

//        if($category->filter)
//            $ret['filter'] = $this->render('//filters/search/' . $category->filter, compact('form', 'model'));

        return $ret;
    }

    /**
     * Загрузить адреса из профиля
     *
     * @return array
     */
    public function actionLoadProfileAddress(){
        $data = Yii::$app->user->identity->address_array;

        $ret = [];
        foreach ($data as $item) {
            $new_item = $item;
            $new_item['position']['lat'] = (float)$item['position']['lat'];
            $new_item['position']['lng'] = (float)$item['position']['lng'];
            $ret[] = $new_item;
        }
        return $ret;
    }

    /**
     * Получить адреса магазина
     *
     * @param $id
     * @return array
     */
    public function actionLoadUserAddress($id){
        $model = User::findOne($id);

        $ret = [];
        foreach ($model->address_array as $item) {
            $new_item = $item;
            $new_item['position']['lat'] = (float)$item['position']['lat'];
            $new_item['position']['lng'] = (float)$item['position']['lng'];
            $ret[] = $new_item;
        }
        return $ret;

    }
}