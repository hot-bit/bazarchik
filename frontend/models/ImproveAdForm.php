<?php

namespace frontend\models;

use cinghie\paypal\components\Paypal;
use common\models\Adv;
use common\models\Orders;
use common\models\User;
use Yii;
use yii\base\Model;

/**
 * ContactForm is the model behind the contact form.
 *
 * @property Adv $_adv
 */
class ImproveAdForm extends Model
{
    public $id;

    public $vip;
    public $prem;
    public $color;
    public $up;
    public $type;

    private $items;
    private $total_price;
    private $cart;
    private $adv;

    const TYPE_FREE = 'free';
    const TYPE_PAYPAL = 'paypal';
    const TYPE_BONUS = 'bonus';


    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
//            [['vip'], 'anyVal', 'params' => ['vip', 'prem', 'color', 'up']],
            [['vip','prem', 'up', 'color'], 'boolean'],
            [['id'], 'integer'],
            ['type', 'string'],
            ['type', 'in', 'range' => ['paypal', 'free', 'bonus']]
        ];
    }


    /**
     * Обработать объявление
     *
     * @return bool
     */
    public function save(){

        if ($this->type == self::TYPE_FREE){
            if($this->_adv->can_free_rise){
                $this->_adv->rise();
                Yii::$app->session->setFlash('success', Yii::t('app', 'Your adv was rised'));
                return true;
            }
        }
        elseif(!$this->color && !$this->prem && !$this->vip && !$this->up){
            Yii::$app->session->setFlash('error', Yii::t('app', 'Select at least one service'));
            return false;
        }
        else{
            $order = new Orders();
            $order->id_adv = $this->id;
            $order->type = Orders::TYPE_ADV;

            if($this->vip) $this->addItem('vip');
            if($this->prem) $this->addItem('prem');
            if($this->color) $this->addItem('color');
            if($this->up) $this->addItem('up');


            $order->items = $this->items;
            if($this->pay()){
                $order->save();
                return true;
            }
        }

        return false;
    }

    /**
     * Добавить сервис в карзину
     *
     * @param $item
     */
    public function addItem($item){
        $this->items[$item] = true;

        $price = Yii::$app->params['service_price'][$item];
        $this->total_price += $price;
    }


    public function pay(){

        if($this->type == self::TYPE_BONUS){
            /** @var User $user */
            $user = Yii::$app->user->identity;
            if($user->money < $this->total_price) {
                Yii::$app->session->setFlash('error', Yii::t('app', 'Not enough bonuses'));
                return false;
            }

            foreach ($this->items as $key => $item){
                $t_field = $key.'_start';
//                if($key == 'prem') $this->_adv->premium = $item;
//                else
                if($key != 'up')
                    $this->_adv->$key = $item;
                else{
                    $this->_adv->date = time();
                    $this->_adv->up_start = time();
                }
                $this->_adv->$t_field = time();
            }

            if($this->_adv->save()) {
                $user->money -= $this->total_price;
                $user->save();

                Yii::$app->session->setFlash('success', Yii::t('app', 'Improvement successfully purchased'));
                return true;
            }

            return false;
        }
        elseif($this->type == self::TYPE_PAYPAL){

            /** @var Paypal $paypal */
            $paypal = Yii::$app->paypal;



            $items = [];
            foreach ($this->items as $key => $item){
                $items[] = [
                    'name' => $key,
                    'quantity' => 1,
                    'price' => Yii::$app->params['service_price'][$key]
                ];
            }
            $params = [
                'currency' => 'EUR', // only support currency same PayPal
                'email' => Yii::$app->params['support_email'],
                'items' => $items
            ];
            $response = Yii::$app->payPalRest->createInvoice($params);
            $link = Yii::$app->payPalRest->getLinkCheckOut($params);
            var_dump($response);

            die();
        }

        return false;
    }

    /**
     * Получить объявление
     *
     * @return bool|static
     */
    public function get_adv(){
        if($this->adv == null){
            if(!$this->id) return false;
            $this->adv = Adv::findOne($this->id);
        }
        return $this->adv;
    }
}
