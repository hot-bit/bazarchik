<?php
namespace frontend\models;

use common\models\Adv;
use common\models\CMS;
use common\models\forms\ParamsForm;
use common\models\User;
use common\models\validators\PhoneValidator;
use Yii;
use yii\base\Model;

/**
 * Signup form
 *
 *
 * @property Adv $_adv
 */
class AddAdvForm extends Model
{

    // переменная для тестов
    public $tests = false;
    public $params;

    public $id;
    public $step = 1;
    private $tmp_adv;

    // STEP 1
    public $name;
    public $email;
    public $phone_code;
    public $phone;
    public $city;
    public $area;

    // STEP 2
    public $category;

    // STEP 3
    public $title;
    public $text;
    public $price;

    // STEP 3
    public $photo;





    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            ['tests', 'boolean'],
            [['name', 'email', 'phone', 'city'], 'required', 'on' => 'step_1'],

            [['category'], 'checkCategory', 'on' => 'step_2'],

            [['title', 'text'], 'required', 'on' => 'step_3'],

            ['phone', PhoneValidator::className()],
            ['phone', 'number'],
            ['price', 'double'],

            [['city', 'area', 'category', 'phone_code', 'category'], 'integer'],
            [['name', 'title', 'text'], 'string'],
            [['name', 'title', 'text', 'price', 'email'], 'trim'],
            ['email', 'email'],
        ];
    }

    
    public function attributeLabels(){

        return [
            'name' =>       Yii::t('app',  'Name'),
            'city' =>       Yii::t('app',  'City'),
            'area' =>       Yii::t('app',  'Area'),
            'text' =>       Yii::t('app',  'Text'),
            'phone' =>      Yii::t('app', 'Phone'),
            'title' =>      Yii::t('app', 'Title'),
            'price' =>      Yii::t('app', 'Price'),
            'category' =>   Yii::t('app', 'Category'),
        ];
    }

    public function checkCategory(){
        if($this->category == 0)
            $this->addError('category', 'Category cannot be empty');
    }


    public function SetUser(){
        /* @var $user User */
        $user = \Yii::$app->user->identity;
        $this->name = $user->name;
        $this->email = $user->email;
        $this->phone = $user->phone;
        $this->phone_code = $user->phone_code;
        $this->city = $user->city;
    }

    public function upload(){

        if($this->step == 5)
            return true;

        if(!$this->load(\Yii::$app->request->post()) || !$this->validate())
            return false;

        if($this->id != null)
            $model = Adv::findOne($this->id);
        else
            $model = new Adv();

        if($model->load(['Adv' => $this->toArray()]) && $model->save()){
            $this->id = $model->id;
            return true;
        }
        return false;
    }

    public function LoadAdv($id){
        $this->id = $id;
        $model = Adv::findOne($id);
        if($this->load(['AddAdvForm' => $model->toArray()]))
            return true;

        return false;
    }

    public function get_adv(){
        if($this->tmp_adv == null)
            $this->tmp_adv = Adv::findOne($this->id);
        return $this->tmp_adv;
    }

    public function canMove($step){
        if(!$this->_adv) return false;

        if($step > 2 && $this->_adv->category == 0)
            return false;

        if($step == 'finish')
            if($this->title == null || $this->text == null ) return false;


        return true;

    }

    /**
     * @param $params ParamsForm
     * @return bool
     */
    public function checkParams($params){

        return true;
    }
}
