<?php
namespace frontend\models;

use common\models\CMS;
use Yii;
use yii\base\Model;
use common\models\User;

/**
 * Signup form
 */
class SignupForm extends Model
{
    public $email;
    public $rule = true;
    public $subscribe = true;

    private $tmp_password;
    private $user;

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            ['email', 'trim'],
            ['email', 'required'],
            ['email', 'email'],
            ['email', 'string', 'max' => 255],
            ['email', 'unique', 'targetClass' => '\common\models\User', 'message' => 'This email address has already been taken.'],

            ['rule', 'required', 'requiredValue' => true, 'message' => Yii::t('app', 'You must confirm the service rules')],
            ['rule', 'boolean'],

            ['subscribe', 'boolean'],
        ];
    }

    /**
     * Signs user up.
     *
     * @return User|null the saved model or null if saving fails
     * @throws \yii\base\Exception
     */
    public function signup()
    {
        if (!$this->validate()) {
            return null;
        }
        
        $this->user = new User();
        $this->user->email = $this->email;
        $hash = Yii::$app->getSecurity()->generateRandomString();
        $this->user->hash = $hash;

       return $this->user->save() ? $this->user : null;
    }

    /**
     * Отправить email Для подтверждения адреса
     *
     * @return bool
     */
    public function sendEmail()
    {
        return Yii::$app->mailer
            ->compose(
                ['html' => 'confirm_registration/registration-confirm-html', 'text' => 'confirm_registration/registration-confirm-text'],
                ['user' => $this->user]
            )
            ->setTo($this->email)
            ->setFrom([Yii::$app->params['support_email'] => 'Cyprus Bazar'])
            ->setSubject(Yii::t('app', 'Confirm your registration on Cyprus Bazar'))
            ->send();
    }
}
