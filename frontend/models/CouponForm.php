<?php
/**
 * Created by PhpStorm.
 * User: York
 * Date: 11.01.2018
 * Time: 12:37
 */

namespace frontend\models;


use common\models\Adv;
use common\models\Coupon;
use common\models\Model;
use common\models\User;
use common\models\validators\PhoneValidator;
use Yii;

/**
 *
 * @property string $f_phone
 */
class CouponForm extends Model
{
    public $rules;
    public $adv_id;
    public $price;
    public $available;
    public $email;
    public $phone;
    public $phone_code;

    /** @var User */
    public $_user;
    /** @var Adv */
    public $_adv;
    /** @var Coupon[] */
    public $_counpons;

    public function rules()
    {
        return [
            [['email', 'phone', 'rules'], 'required'],
            ['rules', 'boolean'],
            ['email', 'email'],
            [['available', 'price', 'phone_code', 'adv_id'], 'number'],
            [['phone'], PhoneValidator::className()],
        ];
    }

    public function attributeLabels(){
        return [
            'price' => Yii::t('app', 'Price'),
            'available' => Yii::t('app', 'Available'),
            'email' => Yii::t('app', 'Email'),
            'phone' => Yii::t('app', 'Phone'),
            'rules' => Yii::t('app', 'Rules')
        ];
    }


    public function SetUser(){
        /** @var User $user */
        $user = \Yii::$app->user->identity;
        $this->email = $user->email;
        $this->phone = $user->phone;
        $this->phone_code = $user->phone_code;
        $this->_user = $user;
    }

    public function SetAdv($id){
        $adv = Adv::findOne($id);
        if(!$adv) return false;

        $coupons = Coupon::find()->where(['adv_id' => $id])->andWhere(['status' => Coupon::STATUS_FREE])->all();
        if(!$coupons) return false;
        $this->_counpons = $coupons;

        $this->available = count($coupons);
        $this->price = $adv->price;
        $this->_adv = $adv;
        return true;
    }

    public function getF_phone(){
        return '+'.$this->phone_code.$this->phone;
    }

    public function buy(){
        if($this->_user->money < $this->_adv->price){
            $this->addError('price', Yii::t('app', 'Not enough bonuses'));
            return false;
        }


        $coupon = $this->_counpons[0];
        $code = $coupon->code;

        $coupon->status = Coupon::STATUS_SOLD;
        if($coupon->save()){
            Yii::$app->session->set('success', Yii::t('app', 'Check your email'));
                return Yii::$app->mailer
                    ->compose(
                        ['html' => 'coupon/coupon-html', 'text' => 'coupon/coupon-text'],
                        ['code' => $code, 'adv' => $this->_adv]
                    )
                    ->setTo($this->email)
                    ->setFrom([Yii::$app->params['support_email'] => 'Cyprus Bazar'])
                    ->setSubject(Yii::t('app', 'Your coupon from Cyprus Bazar'))
                    ->send();
        }
        return false;


    }
}