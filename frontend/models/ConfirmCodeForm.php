<?php
/**
 * Created by PhpStorm.
 * User: York
 * Date: 06.01.2018
 * Time: 12:02
 */

namespace frontend\models;


use common\models\Model;
use Yii;

class ConfirmCodeForm extends Model
{

    public $token;

    public function rules()
    {
        return [
            ['token', 'required'],
            ['token', 'string'],
        ];
    }


    public function attributeLabels()
    {
        return [
            'token' => Yii::t('app', 'Confirmation code')
        ];
    }

}