<?php
/**
 * Created by PhpStorm.
 * User: York
 * Date: 01.12.2017
 * Time: 19:48
 */

namespace frontend\models;


use common\models\Adv;
use common\models\Brand;
use common\models\Categories;
use common\models\forms\SearchForm;
use common\models\Model;
use common\models\MotoBrand;
use common\models\SearchRequest;
use Yii;
use yii\data\Pagination;
use yii\db\ActiveQuery;
use yii\web\NotFoundHttpException;

/**
 * Class Catalog
 * @package frontend\models
 *
 */
class Catalog{
    
    /** @var Adv[] */
    public $premium;

    /** @var Adv[] */
    public $vip;

    /** @var Categories */
    public $category;

    /** @var bool|Brand|MotoBrand|Model */
    public $subcategory_model = false;

    /** @var string|bool  */
    public $subcategory_type = false;

    /** @var integer */
    public $page;

    /** @var bool|Categories */
    public $category_model = false;

    /** @var Adv[] */
    public $ads;

    /** @var Pagination */
    public $pagination;

    /** @var integer */
    public $count_ads;

    /** @var Categories[] */
    public $children_categories;

    /** @var array */
    private $exc_ads = [];

    /** @var array */
    private $category_child;

    /** @var array */
    public $sort_fields = [
        'date', 'date desc',
        'price', 'price desc'
    ];

    /** @var string */
    public $sort = 'date desc';

    /** @var SearchForm */
    public $search_model;

    /** @var CatalogFilter */
    public $filter;

    /** @var bool  */
    public $show_top = true;

    /** @var integer */
    public $max_price = 10000;

    /** @var int  */
    public $min_price = 0;

    /** @var ActiveQuery */
    public $query;

    /**
     * Catalog constructor.
     * @param $category
     * @param $page
     * @param $sort
     * @param $search
     */
    public function __construct($category, $page, $sort, $search){
        $this->category = $category;
        $this->page = $page;
        $this->sort = $sort;

        $this->filter = new CatalogFilter();
        $this->findAds();
        $this->findChildrenCategories();
        $this->breadcrumbs();

        if($search) $this->AfterCatalog();
    }

    private function AfterCatalog(){
        if(isset($_GET['CatalogFilter'])){
            $params = $_GET['CatalogFilter'];
            if($params['category_id'] != $this->category_model->id){
                $newCategory = Categories::findOne($params['category_id']);
                $this->category = $newCategory->url;
                $this->category_model = $newCategory;
            }
            unset($params['category_id']);
            foreach ($params as $key => $value){
                if(empty($value)){ unset($params[$key]); continue;}
                if($key == 'price_to' && ($value >= $this->max_price)) unset($params[$key]);
                if($key == 'price_from' && ($value <= $this->min_price)) unset($params[$key]);
                if($key == 'brand' && $this->category_model->id == Categories::CARS && !$params['model']){
                    if(!$this->subcategory_model) $this->getSubcategory('brand', $value);
//                    else $this->category = $this->subcategory_model->link;

//                    var_dump($this->subcategory_model);

                    unset($params[$key]);
                }
                elseif ($key == 'model' && $this->category_model->id == Categories::CARS){
                    if(!$this->subcategory_model) $this->getSubcategory('model', $value);
//                    else $this->category = $this->subcategory_model->link;
                    unset($params['brand']);
                    unset($params[$key]);
                }
            }



            if(empty($params))
                $redir = ['adv/index', 'cat' => $this->category];
            else
                $redir = ['adv/search', 'cat' => $this->category] + ['CatalogFilter' => $params];

            if(in_array($this->subcategory_type, ['brand', 'model']))
                $this->category_model = Categories::findOne(Categories::CARS);

//            var_dump($redir);
//            die();
            if($_GET['CatalogFilter'] != $params)
                return Yii::$app->response->redirect($redir);

        }
    }

    /**
     * @return array
     */
    public static function DateSort(){
        return [
            'date desc' => Yii::t('app', 'First new'),
            'date' => Yii::t('app', 'First old'),
        ];
    }

    /**
     * @return array
     */
    public static function PriceSort(){
        return [
            'price desc' => Yii::t('app', 'First expensive'),
            'price' => Yii::t('app', 'First cheap'),
        ];
    }

    /**
     * Найти категорию
     *
     * @return bool
     * @throws NotFoundHttpException
     */
    private function findCategory(){
        if(!$this->category) return false;
        $this->category_model = Categories::find()->where(['url' => $this->category])->one();
        if(!$this->category_model) {
            if(!$this->findSubCategory()){
                throw new  NotFoundHttpException(Yii::t('app', 'Category not found'));
            }
        }
    }

    /**
     * Найти подкатегории
     *
     * @return bool
     */
    private function findSubCategory(){
        $this->subcategory_model = Brand::findByUrl($this->category);
        if($this->subcategory_model)
            return $this->SubcategoryItem('brand', 743, 'brand');

        $this->subcategory_model = Model::findByUrl($this->category);
        if($this->subcategory_model){
            $this->SubcategoryItem('model', 743, 'model');

            $this->filter->brand = $this->subcategory_model->brand_id;
            return true;
        }

        $this->subcategory_model = MotoBrand::findByUrl($this->category);
        if($this->subcategory_model)
            return $this->SubcategoryItem('moto_brand', 76, 'brand');

        return false;
    }

    private function getSubcategory($type, $id){
        $this->subcategory_type = $type;
        switch ($type){
            case 'brand':
                $this->subcategory_model = Brand::findOne($id);
                break;
            case 'model':
                $this->subcategory_model = Model::findOne($id);
                break;
            case 'moto_brand':
                $this->subcategory_model = MotoBrand::findOne($id);
                break;
        }
        $this->subcategory_type = $type;
        $this->category = $this->subcategory_model->url;
    }

    /**
     * Подкатегория
     *
     * @param $type
     * @param $category
     * @param $filter
     * @return bool
     */
    private function SubcategoryItem($type, $category, $filter){
        $this->subcategory_type = $type;
        $this->category_model = Categories::findOne($category);
        $this->category = $this->category_model->url;
        $this->filter->{$filter} = $this->subcategory_model->id;
        $this->filter->active = true;
        return true;
    }

    /**
     * Поиск объявлений
     */
    private function findAds(){
        $query = Adv::find();

        $query = $this->setSearch($query);

        $this->findCategory();

        if($this->category){
            $this->category_child = $this->category_model->ChildrenIdList();
            $query->andWhere(['adv.category' => $this->category_child]);
        }

        $this->filter->category_id = $this->category_model ? $this->category_model->id : 0;
        if($this->filter->load(Yii::$app->request->get()) || $this->filter->active)
            $query = $this->filter->setFilters($query);

        $this->query = $this->filter->query;

        if($this->show_top){
            $this->findVip();
            $this->findPremium($query);

            if(!empty($this->exc_ads))
                $query->andWhere("adv.id not in (".implode(',', $this->exc_ads).")");
        }

        if(!$this->query) $this->query = clone $query;
        $this->setMaxPrice($this->query);
        $this->setMinPrice($this->query);
        if(!$this->filter->price_from) $this->filter->price_from = $this->min_price;
        if(!$this->filter->price_to) $this->filter->price_to = $this->max_price;

        $query->groupBy('adv.id');

        $query->andWhere(['status' => Adv::STATUS_ACTIVE]);

        $countQuery = clone $query;
        $this->count_ads = $countQuery->count();


        /** Есди есть премиум но нет обычных */
        if(!$this->count_ads && count($this->premium)){
            $this->count_ads = count($this->premium);
            $this->premium = false;
            $this->exc_ads = [];
        }

        $this->pagination = new Pagination(['totalCount' => $this->count_ads]);

        $count_vip = count($this->vip);
        $count_prem = count($this->premium);

        $this->pagination->defaultPageSize = $this->show_top ? (15 + (6 - ($count_prem + $count_vip))) : 21;

        if(!in_array($this->sort, $this->sort_fields)) $this->sort = 'date desc';

//        echo $query->createCommand()->sql;
//        die();

        $query->orderBy($this->sort);
        $this->ads = $query->offset($this->pagination->offset)->limit($this->pagination->limit)->all();

    }

    /**
     * Найти Vip объявления
     *
     * @return bool
     */
    private function findVip(){
        $query = Adv::find()->where(['adv.vip' => 1, 'adv.status' => 2]);
        if($this->category)
            $query->andWhere(['in', 'adv.category', $this->category_child]);

        if(!empty($this->exc_ads))
            $query->andWhere("adv.id not in (".implode(',', $this->exc_ads).")");

        $query->limit(3);
        $query->orderBy('RAND()');
        $this->vip = $query->all();

        if(!$this->vip) return false;
        foreach ($this->vip as $item)
            $this->exc_ads[] = $item->id;

        return false;
    }

    /**
     * Найти premium объявления
     *
     * @param ActiveQuery $query
     * @return bool
     */
    private function findPremium(ActiveQuery $query){
        $query = clone $query;
        $query->andWhere(['adv.premium' => 1]);

        if($this->category)
            $query->andWhere(['in', 'adv.category', $this->category_child]);



        if(!empty($this->exc_ads))
            $query->andWhere("adv.id not in (".implode(',', $this->exc_ads).")");

        $query->limit(3);
        $query->orderBy('RAND()');
        $this->premium = $query->all();

        if(!$this->premium) return false;
        foreach ($this->premium as $item)
            $this->exc_ads[] = $item->id;

        return false;
    }

    /**
     * Найти дочерние категори
     */
    private function findChildrenCategories(){
        $parent_category_id = 0;
        if($this->category) $parent_category_id = $this->category_model->id;
        $this->children_categories = Categories::find()
            ->where(['parent' => $parent_category_id, 'header' => 0, 'status' => 1])
            ->orderBy('position')
            ->all();
    }

    /**
     * Назначить поиск
     *
     * @param ActiveQuery $query
     * @return ActiveQuery
     */
    private function setSearch(ActiveQuery $query){
        $this->search_model = new SearchForm();
        if(!$this->search_model->load(Yii::$app->request->get())) return $query;

        $this->category = $this->search_model->category_id;

        if($this->category == null)
            $this->show_top = false;

        SearchRequest::Add($this->search_model->search);

        if($this->search_model->category_id != null){
            $this->category_child = $this->search_model->get_category()->ChildrenIdList();
            $query->andWhere(['in', 'category', $this->category_child]);
        }

        $query->where('title LIKE :q', [':q' =>  '%'.$this->search_model->search.'%']);
        if($this->search_model->in_title == null)
            $query->orWhere('text_rdy LIKE :q', [':q' => '%'.$this->search_model->search.'%']);

        return $query;
    }


    /**
     * Назначить максимальную цену
     *
     * @param ActiveQuery $query
     */
    private function setMaxPrice(ActiveQuery $query){
        $query = clone $query;

        $query->orderBy('adv.price DESC');

        /** @var Adv $model */
        $model = $query->one();
        $this->max_price = $model->price;
    }

    /**
     * Назначить минимальную цену
     *
     * @param ActiveQuery $query
     */
    private function setMinPrice(ActiveQuery $query){
        $query = clone $query;

        $query->orderBy('adv.price');

        /** @var Adv $model */
        $model = $query->one();
        $this->min_price = $model->price;
    }

    /**
     * Сбор хлебных крошек
     */
    public function breadcrumbs(){
        $bc = [
            \yii\helpers\Url::to(['adv/index']) => Yii::t('app', 'Catalog')
        ];
        if($this->category_model)
            foreach ($this->category_model->stairs_models as $cat){
                if($cat->id == $this->category_model->id && !$this->subcategory_model) $bc[] = $cat->name;
                else $bc[$cat->link] = $cat->name;
            }
        if($this->search_model->search)
            $bc[] = Yii::t('app', 'Search').': '.$this->search_model->search;

        if($this->subcategory_model){
            switch ($this->subcategory_type){
                case 'model':
                    $bc[$this->subcategory_model->_brand->link] = $this->subcategory_model->_brand->name;
                    $bc[] = $this->subcategory_model->name;
                    break;
                default:
                    $bc[] = $this->subcategory_model->name;
            }
        }

        Yii::$app->breadcrumbs->Items($bc);
    }


    public function SortUrl($sort){
        $uri = $_SERVER['REQUEST_URI'];
        $url = parse_url($uri);

        if(!isset($url['query']))
            return $uri.'?sort=';

        $query = $url['query'];
        $params_items = explode('&', $query);

        $params = [];
        foreach ($params_items as $item){
            list($key, $value) = explode('=', $item);
            $params[$key] = $value;
        }

        unset($params['sort']);

        $params['sort'] = $sort;


        return $url['path'].'?'.http_build_query($params);


    }
}