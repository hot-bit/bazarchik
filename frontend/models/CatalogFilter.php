<?php
/**
 * Created by PhpStorm.
 * User: York
 * Date: 14.12.2017
 * Time: 13:41
 */

namespace frontend\models;


use common\models\Categories;
use Yii;
use yii\base\Model;
use yii\db\ActiveQuery;

/**
 * Class CatalogFilter
 * @package frontend\models
 * @property \yii\db\ActiveQuery $filters
 * @property Categories $_category
 */
class CatalogFilter extends Model{

    public $active = false;

    public $price_from;
    public $price_to;

    public $city_id;
    public $category_id;

    public static $not_params_attr = [
        'price_from', 'price_to', 'city_id', 'category_id',
        'active'
    ];

    // Cars
    public $brand;
    public $model;
    public $body;
    public $year;
    public $mileage;
    public $box;
    public $engine;
    public $power;
    public $drive;
    public $wheel;

    // Property
    public $bedrooms;
    public $area_id;
    public $cover;


    public $query;

    public function rules(){
        return [
            [['price_from', 'category_id', 'price_to', 'city_id', 'brand', 'model', 'body', 'year', 'mileage', 'box', 'engine', 'power', 'drive', 'wheel'], 'integer'],
        ];
    }


    public function attributeLabels(){

        return [
            'price_from' => Yii::t('app', 'Price from'),
            'price_to' => Yii::t('app', 'Price to'),
            'city_id' => Yii::t('app', 'City'),
            'category_id' => Yii::t('app', 'Category'),

            'brand' => Yii::t('app', 'Brand'),
            'model' => Yii::t('app', 'Model'),
            'body' => Yii::t('app', 'Body type'),
            'year' => Yii::t('app', 'Year'),
            'mileage' => Yii::t('app', 'Mileage'),
            'box' => Yii::t('app', 'Transmission type'),
            'engine' => Yii::t('app', 'Engine type'),
            'power' => Yii::t('app', 'Engine volume'),
            'drive' => Yii::t('app', 'Wheels drive'),
            'wheel' => Yii::t('app', 'Wheel RHD / LHD'),
        ];
    }


    public function setFilters(ActiveQuery $query){
        if($this->city_id) $query->andWhere(['adv.city' => $this->city_id]);

        $i = 0; foreach ($this->attributes as $key => $val){
            if($val == '') continue;
            if(in_array($key, self::$not_params_attr)) continue;

            $alias = 'w'.$i;
            $query->joinWith(['_params '.$alias]);

            $query->andWhere([$alias.'.name' => $key])
                ->andWhere([$alias.'.value' => $val]);
            $i++;
        }


        $this->query = clone $query;

        if($this->price_from) $query->andWhere('adv.price >= :price_from', [':price_from' => $this->price_from]);
        if($this->price_to) $query->andWhere('adv.price <= :price_to', [':price_to' => $this->price_to]);

        return $query;
    }

    /**
     * @return null|Categories
     */
    public function get_category(){
        return Categories::findOne($this->category_id);
    }
}