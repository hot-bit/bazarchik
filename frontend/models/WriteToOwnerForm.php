<?php

namespace frontend\models;

use common\models\Adv;
use common\models\AdvChat;
use common\models\AdvMessage;
use common\models\Orders;
use common\models\User;
use Yii;
use yii\base\Model;

/**
 * ContactForm is the model behind the contact form.
 *
 *
 * @property Adv $_adv
 * @property User $_shop
 */
class WriteToOwnerForm extends Model
{

    public $adv_id;
    public $shop_id;

    public $name;
    public $email;
    public $phone;
    public $text;

    private $t_adv;
    private $t_shop;

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['name', 'phone', 'text'], 'required'],
            [['name', 'email', 'phone'], 'string', 'max' => 255],
            ['email', 'email'],
            ['text', 'trim']
        ];
    }

    public function attributeLabels(){
        return [
            [['name']]
        ];
    }

    public function get_adv(){
        if($this->t_adv == null){
            $this->t_adv = Adv::findOne($this->adv_id);
        }
        return $this->t_adv;
    }

    public function get_shop(){
        if($this->t_shop == null){
            $this->t_shop = User::findByCompany($this->shop_id);
        }
        return $this->t_shop;
    }

    /**
     * Сохранить данные
     *
     * @return bool
     */
    public function save(){
        return $this->addMessage() && $this->sendEmail();
    }

    /**
     * Добавить сообщение в базу
     *
     * @return bool
     */
    private function addMessage(){
        if($this->_adv->user == 0){
            $model = new AdvChat();
            $model->user_1 = 0;

            if(Yii::$app->user->isGuest)
                $model->user_2 = $this->email;
            else
                $model->user_2 = $this->_adv->user;

            $model->save();

            $model->addMessage($this->text, 1);
            return true;
        }

        if(Yii::$app->user->isGuest){
            $model = AdvChat::findByEmail($this->email);
            if(!$model){
                $model = new AdvChat();
                $model->user_1 = Yii::$app->user->id;
                $model->user_2 = $this->_adv->user;
                $model->save();
            }
        }
        else{
            $model = AdvChat::findByWriter(Yii::$app->user->id);
            if(!$model){
                $model = new AdvChat();
                $model->user_1 = Yii::$app->user->id;
                $model->user_2 = $this->_adv->user;
                $model->save();
            }
        }

        $model->addMessage($this->text, 1, $this->adv_id);
        return true;
    }


    /**
     * Отправить сообщение по почте
     *
     * @return bool
     */
    private function sendEmail(){
        $subject = 'You have received a new message on your ad: '.$this->_adv->title.' #'.$this->_adv->id;


        return Yii::$app->mailer
            ->compose(
                ['html' => 'write_to_owner/write-to-owner-html', 'text' => 'write_to_owner/write-to-owner-text'],
                ['data' => $this]
            )
            ->setTo($this->_adv->email)
            ->setFrom([Yii::$app->params['support_email'] => 'Cyprus Bazar'])
            ->setSubject($subject)
            ->send();
    }
}
