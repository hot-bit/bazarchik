<?php

namespace frontend\tests\functional;

use common\fixtures\AdvFixture;
use common\fixtures\UserFixture;
use common\models\User;
use frontend\tests\FunctionalTester;
use yii\helpers\Url;

class AdvCest
{

    public function _before(FunctionalTester $I)
    {
        $I->haveFixtures([
            'adv' => [
                'class' => AdvFixture::className(),
                'dataFile' => codecept_data_dir() . 'adv.php'
            ],
            'user' => [
                'class' => UserFixture::className(),
                'dataFile' => codecept_data_dir() . 'user.php'
            ]
        ]);
    }



    public function checkAdvPageWorks(FunctionalTester $I){

        $I->amOnRoute('adv/get', ['url' => 'cat-for-sale', 'id' => 500]);
        $I->see('Cat for sale', 'h1');

    }

    public function checkImprovePageWorks(FunctionalTester $I){

        $I->amLoggedInAs(1);
        $I->amOnRoute('adv/improve', ['id' => 500]);
        $I->see('Cat for sale №:500', 'h1');
    }

    public function checkFreeRiseWorks(FunctionalTester $I){

        $I->amLoggedInAs(1);
        $I->amOnRoute('adv/improve', ['id' => 500]);

        $I->see('Rise in catalog for FREE', 'div');
        $I->submitForm('#improve-ad', ['ImproveAdForm[type]' => 'free'], '.type_free');
        $I->see('Your adv was rised');
    }




}