<?php

namespace frontend\tests\functional;

use common\fixtures\AdvFixture;
use frontend\tests\FunctionalTester;
use yii\helpers\Url;

class AddAdvCest
{

    private $form_id = '#add-ad-form';
    public function _before(FunctionalTester $I)
    {
        $I->haveFixtures([
            'adv' => [
                'class' => AdvFixture::className(),
                'dataFile' => codecept_data_dir() . 'adv.php'
            ]
        ]);
        $I->amOnRoute('adv/add');
    }

    private function insertData($arr){
        $data = [];
        foreach ($arr as $key => $val){
            $data['AddAdvForm['.$key.']'] = $val;
        }

        return $data;
    }

    public function checkOpen(FunctionalTester $I)
    {
        $I->wantTo('check add page works');
        $I->see('Add adv', 'h1');
    }

    public function checkEmptyAddOnFirstPage(FunctionalTester $I)
    {
        $I->submitForm($this->form_id, $this->insertData([
            'name' => '',
            'email' => '',
            'phone' => '',
            'city' => '',
            'area' => '',
        ]));

        $I->seeValidationError('Name cannot be blank.');
        $I->seeValidationError('Phone cannot be blank.');
        $I->seeValidationError('Email cannot be blank.');
    }
    public function checkErrorAddOnFirstPage(FunctionalTester $I)
    {
        $I->submitForm($this->form_id, $this->insertData([
            'name' => 'Nicolas',
            'email' => 'nicolas',
            'phone' => 'Hello',
            'city' => '1',
        ]));

        $I->seeValidationError('Email is not a valid email address');
        $I->seeValidationError('Incorrect phone');
    }
    public function checkOkAddOnFirstPage(FunctionalTester $I)
    {
        $I->submitForm($this->form_id, $this->insertData([
            'name' => 'Nicolas',
            'email' => 'nicolas@gmail.com',
            'phone' => '123456789',
            'city' => '1',
            'area' => '171',
        ]));
        $I->see('Category', 'h3');
    }

    public function checkEmptyAddOnSecondStep(FunctionalTester $I){
        $I->wantTo('see errors on second step if empty');
        $I->amOnRoute('adv/add', ['id' => '100', 'step' => 2]);
        $I->see('Category', 'h3');
        $I->submitForm($this->form_id, $this->insertData(['category' => 0]));
        $I->see('Category cannot be empty', 'li');
    }

    public function checkOkAddOnSecondStep(FunctionalTester $I){
        $I->wantTo('see ok on second step and go to third');
        $I->amOnRoute('adv/add', ['id' => '100', 'step' => 2]);
        $I->see('Category', 'h3');
        $I->submitForm($this->form_id, $this->insertData(['category' => 647]));

        $I->see('Adv information', 'h3');
    }

    public function checkEmptyAddOnThirdStep(FunctionalTester $I){
        $I->wantTo('see errors on third step if empty');
        $I->amOnRoute('adv/add', ['id' => '200', 'step' => 3]);
        $I->see('Adv information', 'h3');
        $I->submitForm($this->form_id, $this->insertData(['title' => '', 'text' => '', 'price' => '']));

        $I->seeValidationError('Title cannot be blank');
        $I->seeValidationError('Text cannot be blank');
    }
    public function checkOkAddOnThirdStep(FunctionalTester $I){
        $I->wantTo('see ok on third step and go to firth');
        $I->amOnRoute('adv/add', ['id' => '200', 'step' => 3]);
        $I->see('Adv information', 'h3');
        $I->submitForm($this->form_id, $this->insertData(['title' => 'Sale cat', 'text' => 'It is not my cat. I found it.', 'price' => '100', 'tests' => true]));

        $I->see('Tester', 'td');
    }





}