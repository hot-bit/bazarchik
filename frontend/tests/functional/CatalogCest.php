<?php

namespace frontend\tests\functional;

use common\fixtures\AdvFixture;
use frontend\tests\FunctionalTester;
use yii\helpers\Url;

class CatalogCest
{
    public function _before(FunctionalTester $I)
    {
        $I->haveFixtures([
            'user' => [
                'class' => AdvFixture::className(),
                'dataFile' => codecept_data_dir() . 'adv.php'
            ]
        ]);
        $I->amOnRoute('adv/index');
    }

    public function checkOpen(FunctionalTester $I)
    {
        $I->wantTo('check catalog page works');
        $I->see('Catalog', 'h1');
    }

    public function checkCountAds(FunctionalTester $I)
    {
        $I->wantTo('check that on page 15 items, 3 vip and min 3 prem');
        $I->seeNumberOfElements('.catalog-item', 21);
        $I->seeNumberOfElements('.catalog-item__vip', 3);
        $I->seeNumberOfElements('.catalog-item__prem', [3, 21]);
    }

    public function checkSearchForm(FunctionalTester $I){
        $I->wantTo('check that search form works');

        $I->submitForm('#top-search', [
            'SearchForm[search]' => 'test',
            'SearchForm[in_title]' => 'on'
        ]);
        $I->seeInField('#searchform-search', 'test');
        $I->see('Search: test', 'h1');
        $I->seeNumberOfElements('.catalog-item', 21);
    }
}