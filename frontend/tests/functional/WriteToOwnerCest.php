<?php

namespace frontend\tests\functional;

use common\fixtures\AdvFixture;
use common\fixtures\UserFixture;
use common\models\User;
use frontend\tests\FunctionalTester;
use yii\helpers\Url;

class WriteToOwnerCest
{

    public function _before(FunctionalTester $I)
    {
        $I->haveFixtures([
            'adv' => [
                'class' => AdvFixture::className(),
                'dataFile' => codecept_data_dir() . 'adv.php'
            ],
            'user' => [
                'class' => UserFixture::className(),
                'dataFile' => codecept_data_dir() . 'user.php'
            ]
        ]);
        $I->amOnRoute('adv/write-to-owner', ['id' => 500]);
    }



    private function insertData($arr){
        $data = [];
        foreach ($arr as $key => $val){
            $data['WriteToOwner['.$key.']'] = $val;
        }

        return $data;
    }

    public function checkPageWorks(FunctionalTester $I){

        $I->wantTo('check write to owner page works');
        $I->amOnRoute('adv/write-to-owner', ['id' => 500]);
        $I->see('Write a message to the owner', 'h1');

    }
    public function checkFormNotCorrect(FunctionalTester $I){

        $I->wantTo('check form not corrected');

        $I->submitForm('#write-to-owner-form', $this->insertData([
            'name' => '',
            'phone' => '',
            'email' => '',

        ]));
        $I->seeValidationError('Name cannot be blank.');
        $I->seeValidationError('Phone cannot be blank.');
    }

    public function checkFormCorrect(FunctionalTester $I){

        $I->wantTo('check form corrected');

        $I->submitForm('#write-to-owner-form', $this->insertData([
            'name' => 'Nicolas',
            'phone' => '',
            'email' => '',

        ]));
        $I->seeValidationError('Name cannot be blank.');
        $I->seeValidationError('Phone cannot be blank.');
    }






}