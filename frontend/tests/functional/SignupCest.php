<?php

namespace frontend\tests\functional;

use frontend\tests\FunctionalTester;

class SignupCest
{
    protected $formId = '#register-auth';


    public function _before(FunctionalTester $I)
    {
        $I->amOnRoute('site/index');
    }

    protected function formParams($email)
    {
        return [
            'SignupForm[email]' => $email,
        ];
    }
    public function signupWithEmptyFields(FunctionalTester $I)
    {
        $I->see('Registration', 'div.register-title');
        $I->submitForm($this->formId, $this->formParams(''));
        $I->seeValidationError('Email cannot be blank');

    }

    public function signupWithWrongEmail(FunctionalTester $I)
    {
        $I->submitForm($this->formId, $this->formParams('ttttt'));
        $I->see('Email is not a valid email address.', '.help-block');
    }

//    public function signupSuccessfully(FunctionalTester $I)
//    {

//        $I->submitForm($this->formId, $this->formParams('tester.email@example.com'));
//
//        $I->seeRecord('common\models\User', [
//            'email' => 'tester.email@example.com',
//        ]);
//
//        $I->see('Logout');
//    }
}
