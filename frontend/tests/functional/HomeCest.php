<?php

namespace frontend\tests\functional;

use common\fixtures\AdvFixture;
use frontend\tests\FunctionalTester;
use yii\helpers\Url;

class HomeCest
{
    public function _before(FunctionalTester $I)
    {
        $I->haveFixtures([
            'user' => [
                'class' => AdvFixture::className(),
                'dataFile' => codecept_data_dir() . 'adv.php'
            ]
        ]);
        $I->amOnPage(\Yii::$app->homeUrl);
    }
    public function checkOpen(FunctionalTester $I)
    {
        $I->wantTo('check main page works');
        $I->see('Find by category');
        $I->seeLink('About us');
//        $I->click('About');
//        $I->see('This is the About page.');
    }

    public function checkCountAds(FunctionalTester $I)
    {
        $I->wantTo('check that on main page 20 ads');
        $I->seeNumberOfElements('.principaly-newads__container', 20);
    }
    public function checkCountNews(FunctionalTester $I)
    {
        $I->wantTo('check that on main page 2 news');
        $I->seeNumberOfElements('.news-container', 2);
    }
}