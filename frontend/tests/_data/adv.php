<?php
/**
 * Created by PhpStorm.
 * User: York
 * Date: 25.10.2017
 * Time: 16:05
 */

$items = [
    [
        'id' => '100',
        'name' => 'Tester',
        'email' => 'tester@mail.ru',
        'phone' => '123654789',
        'city' => '1',
        'area' => '171',
        'step' => '2'
    ],
    [
        'id' => '200',
        'name' => 'Tester',
        'email' => 'tester@mail.ru',
        'phone' => '123654789',
        'city' => '1',
        'area' => '171',
        'category' => '647',

        'step' => '3'
    ],
    [
        'id' => '500',
        'name' => 'Nicolas',
        'email' => 'nicolas.dianna@hotmail.com',
        'phone' => '123654789',
        'city' => '1',
        'area' => '171',
        'category' => '647',
        'user' => 1,
        'title' => 'Cat for sale',
        'url' => 'cat-for-sale',
        'price' => 200,
        'date' => time() - 86400 * 30,
        'date_create' => time() - 86400 * 30,
        'status' => 2,

    ],

];


for($i = 1; $i < 90; $i++){
    $items[] = [
        'id' => $i,
        'name' => 'Tester'.$i,
        'email' => 'tester@mail.ru',
        'phone' => '123654789',
        'city' => '1',
        'area' => '171',
        'title' => 'Title test ad '. $i,
        'url' => 'url-test-'.$i,
        'text' => 'Text test for ad '. $i,
        'text_rdy' => 'Text test for ad '. $i,
        'category' => '647',
        'price' => rand(0, 500),
        'vip' => $i % 10 == 0 ? 1 : 0,
        'vip_start' => $i % 10 == 0 ? (time() - 1000 * $i) : 0,
        'premium' => $i % 10 == 0 ? 1 : 0,
        'prem_start' => $i % 10 == 0 ? (time() - 1000 * $i) : 0,
        'color' => $i % 7 == 0 ? 1 : 0,
        'color_start' => $i % 7 == 0 ? (time() - 1000 * $i) : 0,
        'up_start' => $i % 15 == 0 ? (time() - 1000 * $i) : 0,
        'date' => rand(time() - 3600 * 24 * 30, time() - 3600),
        'date_create' => time() - 3600 * 24 * 30,
        'status' => 2
    ];
}


return $items;