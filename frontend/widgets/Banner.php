<?php
/**
 * Created by PhpStorm.
 * User: York
 * Date: 01.02.2018
 * Time: 15:51
 */

namespace frontend\widgets;


use Yii;
use yii\base\Widget;
use yii\helpers\Html;
use yii\helpers\Url;

class Banner extends Widget
{
    public $count = 1;
    public $place;
    public $max_size = 300;
    public $block_class = '';
    private $items;

    public function init(){
        parent::init();

        $this->items = \common\models\Banner::find()
            ->where(['place' => $this->place])
//            ->andWhere(['size' => $size])
            ->andWhere(['status' => \common\models\Banner::STATUS_ACTIVE])
            ->andWhere(['<', 'start_date', time()])
            ->andWhere(['>', 'finish_date', time()])
            ->limit($this->count)
            ->orderBy('RAND()')
            ->all();
    }

    public function run()
    {
//        if(!$this->items) return '';

        $ret = [];
        foreach ($this->items as $item) {
            $img = Html::img($item->path, ['style' => 'width: '.$this->max_size.'px']);
            $ret[] = Html::tag('div', Html::a($img, $item->link), ['class' => $this->block_class]);
        }
        if(count($ret) < $this->count){
            $ret[] = '<div class="'.$this->block_class.'"><a class="banner-place" style="width: '.$this->max_size.'px;" href="'.Url::to(['banner/index']).'">'.Yii::t('app', 'Place a banner').'</a></div>';
        }

        return implode('', $ret);
    }

}