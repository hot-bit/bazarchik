<?php
/**
 * Created by PhpStorm.
 * User: yorks
 * Date: 31.12.2017
 * Time: 21:37
 */

namespace frontend\widgets;


use yii\helpers\ArrayHelper;
use yii\helpers\Html;

class LinkPager extends \yii\widgets\LinkPager
{
    public $lastPageLabel = false;
    public $nextPageLabel = false;
    public $firstPageLabel = false;
    public $prevPageLabel = false;


    protected function renderPageButton($label, $page, $class, $disabled, $active)
    {
        if($page == 0) $page = -1;
        return parent::renderPageButton($label, $page, $class, $disabled, $active);
    }
}