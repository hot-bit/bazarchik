<?php
/**
 * Created by PhpStorm.
 * User: York
 * Date: 27.09.2017
 * Time: 23:06
 */

namespace frontend\widgets\cms;
 
use yii\helpers\Html;
use yii\widgets\InputWidget;


class Checkbox extends InputWidget {

    public function run(){
        $name = "{$this->model->formName()}[{$this->attribute}]";

        $item = '<checkbox name="'.$name.'" checked="'.$this->model->{$this->attribute}.'">'.$this->model->getAttributeLabel($this->attribute).'</checkbox>';

//        $item = Html::tag('checkbox', $this->model->getAttributeLabel($this->attribute), [
//            'name' => $name,
//        ]);
//        $params = ['class' => 'cb-styled', 'labelOptions' => ['class' => '']];
        $this->field->label(false);
        $this->field->options = ['class' => 'cb-field'];
//        $item = Html::activeCheckbox($this->model, $this->attribute, $params);

//        $item = $this->form->field($this->model, $this->field, $params)->checkbox();
//        if($this->label)
//            $item = $item->label('<div class="cb-label">'.$this->label.'</div>');
        return $item;
    }


}