<?php
namespace frontend\widgets\cms;
use yii\helpers\Html;

/**
 * Created by PhpStorm.
 * User: York
 * Date: 27.09.2017
 * Time: 14:35
 *
 */


class PopularSearch extends \yii\base\Widget {

    /**
     * @var $model \common\models\SearchRequest[]
     */
    private $model;

    public function init(){
        $this->model = \common\models\SearchRequest::find()->where(['ignore' => null])->limit(25)->orderBy('count DESC')->all();
        parent::init();
    }

    public function run(){


        $ret = Html::beginTag('ul');
        foreach ($this->model as $item){
            $ret .= Html::tag('li', Html::a($item->request, ['adv/search', 'q' => $item->request]));
        }
        $ret .= Html::endTag('ul');
        return $ret;


    }
}