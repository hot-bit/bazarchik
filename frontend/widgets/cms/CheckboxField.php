<?php
/**
 * Created by PhpStorm.
 * User: York
 * Date: 27.09.2017
 * Time: 23:06
 */

namespace frontend\widgets\cms;


use yii\base\Widget;
use yii\db\ActiveRecord;
use yii\helpers\Html;
use yii\widgets\ActiveForm;


class CheckboxField extends Widget{

    /**
     * @var $form ActiveForm
     */
    public $form;

    /**
     * @var $model ActiveRecord
     */
    public $model;
    public $field;
    public $label = false;

    public function run(){
        $params = ['options' => ['class' => 'form-group cb-styled-field']];

        $item = $this->form->field($this->model, $this->field, $params)->checkbox();
        if($this->label)
            $item = $item->label('<div class="cb-label">'.$this->label.'</div>');
        return $item;
    }


}