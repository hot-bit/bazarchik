<?php
/**
 * Created by PhpStorm.
 * User: York
 * Date: 27.09.2017
 * Time: 23:06
 */

namespace frontend\widgets\cms;
 
use yii\helpers\Html;
use yii\widgets\InputWidget;


class RadioList extends InputWidget {

    public $items;
    public $empty = 0;
    public $group = true;
    private $inline = false;

    public function run(){


        if($this->value == null) $this->value = $this->empty;

        if(!$this->group) $this->field->options['class'] = '';

        $ret = Html::beginTag('div', ['class' => 'radio-group']);

        $ret .= Html::activeRadioList($this->model, $this->attribute, $this->items, [
            'item' => function($index, $label, $name, $checked, $value){
                $return = Html::beginTag('label', ['class' => 'radio-styled '.($checked ? 'active' : '').' '.($this->inline ? 'inline-radio': '')]);
                $return .= Html::tag('span', '');
                $return .= '<input type="radio" name="' . $name . '" value="' . $value . '">';
                $return .= $label;
                $return .= Html::endTag('label');
                return $return;
            }
        ]);
//        foreach ($this->items as $key => $val){
//            $active = $this->value == $key;
//
//            $ret .= Html::beginTag('div', ['class' => 'radio-styled '.($active ? 'active' : '')]);
//            $ret .= Html::tag('span', '');
//            $ret .= $val;
//            $ret .= Html::endTag('div');
//        }
        $ret .= Html::endTag('div');

        \Yii::$app->view->registerJs("
            $(document).ready(function(){
                $('.radio-styled').click(function(){
                    $(this).parent().find('.radio-styled').removeClass('active');
                    $(this).addClass('active');
                    
                });
            });
        ");

        return $ret;
    }


}