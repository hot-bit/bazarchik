<?php
/**
 * Created by PhpStorm.
 * User: York
 * Date: 27.09.2017
 * Time: 23:06
 */

namespace frontend\widgets\cms;




use yii\helpers\Html;
use yii\widgets\InputWidget;

class RadioGroupField extends InputWidget{

    public $items = [];
    public $empty = 0;

    public function run(){
        $this->value = $this->model->{$this->attribute};
        if(empty($this->value)) $this->value = $this->empty;

        $name = $this->model->formName().'['.$this->attribute.']';
        $ret = Html::beginTag('div', ['class' => 'btn-group']);

        foreach ($this->items as $key => $val){
            $active = $this->value == $key;

            $ret .= Html::beginTag('label', ['class' => 'btn-group-item '.($active ? 'active' : '')]);
            $ret .= Html::radio($name, $active, ['value' => $key]);
            $ret .= $val;
            $ret .= Html::endTag('label');
        }
        $ret .= Html::endTag('div');

        \Yii::$app->view->registerJs("
            $(document).ready(function(){
                $('.btn-group-item').click(function(){
                    $(this).parent().find('.btn-group-item').removeClass('active').find('input[type=\'radio\']').attr('selected', false);
                    $(this).addClass('active').find('input[type=\'radio\']').attr('selected', true);
                    
                });
            });
        ");

        return $ret;

    }


}