<?php
/**
 * Created by PhpStorm.
 * User: York
 * Date: 27.09.2017
 * Time: 23:06
 */

namespace frontend\widgets\cms;




use yii\helpers\Html;
use yii\widgets\InputWidget;

class CarColorField extends InputWidget{

    public $items = [];
    public $active = false;
    public $empty = 0;

    public function run(){
        $ret = Html::beginTag('div', ['class' => 'color-fields type-body']);
        $ret .= Html::activeHiddenInput($this->model, $this->attribute, ['id' => 'item-color-value']);

        $this->value = $this->model->{$this->attribute};
        foreach ($this->items as $item){
            $active = $this->value && $this->value == $item['id'];

            $ret .= Html::beginTag('div', ['class' => 'color-field '.($active ? 'active' : ''), 'data-val' => $item['id']]);
            $ret .= Html::beginTag('label');
                $ret .= Html::tag('span', '', ['style' => $item['color']]);
                $ret .= $item['name'];
            $ret .= Html::endTag('label');
            $ret .= Html::endTag('div');
        }
        $ret .= Html::endTag('div');

        \Yii::$app->view->registerJs("
            $(document).ready(function(){
                $('.color-field').click(function(){
                    var id = $(this).attr('data-val');
                    $('#item-color-value').val(id);
                    $(this).parent().find('.color-field').removeClass('active');
                    $(this).addClass('active'); 
                });
            });
        ");

        return $ret;

    }


}