<?php
/**
 * Created by PhpStorm.
 * User: York
 * Date: 28.09.2017
 * Time: 18:21
 *
 * @var $this \yii\web\View
 *
 */
use common\models\CMS;

?>
<div class="col-sm-12">
    <div class="form-group general-field__container upload-photo__container"
         data-file-uploader-button="<?=Yii::t('app', 'Add photo')?>"
    >
        <div class="upload-container__title">
            <h4><?=Yii::t('app', 'Photo')?></h4>
        </div>
        <input type="file" name="files[]" class="uploader">
    </div>

    <?=\yii\helpers\Html::buttonInput(Yii::t('app', 'Upload photos'), ['class' => ['btn btn--orange btn_upload_images ']])?>

    <?=\yii\helpers\Html::hiddenInput('image_items', '', ['id' => 'image_items'])?>
</div>
