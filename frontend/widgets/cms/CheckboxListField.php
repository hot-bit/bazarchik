<?php
/**
 * Created by PhpStorm.
 * User: York
 * Date: 27.09.2017
 * Time: 23:06
 */

namespace frontend\widgets\cms;


use yii\base\Widget;
use yii\db\ActiveRecord;
use yii\helpers\Html;
use yii\widgets\ActiveForm;


class CheckboxListField extends Widget{

    /**
     * @var $form ActiveForm
     */
    public $form;

    /**
     * @var $model ActiveRecord
     */
    public $model;
    public $field;
    public $label = false;
    public $items;

    public function run(){

        $item = $this->form->field($this->model, $this->field, ['options' => ['class' => '']])->checkboxList($this->items, [
            'item' =>
                function ($index, $label, $name, $checked, $value) {
                    return '<div class="styled-checkbox">'.Html::checkbox($name, $checked, [
                        'class' => 'cb-styled',
                        'value' => $value,
                        'label' => '<span for="' . $label . '">' . $label . '</span>',
                    ]).'</div>';
                },

            'separator' => false,
        ]);
        if(!$this->label) $item->label(false);
        return Html::tag('div', $item, ['class' => 'checkbox-list-wrapper']);
    }


}