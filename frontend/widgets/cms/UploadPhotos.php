<?php
/**
 * Created by PhpStorm.
 * User: York
 * Date: 28.09.2017
 * Time: 18:20
 */

namespace frontend\widgets\cms;


use yii\base\Widget;

class UploadPhotos extends Widget
{

    public function run()
    {

        return $this->render('upload_photos', [

        ]);
    }

}