<?php
/**
 * Created by PhpStorm.
 * User: York
 * Date: 28.09.2017
 * Time: 15:32
 */

namespace frontend\widgets\bazar;


use frontend\models\AddAdvForm;
use Yii;
use yii\base\Widget;

class AddAdvBreadcrumbs extends Widget{

    /**
     * @var $model AddAdvForm
     */
    public $model;
    private $has_filter = false;
    private $step_names;


    public function init(){

        $this->step_names = [
            1 => Yii::t('app', 'Personal data'),
            2 => Yii::t('app', 'Category'),
            3 => Yii::t('app', 'General information'),
            4 => Yii::t('app', 'Photos'),
            5 => Yii::t('app', 'Filters'),
            'finish' => Yii::t('app', 'Finish')
        ];

        if($this->model->_adv && $this->model->_adv->category != 0 && $this->model->_adv->_category->filter)
            $this->has_filter = true;


    }

    public function run(){

        return $this->render('add-adv-breadcrumbs', [
            'model' => $this->model,
            'steps' => $this->step_names,
            'has_filter' => $this->has_filter
        ]);


    }

}