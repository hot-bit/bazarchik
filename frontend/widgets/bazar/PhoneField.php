<?php
/**
 * Created by PhpStorm.
 * User: York
 * Date: 28.09.2017
 * Time: 18:31
 */

namespace frontend\widgets\bazar;


use yii\helpers\Html;
use yii\widgets\InputWidget;

class PhoneField extends InputWidget
{
    public $wrap_class = 'form-group';
    public $code_attr = 'phone_code';
    public $placeholder = '';
    public $input_class = '';
    public $enableValidation = true;

    public function run(){
        $ret = '';
        if($this->enableValidation)
            $this->field->enableAjaxValidation = true;


        $ret .= Html::beginTag('div', ['class' => 'field-group']);
        $ret .= Html::activeDropDownList($this->model, $this->code_attr, \Yii::$app->params['phone_codes']);
        $ret .= Html::activeTextInput($this->model, $this->attribute, ['placeholder' => $this->placeholder, 'class' => $this->input_class]);
        $ret .= Html::endTag('div');

        return $ret;
    }
}

/*
 *
 *
<div class="form-group form-group--small general-field__container field-addadvform-name required">
    <label class="control-label" for="addadvform-name">Name</label>
    <input type="text" id="addadvform-name" class="form-control" name="AddAdvForm[name]" value="Nicolas" placeholder="Your name" aria-required="true">

    <div class="help-block"></div>
</div>
 *
 */