<?php
/**
 * Created by PhpStorm.
 * User: York
 * Date: 15.11.2017
 * Time: 14:39
 *
 * @var $this \yii\web\View
 * @var $steps array
 * @var $has_filter bool
 * @var $model \frontend\models\AddAdvForm
 */

$current_step = $model->step;
$current_step = $current_step == 'finish' ? 6 : $current_step;
?>


<ul class="add-adv__breadcrumbs">
    <? foreach ($steps as $key => $val):
    $k = $key == 'finish' ? 6 : $key;
    if($k == 5 && !$has_filter) continue;
    ?><!--
    --><li>
            <a href="<?=$model->canMove($key) ? \yii\helpers\Url::to(['adv/add', 'step' => $key, 'id' => $model->id]) : '#'?>" class="<?=$k < $current_step ? 'active' : ''?>"><?=$val?></a>
        </li><!--
    --><?endforeach;?>

</ul>