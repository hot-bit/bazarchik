<?php
/**
 * Created by PhpStorm.
 * User: York
 * Date: 28.09.2017
 * Time: 13:17
 */
use common\models\CMS;

/* @var $model \frontend\models\AddAdvForm*/
/* @var $categories \common\models\Categories[] */
/* @var $this \yii\web\View */

$url = \yii\helpers\Url::to(['ajax/category-by-parent', 'id' => '']);

$stairs = false;
if($model->_adv->_category)
    $stairs = $model->_adv->_category->stairs_models;

$active_items = $category_lists = [];
if($stairs)
    foreach ($stairs as $st){
        $active_items[] = $st->id;
        if($st->parent != 0)
            $category_lists[] = \common\models\Categories::find()->where(['parent' => $st->parent])->andWhere('header != 1')->all();
    }

?>



<?=\yii\helpers\Html::activeHiddenInput($model, 'category')?>
<?=\yii\helpers\Html::hiddenInput('last_category_id', 0)?>
<div class="select-category__list">
        <div class="select-category__list-item" data-rel="1"><span><?=$stairs ? $stairs[0]->name : ''?></span></div>
        <div class="select-category__list-item" data-rel="2"><span><?=$stairs ? $stairs[1]->name : ''?></span></div>
        <div class="select-category__list-item" data-rel="3"><span><?=$stairs ? $stairs[2]->name : ''?></span></div>
        <div class="select-category__list-item" data-rel="4"><span><?=$stairs ? $stairs[3]->name : ''?></span></div>
</div>
<div class="select-category__ads">
    <div class="ads-category data-category-select active" data-category-select="2" data-category-clear="[2,3,4]" rel="1">
        <ul>
            <?foreach ($categories as $category):?>
                <li class="ads-category__link <?=in_array($category->id, $active_items) ? 'active' : ''?>" data-id="<?=$category->id?>"><?=$category->name?></li>
            <?endforeach;?>
        </ul>
    </div>

    <?if($stairs):?>
        <div class="ads-category__submenu data-category-select <?= !isset($category_lists[0]) ? 'hide' : ''?>" data-category-select="3" data-category-clear="[3,4]"  rel="2">
            <?if(isset($category_lists[0])):?>
                <ul>
                    <?foreach($category_lists[0] as $item):?>
                        <li class="ads-category__link <?=in_array($item->id, $active_items) ? 'active' : ''?>" data-id="<?=$item->id?>"><?=$item->name?></li>
                    <?endforeach?>
                </ul>
            <?endif?>
        </div>
        <div class="category-submenu__drpd data-category-select  <?= !isset($category_lists[1]) ? 'hide' : ''?>" data-category-select="4" data-category-clear="[4]"  rel="3">
            <?if(isset($category_lists[1])):?>
                <ul>
                    <?foreach($category_lists[1] as $item):?>
                        <li class="ads-category__link <?=in_array($item->id, $active_items) ? 'active' : ''?>" data-id="<?=$item->id?>"><?=$item->name?></li>
                    <?endforeach?>
                </ul>
            <?endif?>
        </div>
        <div class="category-drpd__last data-category-select  <?= !isset($category_lists[2]) ? 'hide' : ''?>" data-category-clear="[]"  rel="4">
            <?if(isset($category_lists[2])):?>
                <ul>
                    <?foreach($category_lists[2] as $item):?>
                        <li class="ads-category__link <?=in_array($item->id, $active_items) ? 'active' : ''?>" data-id="<?=$item->id?>"><?=$item->name?></li>
                    <?endforeach?>
                </ul>
            <?endif?>
        </div>
    <?else:?>

        <div class="ads-category__submenu data-category-select hide" data-category-select="3" data-category-clear="[3,4]"  rel="2"></div>
        <div class="category-submenu__drpd data-category-select hide" data-category-select="4" data-category-clear="[4]"  rel="3"></div>
        <div class="category-drpd__last data-category-select hide" data-category-clear="[]"  rel="4"></div>
    <?endif;?>


</div>


<?
$this->registerJs("
var active = 'active';
var hide = 'hide';

$('.data-category-select').on('click', '[data-id]', function () { 

    var id = $(this).attr('data-id');
    var item_name = $(this).text();
    var parent = $(this).parents('.data-category-select');
    var block_id = parent.attr('rel');
    var next_id = parent.attr('data-category-select');
    var clear = parent.attr('data-category-clear');
     
    parent.find('[data-id]').removeClass('active');
    $(this).addClass('active');
    
    $.each(JSON.parse(clear), function(index, val){
        $('.select-category__list-item[data-rel=\"'+val+'\"]').text('');
        $('.data-category-select[rel=\"'+val+'\"]').addClass(hide);
    });
    
    $('.select-category__list-item[data-rel=\"'+block_id+'\"]').html('<span>'+item_name+'</span>');
 
    $.get('{$url}'+id, {}, function(out){
        
        if(out === false || block_id == 4){ 
            $('.select-category__ads').addClass(hide);
            $('.select-category__list').addClass(active);
            $('#addadvform-category').val(id);
        }
        else{ 
            $('.data-category-select').removeClass('active');
            $('.data-category-select[rel=\"'+next_id+'\"]').addClass(active).removeClass(hide).html(out.data);
            $('#addadvform-category').val(0);
        }
        
    });
});
$('.select-category__list').click(function(){
    $(this).removeClass(active);
    $('.select-category__ads').removeClass(hide);
    $('#addadvform-category').val(0); 
});
$('.data-category-select').on('click', '.select-category__link-back', function(){ 
    var id = $(this).parents('.data-category-select').attr('rel');
    $('.data-category-select').removeClass('active');
    $('.data-category-select[rel='+(id - 1)+']').addClass('active');
    $('#addadvform-category').val(0); 
    $('.select-category__list-item[data-rel=\"'+id+'\"]').text('');
});

");
?>
