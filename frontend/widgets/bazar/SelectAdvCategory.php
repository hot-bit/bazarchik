<?php

/**
 * Created by PhpStorm.
 * User: York
 * Date: 28.09.2017
 * Time: 13:05
 */

namespace frontend\widgets\bazar;


use common\models\Categories;

class SelectAdvCategory extends \yii\base\Widget
{

    /* @var $model \frontend\models\AddAdvForm */
    public $model;

    /* @var $categories Categories[] */
    public $categories;

    public function init(){
        $this->categories = Categories::find()->where('parent = 0')->all();

    }

    public function run(){

        return $this->render('select_adv_category', [
            'model' => $this->model,
            'categories' => $this->categories,
        ]);

    }


}