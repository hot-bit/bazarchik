<?php
/**
 * Created by PhpStorm.
 * User: York
 * Date: 01.02.2018
 * Time: 15:51
 */

namespace frontend\widgets;


use common\models\User;
use yii\base\Widget;

class Shop extends Widget
{
    public $count = 3;
    public $text = true;

    public function run()
    {
        $model = User::find()->where(['is_business' => User::BUSINESS_ACTIVE])->orderBy('RAND()')->limit(3)->all();
        return $this->render('shop', compact('model'));
    }

}