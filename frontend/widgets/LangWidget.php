<?php
/**
 * Created by PhpStorm.
 * User: phpCreator
 * Date: 19.08.2017
 * Time: 22:15
 */

namespace frontend\widgets;

use yii\base\Widget;
use common\models\Lang;

class LangWidget extends Widget{

    public function init(){}

    public function run(){
        return $this->render('lang/view', [
            'current' => Lang::getCurrent(),
            'langs' => Lang::find()->all()
        ]);
    }

}