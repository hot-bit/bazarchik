<?php
/**
 * Created by PhpStorm.
 * User: York
 * Date: 19.02.2018
 * Time: 14:12
 */

namespace frontend\widgets;


use yii\base\Widget;
use yii\helpers\Html;
use yii\helpers\Url;

class Favorite extends Widget{

    public $item_id;
    public $is_active;
    public $text = false;
    public $class = false;

    public function run()
    {
        parent::run();
        $params = [
            'item_id' => $this->item_id,
            'is_active' => (int)$this->is_active,
            '@fav_inc' => 'incrementFavorite',
            '@fav_dec' => 'decrementFavorite',
            'endpoint' => Url::to(['ajax/favorite-change-active'])
        ];
        if($this->text) $params['text'] = $this->text;
        if($this->class) $params['class'] = $this->class;

        return Html::tag('favorite', '', $params);
    }

}