<?php
/**
 * Created by PhpStorm.
 * User: phpCreator
 * Date: 19.08.2017
 * Time: 22:21
 */

use yii\helpers\Html;
?>
<script>
    var route = '<?= Yii::$app->controller->route; ?>';
    if(route == 'site/index'){
        route = '';
    }
    var homeUrl = '<?= substr(Yii::$app->urlManager->createAbsoluteUrl('/'), 0, -2) ?>';
</script>
<div class="header-language">
    <form action="" class="select-language">
        <select id="lang">
            <? foreach ($langs as $lang): ?>
                <option value="<?= $lang->url?>" <?= ($lang->url == $current->url) ? 'selected="selected"' : null ?>><?= strtoupper($lang->url); ?></option>
            <? endforeach; ?>
        </select>
    </form>
</div>
<?php
$js = <<<EOF

$(document).ready(function(){
    $('#lang').change(function(){
        var lang = $('#lang').val();
        window.location.replace(homeUrl+lang+'/'+route);
    });
});

EOF;

$this->registerJs($js);
?>

