<?php
/**
 * Created by PhpStorm.
 * User: York
 * Date: 19.02.2018
 * Time: 20:30
 * 
 * @var $model \common\models\User[]
 */

?>


<div class="partner-content">
    <?foreach($model as $item):?>
        <div class="partner__container  partner__container__image">
            <div class="partner-img">
                <img src="<?= $item->path?>" alt="" style="width: 100%">
            </div>
        </div>
    <?endforeach?>

    <div class="partner__container  partner__container__href">
        <a href="#">
            <?=Yii::t('app', 'Learn more about the Cyprus BAZAR Partner Program');?>
        </a>
    </div>
</div>
