<?php
use yii\web\Request;

$params = array_merge(
    require(__DIR__ . '/../../common/config/params.php'),
    require(__DIR__ . '/../../common/config/params-local.php'),
    require(__DIR__ . '/params.php'),
    require(__DIR__ . '/params-local.php')
);

$baseUrl = str_replace('/frontend/web', '', (new Request)->getBaseUrl());
//$baseUrl = $baseUrl == '/' ? '/en/' : $baseUrl;
return [
	'id' => 'Cyprus BAZAR',
    'language' => 'en-US',
    'basePath' => dirname(__DIR__),
    'bootstrap' => ['log'],
    'modules' => [],
    'controllerNamespace' => 'frontend\controllers',
    'aliases' => [
        '@dist' => '/dist',
        '@npm' => 'node_modules',
    ],
    'components' => [
        'paypal'=> [
            'class'        => 'cinghie\paypal\components\Paypal',
            'clientId'     => 'gm.bazar.cy_api1.gmail.com',
            'clientSecret' => 'AFcWxV21C7fd0v3bYYYRCpSSRl31Ajr0gF5X44O-Q43SLOgU7iYcZbQn',
            'isProduction' => false,
            // This is config file for the PayPal system
            'config'       => [
                'mode' => 'sandbox', // development (sandbox) or production (live) mode
            ]
        ],
        'payPalClassic'    => [
            'class'        => 'kun391\paypal\ClassicAPI',
            'pathFileConfig' => '',

        ],
        'payPalRest'               => [
            'class'        => 'kun391\paypal\RestAPI',
            'pathFileConfig' => '',
            'successUrl' => '',
            'cancelUrl' => '',
        ],
        'favorite' => [
            'class' => 'frontend\components\Favorite',
        ],
        'breadcrumbs' => [
            'class' => 'frontend\components\Breadcrumbs',
        ],
        'seo' => [
            'class' => 'frontend\components\Seo',
            'default_image' => '/dist/img/noimage.jpg',
            'default_title' => 'Cyprus Bazar',
            'default_h1' => '',
            'default_description' => '',
            'default_keywords' => '',
        ],
        'request' => [
            'baseUrl' => $baseUrl,
            'enableCsrfValidation' => false,
            'csrfParam' => '_csrf-frontend',
            'class' => 'frontend\components\LangRequest'
        ],
        'user' => [
            'identityClass' => 'common\models\User',
            'enableAutoLogin' => true,
            'identityCookie' => ['name' => '_identity-frontend', 'httpOnly' => true],
        ],
        'session' => [
            // this is the name of the session cookie used for login on the frontend
            'name' => 'advanced-frontend',
        ],
        'log' => [
            'traceLevel' => YII_DEBUG ? 3 : 0,
            'targets' => [
                [
                    'class' => 'yii\log\FileTarget',
                    'levels' => ['error', 'warning'],
                ],
            ],
        ],
        'errorHandler' => [
            'errorAction' => 'site/error',
        ],
        'urlManager' => [
            'baseUrl' => $baseUrl,
            'enablePrettyUrl' => true,
            'suffix' => '/',
            'showScriptName' => false,
            'class' => 'frontend\components\LangUrlManager',
            'rules' => [
                'add' => 'adv/add',
                'catalog/<cat:[\w-]+>/p-<page:\d+>' => 'adv/index',
                'catalog/p-<page:\d+>' => 'adv/index',
                'catalog/<cat:[\w-]+>' => 'adv/index',
                'catalog' => 'adv/index',
                'search/<cat:[\w-]+>' => 'adv/search',
                'search' => 'adv/search',
                'login-soc' => 'site/login-soc',
                'login' => 'site/login',

                'news/p-<page:\d+>' => 'news/index',
                'news/<url:[\w-]+>-<id:\d+>' => 'news/get',
                'news' => 'news/index',

                'page/<url:[\w-]+>' => 'page/get',

                'adv/<url:[^_]+>_<id:\d+>' => 'adv/get',
                'faq' => 'faq/index',
                'faq/<url:(.*)>' => 'faq/get',
                'shop/write-to-<shop:(.*)>' => 'shop/write-to-owner',
                'shop-<url:(.*)>' => 'shop/get',
                '' => 'site/index',

//                '<controller:\w+>/<action:\w+>' => '<controller>/<action>'
            ],
        ],
    ],
    'params' => $params,
];
