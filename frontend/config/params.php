<?php
return [
    'noimage' => '/img/noimage.jpg',
    'ios_app' => 'https://itunes.apple.com/cy/app/cyprus-bazar/id1225933434?mt=8',
    'android_app' => 'https://play.google.com/store/apps/details?id=ru.kingbird.cyprusbazar',
    'fb_link' => 'https://www.facebook.com/bazarcy/',
    'g_link' => 'http://plus.google.com/+Bazarcyprus',
    'service_price' => [
        'vip' => 5,
        'prem' => 4,
        'color' => 2,
        'up' => 1,
    ],
    'days_for_free_rise' => 14,
    'vip_period' => 7,
    'prem_period' => 7,
    'color_period' => 7,
    'remove_in_day' => 7,


    'adv_top_params' => [
        'brand', 'model', 'body', // cars
        'area_id', // property
    ],
];
