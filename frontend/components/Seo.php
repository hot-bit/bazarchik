<?php
/**
 * Created by PhpStorm.
 * User: York
 * Date: 26.09.2017
 * Time: 22:58
 */

namespace frontend\components;


use common\models\Adv;
use common\models\CMS;
use common\models\Lang;
use Yii;
use yii\base\Component;
use yii\helpers\Html;

/**
 *
 * @property string $description
 * @property string $keywords
 * @property string $text_bottom
 * @property string $h1
 * @property string $image
 * @property string $text_top
 * @property string $canonical
 * @property \common\models\Adv $adv
 * @property string $head
 * @property string $title
 */
class Seo extends Component {

    public $default_title;
    public $default_keywords;
    public $default_description;
    public $default_image;
    public $default_text_bottom = '';
    public $default_text_top = '';
    public $default_h1 = '';
    public $canonicalLang = 'en';

    private $model;
    public function init()
    {
        parent::init();
        $this->canonicalLang = substr(Yii::$app->language, 0, 2);
        $this->SetModel();
    }


    public function getTitle(){
        if(!$this->model) return $this->default_title;
        return $this->model->data[CMS::field('title_')];
    }

    public function getKeywords(){
        if(!$this->model) return $this->default_keywords;
        return $this->model->data[CMS::field('keywords_')];
    }

    public function getDescription(){
        if(!$this->model) return $this->default_description;
        return $this->model->data[CMS::field('description_')];
    }

    public function getH1(){
        if(!$this->model) return $this->default_h1;
        return $this->model->data[CMS::field('h1_')];
    }

    public function getText_bottom(){
        if(!$this->model) return $this->default_text_bottom;
        return $this->model->data[CMS::field('text_bot_')];
    }

    public function getText_top(){
        if(!$this->model) return $this->default_text_top;
        return $this->model->data[CMS::field('text_top_')];
    }
    public function getImage(){
        return $this->default_image;
    }

    private function SetModel(){

        if($_SERVER['REQUEST_URI'] == '/')
            $this->model = \common\models\Seo::find()
                ->where(['url' => '/'])->one();
        else{
            $req = substr($_SERVER['REQUEST_URI'], 3);
            $this->model = \common\models\Seo::find()
                ->where(['url' => $req])->one();
        }
    }


    public function setAdv(Adv $model){
        $this->default_title = $model->title;
        $this->default_description = $model->annonce;
        $this->default_image = $model->main_image;
        if($model->lng)
            $this->canonicalLang = $model->lng;
    }

    public function getHead(){
        $ret = '<!-- Seo data -->';
        $ret .= '<title>'.Html::encode($this->title).'| Cyprus BAZAR</title>';
        $ret .= '<meta name="description" content="'.$this->description.'">';
        $ret .= '<meta name="keywords" content="'.$this->keywords.'">';
        $ret .= '<meta property="fb:app_id" content="998179396929762" />';
        $ret .= '<meta property="fb:admins" content="100012210429093" />';
        $ret .= '<meta property="og:site_name" content="CYPRUS BAZAR"/>';
        $ret .= '<meta property="og:type" content="website"/>';
        $ret .= '<meta property="og:title" content="'.$this->title.'"/>';
        $ret .= '<meta property="og:description" content="'.$this->description.'"/>';
        $ret .= '<meta property="og:image" content="https://www.bazar-cy.com'.$this->image.'"/>';
        $ret .= '<meta property="og:image:type" content="image/jpeg"/>';
        $ret .= '<meta property="og:url" content="https://www.bazar-cy.com'.$_SERVER['REQUEST_URI'].'"/>';
        $ret .= $this->canonical;
        $ret .= '<!-- Seo data END -->';
        return $ret;
    }

    public function getCanonical(){
        $link = substr($_SERVER['REQUEST_URI'], 3);
        $ret = '<link rel="canonical" href="https://www.bazar-cy.com/'.$this->canonicalLang.$link.'" />';

        foreach (Lang::find()->all() as $lng){
            if($lng->url == $this->canonicalLang) continue;
            $ret .= '<link rel="alternate" href="https://www.bazar-cy.com/'.$lng->url.$link.'" hreflang="'.$lng->url.'" />';
        }

        return $ret;
    }

}
