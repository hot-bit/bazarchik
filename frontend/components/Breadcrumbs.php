<?php
/**
 * Created by PhpStorm.
 * User: York
 * Date: 31.10.2017
 * Time: 19:16
 */

namespace frontend\components;


use common\models\CMS;
use yii\base\Component;
use yii\helpers\Html;
use yii\helpers\Url;

class Breadcrumbs extends Component{

    private $items = [];


    public function Items($items){
        $this->items = $items;
    }

    public function run(){


        $data = [];

        foreach ($this->items as $k => $item){
            if(is_numeric($k))
                $data[] = $item;
            else
                $data[] = [
                    'url' =>  $k,
                    'label' => $item
                ];
        }


        $ret = Html::beginTag('section', ['class' => 'breadcrumb-wrapper']);

        $ret .= \frontend\widgets\Breadcrumbs::widget([
            'links' => $data,
        ]);

        $ret .= Html::endTag('section');

        return $ret;
    }

}