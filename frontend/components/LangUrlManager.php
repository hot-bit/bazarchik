<?php
/**
 * Created by PhpStorm.
 * User: phpCreator
 * Date: 19.08.2017
 * Time: 21:42
 */

namespace frontend\components;

use yii\web\UrlManager;
use common\models\Lang;

class LangUrlManager extends UrlManager{

    public function createUrl($params){
        if(isset($params['lang_id'])){
            $lang = Lang::findOne($params['lang_id']);
            if($lang === null){
                $lang = Lang::getDefaultLang();
            }
            unset($params);
        }
        else{
            $lang = \Yii::$app->request->lngCode;
        }

        $url = parent::createUrl($params);

        if($url == '/'){
            return '/'.$lang;
        }
        else{
            return '/'.$lang.$url;
        }
    }

}