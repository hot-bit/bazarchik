<?php
/**
 * Created by PhpStorm.
 * User: York
 * Date: 27.09.2017
 * Time: 15:34
 */

namespace frontend\components;


use Yii;
use yii\base\Component;

/**
 *
 * @property int $count
 */
class Favorite extends Component
{

    private $items;
    private $cookie_name = 'favorite_ads';

    public function init(){
        if(!Yii::$app->user->isGuest)
            $this->items = \common\models\Favorite::findByUser();
        else
            $this->findByCookie();


        parent::init();
    }

    private function add($id){
        if(Yii::$app->user->isGuest){
            $this->items[$id] = $id;
            $this->save();
        }
        else{
            \common\models\Favorite::Add($id);
        }
    }

    private function remove($id){
        if(Yii::$app->user->isGuest){
            unset($this->items[$id]);
            $this->save();
        }
        else{
            \common\models\Favorite::Remove($id);
        }
    }

    public function save(){
        $cookies = Yii::$app->response->cookies;
        $cookies->add(new \yii\web\Cookie([
            'name' => $this->cookie_name,
            'value' => serialize($this->items),
            'expire' => time() + 3600*24*30
        ]));
    }

    public function change($id){
        if(isset($this->items[$id])) $this->remove($id);
        else $this->add($id);
    }

    public function isActive($id){

        if(isset($this->items[$id]))
            return true;

        return false;
    }

    public function getCount(){
        return count($this->items) == 0 ? '' : count($this->items);
    }

    private function findByCookie(){
        $cookies = Yii::$app->request->cookies;
        $items = $cookies->getValue($this->cookie_name);
        $this->items = [];
        if($items) $this->items = unserialize($items);
    }

    public function items(){
        return $this->items;
    }
}