<?php
/**
 * Created by PhpStorm.
 * User: York
 * Date: 12.02.2018
 * Time: 11:05
 */

namespace console\controllers;


use common\models\Adv;
use common\models\Seo;
use common\models\User;
use yii\console\Controller;

class ParserController extends Controller
{
    public function actionPhone(){
        echo "Parse phone \n";
        $user = User::find()->where(['phone_code' => null])->andWhere(['<>', 'phone', ''])->all();

        foreach ($user as $item){
            $phone = preg_replace('([^\d]+)', '', $item->phone);
            $phone_length = strlen($phone);

            if($phone_length > 8)
                $new_phone = substr($phone, $phone_length - 8);
            else
                $new_phone = $phone;

            $item->phone_code = '357';
            $item->phone = $new_phone;
            $item->save();
        }
        echo "Parse finish \n";
    }

    public function actionAdv(){
        echo "Parse ads \n";
        $ads = Adv::find()->all();
        foreach ($ads as $item){
            $phone = preg_replace('([^\d]+)', '', $item->phone);
            $phone_length = strlen($phone);

            if($phone_length > 8)
                $new_phone = substr($phone, $phone_length - 8);
            else
                $new_phone = $phone;

            $item->phone_code = '357';
            $item->phone = $new_phone;
            $item->save();
        }
        echo "Parse finish \n";
    }

    public function actionSeo(){
        echo "Parse seo \n";

        $model = Seo::find()->all();
        foreach ($model as $item){
            if(strpos($item->url, 'category')){
                $item->url = str_replace('/category/', '/catalog/', $item->url);
                $item->information = str_replace('/category/', '/catalog/', $item->information);
                $item->save();
            }
        }

        echo "Parse finish \n";
    }
}