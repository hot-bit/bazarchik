<?php
/**
 * Created by PhpStorm.
 * User: York
 * Date: 27.03.2018
 * Time: 13:56
 */

namespace console\controllers;


use common\models\Adv;
use common\models\AdvCounter;
use yii\console\Controller;

class CronController extends Controller
{
    public function actionDaily(){
        $this->actionResetAdvCounter();
    }

    public function actionCategoryCount(){
        $model = Adv::find()->where(['status' => Adv::STATUS_ACTIVE])->all();


        foreach ($model as $item){

        }
    }

    public function actionResetAdvCounter(){
        \Yii::$app->db->createCommand("UPDATE adv_counter SET today = 0")->execute();
    }

}