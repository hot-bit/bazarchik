<?php

/**
 * Created by PhpStorm.
 * User: York
 * Date: 01.10.2017
 * Time: 22:26
 */

namespace console\controllers;

use common\models\User;
use Yii;
use yii\console\Controller;

class InstallController extends Controller
{

    /**
     * @var $auth yii\rbac\DbManager
     */
    private $auth;

    public function actionIndex(){
        $this->stdout("Welcome to Bazar CMS installation\n\n");
        $this->rbac();
    }

    public function actionUser()
    {
        $name = $this->prompt("Your name: ");
        $email = $this->prompt("Your email: ");
        $password = $this->prompt("Your password: ");

        $user = new User();
        $user->status = User::STATUS_ACTIVE;
        $user->name = $name;
        $user->email = $email;
        $user->setPassword($password);
        $user->generateAuthKey();
        $user->no_save;
        if (!$user->save()) {
            var_dump($user->getErrors());
            die();
        }

        $auth = \Yii::$app->authManager;
        $role = $auth->getRole('admin');

        $auth->revokeAll($user->id);
        $auth->assign($role, $user->id);

    }

    public function rbac(){


        $this->stdout("Create default users permissions and roles \n\n");

        /* Create users */
        // Admin


        $permissions = [
            'ADMIN_PANEL' => 'Enter to admin panel',
            'PROFILE' => 'Enter to profile',
            'PAGE' => 'Show pages',
            'PAGE_CREATE' => 'Create page',
            'PAGE_UPDATE' => 'Update page',
            'PAGE_DELETE' => 'Delete page',
            'NEWS' => 'Show news',
            'NEWS_CREATE' => 'Create news',
            'NEWS_UPDATE' => 'Update news',
            'NEWS_DELETE' => 'Delete news',
            'LANGUAGE_CRUD' => 'Languages',
            'USER_CRUD' => 'Show Users',
            'SEO_CRUD' => 'Show Seo',
            'CATEGORY_CRUD' => 'Show Category',
            'BLOCK_CRUD' => 'Show Text blocks',
            'ROLE_CRUD' => 'Show Roles',
            'ORDERS_CRUD' => 'Show Orders',
            'ADV' => 'Show Ads',
            'ADV_UPDATE' => 'Update ads',
            'SHOP' => 'Moderate shops',
            'BANNERS' => 'Moderate baneners',
            'TRANSLATES' => 'Translates',
            'ORDERS' => 'Orders',

        ];

        $roles = [
            'admin' => 'all',
            'moderator' => ['ADMIN_PANEL', 'PAGE_CREATE', 'PAGE_UPDATE', 'PAGE', 'NEWS', 'NEWS_CREATE', 'NEWS_UPDATE', 'PROFILE', 'ADV', 'ADV_UPDATE', 'ORDERS_CRUD'],
            'user' => ['PROFILE'],
        ];


        $this->auth = Yii::$app->authManager;
        $this->auth->removeAll();


        $perm_data = [];
        foreach ($permissions as $name => $val){
            $perm_data[$name] = $this->makePerm($name, $val);
        }

        $roles_data = [];
        foreach ($roles as $role_name => $perms){
            $role = $this->auth->createRole($role_name);
            $roles_data[$role_name] = $role;
            $this->auth->add($role);
            if($perms == 'all'){
                foreach ($perm_data as $key => $val){
                    $this->auth->addChild($role, $val);
                }
            }
            else{
                foreach ($perms as $val){
                    $this->auth->addChild($role, $perm_data[$val]);
                }
            }
        }

        /* Set permition */
        $this->auth->assign($roles_data['admin'], 7);
        $this->auth->assign($roles_data['admin'], 118);
        $this->auth->assign($roles_data['admin'], 119);
        $this->auth->assign($roles_data['admin'], 145);
        $this->auth->assign($roles_data['admin'], 125);
        $this->auth->assign($roles_data['admin'], 146);

        return true;
    }

    private function makePerm($name, $desc){
        $item = $this->auth->createPermission($name);
        $item->description = $desc;
        $this->auth->add($item);
        return $item;
    }
}