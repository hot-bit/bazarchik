<?php

use yii\db\Migration;

class m170928_044259_alter_adv extends Migration
{
    public function safeUp()
    {

        $this->addColumn('adv', 'step', 'INT(11) NULL DEFAULT NULL');
        $this->addColumn('adv', 'free_rise', $this->integer());

        $this->alterColumn('adv', 'category', 'INT(11) NULL DEFAULT NULL');
        $this->alterColumn('adv', 'area', 'INT(11) NULL DEFAULT NULL');
        $this->alterColumn('adv', 'city', 'INT(11) NULL DEFAULT NULL');
        $this->alterColumn('adv', 'phone', $this->string(255)->notNull());
        $this->alterColumn('adv', 'user', 'INT(11) NULL DEFAULT NULL');
        $this->alterColumn('adv', 'views', 'INT(11) NULL DEFAULT NULL');
        $this->alterColumn('adv', 'premium', $this->boolean());
        $this->alterColumn('adv', 'vip', $this->boolean());
        $this->alterColumn('adv', 'color', $this->boolean());
        $this->alterColumn('adv', 'vip_start', 'INT(11) NULL DEFAULT NULL');
        $this->alterColumn('adv', 'prem_start', 'INT(11) NULL DEFAULT NULL');
        $this->alterColumn('adv', 'color_start', 'INT(11) NULL DEFAULT NULL');
        $this->alterColumn('adv', 'up_start', 'INT(11) NULL DEFAULT NULL');
        $this->alterColumn('adv', 'owner', 'INT(11) NULL DEFAULT NULL');
        $this->alterColumn('adv', 'from_site', 'INT(11) NULL DEFAULT NULL');
        $this->alterColumn('adv', 'ppg_type', 'INT(11) NULL DEFAULT NULL');
        $this->alterColumn('adv', 'from_type', 'INT(11) NULL DEFAULT NULL');
        $this->alterColumn('adv', 'not_email', $this->boolean());
        $this->alterColumn('adv', 'not_skype', $this->boolean());
        $this->alterColumn('adv', 'show_cont', $this->boolean());
        $this->alterColumn('adv', 'show_map', $this->boolean());
        $this->alterColumn('adv', 'show_website', $this->boolean());
        $this->alterColumn('adv', 'upd', 'INT(11) NULL DEFAULT NULL');
        $this->alterColumn('adv', 'is_new', $this->boolean());
        $this->alterColumn('adv', 'notice', $this->boolean());
        $this->alterColumn('adv', 'notice_time', 'INT(11) NULL DEFAULT NULL');
    }

    public function safeDown()
    {
        $this->dropColumn('adv', 'step');
        $this->dropColumn('adv', 'free_rise');
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m170928_044259_alter_adv cannot be reverted.\n";

        return false;
    }
    */
}
