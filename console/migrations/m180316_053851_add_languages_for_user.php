<?php

use yii\db\Migration;

/**
 * Class m180316_053851_add_languages_for_user
 */
class m180316_053851_add_languages_for_user extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('users', 'langs', $this->text());
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m180316_053851_add_languages_for_user cannot be reverted.\n";

    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m180316_053851_add_languages_for_user cannot be reverted.\n";

        return false;
    }
    */
}
