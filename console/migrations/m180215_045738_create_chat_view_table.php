<?php

use yii\db\Migration;

/**
 * Handles the creation of table `chat_view`.
 */
class m180215_045738_create_chat_view_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->createTable('chat_view', [
            'id' => $this->primaryKey(),
            'user_id' => $this->integer(),
            'created_at' => $this->integer(),
            'updated_at' => $this->integer(),
            'is_viewed' => $this->boolean(),
            'message_id' => $this->integer(),
            'chat_id' => $this->integer(),
        ]);

        $this->addForeignKey(
            'idx-chat_view-user_id',
            'chat_view',
            'user_id',
            'users',
            'id'
        );
        $this->addForeignKey(
            'idx-chat_view-chat_id',
            'chat_view',
            'chat_id',
            'chat',
            'id'
        );
        $this->addForeignKey(
            'idx-chat_view-message_id',
            'chat_view',
            'message_id',
            'chat_message',
            'id'
        );
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropTable('chat_view');
    }
}
