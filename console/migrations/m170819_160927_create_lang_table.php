<?php

use yii\db\Migration;

/**
 * Handles the creation of table `lang`.
 */
class m170819_160927_create_lang_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->createTable('lang', [
            'id' => $this->primaryKey(),
            'url' => $this->string(255)->notNull(),
            'local' => $this->string(255)->notNull(),
            'name' => $this->string(255)->notNull(),
            'default' => $this->integer(6)->notNull()->defaultValue(0),
            'date_update' => $this->integer(11)->notNull(),
            'date_create' => $this->integer(11)->notNull(),
        ]);

        $this->batchInsert('lang', ['url', 'local', 'name', 'default', 'date_update', 'date_create'], [
            ['en', 'en-EN', 'English', 1, time(), time()],
            ['gr', 'el-GR', 'Greek', 0, time(), time()],
            ['ru', 'ru-RU', 'Русский', 0, time(), time()],
        ]);
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropTable('lang');
    }
}
