<?php

use yii\db\Migration;

/**
 * Handles the creation of table `chat_message`.
 */
class m180213_093508_create_chat_message_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->createTable('chat_message', [
            'id' => $this->primaryKey(),
            'chat_id' => $this->integer(),
            'created_at' => $this->integer(),
            'updated_at' => $this->integer(),
            'image' => $this->string(),
            'message' => $this->string(),
            'user_id' => $this->integer(),
        ]);

        $this->addForeignKey(
            'idx-chat_message-user_id',
            'chat_message',
            'user_id',
            'users',
            'id'
        );

        $this->addForeignKey(
            'idx-chat_message-chat_id',
            'chat_message',
            'chat_id',
            'chat',
            'id'
        );
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropTable('chat_message');
    }
}
