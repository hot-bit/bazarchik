<?php

use yii\db\Migration;

/**
 * Handles the creation of table `adv_chat`.
 */
class m171114_070401_create_adv_chat_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            // http://stackoverflow.com/questions/766809/whats-the-difference-between-utf8-general-ci-and-utf8-unicode-ci
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }
        $this->createTable('{{%adv_chat}}', [
            'id' => $this->primaryKey(),
            'user_1' => $this->integer(),
            'user_1_email' => $this->string(255),
            'user_1_name' => $this->string(255),
            'user_2' => $this->integer(),
            'created_at' => $this->integer(),
            'updated_at' => $this->integer(),
        ], $tableOptions);
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropTable('{{%adv_chat}}');
    }
}
