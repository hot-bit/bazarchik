<?php

use yii\db\Migration;

class m170914_094837_search_request extends Migration
{
    public function safeUp()
    {
        $this->createTable('search_request', [
            'id' => $this->primaryKey(),
            'request' => $this->string(255),
            'count' => $this->integer(),
            'ignore' => $this->boolean()
        ]);
    }

    public function safeDown()
    {
        $this->dropTable('search_request');
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m170914_094837_search_request cannot be reverted.\n";

        return false;
    }
    */
}
