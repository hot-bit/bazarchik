<?php

use yii\db\Migration;

class m171030_023335_alter_user extends Migration
{
    public function safeUp(){
        $this->addColumn('users', 'phone_code', $this->integer());
        $this->addColumn('adv', 'phone_code', $this->integer());
    }

    public function safeDown()
    {
        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.clients_category
    public function up()
    {

    }

    public function down()
    {
        echo "m171030_023335_user_alter_table cannot be reverted.\n";

        return false;
    }
    */
}
