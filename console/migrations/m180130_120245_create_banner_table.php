<?php

use yii\db\Migration;

/**
 * Handles the creation of table `banner`.
 */
class m180130_120245_create_banner_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $this->createTable('banner', [
            'id' => $this->primaryKey(),
            'created_at' => $this->integer(),
            'updated_at' => $this->integer(),
            'start_date' => $this->integer(),
            'finish_date' => $this->integer(),
            'place' => $this->integer(),
            'size' => $this->integer(),
            'ru' => $this->boolean(),
            'en' => $this->boolean(),
            'gr' => $this->boolean(),
            'status' => $this->boolean(),
            'name' => $this->string(),
            'link' => $this->string(),
            'email' => $this->string(),
            'phone' => $this->string(),
            'image' => $this->string(),
        ]);
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        $this->dropTable('banner');
    }
}
