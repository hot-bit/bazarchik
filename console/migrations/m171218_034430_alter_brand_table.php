<?php

use yii\db\Migration;

/**
 * Class m171218_034430_alter_brand_table
 */
class m171218_034430_alter_brand_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $this->addColumn('{{%brand}}', 'url', $this->string(255));
        $this->addColumn('{{%moto_brand}}', 'url', $this->string(255));
        $this->addColumn('{{%model}}', 'url', $this->string(255));
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        $this->dropColumn('{{%brand}}', 'url');
        $this->dropColumn('{{%moto_brand}}', 'url');
        $this->dropColumn('{{%model}}', 'url');
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m171218_034430_alter_brand_table cannot be reverted.\n";

        return false;
    }
    */
}
