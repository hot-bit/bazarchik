<?php

use yii\db\Migration;

class m171114_073503_alter_reports extends Migration
{
    public function safeUp()
    {
        $this->addColumn('reports', 'status', $this->integer());

    }

    public function safeDown()
    {
        echo "m171114_073503_alter_reports cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m171114_073503_alter_reports cannot be reverted.\n";

        return false;
    }
    */
}
