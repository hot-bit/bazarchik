<?php

use yii\db\Migration;

/**
 * Handles the creation of table `chat`.
 */
class m180213_092637_create_chat_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->createTable('chat', [
            'id' => $this->primaryKey(),
            'user_1' => $this->integer(),
            'user_2' => $this->integer(),
            'created_at' => $this->integer(),
            'updated_at' => $this->integer(),
        ]);

        $this->addForeignKey(
            'idx-chat-user_1',
            'chat',
            'user_1',
            'users',
            'id'
        );
        $this->addForeignKey(
            'idx-chat-user_2',
            'chat',
            'user_2',
            'users',
            'id'
        );
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropTable('chat');
    }
}
