<?php

use yii\db\Migration;

class m171027_091519_alter_adv_images extends Migration
{
    public function safeUp()
    {
        $this->addColumn('adv_images', 'uuid', $this->string(256));
    }

    public function safeDown()
    {
        echo "m171027_091519_alter_adv_images cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m171027_091519_alter_adv_images cannot be reverted.\n";

        return false;
    }
    */
}
