<?php

use yii\db\Migration;

class m170831_120637_url_route extends Migration
{

    // Use up()/down() to run migration code without a transaction.
    public function up()
    {
        $this->createTable('url_route', [
            'id'            => $this->primaryKey(),
            'url'           => $this->string('70'),
            'module_name'   => $this->string('70'),
            'module_id'     => $this->integer(),
            'lng_id'        => $this->integer(),
        ]);
    }

    public function down()
    {
        $this->dropTable('url_route');

        return false;
    }

}
