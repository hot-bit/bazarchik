<?php

use yii\db\Migration;

/**
 * Handles the creation of table `adv_message`.
 */
class m171114_085312_create_adv_message_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            // http://stackoverflow.com/questions/766809/whats-the-difference-between-utf8-general-ci-and-utf8-unicode-ci
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }
        $this->createTable('{{%adv_message}}', [
            'id' => $this->primaryKey(),
            'chat_id' => $this->integer(),
            'text' => $this->text(),
            'created_at' => $this->integer(),
            'updated_at' => $this->integer(),
            'user_num' => $this->integer(),
            'is_new' => $this->boolean(),
            'adv_id' => $this->integer(),
        ], $tableOptions);

        $this->addForeignKey(
            'idx-adv_message-chat_id',
            '{{%adv_message}}',
            'chat_id',
            '{{%adv_chat}}',
            'id'
        );

    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropTable('{{%adv_message}}');
    }
}
