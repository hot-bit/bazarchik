<?php

use yii\db\Migration;

/**
 * Handles the creation of table `translate`.
 */
class m171202_051651_create_translate_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            // http://stackoverflow.com/questions/766809/whats-the-difference-between-utf8-general-ci-and-utf8-unicode-ci
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }
        $this->createTable('{{%translate}}', [
            'id' => $this->primaryKey(),
            'category' => $this->string(),
            'message' => $this->string(),
            'language_id' => $this->integer(),
            'value' => $this->string()
        ], $tableOptions);

    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropTable('{{%translate}}');
    }
}
