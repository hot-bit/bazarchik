<?php

use yii\db\Migration;

/**
 * Handles the creation of table `phone_code_clients`.
 */
class m180219_081802_phone_code_in_clients extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('clients', 'phone_code', $this->integer());
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
    }
}
