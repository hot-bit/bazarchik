<?php

use yii\db\Migration;

class m170820_132033_alter_users_table extends Migration
{
    public function safeUp()
    {
        $this->addColumn('users', 'auth_key', $this->string(255)->after('business_link'));
    }

    public function safeDown()
    {
        $this->dropColumn('users', 'auth_key');
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m170820_132033_alter_users_table cannot be reverted.\n";

        return false;
    }
    */
}
