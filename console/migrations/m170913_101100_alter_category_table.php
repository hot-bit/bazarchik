<?php

use yii\db\Migration;

class m170913_101100_alter_category_table extends Migration
{
    public function safeUp()
    {
        $this->addColumn('categories', 'icon', $this->string(30)->after('status'));
        $this->addColumn('categories', 'color', $this->string(30)->after('status'));


        $this->update('categories', ['icon' => 'car',       'color' => ''], 'id = 1');
        $this->update('categories', ['icon' => 'office',    'color' => 'green-icon'], 'id = 2');
        $this->update('categories', ['icon' => 'briefcase', 'color' => 'red-icon'], 'id = 3');
        $this->update('categories', ['icon' => 'wrench',    'color' => 'red-icon'], 'id = 4');
        $this->update('categories', ['icon' => 'tshirt',    'color' => 'green-icon'], 'id = 5');
        $this->update('categories', ['icon' => 'home',      'color' => 'green-icon'], 'id = 6');
        $this->update('categories', ['icon' => 'iphone',    'color' => ''], 'id = 7');
        $this->update('categories', ['icon' => 'basketball','color' => 'green-icon'], 'id = 8');
        $this->update('categories', ['icon' => 'paw',       'color' => 'green-icon'], 'id = 9');
        $this->update('categories', ['icon' => 'business',  'color' => 'red-icon'], 'id = 10');
        $this->update('categories', ['icon' => 'binoculars','color' => 'gray-icon'], 'id = 753');
        $this->update('categories', ['icon' => 'gift',      'color' => 'gray-icon'], 'id = 752');
    }

    public function safeDown()
    {
        $this->dropColumn('categories', 'icon');
        $this->dropColumn('categories', 'color');
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m170913_101100_alter_category_table cannot be reverted.\n";

        return false;
    }
    */
}
