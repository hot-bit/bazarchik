<?php

use yii\db\Migration;

/**
 * Class m180102_123600_alter_users_table
 */
class m180102_123600_alter_users_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $this->addColumn('users', 'phone_try', $this->integer());
        $this->addColumn('users', 'password_reset_token', $this->string(64));
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        echo "m180102_123600_alter_users_table cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m180102_123600_alter_users_table cannot be reverted.\n";

        return false;
    }
    */
}
