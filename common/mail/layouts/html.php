<?php

/* @var $this \yii\web\View view component instance */
/* @var $message \yii\mail\MessageInterface the message being composed */
/* @var $content string main view render result */



$homeLink = Yii::$app->urlManager->createAbsoluteUrl(['site/index']);
?>
<?php $this->beginPage() ?>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
        "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" lang="en" xml:lang="en">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
    <meta name="viewport" content="width=device-width">
    <title>My Email Templates</title>
    <style>@media only screen {
            html {
                min-height: 100%;
                background: #fff
            }
        }

        @media only screen and (max-width: 550px) {
            table.body img {
                width: auto;
                height: auto
            }

            table.body center {
                min-width: 0 !important
            }

            table.body .container {
                width: 95% !important
            }

            table.body .columns {
                height: auto !important;
                -moz-box-sizing: border-box;
                -webkit-box-sizing: border-box;
                box-sizing: border-box;
                padding-left: 30px !important;
                padding-right: 30px !important
            }

            th.small-6 {
                display: inline-block !important;
                width: 50% !important
            }

            th.small-12 {
                display: inline-block !important;
                width: 100% !important
            }
        }

        @media only screen and (max-width: 550px) {
            th.small-12 {
                display: table-cell !important;
                width: auto !important
            }

            img {
                width: auto;
                height: auto
            }
        }</style>
</head>
<body style="-moz-box-sizing:border-box;-ms-text-size-adjust:100%;-webkit-box-sizing:border-box;-webkit-text-size-adjust:100%;Margin:0;box-sizing:border-box;color:#0a0a0a;font-family:HelveticaNeueCyr,Helvetica,sans-serif;font-size:15px;font-weight:400;line-height:1.2;margin:0;min-width:100%;padding:0;text-align:left;width:100%!important">
<span class="preheader"
      style="color:#fff;display:none!important;font-size:1px;line-height:1px;max-height:0;max-width:0;mso-hide:all!important;opacity:0;overflow:hidden;visibility:hidden"></span>
<table class="body"
       style="Margin:0;background:#fff;border-collapse:collapse;border-spacing:0;color:#0a0a0a;font-family:HelveticaNeueCyr,Helvetica,sans-serif;font-size:15px;font-weight:400;height:100%;line-height:1.2;margin:0;padding:0;text-align:left;vertical-align:top;width:100%">
    <tr style="padding:0;text-align:left;vertical-align:top">
        <td class="center" align="center" valign="top"
            style="-moz-hyphens:auto;-webkit-hyphens:auto;Margin:0;border-collapse:collapse!important;color:#0a0a0a;font-family:HelveticaNeueCyr,Helvetica,sans-serif;font-size:15px;font-weight:400;hyphens:auto;line-height:1.2;margin:0;padding:0;text-align:left;vertical-align:top;word-wrap:break-word">
            <center data-parsed="" style="min-width:520px;width:100%">

                <?= \common\models\Email::spacer(50, true)?>
                <table align="center" class="container mail float-center"
                       style="Margin:0 auto;background:#fff;border-collapse:collapse;border-spacing:0;box-shadow:0 1px 40px 0 rgba(255,126,0,.4);float:none;margin:0 auto;padding:0;text-align:center;vertical-align:top;width:520px">
                    <tbody>
                    <tr style="padding:0;text-align:left;vertical-align:top">
                        <td style="-moz-hyphens:auto;-webkit-hyphens:auto;Margin:0;border-collapse:collapse!important;color:#0a0a0a;font-family:HelveticaNeueCyr,Helvetica,sans-serif;font-size:15px;font-weight:400;hyphens:auto;line-height:1.2;margin:0;padding:0;text-align:left;vertical-align:top;word-wrap:break-word">

                            <?= \common\models\Email::spacer(30)?><!-- header -->
                            <table class="row"
                                   style="border-collapse:collapse;border-spacing:0;display:table;padding:0;position:relative;text-align:left;vertical-align:top;width:100%">
                                <tbody>
                                <tr style="padding:0;text-align:left;vertical-align:top">
                                    <th class="small-6 large-5 columns first"
                                        style="Margin:0 auto;color:#0a0a0a;font-family:HelveticaNeueCyr,Helvetica,sans-serif;font-size:15px;font-weight:400;line-height:1.2;margin:0 auto;padding:0;padding-bottom:0;padding-left:30px;padding-right:15px;text-align:left;width:186.67px">
                                        <table style="border-collapse:collapse;border-spacing:0;padding:0;text-align:left;vertical-align:top;width:100%">
                                            <tr style="padding:0;text-align:left;vertical-align:top">
                                                <th style="Margin:0;color:#0a0a0a;font-family:HelveticaNeueCyr,Helvetica,sans-serif;font-size:15px;font-weight:400;line-height:1.2;margin:0;padding:0;text-align:left">
                                                    <a href="<?= $homeLink?>">
                                                        <img src="https://www.bazar-cy.com/dist/img/logo.png" alt=""
                                                             style="-ms-interpolation-mode:bicubic;clear:both;display:block;max-width:100%;outline:0;text-decoration:none;width:auto">
                                                    </a>
                                                </th>
                                            </tr>
                                        </table>
                                    </th>
                                    <th class="info small-6 large-7 columns last"
                                        style="Margin:0 auto;color:#0a0a0a;font-family:HelveticaNeueCyr,Helvetica,sans-serif;font-size:15px;font-weight:400;line-height:1.2;margin:0 auto;padding:0;padding-bottom:0;padding-left:15px;padding-right:30px;text-align:left;width:273.33px">
                                        <table style="border-collapse:collapse;border-spacing:0;padding:0;text-align:left;vertical-align:top;width:100%">
                                            <tr style="padding:0;text-align:left;vertical-align:top">
                                                <th style="Margin:0;color:#0a0a0a;font-family:HelveticaNeueCyr,Helvetica,sans-serif;font-size:15px;font-weight:400;line-height:1.2;margin:0;padding:0;text-align:left">
                                                    <a href="<?= $homeLink?>" style="text-decoration: none">
                                                    <h6 class="info__subtitle"
                                                        style="Margin:0;Margin-bottom:0;color:#619007;font-family:HelveticaNeueCyr,Helvetica,sans-serif;font-size:16px;font-weight:700;line-height:1.2;margin:0;margin-bottom:0;padding:0;text-align:left;word-wrap:normal">
                                                            <?=Yii::t('app', 'Free ads');?>
                                                    </h6>
                                                    </a>
                                                    <p class="info__description"
                                                       style="Margin:0;Margin-bottom:0;color:#b8b8b8;font-family:HelveticaNeueCyr,Helvetica,sans-serif;font-size:15px;font-weight:400;line-height:1.2;margin:0;margin-bottom:0;padding:0;text-align:left">
<!--                                                        авто, недвижимость, работа, услуги, техника, одежда и многое-->
<!--                                                        другое-->
                                                    </p></th>
                                            </tr>
                                        </table>
                                    </th>
                                </tr>
                                </tbody>
                            </table>
                            <?= \common\models\Email::spacer(30)?><!-- Newsseller -->
                            <table class="row"
                                   style="border-collapse:collapse;border-spacing:0;display:table;padding:0;position:relative;text-align:left;vertical-align:top;width:100%">
                                <tbody>
                                <tr style="padding:0;text-align:left;vertical-align:top">
                                    <th class="info small-12 large-12 columns first last"
                                        style="Margin:0 auto;color:#0a0a0a;font-family:HelveticaNeueCyr,Helvetica,sans-serif;font-size:15px;font-weight:400;line-height:1.2;margin:0 auto;padding:0;padding-bottom:0;padding-left:30px;padding-right:30px;text-align:left;width:490px">
                                        <table style="border-collapse:collapse;border-spacing:0;padding:0;text-align:left;vertical-align:top;width:100%">
                                            <tr style="padding:0;text-align:left;vertical-align:top">
                                                <th style="Margin:0;color:#0a0a0a;font-family:HelveticaNeueCyr,Helvetica,sans-serif;font-size:15px;font-weight:400;line-height:1.2;margin:0;padding:0;text-align:left">

                                                    <?= $content?>
                                                </th>
                                            </tr>
                                        </table>
                                    </th>
                                </tr>
                                </tbody>
                            </table>

                            <table class="row"
                                   style="border-collapse:collapse;border-spacing:0;display:table;padding:0;position:relative;text-align:left;vertical-align:top;width:100%">
                                <tbody>
                                <tr style="padding:0;text-align:left;vertical-align:top">
                                    <th class="info small-12 large-12 columns first last"
                                        style="Margin:0 auto;color:#0a0a0a;font-family:HelveticaNeueCyr,Helvetica,sans-serif;font-size:15px;font-weight:400;line-height:1.2;margin:0 auto;padding:0;padding-bottom:0;padding-left:30px;padding-right:30px;text-align:left;width:490px">
                                        <table style="border-collapse:collapse;border-spacing:0;padding:0;text-align:left;vertical-align:top;width:100%">
                                            <tr style="padding:0;text-align:left;vertical-align:top">
                                                <th style="Margin:0;color:#0a0a0a;font-family:HelveticaNeueCyr,Helvetica,sans-serif;font-size:15px;font-weight:400;line-height:1.2;margin:0;padding:0;text-align:left">
                                                    <?= \common\models\Email::spacer(38)?>
                                                    <p style="Margin:0;Margin-bottom:0;color:#0a0a0a;font-family:HelveticaNeueCyr,Helvetica,sans-serif;font-size:15px;font-weight:400;line-height:1.2;margin:0;margin-bottom:0;padding:0;text-align:left">
                                                        <?=Yii::t('app', 'For more information, please contact');?> :</p>
                                                    <?= \common\models\Email::spacer(25)?>
                                                    <h6 class="info__subtitle"
                                                        style="Margin:0;Margin-bottom:0;color:#619007;font-family:HelveticaNeueCyr,Helvetica,sans-serif;font-size:16px;font-weight:700;line-height:1.2;margin:0;margin-bottom:0;padding:0;text-align:left;word-wrap:normal">
<!--                                                        Анастасия Савинова-->
                                                    </h6>
                                                    <p class="info__description"
                                                       style="Margin:0;Margin-bottom:0;color:#b8b8b8;font-family:HelveticaNeueCyr,Helvetica,sans-serif;font-size:15px;font-weight:400;line-height:1.2;margin:0;margin-bottom:0;padding:0;text-align:left">
                                                        <?=Yii::t('app', 'Cyprus Bazar Manager');?> </p>
                                                    <?= \common\models\Email::spacer(10)?>
                                                    <?/*<p style="Margin:0;Margin-bottom:0;color:#0a0a0a;font-family:HelveticaNeueCyr,Helvetica,sans-serif;font-size:15px;font-weight:400;line-height:1.2;margin:0;margin-bottom:0;padding:0;text-align:left">
                                                        <?=Yii::t('app', 'Phone');?>: <a href="tel:<?= Yii::$app->params['support_phone']?>" target="_blank"
                                                                    value="<?= Yii::$app->params['support_phone']?>" class="info__contacts"
                                                                    style="Margin:0;color:#0a0a0a;font-family:HelveticaNeueCyr,Helvetica,sans-serif;font-weight:700;line-height:1.2;margin:0;padding:0;text-align:left;text-decoration:none"><?= Yii::$app->params['support_phone']?></a></p>
                                                    <p style="Margin:0;Margin-bottom:0;color:#0a0a0a;font-family:HelveticaNeueCyr,Helvetica,sans-serif;font-size:15px;font-weight:400;line-height:1.2;margin:0;margin-bottom:0;padding:0;text-align:left">
                                                        <?=Yii::t('app', 'Fax');?> : <a href="tel:<?= Yii::$app->params['support_fax']?>" target="_blank"
                                                                 value="<?= Yii::$app->params['support_fax']?>" class="info__contacts"
                                                                 style="Margin:0;color:#0a0a0a;font-family:HelveticaNeueCyr,Helvetica,sans-serif;font-weight:700;line-height:1.2;margin:0;padding:0;text-align:left;text-decoration:none"><?= Yii::$app->params['support_fax']?></a></p>*/?>
                                                    <p style="Margin:0;Margin-bottom:0;color:#0a0a0a;font-family:HelveticaNeueCyr,Helvetica,sans-serif;font-size:15px;font-weight:400;line-height:1.2;margin:0;margin-bottom:0;padding:0;text-align:left">
                                                        E-mail: <a href="mailto:<?= Yii::$app->params['support_email']?>" target="_blank"
                                                                   class="info__contacts"
                                                                   style="Margin:0;color:#0a0a0a;font-family:HelveticaNeueCyr,Helvetica,sans-serif;font-weight:700;line-height:1.2;margin:0;padding:0;text-align:left;text-decoration:none"><?= Yii::$app->params['support_email']?></a>
                                                    </p>
                                                    <?= \common\models\Email::spacer(25)?>
                                                    <p style="Margin:0;Margin-bottom:0;color:#0a0a0a;font-family:HelveticaNeueCyr,Helvetica,sans-serif;font-size:15px;font-weight:400;line-height:1.2;margin:0;margin-bottom:0;padding:0;text-align:left">
                                                        <?=Yii::t('app', 'More news can be found on the');?> <a href="<?= $homeLink?>"
                                                                                           style="Margin:0;color:#ff7e00;font-family:HelveticaNeueCyr,Helvetica,sans-serif;font-weight:400;line-height:1.2;margin:0;padding:0;text-align:left;text-decoration:underline"><?=Yii::t('app', 'official website');?></a>.</p>
                                                    <?= \common\models\Email::spacer(50)?>
                                                </th>
                                                <th class="expander"
                                                    style="Margin:0;color:#0a0a0a;font-family:HelveticaNeueCyr,Helvetica,sans-serif;font-size:15px;font-weight:400;line-height:1.2;margin:0;padding:0!important;text-align:left;visibility:hidden;width:0"></th>
                                            </tr>
                                        </table>
                                    </th>
                                </tr>
                                </tbody>
                            </table>
                            <table class="row col col--footer"
                                   style="background-color:#f3f3f3;border-collapse:collapse;border-spacing:0;display:table;padding:0;position:relative;text-align:left;vertical-align:top;width:100%">
                                <tbody>
                                <tr style="padding:0;text-align:left;vertical-align:top">
                                    <th class="small-12 large-12 columns first last"
                                        style="Margin:0 auto;color:#0a0a0a;font-family:HelveticaNeueCyr,Helvetica,sans-serif;font-size:15px;font-weight:400;line-height:1.2;margin:0 auto;padding:0;padding-bottom:0;padding-left:30px;padding-right:30px;text-align:left;width:490px">
                                        <table style="border-collapse:collapse;border-spacing:0;padding:0;text-align:left;vertical-align:top;width:100%">
                                            <tr style="padding:0;text-align:left;vertical-align:top">
                                                <th style="Margin:0;color:#0a0a0a;font-family:HelveticaNeueCyr,Helvetica,sans-serif;font-size:15px;font-weight:400;line-height:1.2;margin:0;padding:0;text-align:left">

                                                    <?= \common\models\Email::spacer(15)?>
                                                    <p class="info__subtext info--gray"
                                                       style="Margin:0;Margin-bottom:0;color:#b8b8b8;font-family:HelveticaNeueCyr,Helvetica,sans-serif;font-size:12px;font-weight:400;line-height:1.667;margin:0;margin-bottom:0;padding:0;text-align:left">
                                                        &copy; CyprusBazar - <?=Yii::t('app', 'Free ads');?> .</p>

                                                    <?= \common\models\Email::spacer(15)?>
                                                </th>
                                                <th class="expander"
                                                    style="Margin:0;color:#0a0a0a;font-family:HelveticaNeueCyr,Helvetica,sans-serif;font-size:15px;font-weight:400;line-height:1.2;margin:0;padding:0!important;text-align:left;visibility:hidden;width:0"></th>
                                            </tr>
                                        </table>
                                    </th>
                                </tr>
                                </tbody>
                            </table>
                        </td>
                    </tr>
                    </tbody>
                </table><!-- footer -->

                <? /*= \common\models\Email::spacer(50, true)?>
                <table align="center" class="container float-center"
                       style="Margin:0 auto;background:#fff;border-collapse:collapse;border-spacing:0;float:none;margin:0 auto;padding:0;text-align:center;vertical-align:top;width:520px">
                    <tbody>
                    <tr style="padding:0;text-align:left;vertical-align:top">
                        <td style="-moz-hyphens:auto;-webkit-hyphens:auto;Margin:0;border-collapse:collapse!important;color:#0a0a0a;font-family:HelveticaNeueCyr,Helvetica,sans-serif;font-size:15px;font-weight:400;hyphens:auto;line-height:1.2;margin:0;padding:0;text-align:left;vertical-align:top;word-wrap:break-word">
                            <table class="row"
                                   style="border-collapse:collapse;border-spacing:0;display:table;padding:0;position:relative;text-align:left;vertical-align:top;width:100%">
                                <tbody>
                                <tr style="padding:0;text-align:left;vertical-align:top">
                                    <th class="small-12 large-12 columns first last"
                                        style="Margin:0 auto;color:#0a0a0a;font-family:HelveticaNeueCyr,Helvetica,sans-serif;font-size:15px;font-weight:400;line-height:1.2;margin:0 auto;padding:0;padding-bottom:0;padding-left:30px;padding-right:30px;text-align:left;width:490px">
                                        <table style="border-collapse:collapse;border-spacing:0;padding:0;text-align:left;vertical-align:top;width:100%">
                                            <tr style="padding:0;text-align:left;vertical-align:top">
                                                <th style="Margin:0;color:#0a0a0a;font-family:HelveticaNeueCyr,Helvetica,sans-serif;font-size:15px;font-weight:400;line-height:1.2;margin:0;padding:0;text-align:left">
                                                    <p class="info info__subtext"
                                                       style="Margin:0;Margin-bottom:0;color:#0a0a0a;font-family:HelveticaNeueCyr,Helvetica,sans-serif;font-size:12px;font-weight:400;line-height:1.667;margin:0;margin-bottom:0;padding:0;text-align:left">
                                                        Вы получили это сообщение, потому что подписались на рассылку на
                                                        сайте <a href="#"
                                                                 style="Margin:0;color:#ff7e00;font-family:HelveticaNeueCyr,Helvetica,sans-serif;font-weight:400;line-height:1.2;margin:0;padding:0;text-align:left;text-decoration:underline">www.bazar-cy.com</a>.
                                                        Если хотите, вы можете <a href="#"
                                                                                  style="Margin:0;color:#ff7e00;font-family:HelveticaNeueCyr,Helvetica,sans-serif;font-weight:400;line-height:1.2;margin:0;padding:0;text-align:left;text-decoration:underline">отписаться</a>
                                                        от получения писем.</p></th>
                                                <th class="expander"
                                                    style="Margin:0;color:#0a0a0a;font-family:HelveticaNeueCyr,Helvetica,sans-serif;font-size:15px;font-weight:400;line-height:1.2;margin:0;padding:0!important;text-align:left;visibility:hidden;width:0"></th>
                                            </tr>
                                        </table>
                                    </th>
                                </tr>
                                </tbody>
                            </table>
                        </td>
                    </tr>
                    </tbody>
                </table>
                 <?*/?>
                <?= \common\models\Email::spacer(50, true)?>
            </center>
        </td>
    </tr>
</table><!-- prevent Gmail on iOS font size manipulation -->
<div style="display:none;white-space:nowrap;font:15px courier;line-height:0">&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;
    &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;
    &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;
</div>
</body>
</html>
<?php $this->endPage() ?>
