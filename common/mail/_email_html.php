<?php
/**
 * Created by PhpStorm.
 * User: York
 * Date: 06.01.2018
 * Time: 11:12
 */
/* @var $title string */
/* @var $text string */
/* @var $meta string */



?>

<table class="row col col--orange"
       style="background-color:#ff7e00;border-collapse:collapse;border-spacing:0;display:table;padding:0;position:relative;text-align:left;vertical-align:top;width:100%">
    <tbody>
    <tr style="padding:0;text-align:left;vertical-align:top">
        <th class="small-12 large-12 columns first last"
            style="Margin:0 auto;color:#0a0a0a;font-family:HelveticaNeueCyr,Helvetica,sans-serif;font-size:15px;font-weight:400;line-height:1.2;margin:0 auto;padding:0;padding-bottom:0;padding-left:30px;padding-right:30px;text-align:left;width:490px">
            <table style="border-collapse:collapse;border-spacing:0;padding:0;text-align:left;vertical-align:top;width:100%">
                <tr style="padding:0;text-align:left;vertical-align:top">
                    <th style="Margin:0;color:#0a0a0a;font-family:HelveticaNeueCyr,Helvetica,sans-serif;font-size:15px;font-weight:400;line-height:1.2;margin:0;padding:0;text-align:left">
                        <?= \common\models\Email::spacer(30)?>
                        <h1 class="title title--white"
                            style="Margin:0;Margin-bottom:0;color:#fff;font-family:HelveticaNeueCyr,Helvetica,sans-serif;font-size:30px;font-weight:400;line-height:1.2;margin:0;margin-bottom:0;padding:0;text-align:left;word-wrap:normal">
                            <?= $title?></h1>
                        <?= \common\models\Email::spacer(30)?>
                    </th>
                    <th class="expander"
                        style="Margin:0;color:#0a0a0a;font-family:HelveticaNeueCyr,Helvetica,sans-serif;font-size:15px;font-weight:400;line-height:1.2;margin:0;padding:0!important;text-align:left;visibility:hidden;width:0"></th>
                </tr>
            </table>
        </th>
    </tr>
    </tbody>
</table><!-- topic -->
<?if(isset($meta)):?>
    <?= \common\models\Email::spacer(12)?>
    <table class="row"
           style="border-collapse:collapse;border-spacing:0;display:table;padding:0;position:relative;text-align:left;vertical-align:top;width:100%">
        <tbody>
        <tr style="padding:0;text-align:left;vertical-align:top">
            <th class="info small-12 large-12 columns first last"
                style="Margin:0 auto;color:#0a0a0a;font-family:HelveticaNeueCyr,Helvetica,sans-serif;font-size:15px;font-weight:400;line-height:1.2;margin:0 auto;padding:0;padding-bottom:0;padding-left:30px;padding-right:30px;text-align:left;width:490px">
                <table style="border-collapse:collapse;border-spacing:0;padding:0;text-align:left;vertical-align:top;width:100%">
                    <tr style="padding:0;text-align:left;vertical-align:top">
                        <th style="Margin:0;color:#0a0a0a;font-family:HelveticaNeueCyr,Helvetica,sans-serif;font-size:15px;font-weight:400;line-height:1.2;margin:0;padding:0;text-align:left">
                                                    <span class="info__subtext info--gray"
                                                          style="color:#b8b8b8;font-size:12px;line-height:1.667"><?= $meta?></span>
                        </th>
                        <th class="expander"
                            style="Margin:0;color:#0a0a0a;font-family:HelveticaNeueCyr,Helvetica,sans-serif;font-size:15px;font-weight:400;line-height:1.2;margin:0;padding:0!important;text-align:left;visibility:hidden;width:0"></th>
                    </tr>
                </table>
            </th>
        </tr>
        </tbody>
    </table>
<?endif?>

<?= \common\models\Email::spacer(30)?>
<table class="row"
       style="border-collapse:collapse;border-spacing:0;display:table;padding:0;position:relative;text-align:left;vertical-align:top;width:100%">
    <tbody>
    <tr style="padding:0;text-align:left;vertical-align:top">
        <th class="small-12 large-12 columns first last"
            style="Margin:0 auto;color:#0a0a0a;font-family:HelveticaNeueCyr,Helvetica,sans-serif;font-size:15px;font-weight:400;line-height:1.2;margin:0 auto;padding:0;padding-bottom:0;padding-left:30px;padding-right:30px;text-align:left;width:490px">
            <table style="border-collapse:collapse;border-spacing:0;padding:0;text-align:left;vertical-align:top;width:100%">
                <tr style="padding:0;text-align:left;vertical-align:top">
                    <th style="Margin:0;color:#0a0a0a;font-family:HelveticaNeueCyr,Helvetica,sans-serif;font-size:15px;font-weight:400;line-height:1.2;margin:0;padding:0;text-align:left">
                            <?= $text?>
                        </th>
                    <th class="expander"
                        style="Margin:0;color:#0a0a0a;font-family:HelveticaNeueCyr,Helvetica,sans-serif;font-size:15px;font-weight:400;line-height:1.2;margin:0;padding:0!important;text-align:left;visibility:hidden;width:0"></th>
                </tr>
            </table>
        </th>
    </tr>
    </tbody>
</table>
