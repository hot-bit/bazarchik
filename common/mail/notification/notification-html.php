<?php
/**
 * Created by PhpStorm.
 * User: York
 * Date: 14.11.2017
 * Time: 20:18
 */
/* @var $this \yii\web\View */
/* @var $data \backend\models\SendEmailForm */


$text = $this->render('@common/mail/notification/notification-data', compact('data'));


echo $this->render('@common/mail/_email_html', [
    'title' => $data->title,
    'text' => $text
]);