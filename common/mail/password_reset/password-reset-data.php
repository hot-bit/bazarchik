<?php
/**
 * Created by PhpStorm.
 * User: yorks
 * Date: 04.01.2018
 * Time: 21:42
 */

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $user common\models\User */


$resetLink = Yii::$app->urlManager->createAbsoluteUrl(['site/reset-password', 'token' => $user->password_reset_token]);
?>
<p style="Margin:0;Margin-bottom:0;color:#0a0a0a;font-family:HelveticaNeueCyr,Helvetica,sans-serif;font-size:15px;font-weight:400;line-height:1.2;margin:0;margin-bottom:0;padding:0;text-align:left">
    Hello <?= Html::encode($user->name) ?>,
</p>
<?= \common\models\Email::spacer(20)?>
<p style="Margin:0;Margin-bottom:0;color:#0a0a0a;font-family:HelveticaNeueCyr,Helvetica,sans-serif;font-size:15px;font-weight:400;line-height:1.2;margin:0;margin-bottom:0;padding:0;text-align:left">
    Follow the link below to reset your password:
</p>
<?= \common\models\Email::spacer(20)?>
<p style="Margin:0;Margin-bottom:0;color:#0a0a0a;font-family:HelveticaNeueCyr,Helvetica,sans-serif;font-size:15px;font-weight:400;line-height:1.2;margin:0;margin-bottom:0;padding:0;text-align:left">
    <?= Html::a(Html::encode($resetLink), $resetLink) ?>
</p>

