<?php
/**
 * Created by PhpStorm.
 * User: yorks
 * Date: 04.01.2018
 * Time: 21:42
 */
/* @var $this yii\web\View */
/* @var $user common\models\User */

$text = $this->render('@common/mail/confirm_profile/profile-confirm-data', compact('user'))
?>

<?= $this->render('@common/mail/_email_html', [
    'title' => Yii::t('app', 'Confirm your email address'),
    'text' => $text
])?>