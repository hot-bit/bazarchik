<?php
/**
 * Created by PhpStorm.
 * User: yorks
 * Date: 04.01.2018
 * Time: 21:42
 */
/* @var string $password */
/* @var $user common\models\User */



$confirmLink = Yii::$app->urlManager->createAbsoluteUrl(['site/confirm', 'token' => $user->hash]);
$confirmPageLink = Yii::$app->urlManager->createAbsoluteUrl(['site/confirm']);
?>
You received this letter because your address was used to confirm on the site of the Cyprus Bazar. If this email was not sent by you, just ignore it.

Your link to continue registration <?= $confirmLink?>

If for some reason you can not follow the link above, go to this address <?= $confirmPageLink?> and enter the code that provided the field provided.

Code: <?= $user->hash?>

