<?php
/**
 * Created by PhpStorm.
 * User: York
 * Date: 11.01.2018
 * Time: 13:57
 */
/* @var $this yii\web\View */
/* @var $user common\models\User */

$confirmLink = Yii::$app->urlManager->createAbsoluteUrl(['site/confirm', 'token' => $user->hash]);
$confirmPageLink = Yii::$app->urlManager->createAbsoluteUrl(['site/confirm']);
?>


<p style="Margin:0;Margin-bottom:0;color:#0a0a0a;font-family:HelveticaNeueCyr,Helvetica,sans-serif;font-size:15px;font-weight:400;line-height:1.2;margin:0;margin-bottom:0;padding:0;text-align:left">
    You received this letter because your address was used to confirm on the site of the Cyprus Bazar. If this email was not sent by you, just ignore it.
</p>
<?= \common\models\Email::spacer(20)?>
<p style="Margin:0;Margin-bottom:0;color:#0a0a0a;font-family:HelveticaNeueCyr,Helvetica,sans-serif;font-size:15px;font-weight:400;line-height:1.2;margin:0;margin-bottom:0;padding:0;text-align:left">
    Your link to continue registration <a style="color:#ff7e00" href="<?= $confirmLink?>"><?= Yii::t('app', 'Confirm registration')?></a>.
</p>
<?= \common\models\Email::spacer(20)?>
<p style="Margin:0;Margin-bottom:0;color:#0a0a0a;font-family:HelveticaNeueCyr,Helvetica,sans-serif;font-size:15px;font-weight:400;line-height:1.2;margin:0;margin-bottom:0;padding:0;text-align:left">
    If for some reason you can not follow the link above, go to this address <a style="color:#ff7e00" href="<?= $confirmPageLink?>"><?= $confirmPageLink?></a> and enter the code that provided the field provided.
</p>
<?= \common\models\Email::spacer(20)?>
<p style="Margin:0;Margin-bottom:0;color:#0a0a0a;font-family:HelveticaNeueCyr,Helvetica,sans-serif;font-size:15px;font-weight:400;line-height:1.2;margin:0;margin-bottom:0;padding:0;text-align:left">
    Code: <b><?= $user->hash?></b>
</p>