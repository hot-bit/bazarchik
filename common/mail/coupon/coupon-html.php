<?php
/**
 * Created by PhpStorm.
 * User: yorks
 * Date: 04.01.2018
 * Time: 21:42
 */
/* @var $this yii\web\View */
/* @var $code string */
/* @var $adv \common\models\Adv */


$text = $this->render('@common/mail/coupon/coupon-data', compact('code', 'adv'))
?>

<?= $this->render('@common/mail/_email_html', [
    'title' => Yii::t('app', 'Take your coupon'),
    'text' => $text
])?>