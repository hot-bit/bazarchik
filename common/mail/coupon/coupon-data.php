<?php
/**
 * Created by PhpStorm.
 * User: yorks
 * Date: 04.01.2018
 * Time: 21:42
 */
/* @var $this yii\web\View */
/* @var $code string */
/* @var $adv \common\models\Adv */
?>


<p style="Margin:0;Margin-bottom:0;color:#0a0a0a;font-family:HelveticaNeueCyr,Helvetica,sans-serif;font-size:15px;font-weight:400;line-height:1.2;margin:0;margin-bottom:0;padding:0;text-align:left">
    You received this letter because you purchased a coupon on the Cyprus Bazaar website for a product or service "<?= $adv->title?>".
</p>
<?= \common\models\Email::spacer(20)?>
<p style="Margin:0;Margin-bottom:0;color:#0a0a0a;font-family:HelveticaNeueCyr,Helvetica,sans-serif;font-size:15px;font-weight:400;line-height:1.2;margin:0;margin-bottom:0;padding:0;text-align:left">
    To use it, contact the owner for the following contacts:
</p>
<?= \common\models\Email::spacer(20)?>
<ul>
    <?if($adv->email):?>
        <li></li>Email: <?= $adv->email?>
    <?endif?>
    <?if($adv->email):?>
        <li></li>Phone: <?= $adv->f_phone?>
    <?endif?>
</ul>

<?= \common\models\Email::spacer(20)?>
<p style="Margin:0;Margin-bottom:0;color:#0a0a0a;font-family:HelveticaNeueCyr,Helvetica,sans-serif;font-size:15px;font-weight:400;line-height:1.2;margin:0;margin-bottom:0;padding:0;text-align:left">
    Your coupon number: <?= $code?>
</p>
