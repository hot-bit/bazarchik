<?php
/**
 * Created by PhpStorm.
 * User: yorks
 * Date: 04.01.2018
 * Time: 21:42
 */
/* @var $this yii\web\View */
/* @var $code string */
/* @var $adv \common\models\Adv */

?>

You received this letter because you purchased a coupon on the Cyprus Bazaar website for a product or service "<?= $adv->title?>".

To use it, contact the owner for the following contacts:
<?if($adv->email):?>
Email: <?= $adv->email?>
<?endif?>
<?if($adv->email):?>
Phone: <?= $adv->f_phone?>
<?endif?>

Your coupon number: <?= $code?>