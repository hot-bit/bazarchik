<?php
/**
 * Created by PhpStorm.
 * User: York
 * Date: 14.11.2017
 * Time: 20:18
 */
/* @var $this \yii\web\View */
/* @var $data \frontend\models\WriteToOwnerForm */

$abs = Yii::$app->urlManager->createAbsoluteUrl('');
?>


<p style="Margin:0;Margin-bottom:0;color:#0a0a0a;font-family:HelveticaNeueCyr,Helvetica,sans-serif;font-size:15px;font-weight:400;line-height:1.2;margin:0;margin-bottom:0;padding:0;text-align:left">
    You have received a message from the user <b><?= $data->name ?></b> on the ad <b>"<a href="<?= $abs.$data->_adv->link?>"><?= $data->_adv->title ?></a>"</b>.
</p>
<?= \common\models\Email::spacer(20)?>
<p style="Margin:0;Margin-bottom:0;color:#0a0a0a;font-family:HelveticaNeueCyr,Helvetica,sans-serif;font-size:15px;font-weight:400;line-height:1.2;margin:0;margin-bottom:0;padding:0;text-align:left">
    <b>User email: </b> <a href="mailto:<?= $data->email ?>"><?= $data->email ?></a>
</p>
<?= \common\models\Email::spacer(20)?>
<p style="Margin:0;Margin-bottom:0;color:#0a0a0a;font-family:HelveticaNeueCyr,Helvetica,sans-serif;font-size:15px;font-weight:400;line-height:1.2;margin:0;margin-bottom:0;padding:0;text-align:left"><b>Message</b></p>
<?= \common\models\Email::spacer(10)?>
<p style="Margin:0;Margin-bottom:0;color:#0a0a0a;font-family:HelveticaNeueCyr,Helvetica,sans-serif;font-size:15px;font-weight:400;line-height:1.2;margin:0;margin-bottom:0;padding:0;text-align:left">
    <?= $data->text ?>
</p>
