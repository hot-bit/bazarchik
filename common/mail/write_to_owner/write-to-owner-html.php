<?php
/**
 * Created by PhpStorm.
 * User: York
 * Date: 14.11.2017
 * Time: 20:18
 */
/* @var $this \yii\web\View */


$text = $this->render('@common/mail/write_to_owner/write-to-owner-data', compact('data'));


echo $this->render('@common/mail/_email_html', [
    'title' => Yii::t('app', 'Question about your ad on the site Cyprus Bazar'),
    'text' => $text
]);