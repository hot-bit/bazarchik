<?php
/**
 * Created by PhpStorm.
 * User: York
 * Date: 02.12.2017
 * Time: 12:57
 */

namespace common\components;


use common\models\Translate;
use yii\i18n\I18N;

/**
 * Class myI18n
 * @package common\components
 * @property array $items
 */
class myI18n extends I18N{

    private $_trans;

    public function TranslateItems(){
        if($this->_trans == null){
            $items = Translate::find()->all();
            $map = [];
            foreach ($items as $item)
                $map[$item->category][$item->message] = $item->value;

            $this->_trans = $map;
        }
        return $this->_trans;
    }

    public function translate($category, $message, $params, $language){

        if(\Yii::$app->params['collect_vars']){
            $this->collectVariables($category, $message);
        }

        return parent::translate($category, $message, $params, $language);
    }


    private function collectVariables($category, $message){

        $file_path = \Yii::getAlias('@host').'/translate.txt';
        $data = [];
        if(is_file($file_path)){
            $data_file = file_get_contents($file_path);
            $data = json_decode($data_file, true);
        }

        if(!isset($data[$category][$message])){
            $data[$category][$message] = 1;
            file_put_contents($file_path, json_encode($data));
        }
    }


    public function format($message, $params, $language){
        return parent::format($message, $params, $language);
    }

}