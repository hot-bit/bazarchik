<?php
/**
 * Created by PhpStorm.
 * User: York
 * Date: 26.09.2017
 * Time: 15:33
 */

namespace common\behaviors;

use common\models\CMS;
use dosamigos\tinymce\TinyMce;
use Yii;
use yii\base\Behavior;
use yii\db\ActiveRecord;
use yii\helpers\Html;

/**
 *
 * @property mixed $name
 * @property mixed $text
 * @property mixed $title
 */
class BazarLanguage extends Behavior{

    public $fields;

    public function LangField($field, $lng, $attr){
        $fld = $field.'_'.$lng->url;
        $text_input_class = 'form-control';
        $ret = '';

        $ret .= Html::activeLabel($this->owner, $fld, ['class' => 'control-label']);
//        $model_field = $form->field($this->owner, $fld);
        switch ($attr['type']){
            case "editor":
                $ret .= TinyMce::widget([
                    'clientOptions' => [
                        'plugins' => [
                            "advlist autolink lists link charmap print preview anchor",
                            "searchreplace visualblocks code fullscreen",
                            "insertdatetime media table contextmenu paste"
                        ],
                        'toolbar' => "undo redo | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link image"
                    ],
                    'model' => $this->owner,
                    'attribute' => $fld,
                ]);
//                $ret .= Html::activeTextarea($this->owner, $fld, ['class' => $text_input_class]);
                break;
            case "textarea":
                $ret .= Html::activeTextarea($this->owner, $fld, ['class' => $text_input_class]);
                break;
            case "textInput":
                $ret .= Html::activeTextInput($this->owner, $fld, ['class' => $text_input_class]);
                break;

        }

        return $ret;
    }



    public function getTitle(){
        return $this->owner->{CMS::field('title_')};
    }

    public function getText(){
        return $this->owner->{CMS::field('text_')};

    }

    public function getName(){
        return $this->owner->{CMS::field('name_')};
    }
}