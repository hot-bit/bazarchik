<?php

namespace common\models;

use Yii;
use yii\helpers\Html;

/**
 * This is the model class for table "pages".
 *
 * @property integer $id
 * @property integer $status
 * @property string $link
 * @property string $text_en
 * @property string $text_ru
 * @property mixed $f_status
 * @property string $text_gr
 */
class Pages extends \yii\db\ActiveRecord
{

    public function behaviors()
    {
        return [
            'lang' => [
                'class' => 'common\behaviors\BazarLanguage',
                'fields' => ['text']
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'pages';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['status', 'link', 'text_en', 'text_ru', 'text_gr'], 'required'],
            [['status'], 'integer'],
            [['text_en', 'text_ru', 'text_gr'], 'string'],
            [['link'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'status' => Yii::t('app', 'Status'),
            'link' => Yii::t('app', 'Link'),
            'text_en' => Yii::t('app', 'Text En'),
            'text_ru' => Yii::t('app', 'Text Ru'),
            'text_gr' => Yii::t('app', 'Text Gr'),
        ];
    }

    public function getF_status(){
        return $this->status ? Html::tag('span', 'Active', ['class' => 'badge badge-success']) :  Html::tag('span', 'Not active', ['class' => 'badge badge-default']);
    }

    public static function findByUrl($url){
        $model = self::find()->where(['link' => $url])->one();

        return $model;
    }
}
