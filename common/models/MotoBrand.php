<?php

namespace common\models;

use http\Url;
use Yii;

/**
 * This is the model class for table "moto_brand".
 *
 * @property integer $id
 * @property string $name
 * @property string $url
 * @property string $link
 * @property integer $count
 * @property integer $count_all
 */
class MotoBrand extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'moto_brand';
    }

    public static function findByUrl($url){
        return self::find()->where(['url' => $url])->one();
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['name', 'url', 'count'], 'required'],
            [['count'], 'integer'],
            [['name', 'url'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'name' => Yii::t('app', 'Name'),
            'url' => Yii::t('app', 'Url'),
            'count' => Yii::t('app', 'Count'),
        ];
    }

    public function getCount_all(){
        return $this->count;
    }

    public function getLink(){
        return Url::to(['adv/index', 'cat' => $this->url]);
    }

    public function getF_name(){
        return $this->name;
    }
}
