<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "text_blocks".
 *
 * @property integer $id
 * @property string $name
 * @property string $text_en
 * @property string $text_ru
 * @property string $text_gr
 * @property string $text
 */
class TextBlocks extends \yii\db\ActiveRecord
{

    public function behaviors()
    {
        return [
            'lang' => [
                'class' => 'common\behaviors\BazarLanguage',
                'fields' => ['text']
            ]
        ];
    }

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'text_blocks';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['name', 'text_en', 'text_ru', 'text_gr'], 'required'],
            [['text_en', 'text_ru', 'text_gr'], 'string'],
            [['name'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Name',
            'text_en' => 'Text En',
            'text_ru' => 'Text Ru',
            'text_gr' => 'Text Gr',
        ];
    }
}
