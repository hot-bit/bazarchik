<?php

namespace common\models;

use Yii;
use yii\helpers\ArrayHelper;
use yii\helpers\Url;

/**
 * This is the model class for table "model".
 *
 * @property integer $id
 * @property integer $brand_id
 * @property string $name
 * @property string $url
 * @property string $link
 * @property integer $count_used
 * @property integer $count_new
 * @property integer $count_rent
 * @property integer $count_all
 * @property string $brand_name
 * @property Brand $_brand
 *
 */
class Model extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'model';
    }

    public static function map($id){
        return ArrayHelper::map(Model::find()->where(['brand_id' => $id])->all(), 'id', 'name');
    }

    public static function findByBrand($brand){
        return self::find()->where(['brand_id' => $brand])->orderBy('name')->all();
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['brand_id', 'name', 'count_used', 'count_new', 'count_rent', 'url'], 'required'],
            [['brand_id', 'count_used', 'count_new', 'count_rent'], 'integer'],
            [['name', 'url'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'brand_id' => Yii::t('app', 'Brand ID'),
            'name' => Yii::t('app', 'Name'),
            'url' => Yii::t('app', 'Url'),
            'count_used' => Yii::t('app', 'Count Used'),
            'count_new' => Yii::t('app', 'Count New'),
            'count_rent' => Yii::t('app', 'Count Rent'),
        ];
    }

    public function get_brand(){
        return $this->hasOne(Brand::className(), ['id' => 'brand_id']);
    }

    public function getBrand_name(){
        return $this->_brand->name;
    }

    public function getCount_all(){
        return $this->count_used;
    }
    public static function findByUrl($url){
        return self::find()->where(['url' => $url])->one();
    }

    public function getLink(){
        return Url::to(['adv/index', 'cat' => $this->url]);
    }

    public function getF_name(){
        return $this->_brand->name.' '.$this->name;
    }
}
