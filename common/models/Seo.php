<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "seo_information".
 *
 * @property integer $id
 * @property string $url
 * @property string $information
 * @property string $data
 */
class Seo extends \yii\db\ActiveRecord
{
    public $data;

    public $title_ru;
    public $title_en;
    public $title_gr;

    public $h1_ru;
    public $h1_en;
    public $h1_gr;

    public $description_ru;
    public $description_en;
    public $description_gr;

    public $text_top_ru;
    public $text_top_en;
    public $text_top_gr;

    public $text_bot_ru;
    public $text_bot_en;
    public $text_bot_gr;

    public $keywords_ru;
    public $keywords_en;
    public $keywords_gr;

    public $image_ru;
    public $image_en;
    public $image_gr;

    public $seoFields = ['title', 'h1', 'keywords', 'description', 'text_top', 'text_bot'];

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'seo_information';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['title_en', 'title_gr', 'title_ru','description_en', 'description_gr', 'description_ru', 'keywords_en', 'keywords_gr', 'keywords_ru', 'h1_en', 'h1_gr', 'h1_ru', 'text_bot_en','text_bot_gr', 'text_bot_ru','text_top_en','text_top_gr','text_top_ru', 'image_en', 'image_gr', 'image_ru'], 'string'],
            [['url'], 'required'],
            [['information'], 'string'],
            [['url'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'url' => Yii::t('app', 'Url'),
            'information' => Yii::t('app', 'Information'),
        ];
    }

    public function afterFind()
    {
        $this->data = unserialize($this->information);
        $this->attributes = $this->data;
        parent::afterFind();
    }

    public function beforeSave($insert)
    {
        $data = [];
        foreach ($this->seoFields as $field) {
            $data[$field.'_en'] = $this->{$field.'_en'};
            $data[$field.'_ru'] = $this->{$field.'_ru'};
            $data[$field.'_gr'] = $this->{$field.'_gr'};
        }
        $this->data = $data;
        $this->information = serialize($this->data);
        return parent::beforeSave($insert);
    }
}
