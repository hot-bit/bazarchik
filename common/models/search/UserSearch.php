<?php

namespace common\models\search;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use common\models\User;

/**
 * UserSearch represents the model behind the search form about `common\models\User`.
 */
class UserSearch extends User
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'role', 'type', 'status', 'gender', 'last_active', 'b_day', 'b_month', 'b_year', 'last_login', 'date_reg', 'gmt', 'city', 'area', 'sms_code', 'success_phone', 'success_email', 'count_sms', 'confirm_site', 'send_sms', 'referral', 'ref_status', 'send_push', 'forum_messages', 'strike', 'is_business', 'business_until', 'phone_code', 'phone_try'], 'integer'],
            [['name', 'password', 'salt', 'phone', 'email', 'avatar', 'skype', 'website', 'company', 'lat', 'lng', 'hash', 'ip', 'fav', 'email_code', 'fb_id', 'g_id', 'fb_access', 'locale', 'address', 'company_description', 'business_link', 'auth_key', 'password_reset_token'], 'safe'],
            [['money'], 'number'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = User::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'role' => $this->role,
            'type' => $this->type,
            'status' => $this->status,
            'gender' => $this->gender,
            'last_active' => $this->last_active,
            'b_day' => $this->b_day,
            'b_month' => $this->b_month,
            'b_year' => $this->b_year,
            'last_login' => $this->last_login,
            'date_reg' => $this->date_reg,
            'gmt' => $this->gmt,
            'city' => $this->city,
            'area' => $this->area,
            'sms_code' => $this->sms_code,
            'success_phone' => $this->success_phone,
            'success_email' => $this->success_email,
            'money' => $this->money,
            'count_sms' => $this->count_sms,
            'confirm_site' => $this->confirm_site,
            'send_sms' => $this->send_sms,
            'referral' => $this->referral,
            'ref_status' => $this->ref_status,
            'send_push' => $this->send_push,
            'forum_messages' => $this->forum_messages,
            'strike' => $this->strike,
            'is_business' => $this->is_business,
            'business_until' => $this->business_until,
            'phone_code' => $this->phone_code,
            'phone_try' => $this->phone_try,
        ]);

        $query->andFilterWhere(['like', 'name', $this->name])
            ->andFilterWhere(['like', 'password', $this->password])
            ->andFilterWhere(['like', 'salt', $this->salt])
            ->andFilterWhere(['like', 'phone', $this->phone])
            ->andFilterWhere(['like', 'email', $this->email])
            ->andFilterWhere(['like', 'avatar', $this->avatar])
            ->andFilterWhere(['like', 'skype', $this->skype])
            ->andFilterWhere(['like', 'website', $this->website])
            ->andFilterWhere(['like', 'company', $this->company])
            ->andFilterWhere(['like', 'lat', $this->lat])
            ->andFilterWhere(['like', 'lng', $this->lng])
            ->andFilterWhere(['like', 'hash', $this->hash])
            ->andFilterWhere(['like', 'ip', $this->ip])
            ->andFilterWhere(['like', 'fav', $this->fav])
            ->andFilterWhere(['like', 'email_code', $this->email_code])
            ->andFilterWhere(['like', 'fb_id', $this->fb_id])
            ->andFilterWhere(['like', 'g_id', $this->g_id])
            ->andFilterWhere(['like', 'fb_access', $this->fb_access])
            ->andFilterWhere(['like', 'locale', $this->locale])
            ->andFilterWhere(['like', 'address', $this->address])
            ->andFilterWhere(['like', 'company_description', $this->company_description])
            ->andFilterWhere(['like', 'business_link', $this->business_link])
            ->andFilterWhere(['like', 'auth_key', $this->auth_key])
            ->andFilterWhere(['like', 'password_reset_token', $this->password_reset_token]);

        return $dataProvider;
    }
}
