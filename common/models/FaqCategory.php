<?php

namespace common\models;

use Yii;
use yii\helpers\ArrayHelper;

/**
 * This is the model class for table "faq_category".
 *
 * @property integer $id
 * @property string $name_en
 * @property string $name_ru
 * @property string $name_gr
 * @property Faq[] $_items
 * @property string $name
 * @property string $link
 */
class FaqCategory extends \yii\db\ActiveRecord
{


    public function behaviors()
    {
        return [
            'lang' => [
                'class' => 'common\behaviors\BazarLanguage',
                'fields' => ['name']
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'faq_category';
    }

    public static function map(){
        return ArrayHelper::map(self::find()->orderBy(CMS::field('name_'))->all(), 'id', CMS::field('name_'));
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['name_en', 'name_ru', 'name_gr', 'link'], 'required'],
            [['name_en', 'name_ru', 'name_gr', 'link'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'name_en' => Yii::t('app', 'Name En'),
            'name_ru' => Yii::t('app', 'Name Ru'),
            'name_gr' => Yii::t('app', 'Name Gr'),
            'link' => Yii::t('app', 'Link'),
        ];
    }


    public function get_items(){
        return $this->hasMany(Faq::className(), ['category_id' => 'id']);
    }
}
