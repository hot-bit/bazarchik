<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "job_type".
 *
 * @property integer $id
 * @property string $name_ru
 * @property string $name_en
 * @property string $name_gr
 */
class JobType extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'job_type';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['name_ru', 'name_en', 'name_gr'], 'required'],
            [['name_ru', 'name_en', 'name_gr'], 'string', 'max' => 16],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'name_ru' => Yii::t('app', 'Name Ru'),
            'name_en' => Yii::t('app', 'Name En'),
            'name_gr' => Yii::t('app', 'Name Gr'),
        ];
    }
}
