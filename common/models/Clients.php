<?php

namespace common\models;

use Yii;
use yii\helpers\Html;

/**
 * This is the model class for table "clients".
 *
 * @property integer $id
 * @property string $name
 * @property string $email
 * @property integer $phone
 * @property integer $phone_code
 * @property string $orig_phone
 * @property integer $user
 * @property integer $status_phone
 * @property integer $status_email
 * @property integer $status
 * @property mixed $f_status_email
 * @property mixed $f_status
 * @property mixed $f_status_phone
 * @property string $fullPhone
 * @property integer $send_date
 */
class Clients extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'clients';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['name'], 'required'],
            [['phone', 'user', 'status_phone', 'status_email', 'status', 'send_date', 'phone_code'], 'integer'],
            [['name'], 'string', 'max' => 50],
            [['email'], 'string', 'max' => 150],
            [['orig_phone'], 'string', 'max' => 255],
            [['phone'], 'unique'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'name' => Yii::t('app', 'Name'),
            'email' => Yii::t('app', 'Email'),
            'phone' => Yii::t('app', 'Phone'),
            'phone_code' => Yii::t('app', 'Phone Code'),
            'orig_phone' => Yii::t('app', 'Orig Phone'),
            'user' => Yii::t('app', 'User'),
            'status_phone' => Yii::t('app', 'Status Phone'),
            'status_email' => Yii::t('app', 'Status Email'),
            'status' => Yii::t('app', 'Status'),
            'send_date' => Yii::t('app', 'Send Date'),
        ];
    }

    public function getF_status(){
        return $this->status ? Html::tag('span', 'Active', ['class' => 'badge badge-success']) :  Html::tag('span', 'Not active', ['class' => 'badge badge-default']);
    }
    public function getF_status_phone(){
        return $this->status_phone ? Html::tag('span', 'Send sms', ['class' => 'badge badge-success']) :  Html::tag('span', 'Not send', ['class' => 'badge badge-default']);
    }
    public function getF_status_email(){
        return $this->status_email ? Html::tag('span', 'Send email', ['class' => 'badge badge-success']) :  Html::tag('span', 'Not email', ['class' => 'badge badge-default']);
    }

    public function updateDate(){
        $this->send_date = time();
        $this->save();
        return true;
    }

    public function getFullPhone(){
        return $this->phone_code.$this->phone;
    }


}
