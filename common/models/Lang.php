<?php

namespace common\models;

use Yii;
use yii\db\ActiveRecord;
use yii\helpers\ArrayHelper;

/**
 * This is the model class for table "lang".
 *
 * @property integer $id
 * @property string $url
 * @property string $local
 * @property string $name
 * @property integer $default
 * @property integer $date_update
 * @property mixed $_translates
 * @property integer $date_create
 */
class Lang extends ActiveRecord
{
    static $current = null;

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'lang';
    }

    /**
     * @inheritdoc
     */
    public function behaviors(){
        return [
            'timestamp' => [
                'class' => 'yii\behaviors\TimestampBehavior',
                'attributes' => [
                    ActiveRecord::EVENT_BEFORE_INSERT => ['date_create', 'date_update'],
                    ActiveRecord::EVENT_BEFORE_UPDATE => ['date_update']
                ]
            ]
        ];
    }

    public static function getCurrent(){
        if(self::$current == null){
            self::$current = self::getDefaultLang();
        }
        return self::$current;
    }

    public static function setCurrent($url = null){
        $language = self::getLangByUrl($url);
        self::$current = ($language === null) ? self::getDefaultLang() : $language;
        Yii::$app->language = self::$current->local;
    }

    public static function getDefaultLang(){
        return Lang::find()->where('`default` = :default', [':default' => 1])->one();
    }

    public static function getLangByUrl($url = null){
        if($url === null){
            return null;
        }
        else{
            $language = Lang::find()->where('`url` = :url', [':url' => $url])->one();
            if($language === null){
                return null;
            }
            else{
                return $language;
            }
        }
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['url', 'local', 'name', 'date_update', 'date_create'], 'required'],
            [['default', 'date_update', 'date_create'], 'integer'],
            [['url', 'local', 'name'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'url' => 'Url',
            'local' => 'Local',
            'name' => 'Name',
            'default' => 'Default',
            'date_update' => 'Date Update',
            'date_create' => 'Date Create',
        ];
    }


    public static function map(){
        return ArrayHelper::map(self::find()->all(), 'id', 'name');
    }

    public function get_translates(){
        return $this->hasMany(Translate::className(), ['id' => 'language_id']);
    }
}
