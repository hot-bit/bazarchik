<?php
namespace common\models;

use common\models\validators\PhoneValidator;
use Yii;
use yii\base\NotSupportedException;
use yii\bootstrap\Html;
use yii\db\ActiveRecord;
use yii\helpers\Url;
use yii\web\IdentityInterface;

/**
 * User model
 *
 * @property integer $id
 * @property string $name
 * @property string $email
 * @property string $auth_key
 * @property integer $status
 * @property mixed $f_status
 * @property string $avatarPath
 * @property mixed $f_success_email
 * @property string $authKey
 * @property mixed $f_success_phone
 * @property string $password write-only password
 * @property int $role [int(11)]
 * @property int $type [int(11)]
 * @property string $salt [varchar(255)]
 * @property string $phone [varchar(255)]
 * @property string $avatar [varchar(255)]
 * @property string $skype [varchar(255)]
 * @property string $website [varchar(255)]
 * @property string $company [varchar(255)]
 * @property string $lat [varchar(255)]
 * @property string $lng [varchar(255)]
 * @property bool $gender [tinyint(4)]
 * @property int $last_active [int(11)]
 * @property int $b_day [int(11)]
 * @property int $b_month [int(11)]
 * @property int $b_year [int(11)]
 * @property int $last_login [int(11)]
 * @property int $date_reg [int(11)]
 * @property bool $gmt [tinyint(4)]
 * @property string $hash [varchar(255)]
 * @property string $ip [varchar(255)]
 * @property int $city [smallint(6)]
 * @property int $area [int(11)]
 * @property string $fav
 * @property int $sms_code [int(11)]
 * @property int $phone_try
 * @property string $email_code [varchar(10)]
 * @property int $success_phone [int(11)]
 * @property int $success_email [int(11)]
 * @property string $fb_id [varchar(255)]
 * @property string $g_id [varchar(255)]
 * @property string $fb_access [varchar(255)]
 * @property float $money [float]
 * @property int $count_sms [int(11)]
 * @property int $confirm_site [int(11)]
 * @property int $send_sms [int(11)]
 * @property int $referral [int(11)]
 * @property int $ref_status [int(11)]
 * @property bool $send_push [tinyint(3)]
 * @property int $forum_messages [int(11)]
 * @property int $strike [int(11)]
 * @property string $locale [varchar(255)]
 * @property string $address
 * @property bool $is_business [tinyint(4)]
 * @property int $business_until [int(11)]
 * @property string $company_description
 * @property string $shop_link
 * @property string $langs
 * @property string $shop_name
 * @property bool $can_use_bonus
 * @property string $business_link [varchar(255)]
 * @property Adv[] $_ads
 * @property array $address_arr
 * @property array $address_array
 * @property string $f_website
 * @property string $password_reset_token
 * @property bool $hasUnreadChat
 * @property mixed $businessStatus
 * @property int $phone_code [int(11)]
 */
class User extends ActiveRecord implements IdentityInterface
{

    const STATUS_DELETED = 0;
    const STATUS_ACTIVE = 1;
    const STATUS_MODERATED = 2;

    const BUSINESS_MODERATE = 1;
    const BUSINESS_ACTIVE = 2;
    const BUSINESS_BANNED = 3;

    public $old_password;
    public $new_password;
    public $new_password_repeat;

    public $role;
    public $no_save = false;
    public $tmp_password;

    public $old_phone;
    public $old_email;
    public $old_is_business;
    public $old_website;

    public $address_array;

    public $langs_arr = [];

    public $imageFile;
    public function behaviors()
    {
        return [
            'avatar' => [
                'class' => 'common\behaviors\CMSImage',
                'directory' => Yii::getAlias('@user'),
                'field' => 'avatar',
                'file' => 'imageFile',
            ],
        ];
    }


    /**
     * Событие после нахождения модели
     */
    public function afterFind(){
        $this->old_phone = $this->phone;
        $this->old_email = $this->email;
        $this->old_is_business = $this->is_business;
        $this->old_website = $this->website;
        if($this->langs)
            $this->langs_arr = json_decode($this->langs);

        $roles = \Yii::$app->authManager->getRolesByUser($this->id);

        foreach ($roles as $role)
            $this->role = $role->name;

        $this->address_array = json_decode($this->address, true);

        parent::afterFind();
    }

    /**
     * @param bool $insert
     * @param array $changedAttributes
     * @throws \Exception
     */
    public function afterSave($insert, $changedAttributes){
        if(!$this->no_save){
            $auth = \Yii::$app->authManager;
            if($this->role == null) $this->role = 'user';
            $role = $auth->getRole($this->role);

            $auth->revokeAll($this->id);
            $auth->assign($role, $this->id);
        }

        parent::afterSave($insert, $changedAttributes);
    }


    /**
     * Событие перед сохранением
     *
     * @param bool $insert
     * @return bool
     * @throws \yii\base\Exception
     */
    public function beforeSave($insert){
        if($insert){
            $this->date_reg = time();
            $this->salt = Yii::$app->security->generateRandomString();
        }
        else{
            $this->langs = json_encode($this->langs_arr);

            if($this->email != $this->old_email & $this->success_email){
                $this->success_email = $this->old_email;
                Yii::$app->session->setFlash('error', Yii::t('app', 'You can\'t change the verified address'));
            }

            if($this->phone != $this->old_phone)
                $this->success_phone = false;

            if(!empty($this->new_password))
                $this->setPassword($this->new_password);

            if (!empty($this->address_array))
                $this->address = json_encode($this->address_array);

            if($this->is_business && !$this->old_is_business)
                $this->status = self::STATUS_MODERATED;
            if($this->website != $this->old_website)
                $this->status = self::STATUS_MODERATED;
        }

        return parent::beforeSave($insert);
    }

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%users}}';
    }

    public function attributeLabels(){
        return [
            'old_password' => Yii::t('app', 'Old password'),
            'new_password' => Yii::t('app', 'New password'),
            'new_password_repeat' => Yii::t('app', 'Repeat new password'),
            'name' => Yii::t('app', 'Name'),
            'phone' => Yii::t('app', 'Phone'),
            'b_day' => Yii::t('app', 'Day of birth'),
            'b_month' => Yii::t('app', 'Month of birth'),
            'b_year' => Yii::t('app', 'Year of birth'),
            'langs_arr' => Yii::t('app', 'I speak this languages'),
            'company' => Yii::t('app', 'Company name'),
            'website' => Yii::t('app', 'Website'),
            'business_link' => Yii::t('app', 'Link for cyprus bazar shop'),
            'company_description' => Yii::t('app', 'Text about company'),
            'city' => Yii::t('app', 'City'),
            'area' => Yii::t('app', 'Area'),
            'imageFile' => Yii::t('app', 'Avatar'),
//            '' => Yii::t('app', ''),
        ];
    }



    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            ['status', 'default', 'value' => self::STATUS_ACTIVE],
            ['status', 'in', 'range' => [self::STATUS_ACTIVE, self::STATUS_DELETED, self::STATUS_MODERATED]],
            [['imageFile'], 'file', 'skipOnEmpty' => true, 'extensions' => 'png, jpg'],

            [['new_password', 'new_password_repeat'], 'required', 'on' => 'set_password'],
            ['new_password', 'string', 'min' => 8, 'on' => 'set_password'],
            ['new_password_repeat', 'compare', 'compareAttribute' => 'new_password'],
            ['old_password', 'changePasswordRule'],
            [['email', 'phone', 'name'], 'required', 'on' => 'update'],
            [['type', 'status', 'gender', 'last_active', 'b_day', 'b_month', 'b_year', 'last_login', 'date_reg', 'gmt', 'city', 'area', 'sms_code', 'phone_try', 'success_phone', 'success_email', 'count_sms', 'confirm_site', 'send_sms', 'referral', 'ref_status', 'send_push', 'forum_messages', 'strike', 'is_business', 'business_until', 'phone_code'], 'integer'],
            [['fav', 'address', 'company_description', 'role'], 'string'],
            [['money'], 'number'],
            ['phone', PhoneValidator::className()],
            [['name', 'password', 'salt', 'phone', 'email', 'avatar', 'skype', 'website', 'company', 'lat', 'lng', 'hash', 'ip', 'fb_id', 'g_id', 'fb_access', 'locale', 'business_link', 'auth_key', 'old_password', 'new_password', 'new_password_repeat'], 'string', 'max' => 255],
            [['email_code'], 'string', 'max' => 10],
            [['email', 'business_link'], 'unique'],
            ['email', 'email'],
            [['address_array', 'langs', 'langs_arr'], 'safe'],
        ];
    }

    /**
     * Правило для изменения пароля
     *
     * @return bool
     */
    public function changePasswordRule(){
        if(!empty($this->old_password) || !empty($this->new_password) || !empty($this->new_password_repeat)){
            if(empty($this->old_password))
                $this->addError('old_password', Yii::t('app', 'Old password cannot be empty'));
            if(empty($this->new_password))
                $this->addError('new_password', Yii::t('app', 'New password cannot be empty'));
            if(empty($this->new_password_repeat))
                $this->addError('new_password_repeat', Yii::t('app', 'New password repeat cannot be empty'));



            if($this->new_password != $this->new_password_repeat)
                $this->addError('new_password_repeat', Yii::t('app', 'New password and new password repeat must be equal'));

            if(!$this->validatePassword($this->old_password))
                $this->addError('old_password', Yii::t('app', 'Incorrect old password'));

            if($this->hasErrors())
                return false;

            return true;
        }
    }

    /**
     * Получить форматированный статус подтверждения email
     *
     * @return string
     */
    public function getF_success_email(){
        return $this->success_email ? Html::tag('span', 'Active', ['class' => 'badge badge-success']) :  Html::tag('span', 'Not active', ['class' => 'badge badge-default']);
    }


    /**
     * Получить форматированный статус подтверждения телефона
     *
     * @return string
     */
    public function getF_success_phone(){
        return $this->success_phone ? Html::tag('span', 'Active', ['class' => 'badge badge-success']) :  Html::tag('span', 'Not active', ['class' => 'badge badge-default']);
    }

    /**
     * Получить форматированный статус пользователя
     *
     * @return string
     */
    public function getF_status(){
        return $this->status ? Html::tag('span', 'Active', ['class' => 'badge badge-success']) :  Html::tag('span', 'Not active', ['class' => 'badge badge-default']);
    }


    /**
     * @inheritdoc
     */
    public static function findIdentity($id)
    {
        return static::findOne(['id' => $id, 'status' => [self::STATUS_ACTIVE, self::STATUS_MODERATED]]);
    }

    /**
     * @inheritdoc
     */
    public static function findIdentityByAccessToken($token, $type = null)
    {
        throw new NotSupportedException('"findIdentityByAccessToken" is not implemented.');
    }

    /**
     * Finds user by email
     *
     * @param $email
     * @return static|null
     */
    public static function findByEmail($email)
    {
        return static::findOne(['email' => $email, 'status' => self::STATUS_ACTIVE]);
    }

    /**
     * Finds user by password reset token
     *
     * @param string $token password reset token
     * @return static|null
     */
    public static function findByPasswordResetToken($token)
    {
        if (!static::isPasswordResetTokenValid($token)) {
            return null;
        }

        return static::findOne([
            'password_reset_token' => $token,
            'status' => self::STATUS_ACTIVE,
        ]);
    }

    /**
     * Finds out if password reset token is valid
     *
     * @param string $token password reset token
     * @return bool
     */
    public static function isPasswordResetTokenValid($token)
    {
        if (empty($token)) {
            return false;
        }

        $timestamp = (int) substr($token, strrpos($token, '_') + 1);
        $expire = Yii::$app->params['user.passwordResetTokenExpire'];
        return $timestamp + $expire >= time();
    }

    /**
     * @inheritdoc
     */
    public function getId()
    {
        return $this->getPrimaryKey();
    }

    /**
     * @inheritdoc
     */
    public function getAuthKey()
    {
        return $this->auth_key;
    }

    /**
     * @inheritdoc
     */
    public function validateAuthKey($authKey)
    {
        return $this->getAuthKey() === $authKey;
    }

    /**
     * Validates password
     *
     * @param string $password password to validate
     * @return bool if password provided is valid for current user
     */
    public function validatePassword($password)
    {
        return (sha1(sha1($password.$this->salt).Yii::$app->params['secret_key']) == $this->password) ? true : false;
    }

    /**
     * Generates password hash from password and sets it to the model
     *
     * @param string $password
     * @throws \yii\base\Exception
     */
    public function setPassword($password)
    {
        if($this->salt == null)
            $this->salt = Yii::$app->security->generateRandomString();
        $this->password = sha1(sha1($password.$this->salt).Yii::$app->params['secret_key']) ;
//        Yii::$app->security->generatePasswordHash($password);
    }

    /**
     * Generates "remember me" authentication key
     */
    public function generateAuthKey()
    {
        $this->auth_key = Yii::$app->security->generateRandomString();
    }

    /**
     * Generates new password reset token
     */
    public function generatePasswordResetToken()
    {
        $this->password_reset_token = Yii::$app->security->generateRandomString() . '_' . time();
    }

    /**
     * Removes password reset token
     */
    public function removePasswordResetToken()
    {
        $this->password_reset_token = null;
    }

    public function getCan_use_bonus(){

        if($this->money > 0)
            return true;
        return false;
    }
    
    public static function findByCompany($url){
        return self::find()->where(['business_link' => $url])->one();
    }

    /**
     * Получить массив арессов
     *
     * @return mixed
     */
    public function getAddress_arr(){
        return json_decode($this->address);
    }

    /**
     * Получить объявления пользователя
     *
     * @return \yii\db\ActiveQuery
     */
    public function get_ads(){
        $model = Adv::find()->where(['owner' => $this->id]);
        if($this->success_email)
            $model->orWhere(['email' => $this->email]);
        if ($this->success_phone)
            $model->orWhere(['phone' => $this->phone]);
        return $model;
    }

    /**
     * Ссылка на магазин
     * Если есть урл, то отдаем его, иначе по id пользователя
     *
     * @return string
     */
    public function getShop_link(){
        return Url::to( $this->business_link ? ['shop/get', 'url' => $this->business_link] : ['shop/get', 'id' => $this->id]);
    }

    /**
     * Получить название магазина
     *
     * @return string
     */
    public function getShop_name(){
        return $this->company ? $this->company : $this->name;
    }

    /**
     * Нормализация вебсайта
     *
     * @return string
     */
    public function getF_website(){
        if (strpos($this->website, 'http') !== false)
            return $this->website;
        return 'http://'.$this->website;
    }

    /**
     * Активация телефона
     */
    public function setSuccessPhone()
    {
        $this->sms_code = null;
        $this->success_phone = true;
        $this->phone_try = null;
        $this->save();
    }

    /**
     * Активация Email адреса
     */
    public function setSuccessEmail()
    {
        $this->email_code = null;
        $this->success_email = true;
        $this->save();
    }

    /** Есть ли непрочитанные сообщения в чате
     * @return bool
     */
    public function getHasUnreadChat(){
        $model = ChatView::findByUser();
        if($model) return true;
        return false;
    }

    public function getBusinessStatus(){
        if($this->is_business == self::BUSINESS_MODERATE) return Yii::t('app', 'Moderated');
        elseif($this->is_business == self::BUSINESS_ACTIVE) return Yii::t('app', 'Active');
        elseif($this->is_business == self::BUSINESS_BANNED) return Yii::t('app', 'Banned');
    }

    public function deleteImage(){
        @unlink(Yii::getAlias('@host').$this->thumb);
        @unlink(Yii::getAlias('@host').$this->path);
        $this->avatar = '';
        $this->save();

    }

    public function getAvatarPath(){
        if($this->avatar)
            return Yii::getAlias('@user').'/'.$this->id.'/'.$this->avatar;
        return '';
    }
}
