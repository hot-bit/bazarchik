<?php

namespace common\models;

use Yii;
use yii\behaviors\TimestampBehavior;

/**
 * This is the model class for table "adv_message".
 *
 * @property integer $id
 * @property integer $adv_id
 * @property integer $chat_id
 * @property string $text
 * @property integer $created_at
 * @property integer $updated_at
 * @property integer $user_num
 * @property integer $is_new
 *
 * @property AdvChat $_chat
 */
class AdvMessage extends \yii\db\ActiveRecord
{

    public function behaviors(){
        return [
            TimestampBehavior::className(),
        ];
    }

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'adv_message';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['chat_id', 'created_at', 'updated_at', 'is_new', 'user_num', 'adv_id'], 'integer'],
            [['text'], 'string'],
            [['chat_id'], 'exist', 'skipOnError' => true, 'targetClass' => AdvChat::className(), 'targetAttribute' => ['chat_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'adv_id' => Yii::t('app', 'Adv ID'),
            'chat_id' => Yii::t('app', 'Chat ID'),
            'text' => Yii::t('app', 'Text'),
            'created_at' => Yii::t('app', 'Create At'),
            'updated_at' => Yii::t('app', 'Update At'),
            'is_new' => Yii::t('app', 'Is New'),
            'user_num' => Yii::t('app', 'Number of user'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function get_chat()
    {
        return $this->hasOne(AdvChat::className(), ['id' => 'chat_id']);
    }
}
