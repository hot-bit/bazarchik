<?php

namespace common\models;

use Yii;
use yii\helpers\Html;

/**
 * This is the model class for table "subscribe".
 *
 * @property integer $id
 * @property integer $user_id
 * @property string $name
 * @property string $email
 * @property integer $category_id
 * @property integer $car_brand
 * @property integer $car_model
 * @property integer $status
 * @property integer $date
 * @property mixed $f_status
 * @property Categories $_category
 * @property void $action
 * @property void $category_name
 * @property Brand $_brand
 * @property Model $_model
 * @property string $code
 */
class Subscribe extends \yii\db\ActiveRecord
{

    const STATUS_ACTIVE = 1;
    const STATUS_STOP = 0;
    const STATUS_INACTIVE = 2;

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'subscribe';
    }

    public function beforeSave($insert)
    {
        if($insert){
            $this->date = time();
            if(!Yii::$app->user->isGuest)
                $this->user_id = Yii::$app->user->id;
            $this->status = self::STATUS_INACTIVE;

            // TODO Отправка email для подтверждения рассылки
        }

        return parent::beforeSave($insert);
    }


    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['name', 'email', 'category_id'], 'required'],
            [['user_id', 'category_id', 'car_brand', 'car_model', 'status', 'date'], 'integer'],
            [['name', 'email', 'code'], 'string', 'max' => 50],
            ['email', 'email'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'user_id' => Yii::t('app', 'User ID'),
            'name' => Yii::t('app', 'Name'),
            'email' => Yii::t('app', 'Email'),
            'category_id' => Yii::t('app', 'Category ID'),
            'car_brand' => Yii::t('app', 'Car Brand'),
            'car_model' => Yii::t('app', 'Car Model'),
            'status' => Yii::t('app', 'Status'),
            'f_status' => Yii::t('app', 'Status'),
            'date' => Yii::t('app', 'Date'),
            'code' => Yii::t('app', 'Code'),
            'category_name' => Yii::t('app', 'Category'),
            'action' => Yii::t('app', 'Actions'),
        ];
    }
    public function getF_status(){
        if($this->status == self::STATUS_ACTIVE)
            return Yii::t('app', 'Active');
        return Yii::t('app', 'Not active');
    }

    public function getAction(){
        $ret = '';
        if($this->status == self::STATUS_ACTIVE)
            $ret .= Html::a(Yii::t('app', 'Stop'), ['profile/subscribe-stop', 'id' => $this->id]);
        else
            $ret .= Html::a(Yii::t('app', 'Start'), ['profile/subscribe-start', 'id' => $this->id]);

        $ret .= ' | ';

        $ret .= Html::a(Yii::t('app', 'Delete'), ['profile/subscribe-delete', 'id' => $this->id], ['confirm' => Yii::t('app', 'Are you sure you want to delete this item?')]);

        return $ret;
    }

    public function getCategory_name(){
        $items = $this->_category->category_stair;

        if($this->car_brand){
            $items[] = $this->_brand->name;

            if($this->car_model)
                $items[] = $this->_model->name;
        }
        return implode(' → ', $items);
    }

    public static function my(){
        return self::find()->where(['user_id' => Yii::$app->user->id])->orWhere(['email' => Yii::$app->user->identity->email])->orderBy('date DESC');
    }

    public function get_category(){
        return $this->hasOne(Categories::className(), ['id' => 'category_id']);
    }

    public function get_brand(){
        return $this->hasOne(Brand::className(), ['id' => 'car_brand']);
    }

    public function get_model(){
        return $this->hasOne(Model::className(), ['id' => 'car_model']);
    }
}
