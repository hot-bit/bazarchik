<?php

namespace common\models;

use Yii;
use yii\behaviors\TimestampBehavior;

/**
 * This is the model class for table "adv_chat".
 *
 * @property integer $id
 * @property integer $user_1
 * @property string $user_1_email
 * @property string $user_1_name
 * @property integer $user_2
 * @property integer $created_at
 * @property integer $updated_at
 *
 * @property \yii\db\ActiveQuery $_messages
 * @property AdvMessage[] $advMessages
 */
class AdvChat extends \yii\db\ActiveRecord
{

    public function behaviors(){
        return [
            TimestampBehavior::className(),
        ];
    }

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'adv_chat';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['user_1', 'user_2', 'created_at', 'updated_at'], 'integer'],
            [['user_1_email', 'user_1_name'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'user_1' => Yii::t('app', 'User 1'),
            'user_1_email' => Yii::t('app', 'User 1 Email'),
            'user_1_name' => Yii::t('app', 'User 1 Name'),
            'user_2' => Yii::t('app', 'User 2'),
            'created_at' => Yii::t('app', 'Create At'),
            'updated_at' => Yii::t('app', 'Update At'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function get_messages()
    {
        return $this->hasMany(AdvMessage::className(), ['chat_id' => 'id']);
    }

    public static function findByEmail($email){
        return self::find()->where(['user_1_email' => $email])->one();
    }

    public static function findByWriter($user_id){
        return self::find()->where(['user_1' => $user_id])->one();
    }

    public function addMessage($text, $user_num, $adv_id = false){
        $model = new AdvMessage();

        $model->chat_id = $this->id;
        $model->is_new = 1;
        $model->user_num = $user_num;
        $model->text = $text;
        if($adv_id) $model->adv_id = $adv_id;
        $model->save();
    }
}
