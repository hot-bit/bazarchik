<?php

namespace common\models;

use common\models\forms\ParamsForm;
use common\models\map\CarBodyType;
use common\models\map\CarBox;
use common\models\map\CarColor;
use common\models\map\CarDrive;
use common\models\map\CarEngine;
use common\models\map\CarParams;
use common\models\map\CarWheel;
use Yii;
use yii\helpers\Html;

/**
 * This is the model class for table "adv_params".
 *
 * @property integer $id
 * @property integer $adv_id
 * @property string $name
 * @property string $val
 * @property bool $is_bool
 * @property mixed $is_list
 * @property string $value
 */
class AdvParams extends \yii\db\ActiveRecord
{

    private $_tmp_val;
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'adv_params';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['adv_id', 'name', 'value'], 'required'],
            [['adv_id'], 'integer'],
            [['name', 'value'], 'string', 'max' => 255],
            [['adv_id', 'name'], 'unique', 'targetAttribute' => ['adv_id', 'name'], 'message' => 'The combination of Adv ID and Name has already been taken.'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'adv_id' => Yii::t('app', 'Adv ID'),
            'name' => Yii::t('app', 'Name'),
            'value' => Yii::t('app', 'Value'),
        ];
    }

    /**
     * @param $id
     * @return false|AdvParams[]|\yii\db\ActiveRecord[]
     */
    public static function findByAdv($id){
        return self::find()->where(['adv_id' => $id])->all();
    }

    public function checkboxFields(){
        $params_form = new ParamsForm();

        $items_rules = $params_form->rules();

        $bool_items = [];
        foreach ($items_rules as $item){
            if($item[1] == 'boolean')
                $bool_items = array_merge($bool_items, $item[0]);
        }
        return $bool_items;
    }

    public function getIs_bool(){
        if(in_array($this->name, $this->checkboxFields()))
            return true;
        return false;
    }

    public function getIs_list(){

        switch ($this->name){
            case 'brand':
                $this->_tmp_val = Brand::findOne($this->value)->name;
                break;
            case 'model':
                $this->_tmp_val = Model::findOne($this->value)->name;
                break;
            case 'body':
                $this->_tmp_val = CarBodyType::Map()[$this->value];
                break;
            case 'box':
                $this->_tmp_val = CarBox::Map()[$this->value];
                break;
            case 'engine':
                $this->_tmp_val = CarEngine::Map()[$this->value];
                break;
            case 'wheel':
                $this->_tmp_val = CarWheel::Map()[$this->value];
                break;
            case 'color':
                $item = CarColor::Map()[$this->value];
                $this->_tmp_val = Html::tag('span', '', ['style' => 'width: 20px; height: 20px; display: inline-block; border-radius: 3px; position: relative; top: 3px; '.$item['color']]). ' ' . $item['name'];
                break;
            case 'drive':
                $this->_tmp_val = CarDrive::Map()[$this->value];
                break;
            case 'climat':
                $this->_tmp_val = CarParams::Climate()[$this->value];
                break;
            case 'light':
                $this->_tmp_val = CarParams::Headlight()[$this->value];
                break;
            case 'salon':
                $this->_tmp_val = CarParams::Saloon()[$this->value];
                break;
            case 'audio':
                $this->_tmp_val = CarParams::Speakers()[$this->value];
                break;
            case 'st_wheel':
                $this->_tmp_val = CarParams::WheelAssist()[$this->value];
                break;
            case 'glass':
                $this->_tmp_val = CarParams::ElWindow()[$this->value];
                break;
        }

        if($this->_tmp_val == null) return false;

        return true;
    }



    public function getVal(){

        if($this->is_bool){
            if($this->value === 0) return false;
            return Yii::t('app', 'Yes');
        }

        if($this->is_list)
            return $this->_tmp_val;


        return $this->value;
    }

}
