<?php
/**
 * Created by PhpStorm.
 * User: York
 * Date: 09.10.2017
 * Time: 12:28
 */

namespace common\models\map;


use Yii;

class CarBox
{

    public static function Map()
    {
        return [
            1 => Yii::t('app', 'Manual'),
            2 => Yii::t('app', 'Automatic'),
            3 => Yii::t('app', 'Robot'),
            4 => Yii::t('app', 'CVT'),
        ];
    }
}