<?php
/**
 * Created by PhpStorm.
 * User: York
 * Date: 01.12.2017
 * Time: 17:58
 */

namespace common\models\map;


class Tires{

    public static function Diameter(){
        $ret = [];
        for($d=12;$d<31;$d++)
            $ret[$d] = $d;

        return $ret;
    }

    public static function Width(){
        $ret = [];
        for($d=125;$d<400;$d+=10)
            $ret[$d] = $d;

        return $ret;
    }

    public static function Height(){
        $ret = [];
        for($d=25;$d<100;$d+=5)
            $ret[$d] = $d;

        return $ret;
    }


}