<?php
/**
 * Created by PhpStorm.
 * User: York
 * Date: 09.10.2017
 * Time: 12:23
 */

namespace common\models\map;


use Yii;

class CarBodyType {

    public static function Map(){
        return [
            1 => Yii::t('app', 'Sedan'),
            2 => Yii::t('app', 'Hatchback'),
            3 => Yii::t('app', 'Versatile'),
            4 => Yii::t('app', 'SUV'),
            5 => Yii::t('app', 'Convertible'),
            6 => Yii::t('app', 'Crossover'),
            7 => Yii::t('app', 'Coupe'),
            8 => Yii::t('app', 'Limousine'),
            9 => Yii::t('app', 'Minivan'),
            10 => Yii::t('app', 'Pickup'),
            11 => Yii::t('app', 'Van'),
            12 => Yii::t('app', 'Minibus'),
        ];
    }

    public static function Full(){
        return [
            [
                'name' => Yii::t('app', 'Sedan'),
                'svg' => 'sedan',
                'id' => 1,
            ],
            [
                'name' => Yii::t('app', 'Hatchback'),
                'svg' => 'hetchback',
                'id' => 2,
            ],
            [
                'name' => Yii::t('app', 'Versatile'),
                'svg' => 'universal',
                'id' => 3,
            ],
            [
                'name' => Yii::t('app', 'SUV'),
                'svg' => 'vnedorojnik',
                'id' => 4,
            ],
            [
                'name' => Yii::t('app', 'Convertible'),
                'svg' => 'cabriolet',
                'id' => 5,
            ],
            [
                'name' => Yii::t('app', 'Crossover'),
                'svg' => 'crosover',
                'id' => 6,
            ],
            [
                'name' => Yii::t('app', 'Coupe'),
                'svg' => 'cupe',
                'id' => 7,
            ],
            [
                'name' => Yii::t('app', 'Limousine'),
                'svg' => 'limusin',
                'id' => 8,
            ],
            [
                'name' => Yii::t('app', 'Minivan'),
                'svg' => 'minivan',
                'id' => 9,
            ],
            [
                'name' => Yii::t('app', 'Pickup'),
                'svg' => 'picap',
                'id' => 10,
            ],
            [
                'name' => Yii::t('app', 'Van'),
                'svg' => 'furgon',
                'id' => 11,
            ],
            [
                'name' => Yii::t('app', 'Minibus'),
                'svg' => 'microautobus',
                'id' => 12,
            ],
        ];
    }

}