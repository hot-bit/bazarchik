<?php
/**
 * Created by PhpStorm.
 * User: York
 * Date: 01.12.2017
 * Time: 18:31
 */

namespace common\models\map;


class Size
{
    public static function SizeC(){
        return array(
            '1' => '50-56 sm (0-2 month)',
            '2' => '62-68 sm (2-6 month)',
            '3' => '74-80 sm (7-12 month)',
            '4' => '86-92 sm (1-2 years)',
            '5' => '98-104 sm (2-4 years)',
            '6' => '122-128 sm (6-8 years)',
            '7' => '134-140 sm (8-10 years)',
            '8' => '146-152 sm (10-12 years)',
        );
    }
    public static function SizeM(){
        return array(
            '1' => '38–39 (S)',
            '2' => '40–41 (S)',
            '3' => '42–43 (S)',
            '4' => '44–45 (S)',
            '5' => '46–48 (M)',
            '6' => '44–46 (S)',
            '7' => '46–48 (M)',
            '8' => '48–50 (L)',
            '9' => '50–52 (XL)',
            '10' => '52–54 (XXL)',
            '11' => '&gt; 54 (XXXL)',
        );
    }
    public static function SizeF(){
        return array(
            '1' => '35–36 (S)',
            '2' => '37–38 (S)',
            '3' => '38–39 (S)',
            '4' => '40–42 (XS)',
            '5' => '42–44 (S)',
            '6' => '44–46 (M)',
            '7' => '46–48 (L)',
            '8' => '48–50 (XL)',
            '9' => '&gt; 50 (XXL)',
        );
    }
}