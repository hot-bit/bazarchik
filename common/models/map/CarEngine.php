<?php
/**
 * Created by PhpStorm.
 * User: York
 * Date: 09.10.2017
 * Time: 12:28
 */

namespace common\models\map;


use Yii;

class CarEngine
{

    public static function Map()
    {
        return [
            1 => Yii::t('app', 'Petrol'),
            2 => Yii::t('app', 'Diesel'),
            3 => Yii::t('app', 'Hybrid'),
            4 => Yii::t('app', 'Electro'),
            5 => Yii::t('app', 'Gas'),
        ];
    }
}