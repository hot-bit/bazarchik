<?php
/**
 * Created by PhpStorm.
 * User: York
 * Date: 09.10.2017
 * Time: 12:28
 */

namespace common\models\map;


use Yii;

class CarParams
{

    public static function WheelAssist(){
        return [
            Yii::t('app', 'Not specify'),
            Yii::t('app', 'hydro-'),
            Yii::t('app', 'electro-'),
            Yii::t('app', 'hydroelectric-')
        ];
    }

    public static function Saloon(){
        return [
            Yii::t('app', 'Not specify'),
            Yii::t('app', 'Leather'),
            Yii::t('app', 'Cloth'),
            Yii::t('app', 'Velous'),
            Yii::t('app', 'Combined')
        ];
    }

    public static function Climate(){
        return [
            Yii::t('app', 'NO Air conditioning'),
            Yii::t('app', 'Air conditioning'),
            Yii::t('app', 'One-zone climat control'),
            Yii::t('app', 'Multi-zone climat control'),
        ];
    }

    public static function ElWindow(){
        return [
            Yii::t('app', '-manual'),
            Yii::t('app', 'Front windows only'),
            Yii::t('app', 'Front and rear windows'),
        ];
    }

    public static function Speakers(){
        return [
            Yii::t('app', 'no audio speakers'),
            Yii::t('app', '{num} audio speakers', ['num' => 2]),
            Yii::t('app', '{num} audio speakers', ['num' => 4]),
            Yii::t('app', '{num} audio speakers', ['num' => 6]),
            Yii::t('app', '{num} audio speakers', ['num' => '8+']),
        ];
    }
    
    public static function Headlight(){
        return [
            Yii::t('app', 'Net defined type'),
            Yii::t('app', 'Halogen'),
            Yii::t('app', 'Xenon'),
            Yii::t('app', 'LED'),
        ];
    }
}