<?php
/**
 * Created by PhpStorm.
 * User: York
 * Date: 01.12.2017
 * Time: 17:49
 */

namespace common\models\map;

use Yii;

class Disk{

    public static function Brand(){

    }

    public static function Type(){
        return [
            1 => Yii::t('app', 'Forged'),
            2 => Yii::t('app', 'Cst'),
            3 => Yii::t('app', 'Stamped'),
            4 => Yii::t('app', 'Spokes'),
            5 => Yii::t('app', 'National')
        ];
    }

    public static function Diameter(){
        $ret = [];
        for($d=7;$d<31;$d++)
            $ret[$d] = $d;

        return $ret;
    }
    public static function Width(){
        $ret = [];
        for($d=4;$d<13.5;$d+=0.5)
            $ret[] = $d;

        return $ret;
    }

    public static function Holes(){
        return [
            3 => 3, 4, 5, 6, 8 => 8, 9, 10
        ];
    }

    public static function HolesDiameter(){
        $arr = [98, 100, 105, 108, 110, 112, 114.3, 115, 118, 120, 125, 127, 130, 135, 139, 139.7, 140, 150, 160,
            165, 165.1, 170, 180, 200, 205, 256];
        $ret = [];
        foreach ($arr as $item)
            $ret[$item] = $item;

        return $ret;
    }

    public static function Outfly(){
        $outfly = array(-65, -50, -44, -40, -36, -35, -32, -30, -28, -25, -24, -22, -20, -16, -15, -14, -13, -12, -12, -10, -8,
            -7, -6, -5, -2, 0, 1, 2 ,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,23,23.5,24,25,26,27,28,29,30,31,31.5,
            32,33,34,35,36,36.6,37,37.5,38,39,39.5,40,40.5,41,41.3,41.5,42,43,43.5,43.8,44,45,45.5,46,47,47.5,48,49,49.5,50,51,52,
            52.2,52.5,53,54,55,56,57,58,59,60,62,63,65,66,67,68,70,75,83,100,102,105,105.5,106,107,108,110,111,115,116,118,120,123,124,
            125,126,127,128,129,130,132,133,134,135,136,138,140,142,143,144,145,147,148,152,156,157,161,163,165,167,168,172,175,185);
        $ret = [];
        foreach ($outfly as $item)
            $ret[$item] = $item;

        return $ret;
    }
}