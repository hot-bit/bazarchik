<?php
/**
 * Created by PhpStorm.
 * User: York
 * Date: 09.10.2017
 * Time: 12:28
 */

namespace common\models\map;


use Yii;

class CarColor
{

    public static function SmallMap(){
        $ret = [];

        foreach (self::Map() as $item){
            $ret[$item['id']] = $item['name'];
        }

        return $ret;
    }

    public static function Map()
    {
        return [
            1 => [
                'id' => 1,
                'name' => Yii::t('app', 'Red'),
                'color' => 'background: #FE0002'
            ],
            2 => [
                'id' => 2,
                'name' => Yii::t('app', 'Brown'),
                'color' => 'background: #974B00;'
            ],
            3 => [
                'id' => 3,
                'name' => Yii::t('app', 'Orange'),
                'color' => 'background: #FF7906;'
            ],
            4 => [
                'id' => 4,
                'name' => Yii::t('app', 'Beige'),
                'color' => 'background: #F1DDBA;'
            ],
            5 => [
                'id' => 5,
                'name' => Yii::t('app', 'Yellow'),
                'color' => 'background: #FFEB78;'
            ],
            6 => [
                'id' => 6,
                'name' => Yii::t('app', 'Green'),
                'color' => 'background: #28B26F;'
            ],
            7 => [
                'id' => 7,
                'name' => Yii::t('app', 'Blue'),
                'color' => 'background: #78C5F3;'
            ],
            8 => [
                'id' => 8,
                'name' => Yii::t('app', 'Dark blue'),
                'color' => 'background: #3144B6;'
            ],
            9 => [
                'id' => 9,
                'name' => Yii::t('app', 'Violet'),
                'color' => 'background: #7F00FF;'
            ],
            10 => [
                'id' => 10,
                'name' => Yii::t('app', 'Purple'),
                'color' => 'background: #C400AA;'
            ],
            11 => [
                'id' => 11,
                'name' => Yii::t('app', 'Pink'),
                'color' => 'background: #FFCADA;'
            ],
            12 => [
                'id' => 12,
                'name' => Yii::t('app', 'White'),
                'color' => 'background: #FFF; border: 1px solid #9B9B9B;'
            ],
            13 => [
                'id' => 13,
                'name' => Yii::t('app', 'Gray'),
                'color' => 'background: #A6A6A6;'
            ],
            14 => [
                'id' => 14,
                'name' => Yii::t('app', 'Black'),
                'color' => 'background: #000;'
            ],
            15 => [
                'id' => 15,
                'name' => Yii::t('app', 'Gold'),
                'color' => 'background: -webkit-gradient(linear,top left,bottom left,color-stop(0%,#FDB226),color-stop(100%,#FFEB78));background: -webkit-linear-gradient(top,#FDB226 0,#FFEB78 100%);background: -o-linear-gradient(top,#FDB226 0,#FFEB78 100%);background: -ms-linear-gradient(top,#FDB226 0,#FFEB78 100%);background-image: linear-gradient(to bottom,#FDB226 0,#FFEB78 100%);border: 1px solid #FDB226;'
            ],
            16 => [
                'id' => 16,
                'name' => Yii::t('app', 'Silver'),
                'color' => 'background: -webkit-gradient(linear,bottom left,top left,color-stop(0%,#FFF),color-stop(100%,#A6A6A6));background: -webkit-linear-gradient(bottom,#FFF 0,#A6A6A6 100%);background: -o-linear-gradient(bottom,#FFF 0,#A6A6A6 100%);background: -ms-linear-gradient(bottom,#FFF 0,#A6A6A6 100%);background-image: linear-gradient(to top,#FFF 0,#A6A6A6 100%);border: 1px solid #9B9B9B;'
            ],
        ];
    }
}
