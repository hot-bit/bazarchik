<?php
/**
 * Created by PhpStorm.
 * User: York
 * Date: 09.10.2017
 * Time: 12:28
 */

namespace common\models\map;


use Yii;

class CarDrive
{

    public static function Map()
    {
        return [
            1 => Yii::t('app', 'Forward'),
            2 => Yii::t('app', 'Rear'),
            3 => Yii::t('app', 'Full'),
        ];
    }
}