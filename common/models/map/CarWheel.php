<?php
/**
 * Created by PhpStorm.
 * User: York
 * Date: 09.10.2017
 * Time: 12:28
 */

namespace common\models\map;


use Yii;

class CarWheel
{

    public static function Map()
    {
        return [
            1 => Yii::t('app', 'Right'),
            2 => Yii::t('app', 'Left'),
        ];
    }
}