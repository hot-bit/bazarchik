<?php
/**
 * Created by PhpStorm.
 * User: yorks
 * Date: 12.06.2017
 * Time: 20:34
 */

namespace common\models;


use Yii;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;

class CMS{

    public static $short_lng;

    public static function PhoneRule(){
        return function ($attr, $params){

        };
    }


    /**
     * Возвращает текущий язык
     *
     * @return string
     *
     */
    public static function Lng(){
        if(!self::$short_lng)
            self::$short_lng = substr(Yii::$app->language, 0, 2);

        return in_array(self::$short_lng, ['en', 'ru', 'gr']) ? self::$short_lng : 'en';
    }


    public static function field($field){
        return $field.self::Lng();
    }

    /**
     * Получить СВГ иконку
     *
     * @param string $name
     * @param array $params
     * @return string
     */
    public static function svg($name, $params = []){

        $item_color = isset($params['color']) ? 'fill: '.$params['color'].';' : '';
        $item_class = isset($params['class']) ? $params['class'] : '';
        $item_size = isset($params['size']) ? 'font-size: '.$params['size'].';' : '';

        return '<svg class="icon icon-'.$name.' '. $item_class.'" style="'.$item_color.' '.$item_size.'">
                <use xlink:href="'.Yii::getAlias('@dist').'/img/icon.svg#icon_'.$name.'"></use>
            </svg>';
    }

    public static function _($name, $params = [], $category = 'app'){
        return Yii::t($category, $name, $params);
    }


    public static function Block($block_name, $clear = false){
        /* @var $model TextBlocks */

        $model = TextBlocks::find()->where(['name' => $block_name])->one();

        if(!$model){
            return Html::tag('div', Yii::t('app', 'Block "{block}" not found', ['block' => '<b>'.$block_name.'</b>']), ['style' => 'color: red; border: 1px solid red; padding: 5px 10px; display: inline-block;    background: #fce9e9; margin: 10px;']);
        }

        if($clear)
            return strip_tags($model->text);
        return $model->text;
    }


    public static function GenerateImageName(){
        return date('dmY_His').'_'.Yii::$app->security->generateRandomString(8);
    }

    public static function dayList(){
        $arr = [];

        for($i = 1; $i <= 31; $i++)
            $arr[$i] = $i;

        return $arr;
    }

    public static function monthList(){
        return [
            1 => Yii::t('app', 'January'),
            2 => Yii::t('app', 'February'),
            3 => Yii::t('app', 'March'),
            4 => Yii::t('app', 'April'),
            5 => Yii::t('app', 'May'),
            6 => Yii::t('app', 'June'),
            7 => Yii::t('app', 'Jule'),
            8 => Yii::t('app', 'August'),
            9 => Yii::t('app', 'September'),
            10 => Yii::t('app', 'October'),
            11 => Yii::t('app', 'November'),
            12 => Yii::t('app', 'December'),
        ];
    }


    public static function yearList(){
        $arr = [];

        for($i = date('Y') - 10; $i >= date('Y') - 100; $i--)
            $arr[$i] = $i;

        return $arr;
    }

    /**
     * Сгенерировать и отправить смс код пользователю
     *
     * @param User $user
     */
    public static function SendSmsCode(User $user){

        $phone = $user->phone_code.$user->phone;
        $user->phone_try = 3;
        $user->sms_code = rand(1000, 9999);
        $user->save();

        self::SendSms($phone, 'confirm code: '.$user->sms_code);
    }

    public static function SendSms($phone, $text, $debug = false){
        if( $curl = curl_init() ) {
            $post = 'AuthKey=x459klSSS34saTa29&Telephone='.$phone.'&Header=CyprusBazar&Body='.$text;
            curl_setopt($curl, CURLOPT_URL, 'http://www.commtor.com/webapi/AccountApi/PostMessage');
            curl_setopt($curl, CURLOPT_RETURNTRANSFER,true);
            curl_setopt($curl, CURLOPT_POST, true);
            curl_setopt($curl, CURLOPT_POSTFIELDS, $post);
            $out = curl_exec($curl);
            curl_close($curl);

            if($debug) echo $out;
        }
    }


    /**
     * Трансилитерация
     *
     * @param $str
     * @return null|string|string[]
     */
    public static function Translit( $str ) {
        $iso9_table = array(
            'А' => 'A', 'Б' => 'B', 'В' => 'V', 'Г' => 'G', 'Ѓ' => 'G',
            'Ґ' => 'G`', 'Д' => 'D', 'Е' => 'E', 'Ё' => 'YO', 'Є' => 'YE',
            'Ж' => 'ZH', 'З' => 'Z', 'Ѕ' => 'Z', 'И' => 'I', 'Й' => 'Y',
            'Ј' => 'J', 'І' => 'I', 'Ї' => 'YI', 'К' => 'K', 'Ќ' => 'K',
            'Л' => 'L', 'Љ' => 'L', 'М' => 'M', 'Н' => 'N', 'Њ' => 'N',
            'О' => 'O', 'П' => 'P', 'Р' => 'R', 'С' => 'S', 'Т' => 'T',
            'У' => 'U', 'Ў' => 'U', 'Ф' => 'F', 'Х' => 'H', 'Ц' => 'TS',
            'Ч' => 'CH', 'Џ' => 'DH', 'Ш' => 'SH', 'Щ' => 'SHH', 'Ъ' => '',
            'Ы' => 'YI', 'Ь' => '', 'Э' => 'E', 'Ю' => 'YU', 'Я' => 'YA',
            'а' => 'a', 'б' => 'b', 'в' => 'v', 'г' => 'g', 'ѓ' => 'g',
            'ґ' => 'g', 'д' => 'd', 'е' => 'e', 'ё' => 'yo', 'є' => 'ye',
            'ж' => 'zh', 'з' => 'z', 'ѕ' => 'z', 'и' => 'i', 'й' => 'y',
            'ј' => 'j', 'і' => 'i', 'ї' => 'yi', 'к' => 'k', 'ќ' => 'k',
            'л' => 'l', 'љ' => 'l', 'м' => 'm', 'н' => 'n', 'њ' => 'n',
            'о' => 'o', 'п' => 'p', 'р' => 'r', 'с' => 's', 'т' => 't',
            'у' => 'u', 'ў' => 'u', 'ф' => 'f', 'х' => 'h', 'ц' => 'ts',
            'ч' => 'ch', 'џ' => 'dh', 'ш' => 'sh', 'щ' => 'shh', 'ь' => '',
            'ы' => 'yi', 'ъ' => "", 'э' => 'e', 'ю' => 'yu', 'я' => 'ya'
        );

        $str = strtr( $str, $iso9_table );
        $str = preg_replace( "/\&/", 'and', $str );
        $str = preg_replace( "/[^A-Za-z0-9`'_\-\.]/", '-', $str );

        return $str;
    }

    /**
     * Преобразовать модель в данные для апи. Удалить или добавить некоторые данные
     *
     * @param $model
     * @param array $fields
     * @param array $removeFields
     * @return array
     */
    public static function apiExport($model, $fields = [], $removeFields = ['created_at', 'updated_at'])
    {
        $modelArray = [];
        if(is_array($model)){
            foreach ($model as $item){
                $arr = [];
                $modelArrayItem = ArrayHelper::toArray($item);
                foreach ($fields as $field)
                    $arr[$field] = $item->{$field};

                // Удалить поля
                if ($removeFields)
                    foreach ($removeFields as $field)
                        unset($modelArrayItem[$field]);

                $modelArray[] = $modelArrayItem + $arr;
            }
        }
        else{
            $modelArray = ArrayHelper::toArray($model);
            foreach ($fields as $field)
                $modelArray[$field] = $model->{$field};

            // Удалить поля
            if ($removeFields)
                foreach ($removeFields as $field)
                    unset($modelArray[$field]);
        }

        return $modelArray;
    }
}