<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "orders".
 *
 * @property integer $id
 * @property integer $id_adv
 * @property integer $user_id
 * @property string $user_email
 * @property integer $price
 * @property integer $date
 * @property string $items
 * @property string $token
 * @property string $transaction_id
 * @property integer $status
 * @property mixed $f_status
 * @property string $f_price
 * @property string $f_items
 * @property integer $type
 */
class Orders extends \yii\db\ActiveRecord
{

    const TYPE_ADV = 0;
    const TYPE_BALANCE = 1;
    const STATUS_OK = 1;
    const STATUS_CANCEL = 0;

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'orders';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id_adv', 'price', 'items'], 'required'],
            [['id_adv', 'user_id', 'price', 'date', 'status', 'type'], 'integer'],
            [['items'], 'string'],
            [['user_email', 'token', 'transaction_id'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'id_adv' => 'Adv',
            'user_id' => 'User ID',
            'user_email' => 'User Email',
            'price' => 'Price',
            'f_price' => 'Price',
            'date' => 'Date',
            'items' => 'Items',
            'f_items' => 'Items',
            'token' => 'Token',
            'transaction_id' => 'Transaction ID',
            'status' => 'Status',
            'f_status' => 'Status',
            'type' => 'Type',
        ];
    }

    /**
     * Получить мои ордера
     *
     * @return \yii\db\ActiveQuery
     */
    public static function my(){
        return self::find()->where(['user_id' => Yii::$app->user->id])->orderBy('date DESC');
    }

    /**
     * Получить форматированный статус
     *
     * @return string
     */
    public function getF_status(){
        if($this->status == self::STATUS_OK)
            return Yii::t('app', 'OK');
        return Yii::t('app', 'Canceled');
    }

    /**
     * Получить форматированный список покупок
     *
     * @return string
     */
    public function getF_items(){
        if($this->type == 1)
            return Yii::t('app', 'Balance');

        if($this->items)
            return implode(', ', $this->items);
        return '';
    }

    /**
     * Получить форматированную цену
     *
     * @return string
     */
    public function getF_price(){
        return '€'.number_format($this->price);
    }

    public function beforeSave($insert){
        if($insert){
            $this->date = time();
            $this->user_id = Yii::$app->user->id;
            $this->user_email = Yii::$app->user->identity->email;
            $this->transaction_id = strtoupper(Yii::$app->security->generateRandomString(16));
        }

        if($this->type == 0)
            $this->items = json_encode($this->items);

        return parent::beforeSave($insert);
    }

    public function afterFind()
    {
        parent::afterFind();

        if ($this->type == 0) {
            $data = json_decode($this->items);

            if (empty($data)) $this->items = [];
            else {
                $items = [];
                foreach ($data as $k => $val)
                    if ($val == true) $items[] = $k;

                $this->items = $items;
            }
        }
    }
}
