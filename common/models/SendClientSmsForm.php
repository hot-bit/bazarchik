<?php
/**
 * Created by PhpStorm.
 * User: yorks
 * Date: 19.06.2017
 * Time: 14:09
 */

namespace common\models;


use yii\base\Model;

class SendClientSmsForm extends Model{

    public $phone;
    public $text;
    public $template_id;
    public $status = true;

    public function rules()
    {
        return [
            [['phone', 'text'], 'required'],
            ['status', 'boolean'],
            ['template_id', 'integer']
        ];
    }

    public function send(){
        if( $curl = curl_init() ) {
            $post = sprintf('AuthKey=x459klSSS34saTa29&Telephone=357%d&Header=CyprusBazar&Body=%s', $this->phone, $this->text);
            curl_setopt($curl, CURLOPT_URL, 'http://www.commtor.com/webapi/AccountApi/PostMessage');
            curl_setopt($curl, CURLOPT_RETURNTRANSFER,true);
            curl_setopt($curl, CURLOPT_POST, true);
            curl_setopt($curl, CURLOPT_POSTFIELDS, $post);
            curl_exec($curl);
            curl_close($curl);
        }
        return true;
    }

}