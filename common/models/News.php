<?php

namespace common\models;

use Yii;
use yii\helpers\Url;

/**
 * This is the model class for table "news".
 *
 * @property integer $id
 * @property string $title_ru
 * @property string $title_en
 * @property string $title_gr
 * @property string $text_ru
 * @property string $text_en
 * @property string $text_gr
 * @property string $image
 * @property integer $date
 * @property integer $status
 * @property mixed $_link
 * @property string $link
 */
class News extends \yii\db\ActiveRecord
{

    const STATUS_NOT_ACTIVE = 0;
    const STATUS_ACTIVE = 1;

    public $imageFile;
    public function behaviors()
    {
        return [
            'lang' => [
                'class' => 'common\behaviors\BazarLanguage',
                'fields' => ['title', 'text']
            ],
            'image' => [
                'class' => 'common\behaviors\CMSImage',
                'directory' => Yii::getAlias('@news'),
                'field' => 'image',
                'file' => 'imageFile'
            ]
        ];
    }

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'news';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['title_en', 'text_en', 'image', 'date', 'status', 'link'], 'required'],
            [['text_ru', 'text_en', 'text_gr'], 'string'],
            [['date', 'status'], 'integer'],
            [['title_ru', 'title_en', 'title_gr', 'image', 'link'], 'string', 'max' => 255],
            [['imageFile'], 'file', 'skipOnEmpty' => true, 'extensions' => 'png, jpg'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'title_ru' => 'Title Ru',
            'title_en' => 'Title En',
            'title_gr' => 'Title Gr',
            'text_ru' => 'Text Ru',
            'text_en' => 'Text En',
            'text_gr' => 'Text Gr',
            'image' => 'Image',
            'date' => 'Date',
            'status' => 'Status',
            'link' => 'Link',
        ];
    }

    public function get_link()
    {
        return Url::to(['news/get', 'id' => $this->id, 'url' => $this->link]);
    }

}
