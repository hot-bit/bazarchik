<?php

namespace common\models;

use Yii;
use yii\data\ActiveDataProvider;
use yii\helpers\ArrayHelper;
use yii\helpers\Url;

/**
 * This is the model class for table "brand".
 *
 * @property integer $id
 * @property string $name
 * @property string $url
 * @property integer $count_new
 * @property integer $count_used
 * @property integer $count_rent
 * @property integer $count_all
 * @property string $link
 * @property Model[] $_models
 */
class Brand extends \yii\db\ActiveRecord
{


    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'brand';
    }

    public static function map(){
        return ArrayHelper::map(Brand::find()->all(), 'id', 'name');
    }

    public static function findByUrl($url){
        return self::find()->where(['url' => $url])->one();
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['name', 'count_new', 'count_used', 'url', 'count_rent'], 'required'],
            [['count_new', 'count_used', 'count_rent'], 'integer'],
            [[ 'url', 'name'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'name' => Yii::t('app', 'Name'),
            'url' => Yii::t('app', 'Url'),
            'count_new' => Yii::t('app', 'Count New'),
            'count_used' => Yii::t('app', 'Count Used'),
            'count_rent' => Yii::t('app', 'Count Rent'),
        ];
    }

    public function getLink(){
        return Url::to(['adv/index', 'cat' => $this->url]);
    }

    public function getCount_all(){
        return $this->count_used;
    }

    public function get_models(){
        return $this->hasMany(Model::className(), ['brand_id' => 'id'])->orderBy('count_used DESC');
    }

    public function getF_name(){
        return $this->name;
    }
 
}
