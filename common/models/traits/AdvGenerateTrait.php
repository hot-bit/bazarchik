<?php
/**
 * Created by PhpStorm.
 * User: York
 * Date: 03.01.2018
 * Time: 10:30
 */

namespace common\models\traits;


use common\models\Adv;
use common\models\Brand;
use common\models\map\CarBodyType;
use common\models\map\CarBox;
use common\models\map\CarColor;
use common\models\map\CarDrive;
use common\models\map\CarEngine;
use common\models\map\CarParams;
use common\models\map\CarWheel;
use common\models\Model;

trait AdvGenerateTrait
{

    public function GenerateTitleCar(){
        $params = $this->params_arr;

        $car_brand = Brand::findOne($params['brand']);
        $car_model = Model::findOne($params['model']);
        $name = '';
        $name .= $car_brand->name;                      // Марка
        $name .= ' '.$car_model->name;                  // Модель
        $name .= ' ('.$params['year'].'),';              // Год
        $name .= ' '.CarBodyType::Map()[$params['body']];   // Кузов
        if($params['volume'])
            $name .= ' '.$params['volume'].'L';                  // Мощность двигателя
        $name .= ' '.CarEngine::Map()[$params['engine']];   // Тип двигателя
        $name .= ' '.CarBox::Map()[$params['box']];         // Каробка передач
        $name .= ' for sale '.$this->_city->name_en;     // Город

        return $name;
    }

    public function GenerateDescriptionCar(){
        $params = $this->params_arr;

        //Peugeot 307 (2006) - 1.6L Diesel, Manual, 210000 km, silver, 5 Doors, Forward wd drive, Right-hand drive
        $car_brand = Brand::findOne($params['brand']);
        $car_model = Model::findOne($params['model']);

        $name = '';
        $name .= $car_brand->name;                      // Марка
        $name .= ' '.$car_model->name;                  // Модель
        $name .= ' ('.$params['year'].'),';              // Год
        $name .= ' '.CarBodyType::Map()[$params['body']];   // Кузов
        if($params['volume'])
            $name .= ' - '.$params['volume'].'L';                  // Мощность двигателя
        $name .= ' '.CarEngine::Map()[$params['engine']];   // Тип двигателя
        $name .= ', '.CarBox::Map()[$params['box']];         // Каробка передач
        $name .= ', '.$params['running'].' km';
        $name .= ', '.CarColor::SmallMap()[$params['color']];
        $name .= ', '.$params['door'].' Doors';
        $name .= ', '.CarDrive::Map()[$params['drive']];
        $name .= ', '.CarWheel::Map()[$params['wheel']];

        return $name;
    }

}