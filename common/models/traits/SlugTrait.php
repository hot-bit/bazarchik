<?php
/**
 * Created by PhpStorm.
 * User: York
 * Date: 03.01.2018
 * Time: 10:47
 */

namespace common\models\traits;


use common\models\CMS;

trait SlugTrait
{

    public function getSlug(){
        if(isset($this->title))
            $name = $this->title;
        else
            $name = $this->name_en;

        return CMS::Translit($name);

    }


}