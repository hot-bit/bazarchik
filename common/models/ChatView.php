<?php

namespace common\models;

use Yii;
use yii\behaviors\TimestampBehavior;

/**
 * This is the model class for table "chat_view".
 *
 * @property integer $id
 * @property integer $user_id
 * @property integer $created_at
 * @property integer $updated_at
 * @property integer $is_viewed
 * @property integer $message_id
 * @property integer $chat_id
 *
 * @property Chat $chat
 * @property ChatMessage $message
 * @property User $user
 */
class ChatView extends \yii\db\ActiveRecord
{

    const NOT_VIEWED = 0;
    const VIEWED = 1;


    public function behaviors()
    {
        return [TimestampBehavior::className()];
    }

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'chat_view';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['user_id', 'created_at', 'updated_at', 'is_viewed', 'message_id', 'chat_id'], 'integer'],
            [['chat_id'], 'exist', 'skipOnError' => true, 'targetClass' => Chat::className(), 'targetAttribute' => ['chat_id' => 'id']],
            [['message_id'], 'exist', 'skipOnError' => true, 'targetClass' => ChatMessage::className(), 'targetAttribute' => ['message_id' => 'id']],
            [['user_id'], 'exist', 'skipOnError' => true, 'targetClass' => User::className(), 'targetAttribute' => ['user_id' => 'id']],
            ['is_viewed', 'boolean'],
            ['is_viewed', 'default', 'value' => self::NOT_VIEWED ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'user_id' => Yii::t('app', 'User ID'),
            'created_at' => Yii::t('app', 'Created At'),
            'updated_at' => Yii::t('app', 'Updated At'),
            'is_viewed' => Yii::t('app', 'Is Viewed'),
            'message_id' => Yii::t('app', 'Message ID'),
            'chat_id' => Yii::t('app', 'Chat ID'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getChat()
    {
        return $this->hasOne(Chat::className(), ['id' => 'chat_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getMessage()
    {
        return $this->hasOne(ChatMessage::className(), ['id' => 'message_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUser()
    {
        return $this->hasOne(User::className(), ['id' => 'user_id']);
    }

    /** Просмотреть все непросмотренные сообщения
     * @param $id
     * @return bool
     */
    public static function ViewAll($id){
        $chat = Chat::findByUsers(Yii::$app->user->id, $id);
        $model = ChatView::findByChat($chat->id);
        if(!$model) return true;
        foreach ($model as $item){
            $item->is_viewed = self::VIEWED;
            $item->save();
        }
        return true;
    }

    /** Найти непрочитанные сообщения по id чата и по пользователю
     * @param $id
     * @return array|Chat[]|ChatView[]|\yii\db\ActiveRecord[]
     */
    public static function findByChat($id)
    {
        return ChatView::find()
            ->where(['chat_id' => $id])
            ->andWhere(['user_id' => Yii::$app->user->id])
            ->andWhere(['is_viewed' => self::NOT_VIEWED])
            ->all();
    }

    /** Найти непрочитанные сообщения по пользователю
     * @return array|Chat[]|ChatView[]|\yii\db\ActiveRecord[]
     */
    public static function findByUser(){
        return ChatView::find()
            ->where(['user_id' => Yii::$app->user->id])
            ->andWhere(['is_viewed' => self::NOT_VIEWED])
            ->all();
    }
}
