<?php

namespace common\models;

use Yii;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;

/**
 * This is the model class for table "clients_template".
 *
 * @property integer $id
 * @property string $name
 * @property string $text
 * @property integer $type
 * @property integer $status
 */
class ClientsTemplate extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'clients_template';
    }

    public static function map($type){
        return ArrayHelper::map(self::find()->where(['type' => $type])->all(), 'id', 'name');
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['name', 'text', 'type', 'status'], 'required'],
            [['text'], 'string'],
            [['type', 'status'], 'integer'],
            [['name'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'name' => Yii::t('app', 'Name'),
            'text' => Yii::t('app', 'Text'),
            'type' => Yii::t('app', 'Type'),
            'status' => Yii::t('app', 'Status'),
        ];
    }

    public function getF_status(){
        return $this->status ? Html::tag('span', 'Active', ['class' => 'badge badge-success']) :  Html::tag('span', 'Not active', ['class' => 'badge badge-default']);
    }


}
