<?php

namespace common\models;

use Imagine\Image\Point;
use yii\imagine\Image;
use Imagine\Gd;
use Imagine\Image\Box;
use Imagine\Image\BoxInterface;

use Yii;

/**
 * This is the model class for table "adv_images".
 *
 * @property integer $id
 * @property integer $adv_id
 * @property string $name
 * @property string $dir
 * @property string $uuid
 * @property string $directory
 * @property string $real_path
 * @property integer $main
 * @property integer $m
 * @property integer $l
 * @property integer $s
 * @property Adv $_adv
 * @property integer $craftMobile
 */
class AdvImages extends \yii\db\ActiveRecord
{

    private $host;

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'adv_images';
    }

    public function __construct(array $config = []){
        $this->host = Yii::getAlias('@host');
        return parent::__construct($config);
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['adv_id', 'name', 'dir'], 'required'],
            [['adv_id', 'main', 'm', 'l', 's', 'craftMobile'], 'integer'],
            [['name'], 'string', 'max' => 50],
            [['dir'], 'string', 'max' => 20],
            [['uuid'], 'string', 'max' => 256],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'adv_id' => 'Adv ID',
            'name' => 'Name',
            'dir' => 'Dir',
            'main' => 'Main',
            'm' => 'M',
            'l' => 'L',
            's' => 'S',
            'uuid' => 'uuid',
            'craftMobile' => 'Craft Mobile',
        ];
    }

    public function beforeSave($insert){
        if($insert){
            $otherImages = self::find()->where(['adv_id' => $this->adv_id])->andWhere(['main' => 1])->count();
            if(!$otherImages) $this->main = 1;
        }
        return parent::beforeSave($insert);
    }

    /**
     * Ссылка на картинку
     *
     * @param bool $size
     * @param bool $wm
     * @return string
     */
    public function Path($size = false, $wm = false){
        // Проверить изображение
        $this->CheckImage();

        // Если нет файла
        if(!is_file($this->host.$this->real_path))
            return Yii::getAlias('@dist').Yii::$app->params['noimage'];

        if($size)
            $path = $this->Size($size);
        else $path = $this->real_path;

        return $path;
    }

    /**
     * Проверить существует ли изображение проверить копии изображения
     */
    private function CheckImage(){
        if(!is_dir($this->host.$this->directory)) mkdir($this->host.$this->directory, 0777, true);

        // Проверить копии с размерами
        $this->CheckCopy();
    }

    /**
     * Получить картинку определенного размера
     *
     * @param $size_name
     * @return string
     */
    public function Size($size_name){
        return $this->directory.$size_name.'/'.$this->name;
    }

    /**
     * Полная директория изображения
     *
     * @return string
     */
    public function getDirectory(){
        return Yii::getAlias('@adv').'/'.$this->dir.'/';
    }

    /**
     * Генерированный путь до оригинальной картинки
     *
     * @return string
     */
    public function getReal_path(){
        return $this->directory.$this->name;
    }

    /**
     * Создать копии изображения
     * - Большое изображение
     * - Изображение в карточке
     * - Изображение в каталоге
     * - Мини иконка для превью
     *
     * @param array $size
     * @param $size_name
     * @return boolean
     */
    private function MakeCopy(array $size, $size_name){

        if(!is_dir($this->host.$this->directory.$size_name)) mkdir($this->host.$this->directory.$size_name, 0777, true);

        if(!is_file($this->host.$this->real_path)) return false;

        $image = Image::getImagine()->open($this->host.$this->real_path);

        $is_crop = isset($size[3]) && $size[3] == 'crop' ? true : false;

        $image->thumbnail(new Box($size[0], $size[1]), $is_crop ? 'outbound' : 'inset')
            ->save($this->host.$this->Size($size_name), ['quality' => $size[2]]);

        return true;
    }

    /**
     * Получить ссылку на изображение с водяным знаком
     *
     * @param $size_name
     * @return string
     */
    public function Watermark($size_name){
        if(!is_file($this->host.$this->directory.$size_name.'/wm'.$this->name))
            $this->MakeWatermark($size_name);

        return $this->directory.$size_name.'/wm'.$this->name;
    }

    /**
     * Создать изображение с водяным знаком
     *
     * @param $size_name
     */
    private function MakeWatermark($size_name){
//        if(!is_dir($this->host.$this->directory.'wm/'.$size_name)) mkdir($this->host.$this->directory.'wm/'.$size_name, 0777, true);

//        if($size_name == 'catalog')
//            $watermark_image = '/dist/img/bazar_logo_sm.png';
//        else
            $watermark_image = '/img/watermark.png';

        if(is_file($this->host.$this->Size($size_name))){
            $new_image = Image::watermark($this->host.$this->Size($size_name), $this->host.'/frontend/web'.$watermark_image);
            $new_image->save($this->host.$this->directory.$size_name.'/wm'.$this->name);
        }

    }

    /**
     * Проверить копии изображения и создать если отсутствуют
     */
    private function CheckCopy(){
        $image_sizes = Yii::$app->params['images']['adv'];
        foreach ($image_sizes as $size_name => $size){
            if(!is_file($this->host.$this->directory.$size_name.'/'.$this->name)){
                $this->MakeCopy($size, $size_name);
            }
        }
    }

    /**
     * Скопировать изображение с сайта (используется для дев. серваков)
     */
    private function DownloadFromSite(){

        @copy('https://www.bazar-cy.com'.$this->real_path, $this->host.$this->real_path);
    }

    /**
     * Получить объявление
     *
     * @return \yii\db\ActiveQuery
     */
    public function get_adv(){
        return $this->hasOne(Adv::className(), ['id' => 'adv_id']);
    }


    public function beforeDelete(){
        $image_sizes = Yii::$app->params['images']['adv'];
        unlink($_SERVER['DOCUMENT_ROOT'].Yii::getAlias('@adv').'/'.$this->dir.'/'.$this->name);
        foreach ($image_sizes as $size_name => $size){
            @unlink($_SERVER['DOCUMENT_ROOT'].Yii::getAlias('@adv').'/'.$this->dir.'/'.$size_name.'/'.$this->name);
        }
        return parent::beforeDelete();
    }

    /**
     * Вращать изображение влево
     */
    public function RotateLeft(){
        $image = Image::getImagine()->open($this->host.$this->real_path);
        $image->rotate(-90);
        $image->save($this->host.$this->real_path);
    }

    /**
     * Вращать изображение вправо
     */
    public function RotateRight(){
        $image = Image::getImagine()->open($this->host.$this->real_path);
        $image->rotate(90);
        $image->save($this->host.$this->real_path);
    }
}
