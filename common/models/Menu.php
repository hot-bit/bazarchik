<?php
/**
 * Created by PhpStorm.
 * User: yorks
 * Date: 09.05.2017
 * Time: 18:06
 */

namespace common\models;


use Yii;
use yii\helpers\Html;

class Menu{
    public static function AdminMenu(){
        $item_class = 'nav-item';
        $submenu_class = 'nav-dropdown-items';

        return [
            [
                'label' => self::icon('speedometer').'Home',
                'url' => ['/default/index'],
                'options' => ['class' => $item_class],
                'encode' => false,
            ],
            [
                'label' => self::icon('grid').'Adv',
                'url' => ['/adv/index'],
                'options' => ['class' => $item_class],
                'encode' => false,
            ],
            [
                'label' =>  self::icon('people').'Users',
                'options' => ['class' => $item_class.' nav-dropdown'],
                'url' => '#',
                'encode' => false,
                'template' => '<a href="{url}" class="nav-link nav-dropdown-toggle">{label}</a>',
                'items' => [
                    [
                        'label' =>  self::icon('directions').'Groups',
                        'url' => ['/role/index'],
                        'options' => ['class' => $item_class],
                        'encode' => false,
                    ],
                    [
                        'label' =>  self::icon('user-follow').'Clients',
                        'url' => ['/client/index'],
                        'options' => ['class' => $item_class],
                        'encode' => false,
                    ],
                    [
                        'label' =>  self::icon('user').'Users',
                        'url' => ['/user/index'],
                        'options' => ['class' => $item_class],
                        'encode' => false,
                    ],
                ],
            ],
            [
                'label' =>  self::icon('screen-desktop').'Web Content',
                'options' => ['class' => $item_class.' nav-dropdown'],
                'url' => '#',
                'encode' => false,
                'template' => '<a href="{url}" class="nav-link nav-dropdown-toggle">{label}</a>',
                'items' => [
                    [
                        'label' =>  self::icon('doc').'Pages',
                        'url' => ['/page/index'],
                        'options' => ['class' => $item_class],
                        'encode' => false,
                    ],
                    [
                        'label' =>  self::icon('speech').'News',
                        'url' => ['/news/index'],
                        'options' => ['class' => $item_class],
                        'encode' => false,
                    ],
                    [
                        'label' =>  self::icon('question').'FAQ',
                        'url' => ['/faq/index'],
                        'options' => ['class' => $item_class],
                        'encode' => false,
                    ],
                ],
            ],
            [
                'label' =>  self::icon('social-dropbox').'Database',
                'options' => ['class' => $item_class.' nav-dropdown'],
                'url' => '#',
                'encode' => false,
                'template' => '<a href="{url}" class="nav-link nav-dropdown-toggle">{label}</a>',
                'items' => [
                    [
                        'label' =>  self::icon('list').'Categories',
                        'url' => ['/category/index'],
                        'options' => ['class' => $item_class],
                        'encode' => false,
                    ],
                    [
                        'label' =>  self::icon('globe').'Seo',
                        'url' => ['/seo/index'],
                        'options' => ['class' => $item_class],
                        'encode' => false,
                    ],
                    [
                        'label' =>  self::icon('globe-alt').'City',
                        'url' => ['/city/index'],
                        'options' => ['class' => $item_class],
                        'encode' => false,
                    ],
                    [
                        'label' =>  self::icon('map').'Area',
                        'url' => ['/area/index'],
                        'options' => ['class' => $item_class],
                        'encode' => false,
                    ],
                    [
                        'label' =>  self::icon('puzzle').'Text blocks',
                        'url' => ['/text/index'],
                        'options' => ['class' => $item_class],
                        'encode' => false,
                    ],
                    [
                        'label' =>  self::fa('car').'Car brands',
                        'url' => ['/brands/default/index'],
                        'options' => ['class' => $item_class],
                        'encode' => false,
                    ],
                    [
                        'label' =>  self::fa('motorcycle').'Moto brands',
                        'url' => ['/brands/moto/index'],
                        'options' => ['class' => $item_class],
                        'encode' => false,
                    ],
                ],
            ],
            [
                'label' =>  self::icon('credit-card').'Money',
                'options' => ['class' => $item_class.' nav-dropdown'],
                'url' => '#',
                'encode' => false,
                'template' => '<a href="{url}" class="nav-link nav-dropdown-toggle">{label}</a>',
                'items' => [
                    [
                        'label' =>  self::icon('basket-loaded').'Orders',
                        'url' => ['/order/index'],
                        'options' => ['class' => $item_class],
                        'encode' => false,
                    ],
                    [
                        'label' =>  self::icon('picture').'Banners',
                        'url' => ['/banner/index'],
                        'options' => ['class' => $item_class],
                        'encode' => false,
                    ],
                    [
                        'label' =>  self::icon('present').'Coupons',
                        'url' => ['/coupon/index'],
                        'options' => ['class' => $item_class],
                        'encode' => false,
                    ],
                    [
                        'label' =>  self::icon('briefcase').'Shops',
                        'url' => ['/shop/index'],
                        'options' => ['class' => $item_class],
                        'encode' => false,
                    ],
                ],
            ],
            [
                'label' => self::icon('question').'Translate',
                'url' => ['/translate/index'],
                'options' => ['class' => $item_class],
                'encode' => false,
            ],
        ];
    }

    private static function icon($icon){
        return Html::tag('i', '', ['class' => 'icon-'.$icon]);
    }
    private static function fa($icon){
        return Html::tag('i', '', ['class' => 'fa fa-'.$icon]);
    }

    public static function ProfileMenu(){
        /** @var User $user */
        $user = Yii::$app->user->identity;

        $shop_link = $user->business_link ? ['shop/get', 'url' => $user->business_link] : ['shop/get', 'id' => $user->id];
        $shop = [
            'icon' => 'logMyShop.svg',
            'name' => Yii::t('app', 'My shop'),
            'link' => $shop_link,
            'hide' => $user->is_business ? false : true
        ];
        return [
            [
                'icon' => 'logMyAds.svg',
                'name' => Yii::t('app', 'My ads'),
                'link' => ['profile/ads'],
            ],
            [
                'icon' => 'logMySubscr.svg',
                'name' => Yii::t('app', 'My subscribes'),
                'link' => ['profile/subscribes'],
            ],
            [
                'icon' => 'likestrokeUser.svg',
                'name' => Yii::t('app', 'Favorite'),
                'link' => ['site/favorite'],
            ],
            [
                'icon' => 'logMyOperations.svg',
                'name' => Yii::t('app', 'Operations'),
                'link' => ['profile/operations'],
            ],
            [
                'icon' => 'logMyBonuses.svg',
                'name' => Yii::t('app', 'Exchange of bonuses'),
                'link' => ['profile/bonus'],
            ],
            [
                'icon' => 'logMySettings.svg',
                'name' => Yii::t('app', 'Profile settings'),
                'link' => ['profile/settings'],
            ],
            $shop
        ];
    }
}