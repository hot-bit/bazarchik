<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "coupon".
 *
 * @property integer $id
 * @property integer $adv_id
 * @property integer $status
 * @property integer $date
 * @property integer $date_active
 * @property string $code
 * @property integer $user_id
 */
class Coupon extends \yii\db\ActiveRecord
{

    const STATUS_FREE = 0;
    const STATUS_SOLD = 1;

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'coupon';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['adv_id', 'status', 'date', 'date_active', 'code', 'user_id'], 'required'],
            [['adv_id', 'status', 'date', 'date_active', 'user_id'], 'integer'],
            [['code'], 'string', 'max' => 20],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'adv_id' => 'Adv ID',
            'status' => 'Status',
            'date' => 'Date',
            'date_active' => 'Date Active',
            'code' => 'Code',
            'user_id' => 'User ID',
        ];
    }

    public static function findFree(){
        return self::find()->where(['status' => self::STATUS_FREE])->all();
    }

    public static function findFreeAds(){
        $free_coupons = self::findFree();
        $free_arr = [];

        foreach ($free_coupons as $item)
            $free_arr[$item->adv_id] = $item->adv_id;

        return Adv::find()->where(['status' => Adv::STATUS_ACTIVE])->andWhere(['id' => $free_arr])->orderBy('date DESC')->all();
    }
}
