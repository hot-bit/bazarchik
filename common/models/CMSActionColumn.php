<?php
/**
 * Created by PhpStorm.
 * User: yorks
 * Date: 15.06.2017
 * Time: 8:55
 */

namespace common\models;


use Yii;
use yii\grid\ActionColumn;
use yii\helpers\Html;

class CMSActionColumn extends ActionColumn{

    public $template = '{update} {delete}';


    protected function initDefaultButton($name, $iconName, $additionalOptions = [])
    {
        if (!isset($this->buttons[$name]) && strpos($this->template, '{' . $name . '}') !== false) {
            $this->buttons[$name] = function ($url, $model, $key) use ($name, $iconName, $additionalOptions) {
                switch ($name) {
                    case 'view':
                        $iconName = 'eye';
                        $title = Yii::t('yii', 'View');
                        break;
                    case 'update':
                        $iconName = 'pencil';
                        $title = Yii::t('yii', 'Update');
                        break;
                    case 'delete':
                        $iconName = 'trash';
                        $title = Yii::t('yii', 'Delete');
                        break;
                    default:
                        $title = ucfirst($name);
                }
                $options = array_merge([
                    'title' => $title,
                    'aria-label' => $title,
                    'data-pjax' => '0',
                ], $additionalOptions, $this->buttonOptions);
                $icon = Html::tag('span', '', ['class' => "icon-$iconName"]);
                return Html::a($icon, $url, $options);
            };
        }
    }
}