<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "search_request".
 *
 * @property integer $id
 * @property string $request
 * @property integer $count
 * @property integer $ignore
 */
class SearchRequest extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'search_request';
    }

    public static function Add($q){
        $model = SearchRequest::find()->where(['request' => $q])->one();
        if(!$model){
            $model = new SearchRequest();
            $model->request = $q;
            $model->count = 0;
        }

        $model->count++;
        $model->save();
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['count', 'ignore'], 'integer'],
            [['request'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'request' => Yii::t('app', 'Request'),
            'count' => Yii::t('app', 'Count'),
            'ignore' => Yii::t('app', 'Ignore'),
        ];
    }
}
