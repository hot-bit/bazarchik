<?php

namespace common\models;

use common\behaviors\CMSImage;
use Yii;
use yii\behaviors\TimestampBehavior;
use yii\helpers\Html;

/**
 * This is the model class for table "banner".
 *
 * @property int $id
 * @property int $created_at
 * @property int $updated_at
 * @property int $start_date
 * @property int $finish_date
 * @property int $place
 * @property int $size
 * @property int $ru
 * @property int $en
 * @property int $gr
 * @property int $status
 * @property string $name
 * @property string $link
 * @property string $email
 * @property string $phone
 * @property string $image
 */
class Banner extends \yii\db\ActiveRecord
{
    const STATUS_MODERATE = 0;
    const STATUS_ACTIVE = 1;
    const STATUS_BLOCK = 2;
    const STATUS_EXPIRE = 3;
    const SIZE_MAIN = 100;
    const PLACE_CATALOG = 1;
    const PLACE_ADV = 2;
    const PLACE_ALL = 0;

    public $imageField;
    public $start_date_formatted;
    public $finish_date_formatted;

    public function behaviors()
    {
        return [
            TimestampBehavior::className(),
            'image' => [
                'class' => CMSImage::className(),
                'field' => 'image',
                'file' => 'imageField',
                'directory' => Yii::getAlias('@banners')
            ]
        ];
    }

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'banner';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['name', 'phone', 'email', 'link', 'place'], 'required'],
            [['created_at', 'updated_at', 'start_date', 'finish_date', 'place', 'size', 'status'], 'integer'],
            [['en', 'ru', 'gr'], 'boolean'],
            [['name', 'link', 'email', 'phone', 'image', 'start_date_formatted', 'finish_date_formatted'], 'string', 'max' => 255],
            [['email'], 'email'],
            ['imageField', 'image', 'extensions' => ['jpg', 'png']],
            ['status', 'default', 'value' => self::STATUS_MODERATE]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'created_at' => Yii::t('app', 'Created At'),
            'updated_at' => Yii::t('app', 'Updated At'),
            'start_date' => Yii::t('app', 'Start Date'),
            'finish_date' => Yii::t('app', 'Finish Date'),
            'place' => Yii::t('app', 'Place'),
            'size' => Yii::t('app', 'Size'),
            'ru' => Yii::t('app', 'Ru'),
            'en' => Yii::t('app', 'En'),
            'gr' => Yii::t('app', 'Gr'),
            'status' => Yii::t('app', 'Status'),
            'name' => Yii::t('app', 'Name'),
            'link' => Yii::t('app', 'Link'),
            'email' => Yii::t('app', 'Email'),
            'phone' => Yii::t('app', 'Phone'),
            'image' => Yii::t('app', 'Image'),
        ];
    }

    public function afterFind(){
        parent::afterFind();

        $this->start_date_formatted = date('Y-m-d', $this->start_date);
        $this->finish_date_formatted = date('Y-m-d', $this->finish_date);
    }

    public function beforeSave($insert){

        $this->start_date = strtotime($this->start_date_formatted);
        $this->finish_date = strtotime($this->finish_date_formatted);

        return parent::beforeSave($insert);
    }


    /**
     * Список расположений баннера
     *
     * @return array
     */
    public static function placeList(){
        return [
            self::PLACE_CATALOG => Yii::t('app', 'In catalog'),
            self::PLACE_ADV => Yii::t('app', 'In adv page'),
            self::PLACE_ALL => Yii::t('app', 'Everywhere'),
        ];
    }

    /**
     * Список статусов
     *
     * @return array
     */
    public static function statusList(){
        return [
            self::STATUS_MODERATE => Yii::t('app', 'Moderated'),
            self::STATUS_ACTIVE => Yii::t('app', 'Active'),
            self::STATUS_BLOCK => Yii::t('app', 'Blocked'),
            self::STATUS_EXPIRE => Yii::t('app', 'Expire')
        ];
    }

    /** Статус в текстовом виде
     * @return mixed
     */
    public function getStatusFormatted(){
        return self::statusList()[$this->status];
    }

    /** Место в текстовом виде
     * @return mixed
     */
    public function getPlaceFormatted(){
        return self::placeList()[$this->place];
    }

}
