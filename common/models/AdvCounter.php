<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "adv_counter".
 *
 * @property integer $id
 * @property integer $total
 * @property integer $today
 * @property integer $last_date
 */
class AdvCounter extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'adv_counter';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'total', 'today', 'last_date'], 'required'],
            [['id', 'total', 'today', 'last_date'], 'integer'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'total' => Yii::t('app', 'Total'),
            'today' => Yii::t('app', 'Today'),
            'last_date' => Yii::t('app', 'Last Date'),
        ];
    }
}
