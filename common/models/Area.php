<?php

namespace common\models;

use Yii;
use yii\helpers\ArrayHelper;

/**
 * This is the model class for table "area".
 *
 * @property integer $id
 * @property string $name_ru
 * @property string $name_en
 * @property string $name_gr
 * @property integer $city
 * @property double $lat
 * @property double $lng
 */
class Area extends \yii\db\ActiveRecord
{

    public function behaviors()
    {
        return [
            'lang' => [
                'class' => 'common\behaviors\BazarLanguage',
                'fields' => ['name']
            ],
        ];
    }
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'area';
    }

    public static function map($city = false){
        $model = self::find();
        if($city) $model->where(['city' => $city]);
        return ArrayHelper::map($model->orderBy('name_'.CMS::Lng())->all(), 'id', 'name_'.CMS::Lng());
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['name_ru', 'name_en', 'name_gr', 'city', 'lat', 'lng'], 'required'],
            [['city'], 'integer'],
            [['lat', 'lng'], 'number'],
            [['name_ru', 'name_en', 'name_gr'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'name_ru' => Yii::t('app', 'Name Ru'),
            'name_en' => Yii::t('app', 'Name En'),
            'name_gr' => Yii::t('app', 'Name Gr'),
            'city' => Yii::t('app', 'City'),
            'lat' => Yii::t('app', 'Lat'),
            'lng' => Yii::t('app', 'Lng'),
        ];
    }

    public function get_city(){
        return $this->hasOne(City::className(), ['id' => 'city']);
    }

    public function getCity_name(){
        return $this->_city->name;
    }

}
