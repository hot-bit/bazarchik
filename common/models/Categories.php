<?php

namespace common\models;

use Yii;
use yii\db\Query;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\helpers\Url;

/**
 * This is the model class for table "categories".
 *
 * @property integer $id
 * @property string $name_ru
 * @property string $name_en
 * @property string $name_gr
 * @property integer $parent
 * @property integer $level
 * @property string $filter
 * @property string $url
 * @property string $color
 * @property string $icon
 * @property integer $slave
 * @property integer $position
 * @property integer $status
 * @property integer $header
 * @property integer $count
 * @property string $iconCategoryMobile
 * @property integer $hidden_for_mobile
 * @property Categories[] $childs
 * @property string $link
 * @property mixed $name
 * @property int $count_all
 * @property Categories $_parent
 * @property array $category_stair
 * @property Categories[] $stairs_models
 * @property bool $has_filter
 * @property int $count_today
 * @property int $top_category
 */
class Categories extends \yii\db\ActiveRecord
{

    const CARS = 743;
    const FREE = 752;
    const LOST = 753;

    private $_count_today;
    public $nodes;

    public static function firstLevelMap()
    {
        $model = self::find()->where(['level' => 1])->orderBy('position')->all();
        return ArrayHelper::map($model, 'id', 'name');
    }

    public function behaviors()
    {
        return [
            'lang' => [
                'class' => 'common\behaviors\BazarLanguage',
                'fields' => ['name']
            ],
        ];
    }


    public static $color_list = [
        '' => 'orange',
        'red-icon' => 'red',
        'green-icon' => 'green',
        'gray-icon' => 'gray',
    ];
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'categories';
    }

    public static function map(){
        $model = self::find();
        return ArrayHelper::map($model->orderBy('name_'.CMS::Lng())->all(), 'id', 'name_'.CMS::Lng());
    }


    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['name_ru', 'name_en', 'url'], 'required'],
            [['parent', 'level', 'slave', 'position', 'status', 'header', 'count', 'hidden_for_mobile'], 'integer'],
            [['url'], 'string'],
            [['name_ru', 'name_en', 'name_gr', 'iconCategoryMobile', 'icon', 'color'], 'string', 'max' => 255],
            [['filter'], 'string', 'max' => 16],
        ];
    }

    public function getChilds(){
        return $this->hasMany(Categories::className(), ['parent' => 'id'])->where(['header' => 0])->orderBy('position');
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'name_ru' => Yii::t('app', 'Name Ru'),
            'name_en' => Yii::t('app', 'Name En'),
            'name_gr' => Yii::t('app', 'Name Gr'),
            'parent' => Yii::t('app', 'Parent'),
            'level' => Yii::t('app', 'Level'),
            'filter' => Yii::t('app', 'Filter'),
            'url' => Yii::t('app', 'Url'),
            'slave' => Yii::t('app', 'Slave'),
            'position' => Yii::t('app', 'Position'),
            'status' => Yii::t('app', 'Status'),
            'header' => Yii::t('app', 'Header'),
            'color' => Yii::t('app', 'Color'),
            'icon' => Yii::t('app', 'Icon'),
            'count' => Yii::t('app', 'Count'),
            'iconCategoryMobile' => Yii::t('app', 'Icon Category Mobile'),
            'hidden_for_mobile' => Yii::t('app', 'Hidden For Mobile'),
        ];
    }


    /* ***********************
     *  Админка
     *************************/
    public static function GenerateOptions(){

        $data = self::AdminTreeArr();

        return self::GenerateOptionsTree($data);
    }

    private static function GenerateOptionsTree($data, $lvl = 0){
        $ret = '';
        foreach($data as $tree){
            if($tree->header == 1) continue;
            $ret .= '<option value="'.$tree->id.'" style="background:'.($lvl == 0 ? '#ccc' : '#fff;').'">'.str_repeat('&nbsp;&nbsp;&nbsp;', $lvl).' '.$tree[CMS::field('name_')].'</option>';
            if($tree->nodes != null){
                $ret .= self::GenerateOptionsTree($tree->nodes, $lvl+1);
            }
        }
        return $ret;
    }


    public static function AdminTree(){
        $ret = Html::beginTag('ul', ['class' => 'lvl0']);

        $data = self::AdminTreeArr();

        foreach($data as $item){
            $ret .= Html::beginTag('li');
            $ret .= Yii::$app->controller->renderPartial('/category/tree_item', ['item' => $item]);
            if($item->nodes != null) {
                $ret .= self::GenerateItem($item->nodes, $item);
            }
            $ret .= Html::endTag('li');
        }
        $ret .= Html::endTag('ul');
        return $ret;
    }

    private static function GenerateItem($data, $parent){
        $ret = Html::beginTag('ul', ['class' => 'tree_nodes lvl'.$parent['level']]);
        foreach($data as $item){
            $ret .= Html::beginTag('li');
            $ret .= Yii::$app->controller->renderPartial('/category/tree_item', ['item' => $item]);
            if($item->nodes != null) {
                $ret .= self::GenerateItem($item->nodes, $item);
            }
            $ret .= Html::endTag('li');
        }
        $ret .= Html::endTag('ul');
        return $ret;
    }

    private static function AdminTreeArr($lvl = 0, $prnt = 0){
        $model = Categories::find()->orderBy('position')->all();

        $parents = array();
        foreach($model as $item){
            if(!isset($parents[$item->parent]))
                $parents[$item->parent] = 1;
            else
                $parents[$item->parent] += 1;
        }

        $tree = array();
        foreach($model as $item){
            if($item->parent != $prnt){
                continue;
            }
            if($item->parent == $prnt){
                $tree[$item->id] = $item;
                $tree[$item->id]->nodes = array();
                $tree[$item->id]->nodes = self::AdminTreeArrItem($model, $item->id, $tree[$item->id]->nodes, $parents);
            }
        }
        return $tree;
    }

    private static function AdminTreeArrItem($model, $parent, $itemTree, $parents){
        foreach($model as $item){
            if($item->parent == $parent){
                $itemTree[$item->id] = $item;

                if(!isset($parents[$item->id]))
                    continue;
                $itemTree[$item->id]->nodes = array();
                $itemTree[$item->id]->nodes = self::AdminTreeArrItem($model, $item->id,  $itemTree[$item->id]->nodes, $parents);
            }
        }
        return $itemTree;
    }

    public function beforeSave($insert){
        $this->status = isset($_POST['Categories']['status']) ? 1 : 0;
        $this->header = isset($_POST['Categories']['header']) ? 1 : 0;

        return parent::beforeSave($insert);
    }


    /* ***********************
     *  Фронтенд
     *************************/


    /**
     * Получить дерево категорий 1 и 2 уровня
     *
     * @return array
     */
    public static function Tree(){
        $categories = Categories::find()->where(['level' => [1,2], 'status' => 1])->orderBy('position')->all();

        $cat_list = [];
        foreach ($categories as $item){
            $cat_list[$item->parent][] = $item;
        }

        return $cat_list;

    }

    /**
     * Название категории с учетом языка
     *
     * @return mixed
     */
    public function getName(){

        return $this->{CMS::field('name_')};
    }

    /**
     * Ссылка на категорию
     *
     * @return string
     */
    public function getLink(){
        return Url::to(['adv/index', 'cat' => $this->url]);
    }

    /**
     * Получить массив всех id потомков
     *
     * @return array
     */
    public function ChildrenIdList(){
        $id_list = [$this->id];
        $model = self::find()->all();

        $cat_list = [];
        foreach ($model as $item)
            $cat_list[$item->parent][] = $item;

        // level 1
        if(isset($cat_list[$this->id])){
            foreach ($cat_list[$this->id] as $item){
                $id_list[] = $item->id;

                // level 2
                if(isset($cat_list[$item->id])){
                    foreach ($cat_list[$item->id] as $child){
                        $id_list[] = $child->id;

                        // level 3
                        if(isset($cat_list[$child->id])){
                            foreach ($cat_list[$child->id] as $child2){
                                $id_list[] = $child2->id;
                            }
                        }
                    }
                }
            }
        }

        return $id_list;
    }

    /**
     * Кол-во объявлений всего
     *
     * @return int
     */
    public function getCount_all(){
        return $this->count;
    }

    /**
     * Кол-во объявлений за сегодня
     *
     * @return int
     */
    public function getCount_today(){
        if($this->_count_today == null){

            $child_categories = $this->ChildrenIdList();

            $query = new Query();
            $query->select('count')
                ->from('category_updates')
                ->where(['category_id' => $child_categories]);
            $categories = $query->all();

            $total = 0;
            foreach ($categories as $item)
                $total += $item['count'];

            $this->_count_today = $total;

        }

        return $this->_count_today;
    }

    /**
     * Получить модели родителей
     *
     * @return \yii\db\ActiveQuery
     */
    public function get_parent(){
        return $this->hasOne(Categories::className(), ['id' => 'parent']);
    }

    /**
     * Получить массив имен родителей до начального элемента
     *
     * @return array
     */
    public function getCategory_stair(){
        $category = $this;
        $items = [];
        while ($category->parent != 0){

            $items[] = $category->name;
            $category = $category->_parent;
        }
        $items[] = $category->name;

        $items = array_reverse($items);
        return $items;
    }

    /**
     * Получить модели родителей до начального элемента
     *
     * @return array
     */
    public function getStairs_models(){
        $category = $this;
        $items = [];
        while ($category->parent != 0){

            $items[] = $category;
            $category = $category->_parent;
        }
        $items[] = $category;

        $items = array_reverse($items);
        return $items;
    }

    /**
     * Есть ли у категории фильтр
     *
     * @return bool
     */
    public function getHas_filter(){
        if($this->filter != '0' && !empty($this->filter))
            return true;
        return false;
    }

    /**
     * Получить id категории 1го уровная
     *
     * @return int
     */
    public function getTop_category(){
        return $this->stairs_models[0]->id;
    }


}
