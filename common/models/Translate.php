<?php

namespace common\models;

use Yii;
use yii\helpers\ArrayHelper;

/**
 * This is the model class for table "translate".
 *
 * @property integer $id
 * @property string $category
 * @property string $message
 * @property integer $language_id
 * @property string $value
 *
 * @property Lang $_language
 */
class Translate extends \yii\db\ActiveRecord
{
    public $tmp_values;

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'translate';
    }

    public static function findByMessage($category, $message){
        return self::find()->where(['category' => $category, 'message' => $message])->one();
    }

    private static function findBySimilar($category, $message, $language){
        return self::find()->where(['category' => $category, 'message' => $message, 'language_id' => $language])->one();
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['language_id'], 'integer'],
            [['category', 'message', 'value'], 'string', 'max' => 255],
            [['language_id'], 'exist', 'skipOnError' => true, 'targetClass' => Lang::className(), 'targetAttribute' => ['language_id' => 'id']],
            ['tmp_values', 'safe']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'category' => Yii::t('app', 'Category'),
            'message' => Yii::t('app', 'Message'),
            'language_id' => Yii::t('app', 'Language ID'),
            'value' => Yii::t('app', 'Value'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function get_language()
    {
        return $this->hasOne(Lang::className(), ['id' => 'language_id']);
    }

    public function beforeSave($insert){

        if(!$insert){
            if($this->language_id == 1){

                foreach ($this->tmp_values as $key => $val){
                    if($key != 1){
                        $model = Translate::findBySimilar($this->category, $this->message, $key);

                        if($model){
                            $model->value = $val;
                            $model->save();
                        }
                    } else {
                        $this->value = $val;
                    }
                }
            }
        }

        self::GenerateFiles();
        return parent::beforeSave($insert);
    }

    /**
     * Сгенерировать файл переводов
     */
    private static function GenerateFiles(){
        $dir = Yii::getAlias('@frontend').'/messages/';

        $files = [];
        $data = Translate::find()->all();

        foreach ($data as $item){
            $files[$item->language_id][$item->category][$item->message] = $item->value;
        }

        $langs = ArrayHelper::map(Lang::find()->all(), 'id', 'local');

        foreach ($files as $lng_id => $data2){
            $dir_lng = $dir.$langs[$lng_id].'/';
            if(!is_dir($dir_lng)) mkdir($dir_lng, 0777, true);
            foreach ($data2 as $category => $data3){
                $values = [];
                foreach ($data3 as $message => $value){
                    $message = str_replace("'", "\\'", $message);
                    $value = str_replace("'", "\\'", $value);
                    $values[] = "'$message' => '$value'";
                }

                $inc = '<?php return ['.implode(',', $values).'];';
                file_put_contents($dir_lng.$category.'.php', $inc);
            }
        }


    }
}
