<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "url_route".
 *
 * @property integer $id
 * @property string $url
 * @property string $module_name
 * @property integer $module_id
 * @property integer $lng_id
 */
class UrlRoute extends \yii\db\ActiveRecord
{

    public static function ModuleList(){
        return [
            'category' => Yii::t('app', 'Categories'),
            'faq' => Yii::t('app', 'Faq'),
        ];
    }

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'url_route';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['module_id', 'lng_id'], 'integer'],
            [['url', 'module_name'], 'string', 'max' => 70],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'url' => 'Url',
            'module_name' => 'Module Name',
            'module_id' => 'Module ID',
            'lng_id' => 'Lng',
        ];
    }
}
