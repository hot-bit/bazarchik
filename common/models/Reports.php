<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "reports".
 *
 * @property integer $id
 * @property integer $adv_id
 * @property string $text
 * @property integer $date
 * @property string $ip
 * @property integer $status
 */
class Reports extends \yii\db\ActiveRecord
{
    public static $texts = [
        1 => 'Product is sold',
        2 => 'Incorrect price',
        3 => 'Can\'t call',
        4 => 'Contacts and links in desc',
        5 => 'Other reason',
    ];

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'reports';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['adv_id', 'text'], 'required'],
            [['adv_id', 'date', 'status'], 'integer'],
            [['text'], 'string'],
            [['ip'], 'string', 'max' => 20],
        ];
    }

    public function beforeSave($insert){
        if($insert){
            $this->date = time();
            $this->ip = $_SERVER['REMOTE_ADDR'];
            $this->status = 0;
        }

        return parent::beforeSave($insert);
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'adv_id' => Yii::t('app', 'Adv ID'),
            'text' => Yii::t('app', 'Text'),
            'date' => Yii::t('app', 'Date'),
            'ip' => Yii::t('app', 'Ip'),
            'status' => Yii::t('app', 'Status'),
        ];
    }
}
