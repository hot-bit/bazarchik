<?php

namespace common\models;

use Yii;
use yii\behaviors\TimestampBehavior;

/**
 * This is the model class for table "chat_message".
 *
 * @property integer $id
 * @property integer $chat_id
 * @property integer $created_at
 * @property integer $updated_at
 * @property string $image
 * @property string $message
 * @property integer $user_id
 *
 * @property Chat $chat
 * @property string $userName
 * @property User $user
 */
class ChatMessage extends \yii\db\ActiveRecord
{

    public function behaviors()
    {
        return [TimestampBehavior::className()];
    }

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'chat_message';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['chat_id', 'created_at', 'updated_at', 'user_id'], 'integer'],
            [['image', 'message'], 'string', 'max' => 255],
            [['chat_id'], 'exist', 'skipOnError' => true, 'targetClass' => Chat::className(), 'targetAttribute' => ['chat_id' => 'id']],
            [['user_id'], 'exist', 'skipOnError' => true, 'targetClass' => User::className(), 'targetAttribute' => ['user_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'chat_id' => Yii::t('app', 'Chat ID'),
            'created_at' => Yii::t('app', 'Created At'),
            'updated_at' => Yii::t('app', 'Updated At'),
            'image' => Yii::t('app', 'Image'),
            'message' => Yii::t('app', 'Message'),
            'user_id' => Yii::t('app', 'User ID'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getChat()
    {
        return $this->hasOne(Chat::className(), ['id' => 'chat_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUser()
    {
        return $this->hasOne(User::className(), ['id' => 'user_id']);
    }

    /** Получить имя пользователя
     * @return string
     */
    public function getUserName(){
        if($this->user_id == Yii::$app->user->id)
            return Yii::t('app', 'You');
        else
            return $this->user->name;
    }

    /**
     * @param bool $insert
     * @param array $changedAttributes
     */
    public function afterSave($insert, $changedAttributes)
    {
        if($insert){
            $model = new ChatView();
            $model->user_id = $this->chat->partnerId;
            $model->chat_id = $this->chat_id;
            $model->message_id = $this->id;
            $model->save();
        }

        parent::afterSave($insert, $changedAttributes);
    }
}
