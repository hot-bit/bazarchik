<?php

namespace common\models;

use Yii;
use yii\helpers\Html;

/**
 * This is the model class for table "faq".
 *
 * @property integer $id
 * @property string $title_ru
 * @property string $title_en
 * @property string $title_gr
 * @property string $text_ru
 * @property string $text_en
 * @property string $text_gr
 * @property string $title
 * @property string $text
 * @property integer $status
 * @property string $link
 * @property mixed $f_status
 * @property FaqCategory $_category
 * @property mixed $category_name
 * @property integer $category_id
 */
class Faq extends \yii\db\ActiveRecord
{


    public function behaviors()
    {
        return [
            'lang' => [
                'class' => 'common\behaviors\BazarLanguage',
                'fields' => ['title', 'text']
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'faq';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['title_ru', 'title_en', 'title_gr', 'text_ru', 'text_en', 'text_gr', 'link', 'category_id'], 'required'],
            [['text_ru', 'text_en', 'text_gr'], 'string'],
            [['status', 'category_id'], 'integer'],
            [['title_ru', 'title_en', 'title_gr', 'link'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'title_ru' => Yii::t('app', 'Title Ru'),
            'title_en' => Yii::t('app', 'Title En'),
            'title_gr' => Yii::t('app', 'Title Gr'),
            'text_ru' => Yii::t('app', 'Text Ru'),
            'text_en' => Yii::t('app', 'Text En'),
            'text_gr' => Yii::t('app', 'Text Gr'),
            'status' => Yii::t('app', 'Status'),
            'link' => Yii::t('app', 'Link'),
            'category_id' => Yii::t('app', 'Category ID'),
        ];
    }

    public function getF_status(){
        return $this->status ? Html::tag('span', 'Active', ['class' => 'badge badge-success']) :  Html::tag('span', 'Not active', ['class' => 'badge badge-default']);
    }

    public function getCategory_name(){
        $field_name = CMS::field('name_');
        return $this->_category->$field_name;
    }

    public function get_category(){
        return $this->hasOne(FaqCategory::className(), ['id' => 'category_id']);
    }

    public static function findByUrl($link){
        return self::find()->where(['link' => $link])->one();
    }
}
