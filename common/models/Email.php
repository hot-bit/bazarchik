<?php
/**
 * Created by PhpStorm.
 * User: York
 * Date: 06.01.2018
 * Time: 10:48
 */

namespace common\models;


class Email
{

    public static function spacer($height, $center = false)
    {
        return '
        <table class="spacer ' . ($center ? 'float-center' : '') . '"
               style="border-collapse:collapse;border-spacing:0;padding:0;text-align:left;vertical-align:top;width:100%">
            <tbody>
            <tr style="border:none;margin:0;padding:0;text-align:left;vertical-align:top">
                <td height="' . $height . 'px"
                    style="-moz-hyphens:auto;-webkit-hyphens:auto;Margin:0;border:none;border-collapse:collapse!important;color:#1d1d1d;font-family:Helvetica,Arial,sans-serif;font-size:' . $height . 'px;font-weight:400;hyphens:auto;line-height: ' . $height . 'px;margin:0;mso-line-height-rule:exactly;padding:0;text-align:left;vertical-align:top;word-wrap:break-word">
                    &#xA0;
                </td>
            </tr>
            </tbody>
        </table>
        ';
    }

}