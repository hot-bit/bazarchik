<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "favorite".
 *
 * @property integer $user_id
 * @property integer $adv_id
 * @property integer $id
 * @property integer $favorite_date
 */
class Favorite extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'favorite';
    }

    public static function Add($id){
        $model = Favorite::findByAdv($id);
        if($model) return false;

        $model = new Favorite();
        $model->adv_id = $id;
        $model->user_id = Yii::$app->user->id;
        $model->save();
    }

    public static function Remove($id){
        Favorite::findByAdv($id)->delete();
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['user_id', 'adv_id'], 'required'],
            [['user_id', 'adv_id'], 'integer'],
        ];
    }

    public function beforeSave($insert){

        $this->favorite_date = time();

        return parent::beforeSave($insert);
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'user_id' => Yii::t('app', 'User ID'),
            'adv_id' => Yii::t('app', 'Adv ID'),
            'id' => Yii::t('app', 'ID'),
            'favorite_date' => Yii::t('app', 'Favorite Date'),
        ];
    }


    public static function findByAdv($id){
        if(Yii::$app->user->isGuest)
            return false;
        $model = self::find()->where(['adv_id' => $id, 'user_id' => Yii::$app->user->id])->one();

        if($model) return $model;
        return false;
    }

    public static function findByUser(){
        if(Yii::$app->user->isGuest)
            return false;
        $model = self::find()->where(['user_id' => Yii::$app->user->id])->all();

        $items = [];
        foreach ($model as $item)
            $items[$item->adv_id] = $item->adv_id;

        return $items;
    }
}
