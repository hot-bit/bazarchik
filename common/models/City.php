<?php

namespace common\models;

use Yii;
use yii\helpers\ArrayHelper;

/**
 * This is the model class for table "city".
 *
 * @property integer $id
 * @property string $name_ru
 * @property string $name_en
 * @property string $name_gr
 * @property string $name
 * @property double $lat
 * @property double $lng
 */
class City extends \yii\db\ActiveRecord
{

    public function behaviors()
    {
        return [
            'lang' => [
                'class' => 'common\behaviors\BazarLanguage',
                'fields' => ['name']
            ],
        ];
    }
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'city';
    }

    public static function map(){
        return ArrayHelper::map(self::find()->orderBy(CMS::field('name_'))->all(), 'id', CMS::field('name_'));
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['name_ru', 'name_en', 'name_gr', 'lat', 'lng'], 'required'],
            [['lat', 'lng'], 'number'],
            [['name_ru', 'name_en', 'name_gr'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'name_ru' => Yii::t('app', 'Name Ru'),
            'name_en' => Yii::t('app', 'Name En'),
            'name_gr' => Yii::t('app', 'Name Gr'),
            'lat' => Yii::t('app', 'Lat'),
            'lng' => Yii::t('app', 'Lng'),
        ];
    }

}
