<?php
namespace common\models\forms;

use common\models\Adv;
use common\models\Categories;
use common\models\CMS;
use common\models\User;
use Yii;
use yii\base\Model;

/**
 * Search form
 *
 * @property Categories $_category
 *
 */
class SearchForm extends Model
{
    public $search;
    public $category_id;
    public $in_title;
    public $photo;
    public $only_new;
    public $city_id;


    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['search'], 'string'],
            [['in_title', 'only_new', 'photo'], 'boolean'],
        ];
    }

    public function attributeLabels(){

        return [
            'city_id' => Yii::t('app', 'City'),
            'in_title' => Yii::t('app', 'In title'),
            'photo' => Yii::t('app', 'With photo'),
            'only_new' => Yii::t('app', 'Only new'),
        ];
    }

    /**
     * @return false|Categories
     */
    public function get_category(){
        return Categories::findOne($this->category_id);
    }


}
