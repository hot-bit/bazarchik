<?php
namespace common\models\forms;

use common\models\Adv;
use common\models\AdvParams;
use common\models\Brand;
use common\models\CMS;
use common\models\map\CarBodyType;
use common\models\User;
use Yii;
use yii\base\Model;
use yii\helpers\ArrayHelper;

/**
 * Search form
 *
 */
class ParamsForm extends Model
{
    public $step;
    private $_params;
    public $id;

    // Cars
    public $brand;
    public $model;
    public $body;
    public $year;
    public $running;
    public $box;
    public $volume;
    public $engine;
    public $power;
    public $drive;
    public $wheel;
    public $vin;
    public $color;
    public $doors;

    public $st_wheel = 0;
    public $salon = 0;
    public $salon_leather_wheel;
    public $salon_hatch;

    public $climat = 0;
    public $climate_wheel_contr;
    public $climate_aterm;

    public $help_secure_abs;
    public $help_secure_asr;
    public $help_secure_esp;
    public $help_secure_ebd;
    public $help_secure_eba;
    public $help_secure_eds;
    public $help_secure_pds;

    public $heating_front_seat;
    public $heating_rear_seat;
    public $heating_mirrors;
    public $heating_rear_window;
    public $heating_wheel;

    public $glass = 0;

    public $el_front_seats;
    public $el_rear_seats;
    public $el_mirrors;
    public $el_wheel;
    public $el_folding_mirrors;

    public $mem_front_seats;
    public $mem_rear_seats;
    public $mem_mirrors;
    public $mem_wheel;

    public $help_jockey;
    public $help_rain_sensor;
    public $help_light_sensor;

    public $help_r_park_sensor;
    public $help_f_park_sensor;
    public $help_blind_spot;

    public $help_rear_camera;
    public $help_cruise_control;
    public $help_computer;

    public $hijack_signal;
    public $hijack_central_lock ;
    public $hijack_immobi;
    public $hijack_sputnik;

    public $airbags_front;
    public $airbags_knee;
    public $airbags_blinds;
    public $airbags_front_side;
    public $airbags_rear_side;

    public $media_cd;
    public $media_mp3;
    public $media_radio;

    public $media_tv;
    public $media_video;
    public $media_wheel_control;

    public $media_usb;
    public $media_aux;
    public $media_bluetooth;
    public $media_gps;

    public $audio = 0;
    public $sub;

    public $light = 0;
    public $light_afog;
    public $light_cleaner;
    public $light_adaptive;

    public $disc;
    public $TI_service_book;
    public $TI_dealer;
    public $TI_warranty;

    // Property
    public $city_id;
    public $area_id;
    public $cover;
    public $bedrooms;

    //Tires
    public $diam;
    public $width_prof;
    public $height_prof;
    public $round;

    // Disk
    public $type;
    public $width;
    public $holes;
    public $holes_diameter;
    public $outfly;

    // Cloth
    public $size;
    public $place;



    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['vin'], 'string', 'max' => 255],
            [['volume'], 'number'],
            [['brand', 'model', 'year', 'running', 'body', 'box', 'color', 'engine', 'drive', 'wheel', 'doors', 'audio', 'st_wheel', 'salon', 'climat', 'glass', 'light'] , 'integer'],
            [['salon_leather_wheel', 'salon_hatch', 'climate_aterm', 'climate_wheel_contr', 'heating_front_seat', 'heating_mirrors', 'heating_rear_seat', 'heating_rear_window', 'heating_wheel', 'help_blind_spot', 'help_computer', 'help_cruise_control', 'help_f_park_sensor', 'help_jockey', 'help_light_sensor', 'help_r_park_sensor', 'help_rain_sensor', 'help_rear_camera', 'help_secure_abs', 'help_secure_asr', 'help_secure_eba', 'help_secure_ebd', 'help_secure_eds', 'help_secure_esp', 'help_secure_pds', 'mem_front_seats', 'mem_mirrors', 'mem_rear_seats', 'mem_wheel', 'media_mp3', 'sub', 'hijack_central_lock', 'hijack_immobi', 'hijack_signal', 'hijack_sputnik', 'media_cd', 'media_radio', 'media_tv', 'media_video', 'media_wheel_control', 'el_wheel', 'media_usb', 'media_aux', 'media_bluetooth', 'media_gps', 'TI_dealer', 'TI_service_book', 'TI_warranty', 'airbags_blinds', 'airbags_front', 'airbags_front_side', 'airbags_knee', 'airbags_rear_side', 'light_afog', 'light_adaptive', 'light_cleaner', 'el_folding_mirrors', 'el_front_seats', 'el_mirrors', 'el_rear_seats'], 'boolean'],
        ];
    }

    public function attributeLabels(){

        return [

            'brand' => Yii::t('app', 'Brand'),
            'model' => Yii::t('app', 'Model'),
            'body' => Yii::t('app', 'Body type'),
            'year' => Yii::t('app', 'Year'),
            'mileage' => Yii::t('app', 'Mileage'),
            'box' => Yii::t('app', 'Transmission type'),
            'engine' => Yii::t('app', 'Engine type'),
            'power' => Yii::t('app', 'Engine volume'),
            'drive' => Yii::t('app', 'Wheels drive'),
            'vin' => Yii::t('app', 'Reg plates'),
            'wheel' => Yii::t('app', 'Wheel RHD / LHD'),
            'volume' => Yii::t('app', 'Engine volume'),


            'active_save' => Yii::t('app', 'Active save'),
            'st_wheel' => Yii::t('app', 'Driving wheel assistance'),
            'salon' => Yii::t('app', 'Saloon'),
            'salon_leather_wheel' => Yii::t('app', 'Leather steering wheel'),
            'salon_hatch' => Yii::t('app', 'Hatch'),
            'climat' => Yii::t('app', 'Climate control'),
            'climate_wheel_contr' => Yii::t('app', 'Steering wheel controls'),
            'climate_aterm' => Yii::t('app', 'Athermal glazing'),
            'help_secure_abs' => Yii::t('app', 'Anti-lock brakes system (ABS)'),
            'help_secure_asr' => Yii::t('app', 'Tracktion control (ASR)'),
            'help_secure_esp' => Yii::t('app', 'Stability programe (ESP/ESC/DSC)'),
            'help_secure_ebd' => Yii::t('app', 'Brake force distribution (EBD/EBV)'),
            'help_secure_eba' => Yii::t('app', 'Brake assist (EBA/BAS/BA)'),
            'help_secure_eds' => Yii::t('app', 'Electric differential lock (EDS/XDS/ETS)'),
            'help_secure_pds' => Yii::t('app', 'Pedestrian detection system (PDS)'),
            'heating_front_seat' => Yii::t('app', 'Front seat'),
            'heating_rear_seat' => Yii::t('app', 'Rear seat'),
            'heating_mirrors' => Yii::t('app', 'Mirrors'),
            'heating_rear_window' => Yii::t('app', 'Rear window'),
            'heating_wheel' => Yii::t('app', 'Wheel'),
            'glass' => Yii::t('app', 'Electric power windows'),
            'el_front_seats' => Yii::t('app', 'Front seats'),
            'el_rear_seats' => Yii::t('app', 'Rear seats'),
            'el_mirrors' => Yii::t('app', 'Mirrors'),
            'el_wheel' => Yii::t('app', 'Steering column'),
            'el_folding_mirrors' => Yii::t('app', 'Folding mirrors'),
            'mem' => Yii::t('app', 'Memory function'),
            'mem_front_seats' => Yii::t('app', 'Front seats'),
            'mem_rear_seats' => Yii::t('app', 'Rear seats'),
            'mem_mirrors' => Yii::t('app', 'Mirrors'),
            'mem_wheel' => Yii::t('app', 'Steering column'),
            'help' => Yii::t('app', 'Driving assistance'),
            'help_jockey' => Yii::t('app', 'Automatic jockey'),
            'help_rain_sensor' => Yii::t('app', 'Rain sensor'),
            'help_light_sensor' => Yii::t('app', 'Light sensor'),
            'help_r_park_sensor' => Yii::t('app', 'Rear parking sensors'),
            'help_f_park_sensor' => Yii::t('app', 'Front parking sensors'),
            'help_blind_spot' => Yii::t('app', 'Blind spot monitoring system'),
            'help_rear_camera' => Yii::t('app', 'Rear view camera'),
            'help_cruise_control' => Yii::t('app', 'Cruise control'),
            'help_computer' => Yii::t('app', 'On-board computer'),
            'hijack' => Yii::t('app', 'Anti-theft system'),
            'hijack_signal' => Yii::t('app', 'Signaling'),
            'hijack_central_lock ' => Yii::t('app', 'Central locking'),
            'hijack_immobi' => Yii::t('app', 'Immobilizer'),
            'hijack_sputnik' => Yii::t('app', 'Satelite'),
            'airbags' => Yii::t('app', 'Airbags'),
            'airbags_front' => Yii::t('app', 'Front airbags'),
            'airbags_knee' => Yii::t('app', 'Knee airbags'),
            'airbags_blinds' => Yii::t('app', 'Blinds airbags'),
            'airbags_front_side' => Yii::t('app', 'front side airbags'),
            'airbags_rear_side' => Yii::t('app', 'rear side airbags'),
            'media' => Yii::t('app', 'Multimedia and navigation'),
            'media_cd' => Yii::t('app', 'CD/DVD/Blue-ray'),
            'media_mp3' => Yii::t('app', 'MP3'),
            'media_radio' => Yii::t('app', 'Radio'),
            'media_tv' => Yii::t('app', 'TV'),
            'media_video' => Yii::t('app', 'Video'),
            'media_wheel_control' => Yii::t('app', 'Steering wheel controls'),
            'media_usb' => Yii::t('app', 'USB'),
            'media_aux' => Yii::t('app', 'AUX'),
            'media_bluetooth' => Yii::t('app', 'Bluetooth'),
            'media_gps' => Yii::t('app', 'GPS navigator'),
            'audio' => Yii::t('app', 'Number of speakers'),
            'sub' => Yii::t('app', 'Subwoofer'),
            'light' => Yii::t('app', 'Headlights type'),
            'light_afog' => Yii::t('app', 'Antifog lamps'),
            'light_cleaner' => Yii::t('app', 'Headlight washers'),
            'light_adaptive' => Yii::t('app', 'Adaptive lighting'),
            'disc' => Yii::t('app', 'Rims size (inches)'),
            'TI' => Yii::t('app', 'Inspection information'),
            'TI_service_book' => Yii::t('app', 'Has service book'),
            'TI_dealer' => Yii::t('app', 'Serviced by dealer'),
            'TI_warranty' => Yii::t('app', 'Under warranty'),

            'city_id' => Yii::t('app', 'City'),
            'area_id' => Yii::t('app', 'Area'),
            'cover' => Yii::t('app', 'Covered area'),
            'bedrooms' => Yii::t('app', 'Bedrooms'),

            'diam' => Yii::t('app', 'Diameter'),
            'width_prof' => Yii::t('app', 'Profile width'),
            'height_prof' => Yii::t('app', 'Profile height'),
            'round' => Yii::t('app', 'Axis'),

            'type' => Yii::t('app', 'Disk type'),
            'width' => Yii::t('app', 'Rim Width (inch)'),
            'holes' => Yii::t('app', 'Number of holes'),
            'holes_diameter' => Yii::t('app', 'The hole diameter (mm)'),
            'outfly' => Yii::t('app', 'Departure (ET) in mm'),


            'size' => Yii::t('app', 'Size'),
            'place' => Yii::t('app', 'Place count'),
        ];
    }

    public function findModel(){
        $model = AdvParams::findByAdv($this->id);
        if($model)
            foreach ($model as $item){
                $this->_params[$item->name] = $item;
            }
    }

    public function loadModel(){
        $model = AdvParams::findByAdv($this->id);
        if($model)
            foreach ($model as $item)
                $this->{$item->name} = $item->value;

    }

    public function upload($id){
        if($this->step != 5) return true;
        if($this->step == 5 && $this->load(\Yii::$app->request->post()) && $this->validate()){

            $this->id = $id;
            $this->findModel();

            foreach ($this->attributes as $key => $attribute){
                if($attribute == 0) continue;

                if(!isset($this->_params[$key])){
                    $model = new AdvParams();
                    $model->adv_id = $this->id;
                    $model->name = $key;
                    $model->value = $attribute;
                }
                else{
                    $model = $this->_params[$key];
                    $model->value = $attribute;
                }
                $model->save();

            }

            return true;
        }
        return false;
    }

}
