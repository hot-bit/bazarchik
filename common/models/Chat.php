<?php

namespace common\models;

use Yii;
use yii\behaviors\TimestampBehavior;

/**
 * This is the model class for table "chat".
 *
 * @property integer $id
 * @property integer $user_1
 * @property integer $user_2
 * @property integer $created_at
 * @property integer $updated_at
 *
 * @property User $user1
 * @property User $user2
 * @property string $partnerName
 * @property void $partnerAvatar
 * @property int $partnerId
 * @property \common\models\User $partner
 * @property bool $hasUnread
 * @property ChatMessage[] $chatMessages
 */
class Chat extends \yii\db\ActiveRecord
{


    public function behaviors()
    {
        return [
            TimestampBehavior::className()
        ];
    }

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'chat';
    }

    public static function findByUser()
    {
        $model = self::find()
            ->where(['user_1' => Yii::$app->user->id])
            ->orWhere(['user_2' => Yii::$app->user->id])
            ->orderBy('updated_at DESC')
            ->all();
        return $model;
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['user_1', 'user_2', 'created_at', 'updated_at'], 'integer'],
            [['user_1'], 'exist', 'skipOnError' => true, 'targetClass' => User::className(), 'targetAttribute' => ['user_1' => 'id']],
            [['user_2'], 'exist', 'skipOnError' => true, 'targetClass' => User::className(), 'targetAttribute' => ['user_2' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'user_1' => Yii::t('app', 'User 1'),
            'user_2' => Yii::t('app', 'User 2'),
            'created_at' => Yii::t('app', 'Created At'),
            'updated_at' => Yii::t('app', 'Updated At'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUser1()
    {
        return $this->hasOne(User::className(), ['id' => 'user_1']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUser2()
    {
        return $this->hasOne(User::className(), ['id' => 'user_2']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getChatMessages()
    {
        return $this->hasMany(ChatMessage::className(), ['chat_id' => 'id'])->orderBy('created_at');
    }

    /** Создать сообщение
     * @param $message
     * @return void
     */
    public function createMessage($message){
        $model = new ChatMessage();
        $model->user_id = Yii::$app->user->id;
        $model->message = $message;
        $model->chat_id = $this->id;
        $model->save();
        $this->save();
    }

    /** Найти чат по пользователю
     * @param $user1
     * @param $user2
     * @return array|Chat|null|\yii\db\ActiveRecord
     */
    public static function findByUsers($user1, $user2)
    {
        return self::find()
            ->where(['user_1' => $user1, 'user_2' => $user2])
            ->orWhere(['user_2' => $user1, 'user_1' => $user2])
            ->one();
    }

    /** Получить имя партнера
     * @return string
     */
    public function getPartnerName(){
        if(Yii::$app->user->id == $this->user_1) $user = $this->user2;
        else $user = $this->user1;

        return $user->name;
    }

    /** Получить id партнера
     * @return int
     */
    public function getPartnerId(){
        if(Yii::$app->user->id == $this->user_1) return $this->user_2;
        return $this->user_1;
    }

    /** Получить модель партнера
     * @return User
     */
    public function getPartner(){
        if(Yii::$app->user->id == $this->user_1) return $this->user2;
        return $this->user1;
    }

    /** Получить аватарку
     * @return mixed
     */
    public function getPartnerAvatar(){
        return $this->partner->thumb;
    }

    /** Есть ли в этом чате непрочитанные сообщения
     * @return bool
     */
    public function getHasUnread(){
        $model = ChatView::findByChat($this->id);
        if($model) return true;
        return false;
    }
}
