<?php
/**
 * Created by PhpStorm.
 * User: York
 * Date: 31.12.2017
 * Time: 7:03
 */

namespace common\models\validators;


use Yii;
use yii\validators\Validator;

class PhoneValidator extends Validator
{

    public function validateAttribute($model, $attribute){

        if(!in_array($model->phone_code, \Yii::$app->params['phone_codes'])){
            $this->addError($model, $attribute, 'Incorrect phone code');
        }

        $phone_code = $model->phone_code;

        $count_code = strlen($phone_code);
        $count_phone = strlen($model->$attribute);

        if($count_code == 1 && $count_phone != 10)
            $this->addError($model, $attribute, Yii::t('app', 'Phone must have {count} symbols', ['count' => 10]));
        elseif($count_code == 2 && $count_phone != 9)
            $this->addError($model, $attribute, Yii::t('app', 'Phone must have {count} symbols', ['count' => 9]));
        elseif($count_code == 3 && $count_phone != 8)
            $this->addError($model, $attribute, Yii::t('app', 'Phone must have {count} symbols', ['count' => 8]));
    }
}