<?php

namespace common\models;

use common\models\traits\AdvGenerateTrait;
use common\models\traits\SlugTrait;
use common\models\validators\PhoneValidator;
use Yii;
use yii\db\ActiveRecord;
use yii\helpers\Html;
use yii\helpers\Url;

/**
 * This is the model class for table "adv".
 *
 * @property integer $id
 * @property string $title
 * @property string $img
 * @property string $url
 * @property integer $status
 * @property string $text
 * @property string $params
 * @property string $text_rdy
 * @property integer $category
 * @property integer $user
 * @property string $name
 * @property string $email
 * @property string $phone
 * @property integer $area
 * @property integer $city
 * @property string $address
 * @property double $price
 * @property string $currency
 * @property integer $date_create
 * @property integer $date
 * @property string $block
 * @property integer $views
 * @property integer $premium
 * @property integer $vip
 * @property integer $color
 * @property integer $vip_start
 * @property integer $prem_start
 * @property integer $color_start
 * @property integer $up_start
 * @property integer $owner
 * @property string $from_id
 * @property integer $from_site
 * @property integer $ppg_type
 * @property integer $from_type
 * @property integer $not_email
 * @property integer $not_skype
 * @property string $added_ip
 * @property integer $show_cont
 * @property integer $show_map
 * @property integer $show_website
 * @property integer $upd
 * @property string $lng
 * @property integer $is_new
 * @property integer $notice
 * @property string $title_original
 * @property string $f_status
 * @property Categories $_category
 * @property bool $has_user
 * @property bool $some_photo
 * @property string $link
 * @property string $f_price
 * @property bool $is_vip
 * @property AdvImages $main_image
 * @property bool $is_color
 * @property bool $is_prem
 * @property Area $_area
 * @property User $_owner
 * @property City $_city
 * @property AdvImages[] $images
 * @property integer $notice_time
 * @property integer $step
 * @property string $category_stair
 * @property int $phone_code [int(11)]
 * @property int $free_rise
 * @property bool $can_free_rise
 * @property AdvParams[] $_params
 * @property AdvCounter $_views
 * @property int $views_today
 * @property string $color_status
 * @property bool $is_owner
 * @property int $views_total
 * @property array $params_arr
 * @property array $improve_list
 * @property AdvImages[] $Images
 * @property Brand $car_brand
 * @property mixed $status_name
 * @property string|int $free_rise_in
 * @property Model $car_model
 * @property boolean $is_coupon
 * @property bool|string $annonce
 * @property Adv $prevAd
 * @property Adv $nextAd
 * @property string $f_phone
 *
 */
class Adv extends ActiveRecord
{

    use AdvGenerateTrait, SlugTrait;

    const STATUS_BLOCK = 0;
    const STATUS_MODERATE = 1;
    const STATUS_ACTIVE = 2;
    const STATUS_PAUSE = 3;
    const STATUS_STOP = 4;
    const STATUS_REMOVED = 5;

    public $prem;

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'adv';
    }

    public static function findActive(){
        return self::find()->where(['status' => 2]);
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['img', 'text', 'text_rdy', 'address'], 'string'],
            [['premium', 'prem', 'vip', 'color'], 'boolean'],
            [['status', 'category', 'phone_code', 'user', 'area', 'city', 'date_create', 'date', 'views', 'vip_start', 'prem_start', 'color_start', 'up_start', 'owner', 'from_site', 'ppg_type', 'from_type', 'not_email', 'not_skype', 'show_cont', 'show_map', 'show_website', 'upd', 'is_new', 'notice', 'notice_time', 'step', 'free_rise'], 'integer'],
            [['title', 'url', 'block', 'title_original'], 'string', 'max' => 255],
            [['name', 'email'], 'string', 'max' => 100],
            [['phone'], 'number'],
            ['price', 'number', 'integerOnly' => false],
            [['phone'], PhoneValidator::className()],
            [['currency'], 'string', 'max' => 5],
            [['from_id'], 'string', 'max' => 20],
            [['added_ip'], 'string', 'max' => 16],
            [['lng'], 'string', 'max' => 2],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'title' => 'Title',
            'img' => 'Img',
            'url' => 'Url',
            'status' => 'Status',
            'text' => 'Text',
            'params' => 'Params',
            'text_rdy' => 'Text Rdy',
            'category' => 'Category',
            'user' => 'User',
            'name' => 'Name',
            'email' => 'Email',
            'phone' => 'Phone',
            'area' => 'Area',
            'city' => 'City',
            'address' => 'Address',
            'price' => 'Price',
            'currency' => 'Currency',
            'date_create' => 'Date Create',
            'date' => 'Date',
            'block' => 'Block',
            'views' => 'Views',
            'premium' => 'Premium',
            'vip' => 'Vip',
            'color' => 'Color',
            'vip_start' => 'Vip Start',
            'prem_start' => 'Prem Start',
            'color_start' => 'Color Start',
            'up_start' => 'Up Start',
            'owner' => 'Owner',
            'from_id' => 'From ID',
            'from_site' => 'From Site',
            'ppg_type' => 'Ppg Type',
            'from_type' => 'From Type',
            'not_email' => 'Not Email',
            'not_skype' => 'Not Skype',
            'added_ip' => 'Added Ip',
            'show_cont' => 'Show Cont',
            'show_map' => 'Show Map',
            'show_website' => 'Show Website',
            'upd' => 'Upd',
            'lng' => 'Lng',
            'is_new' => 'Is New',
            'notice' => 'Notice',
            'title_original' => 'Title Original',
            'notice_time' => 'Notice Time',
            'step' => 'Step',
            'free_rise' => 'Free rise',
        ];
    }

    public function afterFind()
    {
        $this->prem = $this->premium;
        parent::afterFind(); // TODO: Change the autogenerated stub
    }

    public function beforeValidate()
    {

        return parent::beforeValidate();
    }

    public function beforeSave($insert){

        $this->premium = $this->prem;

        if($insert){
            $this->date = time();
            $this->date_create = time();
        }
        if(!$this->url) $this->url = $this->getSlug();

        return parent::beforeSave($insert);
    }


    public function beforeDelete(){
        foreach ($this->_params as $param)
            $param->delete();

        foreach ($this->images as $image)
            @$image->delete();

        return parent::beforeDelete();
    }


    /* ***********************
     *  Связи
     *************************/

    /**
     * Получить модель города
     *
     * @return \yii\db\ActiveQuery
     */
    public function get_city(){
        return $this->hasOne(City::className(), ['id' => 'city']);
    }

    /**
     * Получить модель района
     *
     * @return \yii\db\ActiveQuery
     */
    public function get_area(){
        return $this->hasOne(Area::className(), ['id' => 'area']);
    }

    /**
     * Получить модель категории
     *
     * @return \yii\db\ActiveQuery
     */
    public function get_category(){
        return $this->hasOne(Categories::className(), ['id' => 'category']);
    }

    /**
     * Получить изображения
     *
     * @return \yii\db\ActiveQuery
     */
    public function getImages(){
        return $this->hasMany(AdvImages::className(), ['adv_id' => 'id'])->orderBy('main DESC, id');
    }

    /**
     * Получить модель пользователя
     *
     * @return \yii\db\ActiveQuery
     */
    public function get_owner(){
        if($this->user)
            return $this->hasOne(User::className(), ['id' => 'user']);
        else
            return $this->hasOne(User::className(), ['email' => 'email'])->where(['success_email' => true]);

    }

    /**
     * Получить форматированный статус
     *
     * @return string
     */
    public function getF_status(){
        if($this->status == self::STATUS_BLOCK)
            return '<span class="bg-danger">Banned</span>';
        if($this->status == self::STATUS_MODERATE)
            return '<span class="bg-warning">Moderated</span>';
        if($this->status == self::STATUS_ACTIVE)
            return '<span class="bg-success">Active</span>';
        if($this->status == self::STATUS_PAUSE)
            return '<span class="bg-info">Pause</span>';
        if($this->status == self::STATUS_STOP)
            return '<span class="bg-danger">Stopped</span>';
    }

    public function getStatus_name(){
        if($this->status == self::STATUS_BLOCK)
            return Yii::t('app', 'Banned');
        if($this->status == self::STATUS_MODERATE)
            return Yii::t('app', 'Moderated');
        if($this->status == self::STATUS_ACTIVE)
            return Yii::t('app', 'Active');
        if($this->status == self::STATUS_PAUSE)
            return Yii::t('app', 'Pause');
        if($this->status == self::STATUS_STOP)
            return Yii::t('app', 'Stopped');
        if($this->status == self::STATUS_REMOVED)
            return Yii::t('app', 'Removed');
    }

    /**
     * Цвет для статуса объявления (Админка)
     *
     * @return string
     */
    public function getColor_status(){
        if($this->status == self::STATUS_BLOCK)
            return 'danger';
        if($this->status == self::STATUS_MODERATE)
            return 'warning';
        if($this->status == self::STATUS_ACTIVE)
            return 'success';
        if($this->status == self::STATUS_PAUSE)
            return 'info';
        if($this->status == self::STATUS_STOP)
            return 'danger';
    }

    /**
     * Получить форматированную цену
     *
     * @return string
     */
    public function getF_price(){
        if(in_array($this->_category->top_category, [Categories::FREE, Categories::LOST]) && $this->price == 0)
            return Yii::t('app', 'Free');

        if($this->price == 0) return Yii::t('app', 'On request');
        return '€ '.number_format($this->price);
    }

    /**
     * Получить ссылку на главное изображение
     *
     * @return string
     */
    public function getMain_image(){

        $image = $this->getImages()->orderBy('main DESC')->one();
        if(!$image) return Yii::getAlias('@dist').Yii::$app->params['noimage'];

        return $image->Path('catalog');
    }

    /**
     * Получить ссылку на страницу объявления
     *
     * @return string
     */
    public function getLink(){
        return Url::to(['adv/get', 'url' => $this->url, 'id' => $this->id]) ;
    }

    /**
     * Является ли объявление VIP
     *
     * @return bool
     */
    public function getIs_vip(){
        return $this->vip && time() - $this->vip_start < Yii::$app->params['vip_period'] * 3600 * 24;
    }

    /**
     * Является ли объявление PREMIUM
     *
     * @return bool
     */
    public function getIs_prem(){
        return $this->premium && time() - $this->prem_start < Yii::$app->params['prem_period'] * 3600 * 24;
    }

    /**
     * Является ли объявление выделеным цветов
     *
     * @return bool
     */
    public function getIs_color(){
        return $this->color && time() - $this->color_start < Yii::$app->params['color_period'] * 3600 * 24;
    }

    /**
     * Есть ли у объявления юзер
     *
     * @return bool
     */
    public function getHas_user(){
        return $this->user != 0;
    }

    /**
     * Для показа значка с доп фото
     *
     * @return bool
     */
    public function getSome_photo(){
        return $this->getImages()->count() > 1 ? true : false;
    }

    /**
     * Название категори с начальной категории
     *
     * @return string
     */
    public function getCategory_stair(){
        $items = $this->_category->category_stair;

        return implode(' → ', $items);
    }

    /**
     * Можно ли использовать бесплатное поднятие объявления
     *
     * @return bool
     */
    public function getCan_free_rise(){
        if(Yii::$app->user->isGuest) return false;
        if(Yii::$app->user->identity->email != $this->email && Yii::$app->user->id != $this->user && Yii::$app->user->identity->phone != $this->phone)
            return false;

        $days_for_free = \Yii::$app->params['days_for_free_rise'];
        if($this->free_rise == 0 || (time() - $this->free_rise) > $days_for_free * 3600 * 24)
            return true;
        return false;
    }

    /**
     * Время через которое можно поднять объявление
     *
     * @return int|string
     */
    public function getFree_rise_in(){
        $time = time() - $this->free_rise;
        $days_for_free = \Yii::$app->params['days_for_free_rise'];
        if($this->free_rise == 0 || $time > $days_for_free * 86400)
            return 0;

//        var_dump(($days_for_free - 1) * 86400);
//        return $time;
        if(($days_for_free) * 86400 - $time < 86400){
            $t = ($days_for_free) * 86400 - $time;
            $minutes = ceil($t / 60);
            $hours = floor($minutes / 60);
            $minutes = ceil($minutes % 60);
            $hours = strlen($hours) == 1 ? '0'.$hours : $hours;
            $minutes = strlen($minutes) == 1 ? '0'.$minutes : $minutes;
            return $hours.':'.$minutes;
        }

        return ($days_for_free - ceil($time / (3600 * 24))).' '. Yii::t('app', 'days');
    }

    public function getImprove_list(){
        $ret = [];
        if($this->is_color)
            $ret[] = Yii::t('app', 'Highlight');
        if($this->is_prem )
            $ret[] = Yii::t('app', 'Premium ad');
        if($this->is_vip)
            $ret[] = Yii::t('app', 'VIP ad');


        return $ret;
    }


    /**
     * Поднять объявление в поиске
     */
    public function rise(){
        $this->up_start;
        $this->date = time();
        $this->free_rise = time();
        if($this->save())
            return true;
        else{
            var_dump($this->errors);
        }

        return false;
    }

    /**
     * Сделать объявление VIP
     */
    public function makeVip(){
        if($this->vip == 1 && $this->vip_start > time()){
            $left = time() - $this->vip_start;
            $this->vip_start = time() + $left;
        }
        else{
            $this->vip_start = time();
            $this->vip = 1;
        }
        $this->save();
    }

    /**
     * Сделать объявление премиум
     */
    public function makePremium(){
        if($this->premium == 1 && $this->prem_start > time()){
            $left = time() - $this->prem_start;
            $this->prem_start = time() + $left;
        }
        else{
            $this->prem_start = time();
            $this->premium = 1;
        }
        $this->save();
    }

    /**
     * Выделить объявление цветом
     */
    public function makeColor(){
        if($this->color == 1 && $this->color_start > time()){
            $left = time() - $this->color_start;
            $this->color_start = time() + $left;
        }
        else{
            $this->color_start = time();
            $this->color = 1;
        }
        $this->save();
    }

    /**
     * Параметры объявления
     *
     * @return \yii\db\ActiveQuery
     */
    public function get_params(){
        return $this->hasMany(AdvParams::className(), ['adv_id' => 'id']);
    }

    /**
     * Модели просмотров объявления
     *
     * @return \yii\db\ActiveQuery
     */
    public function get_views(){
        return $this->hasOne(AdvCounter::className(), ['id' => 'id']);
    }

    /**
     * Кол-во просмотров за сегодня
     *
     * @return int
     */
    public function getViews_today(){
        $views = $this->_views;
        return $views ? $views->today : 0;
    }

    /**
     * Общее кол-во просмотров
     *
     * @return int
     */
    public function getViews_total(){
        $views = $this->_views;
        return $views ? $views->total : 0;
    }

    /**
     * Является ли текущий пользователь владельцем объявления
     *
     * @return bool
     */
    public function getIs_owner(){
        if(Yii::$app->user->isGuest) return false;
        /** @var User $user */
        $user = Yii::$app->user->identity;

        if($this->user == $user->id) return true;
        if($user->success_email && $user->email == $this->email) return true;
        if($user->success_phone && $user->phone == $this->phone) return true;

        return false;
    }

    /**
     * Перевести параметры в массив
     *
     * @return array
     */
    public function getParams_arr(){
        $arr = [];
        foreach ($this->_params as $item) {
            $arr[$item->name] = $item->value;
        }
        return $arr;
    }

    /**
     * Получить модель бренда машины
     *
     * @return null|static
     */
    public function getCar_brand(){
        return Brand::findOne($this->params_arr['brand']);
    }

    /**
     * Получить модель модели машины
     *
     * @return null|static
     */
    public function getCar_model(){
        return Model::findOne($this->params_arr['model']);
    }

    /**
     * Является ли объявление купоном
     *
     * @return bool
     */
    public function getIs_coupon(){
        $coupons = Coupon::find()->where(['adv_id' => $this->id])->andWhere(['status' => Coupon::STATUS_FREE])->count();
        if($coupons) return true;
        return false;
    }

    /**
     * Форматированный вывод телефона
     *
     * @return string
     */
    public function getF_phone(){
        return '+'.$this->phone_code.$this->phone;
    }

    /** Аннонс
     * @return bool|string
     */
    public function getAnnonce(){
        $text = strip_tags($this->text_rdy);
        return substr($text, 0, 150);
    }

    public function addViews(){
        $model = $this->_views;
        if(!$model){
            $model = new AdvCounter();
            $model->id = $this->id;
        }

        $model->total += 1;
        $model->today += 1;
        $model->last_date = time();
        $model->save();
    }

    public function getNextAd(){
        return Adv::find()
            ->where(['<', 'id', $this->id])
            ->andWhere(['status' => self::STATUS_ACTIVE])
            ->andWhere(['category' => $this->category])
            ->orderBy('date DESC')
            ->limit(1)
            ->one();
    }
    public function getPrevAd(){
        return Adv::find()
            ->where(['>', 'id', $this->id])
            ->andWhere(['status' => self::STATUS_ACTIVE])
            ->andWhere(['category' => $this->category])
            ->orderBy('date')
            ->limit(1)
            ->one();
    }
}
