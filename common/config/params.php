<?php
return [
    'user.passwordResetTokenExpire' => 3600,
    'secret_key' => '7.,F-YG_{<0HVlKq5yH)uC*b}}F~6?/p',
    'adminEmail' => 'support@bazar-cy.com',
    'support_email' => 'support@bazar-cy.com',
    'support_phone' => '',
    'support_fax' => '',

    'phone_codes' => [
        '357' => '+357',
        '7' => '+7',
        '44' => '+44',
        '30' => '+30',
    ],
    'sms_phone_codes' => [
        '357' => '+357'
    ],
    'images' => [
        'adv' => [
            'catalog' => [240, 160, 60, 'crop'],
            'item' => [560, 372, 80],
            'thumb' => [70, 60, 40, 'crop'],
            'big' => [800, 600, 90],
        ],
        'news' => [
            'big' => [560, 350, 80],
        ]
    ],
    'collect_vars' => true,
];
