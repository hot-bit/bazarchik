<?php
namespace common\fixtures;

use yii\test\ActiveFixture;

class AdvFixture extends ActiveFixture
{
    public $modelClass = 'common\models\Adv';
}