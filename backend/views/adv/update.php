<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model \common\models\Adv */

$this->title = 'Update Adv: ' . $model->title . ' ID:'.$model->id;
$this->params['breadcrumbs'][] = ['label' => 'Advs', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->title, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="row">
    <div class="col-md-12">

        <div class="card">
            <div class="card-header">
                <strong><?= Html::encode($this->title) ?></strong>
            </div>
            <div class="card-block">

                <?= $this->render('_form', [
                    'model' => $model,
                    'params_model' => $params_model
                ]) ?>

            </div>
        </div>
    </div>
</div>

