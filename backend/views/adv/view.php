<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\Adv */

$this->title = $model->title;
$this->params['breadcrumbs'][] = ['label' => 'Advs', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>

<div class="row">
    <div class="col-md-12">

        <div class="card">
            <div class="card-header">
                <strong><?= Html::encode($this->title) ?></strong>
            </div>
            <div class="card-block">

    <p>
        <?= Html::a('Update', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Delete', ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            'title',
            'url',
            'status',
            'text:ntext',
            'text_rdy:ntext',
            'category',
            'user',
            'name',
            'email:email',
            'phone',
            'area',
            'city',
            'address:ntext',
            'price',
            'currency',
            'date_create:date',
            'date:date',
            'views',
            'premium:boolean',
            'vip:boolean',
            'color:boolean',
            'owner',
            'not_email:boolean',
            'not_skype:boolean',
            'added_ip',
            'show_cont:boolean',
            'show_map:boolean',
            'show_website:boolean',
            'lng',
            'is_new:boolean',
        ],
    ]) ?>

</div>
</div>
</div>
</div>
