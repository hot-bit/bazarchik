<?php

use common\models\Categories;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model \common\models\Adv */
/* @var $form yii\widgets\ActiveForm */
/* @var $params_model \common\models\forms\ParamsForm  */

$category_arr = [
    'id' => [],
    'options' => []
];
foreach($model->_category->stairs_models as $item) {
    $category_arr['id'][] = $item->id;
    $category_arr['options'][] = ArrayHelper::map(Categories::find()->where(['parent' => $item->parent])->andWhere(['header' => 0])->all(), 'id', 'name');
};


if(empty($model->text_rdy)) $model->text_rdy = $model->text;

$filter = $model->_category->filter;


$this->registerJs("
    $('.category-wrap').on('change' ,'.category-select', function(){
        var level = parseInt($(this).parent().attr('data-level'));
        var val = $(this).val();
        
        $.post('".\yii\helpers\Url::to(['ajax/get-category-select'])."', {parent:val}, function(o){ 
            $('.category-area[data-level=\"'+(level+1)+'\"]').html(o);
            $('#adv-category').val(val)
            for(var i = level+2; i <= 4; i++ ){
                $('.category-area[data-level=\"'+i+'\"]').html('')
            }
        })
        
    });
");


$needGenerate = false;
if($model->category == Categories::CARS)
    $needGenerate = true;
?>
<style>
    .field-group {
        height: 30px;
    }
    .field-group select{
        height: 100%;
    }
    .field-group input{
        padding-left: 10px;
        height: 100%;
        margin-left: -1px;
    }
</style>
<div class="adv-form vue-block">

    <?php $form = ActiveForm::begin(); ?>
    
    <?= $form->errorSummary([$model, $params_model])?>

    <div class="row">

        <div class="col-sm-3">
            <?= $form->field($model, 'lng')->dropDownList( ArrayHelper::map(\common\models\Lang::find()->all(), 'url', 'name')) ?>

            <?= $form->field($model, 'is_new')->checkbox() ?>

            <?= $form->field($model, 'name')->textInput(['maxlength' => true, 'readonly' => true]) ?>

            <?= $form->field($model, 'email')->textInput(['maxlength' => true, 'readonly' => true]) ?>

            <?= $form->field($model, 'phone')->widget(\frontend\widgets\bazar\PhoneField::className(), [
                'enableValidation' => false
            ]) ?>

            <?= $form->field($model, 'city')->dropDownList(\common\models\City::map()) ?>

            <?= $form->field($model, 'area')->dropDownList(\common\models\Area::map($model->city)) ?>

            <?= $form->field($model, 'price') ?>

            <?= Html::activeHiddenInput($model, 'category', ['id' => 'adv-category'])?>

            <label for=""><?=Yii::t('app', 'Categories');?> </label>
            <div class="form-group category-wrap">

                <div class="category-area" data-level="1">
                    <?if($category_arr['options'][0]):?>
                        <?= Html::dropDownList('category',  $category_arr['id'][0], $category_arr['options'][0], ['class' => 'form-control category-select', 'prompt' => Yii::t('app', '-- none --')])?>
                    <?endif?>
                </div>
                <div class="category-area" data-level="2">
                    <?if($category_arr['options'][1]):?>
                        <?= Html::dropDownList('category',  $category_arr['id'][1], $category_arr['options'][1], ['class' => 'form-control category-select', 'prompt' => Yii::t('app', '-- none --')])?>
                    <?endif?>
                </div>
                <div class="category-area" data-level="3">
                    <?if($category_arr['options'][2]):?>
                        <?= Html::dropDownList('category',  $category_arr['id'][2], $category_arr['options'][2], ['class' => 'form-control category-select', 'prompt' => Yii::t('app', '-- none --')])?>
                    <?endif?>
                </div>
                <div class="category-area" data-level="4">
                    <?if($category_arr['options'][3]):?>
                        <?= Html::dropDownList('category',  $category_arr['id'][3], $category_arr['options'][3], ['class' => 'form-control category-select', 'prompt' => Yii::t('app', '-- none --')])?>
                    <?endif?>
                </div>
            </div>


        </div>

        <div class="col-sm-6">
            <admin-adv-title title="<?= $model->title?>"
                             need_generate="<?= $needGenerate ? 1 : 0?>"
                             url="<?= $model->url?>"
                             ajaxurl="<?= \yii\helpers\Url::to(['ajax/'])?>"
                             adv_id="<?= $model->id?>"></admin-adv-title>

<!--            --><?//= $form->field($model, 'title')->textInput(['maxlength' => true, 'id' => 'a-title']) ?>
<!---->
<!--            --><?//= $form->field($model, 'url')->textInput(['maxlength' => true, 'readonly' => true]) ?>

            <?= $form->field($model, 'text')->textarea(['rows' => 6, 'id' => 'a-text', 'class' => 'form-control editor']) ?>

            <?= $form->field($model, 'text_rdy')->textarea(['rows' => 6]) ?>
        </div>
    </div>


    <div class="col-sm-6">
        <?if($filter):?>
            <?=$this->render('//filters/add-adv/'.$filter, ['id' => $model->id, 'form' => $form, 'model' => $params_model])?>
        <?endif?>
    </div>

    <upload-images adv_id="<?= $model->id ?>" text_upload="<?= Yii::t('app', 'Upload images'); ?> "></upload-images>


    <div class="form-group" style="margin-top: 40px">
        <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
        <admin-moderate-ad adv_id="<?= $model->id ?>" url="<?= \yii\helpers\Url::to(['adv/']) ?>"></admin-moderate-ad>
    </div>

    <?php ActiveForm::end(); ?>

</div>
