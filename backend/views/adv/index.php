<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\Pjax;
/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Ads';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="adv-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <?php Pjax::begin(); ?>
    <div class="visible-desktop">
        <?= GridView::widget([
            'dataProvider' => $dataProvider,
            'filterModel' => $searchModel,
            'columns' => [
                'id',
                'title',
                'email:email',
                'phone',
                'date_create:date',
                'status' => [
                    'attribute' => 'status',
                    'format' => 'html',
                    'value' => 'f_status'
                ],
                ['class' => 'common\models\CMSActionColumn'],
            ],
        ]); ?>
    </div>

    <div class="visible-mobile">
        <?= \yii\widgets\ListView::widget([
            'dataProvider' => $dataProvider,
            'itemView' => function(\common\models\Adv $model, $key, $index, $widget){
                return Html::beginTag('div', ['class' => 'item-block bg-'.$model->color_status]).'#'.$model->id. '<br /> '.Html::a($model->title, ['update', 'id' => $key]).Html::endTag('div');
            }
        ])?>
    </div>
    <?php Pjax::end(); ?></div>
