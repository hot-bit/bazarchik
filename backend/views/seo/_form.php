<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model common\models\Seo */
/* @var $form yii\widgets\ActiveForm */

$langs = ['en', 'ru', 'gr'];
?>

<div class="seo-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'url')->textInput(['maxlength' => true]) ?>

    <div>
        <ul class="nav nav-tabs" role="tablist">
            <li class="nav-item">
                <a href="#lng-en" class="nav-link active" role="tab" aria-controls="lng-en" data-toggle="tab">English</a>
            </li>
            <li class="nav-item">
                <a href="#lng-gr" class="nav-link" role="tab" aria-controls="lng-gr" data-toggle="tab">Greek </a>
            </li>
            <li class="nav-item">
                <a href="#lng-ru" class="nav-link" role="tab" aria-controls="lng-ru" data-toggle="tab">Русский </a>
            </li>
        </ul>
        <div class="tab-content">

            <? $i = 0; foreach($langs as $lng):?>
                <div class="tab-pane <?= $i == 0 ? 'active' : ''?>" id="lng-<?= $lng?>" role="tabpanel">
                    <?=$form->field($model, 'title_'.$lng)->textInput()?>
                    <?=$form->field($model, 'keywords_'.$lng)->textInput()?>
                    <?=$form->field($model, 'description_'.$lng)->textarea()?>
                    <?=$form->field($model, 'h1_'.$lng)->textInput()?>
                    <?=$form->field($model, 'text_top_'.$lng)->textarea()?>
                    <?=$form->field($model, 'text_bot_'.$lng)->textarea()?>
                </div>
            <? $i++; endforeach?>

        </div>
    </div>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? Yii::t('app', 'Create') : Yii::t('app', 'Update'), ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
