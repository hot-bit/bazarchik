<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model common\models\Pages */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="pages-form">

    <?php $form = ActiveForm::begin(); ?>


    <?=$form->errorSummary($model)?>


    <?= $form->field($model, 'link')->textInput(['maxlength' => true]) ?>

    <ul class="nav nav-tabs" role="tablist">
        <li class="nav-item"><a class="nav-link active" data-toggle="tab" href="#eng" role="tab" aria-controls="eng">English</a></li>
        <li class="nav-item"><a class="nav-link" data-toggle="tab" href="#rus" role="tab" aria-controls="rus">Russian</a></li>
        <li class="nav-item"><a class="nav-link" data-toggle="tab" href="#greek" role="tab" aria-controls="greek">Greek</a></li>
    </ul>
    <div class="tab-content">
        <div class="tab-pane active" id="eng" role="tabpanel" >
            <?= $form->field($model, 'name_en')->textInput() ?>
        </div>
        <div class="tab-pane" id="rus" role="tabpanel">
            <?= $form->field($model, 'name_ru')->textInput() ?>
        </div>
        <div class="tab-pane" id="greek" role="tabpanel">
            <?= $form->field($model, 'name_gr')->textInput() ?>
        </div>
    </div>







    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? Yii::t('app', 'Create') : Yii::t('app', 'Update'), ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
