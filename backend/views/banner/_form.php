<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model common\models\Banners */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="banners-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'name')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'link')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'status')->dropDownList(\common\models\Banner::statusList()) ?>

    <?= $form->field($model, 'start_date_formatted')->textInput(['type' => 'date']) ?>

    <?= $form->field($model, 'finish_date_formatted')->textInput(['type' => 'date']) ?>

    <?= $form->field($model, 'place')->dropDownList(\common\models\Banner::placeList(), ['prompt' => '-- Choose a place --']) ?>

    <?= $form->field($model, 'ru')->checkbox() ?>

    <?= $form->field($model, 'en')->checkbox() ?>

    <?= $form->field($model, 'gr')->checkbox() ?>


    <?=\backend\widgets\bazar\ImageField::widget([
        'model' => $model,
        'form' => $form,
        'field' => 'imageField'
    ])?>


    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
