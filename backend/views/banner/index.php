<?php

use yii\grid\GridView;
use yii\helpers\Html;
use yii\widgets\Pjax;

/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Banners';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="row">
    <div class="col-md-12">

        <div class="card">
            <div class="card-header">
                <strong><?= Html::encode($this->title) ?></strong>
            </div>
            <div class="card-block">

                <p>
                    <?= Html::a('Create Banners', ['create'], ['class' => 'btn btn-success']) ?>
                </p>
                <?php Pjax::begin(); ?>                                            <?= GridView::widget([
                    'dataProvider' => $dataProvider,
                    'columns' => [

                        'id',
                        'thumb:image',
                        'name',
                        'email',
                        'link',
                        'statusFormatted:text:Status',
                        'placeFormatted:text:Place',
                        'created_at:date',
                        // 'date_to',
                        // 'ru',
                        // 'en',
                        // 'gr',
                        // 'right',
                        // 'adv_right',
                        // 'bottom',
                        // 'adv_bottom',
                        // 'inside',
                        // 'img',

                        ['class' => 'common\models\CMSActionColumn'],
                    ],
                ]); ?>
                <?php Pjax::end(); ?>            </div>
        </div>
    </div>
</div>
