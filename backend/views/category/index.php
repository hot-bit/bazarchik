<?php

/* @var $this yii\web\View */
use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Categories';
$this->params['breadcrumbs'][] = 'Categories';
?>


<div class="row">
    <div class="col-md-6">

        <div class="card">
            <div class="card-header">
                <strong><?= Html::encode($this->title) ?></strong>
            </div>
            <div class="card-block">
                <div class="admin_tree">
                    <?=\common\models\Categories::AdminTree()?>
                </div>

            </div>
        </div>

    </div>
    <div class="col-md-6">

        <div class="card">
            <div class="card-header">
                <strong>Create new Category</strong>
            </div>
            <div class="card-block">

                <?php $form = ActiveForm::begin(['options' => ['class' => 'admin_category_form']]); ?>

                    <div class="form-group field-categories-parent required has-success">
                        <label class="control-label" for="categories-parent">Parent</label>
                        <select id="categories-parent" class="form-control" name="Categories[parent]" aria-required="true" aria-invalid="false">
                             <?=\common\models\Categories::GenerateOptions()?>
                        </select>
                        <div class="help-block"></div>
                    </div>

                    <?=$form->field($model, 'name_en')->textInput(['class' => 'form-control'])?>
                    <?=$form->field($model, 'name_ru')->textInput(['class' => 'form-control'])?>
                    <?=$form->field($model, 'name_gr')->textInput(['class' => 'form-control'])?>
                    <?=$form->field($model, 'url')->textInput(['class' => 'form-control'])?>
                    <?=$form->field($model, 'icon')->textInput(['class' => 'form-control'])?>
                    <?=$form->field($model, 'color')->dropDownList(\common\models\Categories::$color_list, ['class' => 'form-control'])?>
                    <?=$form->field($model, 'status')->checkbox()?>
                    <?=$form->field($model, 'header')->checkbox()?>
                    <div><?=Html::submitButton('Save', ['class' => 'btn btn-success'])?></div>
                <?php ActiveForm::end(); ?>
            </div>
        </div>

    </div>
</div>
