<?php
/**
 * Created by PhpStorm.
 * User: yorks
 * Date: 14.06.2017
 * Time: 12:58
 */
use yii\helpers\Html;
use yii\widgets\ActiveForm;


/*



    <?if($item->status== 1):?><span class="icon-eye"></span><?endif?>
    <?if($item->header == 1):?><span class="icon-tag"></span><?endif?>
    <div class="float-right">
        <a class="" onclick="editItem($(this).parent());" href="#"><span class="icon-wrench"></span></a>
        <a href="/admin/category/catDel/<?=$item->id?>" onclick="confirm('Delete category?');"><span class="icon-trash"></span></a>
    </div>

 */
?>

<span class="admin_tree_item" rel="<?=$item->id?>">

    <div class="admin_tree_item_title">
        <div class="admin_tree_item_left_side">
            <?if($item->nodes != null):?><a href="#" class="tree_pl icon-plus"></a><?endif?>
            <?=$item->name_en?> #<?=$item->id?>
        </div>
        <a href="#" class="admin_tree_item_right_side open_tree_body icon-arrow-down"></a>
    </div>
    <div class="admin_tree_item_body">
        <?php $form = ActiveForm::begin(['options' => ['class' => 'admin_category_form']]); ?>
            <div class="form-group">
                <div class="nput-prepend input-group">
                    <span class="input-group-addon">EN</span>
                    <?=Html::activeTextInput($item, 'name_en', ['class' => 'form-control'])?>
                </div>
            </div>
            <div class="form-group">
                <div class="nput-prepend input-group">
                    <span class="input-group-addon">RU</span>
                    <?=Html::activeTextInput($item, 'name_ru', ['class' => 'form-control'])?>
                </div>
            </div>
            <div class="form-group">
                <div class="input-prepend input-group">
                    <span class="input-group-addon">GR</span>
                    <?=Html::activeTextInput($item, 'name_gr', ['class' => 'form-control'])?>
                </div>
            </div>
            <div class="form-group">
                <div class="input-prepend input-group">
                    <span class="input-group-addon">URL</span>
                    <?=Html::activeTextInput($item, 'url', ['class' => 'form-control'])?>
                </div>
            </div>
            <div class="form-group">
                <div class="input-prepend input-group">
                    <span class="input-group-addon">POS</span>
                    <?=Html::activeTextInput($item, 'position', ['class' => 'form-control'])?>
                </div>
            </div>
            <?if($item->level == 1):?>
                <div class="form-group">
                    <div class="input-prepend input-group">
                        <span class="input-group-addon">COLOR</span>
                        <?=Html::activeDropDownList($item, 'color', \common\models\Categories::$color_list, ['class' => 'form-control'])?>
                    </div>
                </div>
                <div class="form-group">
                    <div class="input-prepend input-group">
                        <span class="input-group-addon">ICON</span>
                        <?=Html::activeTextInput($item, 'icon', ['class' => 'form-control'])?>
                    </div>
                </div>
            <?endif;?>
            <div class="form-group">

                <div class="float-left">
                    Status
                    <label class="switch switch-3d switch-success">
                        <?=Html::checkbox('Categories[status]', $item->status, ['class' => 'switch-input'])?>
                        <span class="switch-label"></span>
                        <span class="switch-handle"></span>
                    </label>
                </div>
                <div class="float-left" style="margin-left: 10px;">
                    Header
                    <label class="switch switch-3d switch-success">
                        <?=Html::checkbox('Categories[header]', $item->header, ['class' => 'switch-input'])?>
                        <span class="switch-label"></span>
                        <span class="switch-handle"></span>
                    </label>
                </div>
                <div class="clearfix"></div>
            </div>
            <div class="form-group">
                <?=Html::activeHiddenInput($item, 'id')?>
                <?=Html::submitButton(Html::tag('i', '', ['class' => 'icon-check']).' Save', ['class' => 'btn btn-success'])?>
                <?=Html::a(Html::tag('i', '', ['class' => 'icon-trash']).' Delete', '#', ['class' => 'btn btn-danger'])?>
            </div>

        <?php ActiveForm::end(); ?>
    </div>
</span>