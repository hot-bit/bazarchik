<?php
/**
 * Created by PhpStorm.
 * User: York
 * Date: 01.02.2018
 * Time: 16:48
 */

/* @var $model \common\models\User */

use yii\helpers\Html;
use yii\widgets\DetailView;

$this->title = 'User\'s shop';
$this->params['breadcrumbs'][] = ['label' => 'Shops', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>

<div class="row">
    <div class="col-md-12">

        <div class="card">
            <div class="card-header">
                <strong><?= Html::encode($this->title) ?></strong>
            </div>
            <div class="card-block">

                <div style="margin-bottom: 20px">
                    <?if($model->is_business == \common\models\User::BUSINESS_MODERATE):?>
                        <a href="<?= \yii\helpers\Url::to(['activate', 'id' => $model->id])?>" class="btn btn-success">Activate user</a>
                    <?endif?>
                    <a href="<?= \yii\helpers\Url::to(['block', 'id' => $model->id])?>" class="btn btn-danger">Block user</a>
                </div>


                <?= DetailView::widget([
                    'model' => $model,
                    'attributes' => [
                        'id',
                        'shop_name',
                        'phone',
                        'email',
                        'business_link',
                        'company_description',
                        'addresses' => [
                            'attribute' => 'address',
                            'value' => function($item){
                                $ret = [];
                                if($item->address_array)
                                    foreach ($item->address_array as $value)
                                        $ret[] = $value['name'];
                                return implode(' | ', $ret);
                            }
                        ],
                    ],
                ]) ?>

            </div>
        </div>
    </div>
</div>

