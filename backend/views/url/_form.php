<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model common\models\UrlRoute */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="url-route-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'url')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'module_name')->dropDownList(\common\models\UrlRoute::ModuleList()) ?>

    <?= $form->field($model, 'module_id')->textInput() ?>

    <?= $form->field($model, 'lng_id')->dropDownList(\common\models\Lang::map(), ['prompt' => '-- All --']) ?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
