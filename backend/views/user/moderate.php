<?php
/**
 * Created by PhpStorm.
 * User: York
 * Date: 11.01.2018
 * Time: 14:41
 */

use yii\grid\GridView;
use yii\helpers\Html;
use yii\widgets\Pjax;

/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */
/* @var $searchModel common\models\search\UserSearch */

$this->title = Yii::t('app', 'Users');
$this->params['breadcrumbs'][] = $this->title;
?>




<div class="row">
    <div class="col-md-12">

        <div class="card">
            <div class="card-header">
                <strong><?= Html::encode($this->title) ?></strong>
            </div>
            <div class="card-block">
                <?php // echo $this->render('_search', ['model' => $searchModel]); ?>
                <?php Pjax::begin(); ?>    <?= GridView::widget([
                    'dataProvider' => $dataProvider,
//                    'filterModel' => $searchModel,
                    'columns' => [
                        'id',
                        'name',
                        'email:email',
                        'f_success_email:html:Active email',
                        'phone',
                        'f_success_phone:html:Active phone',
                        'website:url',
                        'f_status:html:Status',

                        [
                            'class' => 'common\models\CMSActionColumn',
                            'template' => '{approve} {ban} {update}',
                            'buttons' => [
                                'approve' => function($url, $model){
                                    return Html::a('<i class="icon-check icons"></i>', ['approve', 'id' => $model->id]);
                                },
                                'ban' => function($url, $model){
                                    return Html::a('<i class="icon-close icons"></i>', ['ban', 'id' => $model->id]);
                                }

                            ]
                        ],
                    ],
                ]); ?>
                <?php Pjax::end(); ?>

            </div>
        </div>

    </div>
</div>

