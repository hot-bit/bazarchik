<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model common\models\Users */

$this->title = $model->name;
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Users'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>

<div class="row">
    <div class="col-md-12">

        <div class="card">
            <div class="card-header">
                <strong><?= Html::encode($this->title) ?></strong>
            </div>
            <div class="card-block">


    <p>
        <?= Html::a(Yii::t('app', 'Update'), ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a(Yii::t('app', 'Delete'), ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => Yii::t('app', 'Are you sure you want to delete this item?'),
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            'name',
            'role',
            'type',
            'status',
            'password',
            'salt',
            'phone',
            'email:email',
            'avatar',
            'skype',
            'website',
            'company',
            'lat',
            'lng',
            'gender',
            'last_active',
            'b_day',
            'b_month',
            'b_year',
            'last_login',
            'date_reg',
            'gmt',
            'hash',
            'ip',
            'city',
            'area',
            'fav:ntext',
            'sms_code',
            'email_code:email',
            'success_phone',
            'success_email:email',
            'fb_id',
            'g_id',
            'fb_access',
            'money',
            'count_sms',
            'confirm_site',
            'send_sms', 
            'ref_status',
            'forum_messages',
            'strike',
            'address:ntext',
            'is_business',
            'business_until',
            'company_description:ntext',
            'business_link',
        ],
    ]) ?>

</div>
</div>
</div>
</div>
