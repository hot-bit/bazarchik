<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model common\models\search\UserSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="user-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <?= $form->field($model, 'id') ?>

    <?= $form->field($model, 'name') ?>

    <?= $form->field($model, 'role') ?>

    <?= $form->field($model, 'type') ?>

    <?= $form->field($model, 'status') ?>

    <?php // echo $form->field($model, 'password') ?>

    <?php // echo $form->field($model, 'salt') ?>

    <?php // echo $form->field($model, 'phone') ?>

    <?php // echo $form->field($model, 'email') ?>

    <?php // echo $form->field($model, 'avatar') ?>

    <?php // echo $form->field($model, 'skype') ?>

    <?php // echo $form->field($model, 'website') ?>

    <?php // echo $form->field($model, 'company') ?>

    <?php // echo $form->field($model, 'lat') ?>

    <?php // echo $form->field($model, 'lng') ?>

    <?php // echo $form->field($model, 'gender') ?>

    <?php // echo $form->field($model, 'last_active') ?>

    <?php // echo $form->field($model, 'b_day') ?>

    <?php // echo $form->field($model, 'b_month') ?>

    <?php // echo $form->field($model, 'b_year') ?>

    <?php // echo $form->field($model, 'last_login') ?>

    <?php // echo $form->field($model, 'date_reg') ?>

    <?php // echo $form->field($model, 'gmt') ?>

    <?php // echo $form->field($model, 'hash') ?>

    <?php // echo $form->field($model, 'ip') ?>

    <?php // echo $form->field($model, 'city') ?>

    <?php // echo $form->field($model, 'area') ?>

    <?php // echo $form->field($model, 'fav') ?>

    <?php // echo $form->field($model, 'sms_code') ?>

    <?php // echo $form->field($model, 'email_code') ?>

    <?php // echo $form->field($model, 'success_phone') ?>

    <?php // echo $form->field($model, 'success_email') ?>

    <?php // echo $form->field($model, 'fb_id') ?>

    <?php // echo $form->field($model, 'g_id') ?>

    <?php // echo $form->field($model, 'fb_access') ?>

    <?php // echo $form->field($model, 'money') ?>

    <?php // echo $form->field($model, 'count_sms') ?>

    <?php // echo $form->field($model, 'confirm_site') ?>

    <?php // echo $form->field($model, 'send_sms') ?>

    <?php // echo $form->field($model, 'referral') ?>

    <?php // echo $form->field($model, 'ref_status') ?>

    <?php // echo $form->field($model, 'send_push') ?>

    <?php // echo $form->field($model, 'forum_messages') ?>

    <?php // echo $form->field($model, 'strike') ?>

    <?php // echo $form->field($model, 'locale') ?>

    <?php // echo $form->field($model, 'address') ?>

    <?php // echo $form->field($model, 'is_business') ?>

    <?php // echo $form->field($model, 'business_until') ?>

    <?php // echo $form->field($model, 'company_description') ?>

    <?php // echo $form->field($model, 'business_link') ?>

    <?php // echo $form->field($model, 'auth_key') ?>

    <?php // echo $form->field($model, 'phone_code') ?>

    <?php // echo $form->field($model, 'phone_try') ?>

    <?php // echo $form->field($model, 'password_reset_token') ?>

    <div class="form-group">
        <?= Html::submitButton(Yii::t('app', 'Search'), ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton(Yii::t('app', 'Reset'), ['class' => 'btn btn-default']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
