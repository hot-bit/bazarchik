<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\helpers\Url;
use yii\widgets\Pjax;
/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('app', 'Users');
$this->params['breadcrumbs'][] = $this->title;
?>


<div class="row">
    <div class="col-md-12">

        <div class="card">
            <div class="card-header">
                <strong><?= Html::encode($this->title) ?></strong>
            </div>
            <div class="card-block">
                <p>
                    <?= Html::a(Yii::t('app', 'User list'), ['index'], ['class' => 'btn btn-primary']) ?>
                </p>
                <?php Pjax::begin(); ?>
                <?= GridView::widget([
                    'dataProvider' => $dataProvider,
                    'columns' => [
                        'id',
                        'name',
                        'email',
                        'phone',
                        'company',
                        'website',

                        [
                            'class' => 'yii\grid\ActionColumn',
                            'template' => '{siteApprove} {siteDeny}',
                            'buttons' => [
                                'siteApprove' => function ($ur, $model) {
                                    return Html::a(
                                        '<span class="icon-check"></span>',
                                        '/admin/user/site_status?id='.$model->id.'&status=1',
                                        [
                                            'title' => 'Approve',
                                            'data-pjax' => '0',
                                        ]
                                    );
                                },
                                'siteDeny' => function ($url, $model) {
                                    return Html::a(
                                        '<span class="icon-close"></span>',
                                        '/admin/user/site_status?id='.$model->id.'&status=2',
                                        [
                                            'title' => 'Deny',
                                            'data-pjax' => '0',
                                        ]
                                    );
                                },
                            ],
                        ],
                    ],
                ]); ?>
                <?php Pjax::end(); ?>

            </div>
        </div>

    </div>
</div>



