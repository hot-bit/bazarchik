<?php
/**
 * Created by PhpStorm.
 * User: York
 * Date: 28.09.2017
 * Time: 22:42
 *
 * @var \common\models\forms\ParamsForm $model
 */

?>

<div class="row">
    <div class="col-sm-3">
        <?= $form->field($model, 'city_id')->dropDownList(\common\models\City::map(), [
            'onchange' => "
             $.ajax({
                type     :'POST',
                cache    : false,
                url  : '" . \yii\helpers\Url::to(['ajax/area-by-city']) . "',
                data: {id: $(this).val(), model:'ParamsForm', dest:'common-models-forms'},
                success  : function(response) {
                    $('.field-paramsform-area .SumoSelect').remove();
                    $('.field-paramsform-area').append(response.data);  
                    $('#paramsform-area').SumoSelect();
                }
            });  
            return false;",
        ]) ?>
    </div>
    <div class="col-sm-3">
        <?= $form->field($model, 'area_id')->dropDownList(\common\models\Area::map($model->city_id), ['prompt' => Yii::t('app', 'Select area')]) ?>
    </div>
    <div class="col-sm-3">
        <?= $form->field($model, 'bedrooms')->dropDownList(\common\models\map\Bedrooms::Map(), ['prompt' => Yii::t('app', '-- None --')]) ?>
    </div>
    <div class="col-sm-3">
        <?= $form->field($model, 'cover')->textInput() ?>
    </div>
</div>