<?php
/**
 * Created by PhpStorm.
 * User: York
 * Date: 01.12.2017
 * Time: 18:33
 */

?>

<div class="row">

    <div class="col-sm-6">
        <?= $form->field($model, 'year')->textInput() ?>
    </div>
    <div class="col-sm-6"><?= $form->field($model, 'running')->textInput() ?></div>
    <div class="col-sm-6">
        <?= $form->field($model, 'place')->textInput() ?>
    </div>
    <div class="col-sm-6">
        <?= $form->field($model, 'vin')->textInput() ?>
    </div>
</div>