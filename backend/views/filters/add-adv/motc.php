<?php
/**
 * Created by PhpStorm.
 * User: York
 * Date: 28.09.2017
 * Time: 22:42
 */

?>


<div class="row">
    <div class="col-sm-3">
        <?= $form->field($model, 'brand')->dropDownList(\common\models\Brand::map()) ?>
    </div>
    <div class="col-sm-3">
        <?= $form->field($model, 'year')->textInput() ?>
    </div>
    <div class="col-sm-3">
        <?= $form->field($model, 'vin')->textInput() ?>
    </div>
    <div class="col-sm-3"><?= $form->field($model, 'running')->textInput() ?></div>
</div>

<?= $form->field($model, 'color')->widget(\frontend\widgets\cms\CarColorField::className(), [
    'items' => \common\models\map\CarColor::Map()
]) ?>

<?= $form->field($model, 'box')->widget(\frontend\widgets\cms\RadioGroupField::className(), [
    'items' => \common\models\map\CarBox::Map(),
    'empty' => 1
]) ?>