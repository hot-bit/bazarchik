<?php
/**
 * Created by PhpStorm.
 * User: York
 * Date: 28.09.2017
 * Time: 22:42
 *
 * @var $id integer
 * @var $form ActiveForm
 * @var $model \common\models\forms\ParamsForm
 */

use common\models\map\CarParams;
use yii\widgets\ActiveForm;

?>

<div class="row">
    <div class="col-sm-3">
        <?= $form->field($model, 'brand')->dropDownList(\common\models\Brand::map(), [
            'onchange' => "
             $.ajax({
                type     :'POST',
                cache    : false,
                url  : '" . \yii\helpers\Url::to(['ajax/car-models']) . "',
                data: {id: $(this).val(), model:'ParamsForm', dest:'common-models-forms'},
                success  : function(response) {
                    $('.field-paramsform-model .SumoSelect').remove();
                    $('.field-paramsform-model').append(response.data);  
                    $('#paramsform-model').SumoSelect();
                }
            });  
            return false;",
        ]) ?>
    </div>
    <div class="col-sm-3">
        <?= $form->field($model, 'model')->dropDownList(\common\models\Model::map($model->brand), ['prompt' => Yii::t('app', 'Select model')]) ?>
    </div>
    <div class="col-sm-3">
        <?= $form->field($model, 'year')->textInput() ?>
    </div>
    <div class="col-sm-3">
        <?= $form->field($model, 'volume')->textInput() ?>
    </div>
    <div class="col-sm-3">
        <?= $form->field($model, 'vin')->textInput() ?>
    </div>
</div>
<?if(!isset($new) || !$new):?>
    <div class="row">
        <div class="col-sm-3"><?= $form->field($model, 'running')->textInput() ?></div>
    </div>
<?endif?>


<div class="row">
    <div class="col-sm-6">

        <?= $form->field($model, 'body')->dropDownList(\common\models\map\CarBodyType::Map()) ?>

        <?= $form->field($model, 'doors')->dropDownList([Yii::t('app', 'Not specify'), 2, 3, 4, 5]) ?>

        <?= $form->field($model, 'color')->dropDownList(\common\models\map\CarColor::SmallMap()) ?>

        <?= $form->field($model, 'engine')->dropDownList(\common\models\map\CarEngine::Map()) ?>
    </div>
    <div class="col-sm-6">

        <?= $form->field($model, 'box')->dropDownList(\common\models\map\CarBox::Map()) ?>

        <?= $form->field($model, 'drive')->dropDownList(\common\models\map\CarDrive::Map()) ?>

        <?= $form->field($model, 'wheel')->dropDownList(\common\models\map\CarWheel::Map()) ?>

    </div>
</div>

<div class="row">
    <div class="col-sm-6">
        <div class="form-group">
            <?= $form->field($model, 'st_wheel')->dropDownList(CarParams::WheelAssist()) ?>
        </div>

        <div class="form-group">
            <?=$form->field($model, 'climat')->dropDownList( CarParams::Climate())?>
            <?=$form->field($model, 'climate_wheel_contr')->checkbox( )?>
            <?=$form->field($model, 'climate_aterm')->checkbox( )?>
        </div>

        <div class="form-group">
            <label style="font-weight: bold;"><?= $model->getAttributeLabel('heating') ?></label>
            <?= $form->field($model, 'heating_front_seat')->checkbox( ) ?>
            <?= $form->field($model, 'heating_rear_seat')->checkbox( ) ?>
            <?= $form->field($model, 'heating_mirrors')->checkbox( ) ?>
            <?= $form->field($model, 'heating_rear_window')->checkbox( ) ?>
            <?= $form->field($model, 'heating_wheel')->checkbox( ) ?>
        </div>

        <div class="form-group">
            <label style="font-weight: bold;"><?= $model->getAttributeLabel('mem') ?></label>
            <?= $form->field($model, 'mem_front_seats')->checkbox( ) ?>
            <?= $form->field($model, 'mem_rear_seats')->checkbox( ) ?>
            <?= $form->field($model, 'mem_mirrors')->checkbox( ) ?>
            <?= $form->field($model, 'mem_wheel')->checkbox( ) ?>
        </div>

        <div class="form-group">
            <?=$form->field($model, 'climat')->dropDownList( CarParams::Speakers())?>
            <?= $form->field($model, 'sub')->checkbox( ) ?>
        </div>

        <div class="form-group">
            <label style="font-weight: bold;"><?= $model->getAttributeLabel('hijack') ?></label>
            <?= $form->field($model, 'hijack_signal')->checkbox( ) ?>
            <?= $form->field($model, 'hijack_central_lock')->checkbox( ) ?>
            <?= $form->field($model, 'hijack_immobi')->checkbox( ) ?>
            <?= $form->field($model, 'hijack_sputnik')->checkbox( ) ?>
        </div>

        <div class="form-group">
            <label style="font-weight: bold;"><?= $model->getAttributeLabel('media') ?></label>
            <?= $form->field($model, 'media_cd')->checkbox( ) ?>
            <?= $form->field($model, 'media_mp3')->checkbox( ) ?>
            <?= $form->field($model, 'media_radio')->checkbox( ) ?>
            <?= $form->field($model, 'media_tv')->checkbox( ) ?>
            <?= $form->field($model, 'media_video')->checkbox( ) ?>
            <?= $form->field($model, 'media_wheel_control')->checkbox( ) ?>
            <?= $form->field($model, 'media_usb')->checkbox( ) ?>
            <?= $form->field($model, 'media_aux')->checkbox( ) ?>
            <?= $form->field($model, 'media_bluetooth')->checkbox( ) ?>
            <?= $form->field($model, 'media_gps')->checkbox( ) ?>
        </div>

        <div class="form-group">
            <label style="font-weight: bold;"><?= $model->getAttributeLabel('TI') ?></label>
            <?= $form->field($model, 'TI_service_book')->checkbox( ) ?>
            <?= $form->field($model, 'TI_dealer')->checkbox( ) ?>
            <?= $form->field($model, 'TI_warranty')->checkbox( ) ?>
        </div>


    </div>

    <div class="col-sm-6">

        <div class="form-group">
            <?=$form->field($model, 'salon')->radioList( CarParams::Saloon())?>
            <?=$form->field($model, 'salon_leather_wheel')->checkbox( )?>
            <?=$form->field($model, 'salon_hatch')->checkbox( )?>
            <?=$form->field($model, 'climate_aterm')->checkbox( )?>
        </div>

        <div class="form-group">
            <label style="font-weight: bold;"><?= $model->getAttributeLabel('active_save') ?></label>
            <?=$form->field($model, 'help_secure_abs')->checkbox( )?>
            <?=$form->field($model, 'help_secure_asr')->checkbox( )?>
            <?=$form->field($model, 'help_secure_esp')->checkbox( )?>
            <?=$form->field($model, 'help_secure_ebd')->checkbox( )?>
            <?=$form->field($model, 'help_secure_eba')->checkbox( )?>
            <?=$form->field($model, 'help_secure_eds')->checkbox( )?>
            <?=$form->field($model, 'help_secure_pds')->checkbox( )?>
        </div>

        <div class="form-group">
            <label style="font-weight: bold;"><?= $model->getAttributeLabel('active_save') ?></label>
            <?= $form->field($model, 'el_front_seats')->checkbox( ) ?>
            <?= $form->field($model, 'el_rear_seats')->checkbox( ) ?>
            <?= $form->field($model, 'el_mirrors')->checkbox( ) ?>
            <?= $form->field($model, 'el_wheel')->checkbox( ) ?>
            <?= $form->field($model, 'el_folding_mirrors')->checkbox( ) ?>
        </div>

        <?=$form->field($model, 'climat')->dropDownList( CarParams::ElWindow())?>

        <div class="form-group">
            <label style="font-weight: bold;"><?= $model->getAttributeLabel('help') ?></label>
            <?= $form->field($model, 'help_jockey')->checkbox( ) ?>
            <?= $form->field($model, 'help_rain_sensor')->checkbox( ) ?>
            <?= $form->field($model, 'help_light_sensor')->checkbox( ) ?>
            <?= $form->field($model, 'help_r_park_sensor')->checkbox( ) ?>
            <?= $form->field($model, 'help_f_park_sensor')->checkbox( ) ?>
            <?= $form->field($model, 'help_blind_spot')->checkbox( ) ?>
            <?= $form->field($model, 'help_rear_camera')->checkbox( ) ?>
            <?= $form->field($model, 'help_cruise_control')->checkbox( ) ?>
            <?= $form->field($model, 'help_computer')->checkbox( ) ?>
        </div>

        <div class="form-group">
            <label style="font-weight: bold;"><?= $model->getAttributeLabel('airbags') ?></label>
            <?= $form->field($model, 'airbags_front')->checkbox( ) ?>
            <?= $form->field($model, 'airbags_knee')->checkbox( ) ?>
            <?= $form->field($model, 'airbags_blinds')->checkbox( ) ?>
            <?= $form->field($model, 'airbags_front_side')->checkbox( ) ?>
            <?= $form->field($model, 'airbags_rear_side')->checkbox( ) ?>
        </div>

        <div class="form-group">
            <?=$form->field($model, 'climat')->dropDownList( CarParams::Headlight())?>
            <?= $form->field($model, 'light_afog')->checkbox( ) ?>
            <?= $form->field($model, 'light_cleaner')->checkbox( ) ?>
            <?= $form->field($model, 'light_adaptive')->checkbox( ) ?>
        </div>

    </div>
</div>

