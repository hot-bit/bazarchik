<?php
/**
 * Created by PhpStorm.
 * User: York
 * Date: 28.09.2017
 * Time: 22:45
 *
 * @var \yii\widgets\ActiveForm $form
 */
?>

<div class="row">
    <div class="col-sm-3">
        <?=$form->field($model, 'type')->dropDownList(\common\models\map\Disk::Type())?>
    </div>
    <div class="col-sm-3">
        <?=$form->field($model, 'diam')->dropDownList(\common\models\map\Disk::Diameter())?>
    </div>
    <div class="col-sm-3">
        <?=$form->field($model, 'width')->dropDownList(\common\models\map\Disk::Width())?>
    </div>
    <div class="col-sm-3">
        <?=$form->field($model, 'holes')->dropDownList(\common\models\map\Disk::Holes())?>
    </div>
    <div class="col-sm-3">
        <?=$form->field($model, 'holes_diameter')->dropDownList(\common\models\map\Disk::HolesDiameter())?>
    </div>
    <div class="col-sm-3">
        <?=$form->field($model, 'outfly')->dropDownList(\common\models\map\Disk::Outfly())?>
    </div>
</div>