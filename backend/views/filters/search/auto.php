<?php
/**
 * Created by PhpStorm.
 * User: York
 * Date: 28.09.2017
 * Time: 22:42
 *
 * @var $model \frontend\models\CatalogFilter
 * @var $form \yii\widgets\ActiveForm
 */

use common\models\CMS;

$brand_model = [];
if($model->brand)
    $brand_model = \yii\helpers\ArrayHelper::map(\common\models\Model::findByBrand($model->brand), 'id', 'name');

?>


<?= $form->field($model, 'brand')->dropDownList(\common\models\Brand::map(), [
    'class' => 'SelectBox-search',
    'prompt' => Yii::t('app', 'All brands'),
    'onchange' => "
         $.ajax({
            type     :'POST',
            cache    : false,
            url  : '" . \yii\helpers\Url::to(['ajax/car-models']) . "',
            data: {id: $(this).val(), model:'CatalogFilter', dest:'frontend-models'}, 
            success  : function(response) {
                $('.field-catalogfilter-model .SumoSelect').remove();
                $('.field-catalogfilter-model').append(response.data);  
                $('#catalogfilter-model').SumoSelect();
            }
        });  
        return false;",
]) ?>
<?= $form->field($model, 'model')->dropDownList($brand_model, [
    'prompt' => Yii::t('app', 'Select brand'),
    'class' => 'SelectBox-search',
]) ?>
<?= $form->field($model, 'body')->dropDownList(\common\models\map\CarBodyType::Map(), [
    'placeholder' => Yii::t('app', 'Select body'),
    'multiple' => true,
    'class' => 'SelectBox',
]) ?>
<?//= $form->field($model, 'year')->widget(\kartik\range\RangeInput::className(), [
//    'html5Options' => ['min'=>0, 'max'=>1, 'step'=>1],
//    'addon' => ['append'=>['content'=>'star']]
//]) ?>
<?= $form->field($model, 'box')->dropDownList(\common\models\map\CarBox::Map(), [
    'placeholder' => Yii::t('app', 'Select transmission'),
    'multiple' => true,
    'class' => 'SelectBox',
]) ?>
<?= $form->field($model, 'engine')->dropDownList(\common\models\map\CarEngine::Map(), [
    'placeholder' => Yii::t('app', 'Select engine'),
    'multiple' => true,
    'class' => 'SelectBox',
]) ?>
<?= $form->field($model, 'drive')->dropDownList(\common\models\map\CarDrive::Map(), [
    'placeholder' => Yii::t('app', 'Select wheels drive'),
    'multiple' => true,
    'class' => 'SelectBox',
]) ?>
<?= $form->field($model, 'wheel')->dropDownList(\common\models\map\CarWheel::Map(), [
    'prompt' => Yii::t('app', 'Any'),
    'class' => 'SelectBox',
]) ?>



<!--<div class="sidebar-find__block" data-toggle="true">-->
<!--    <a href="#" class="sidebar-find__block-link">--><? //=Yii::t('app', 'Model')?><!--</a>-->
<!--    <div class="sidebar-find__block-content">-->
<!--        --><? //=$form->field($model, 'model')->dropDownList([
//            'prompt' => Yii::t('app', 'Select brand'),
//        ])?>
<!--    </div>-->
<!--</div>-->

