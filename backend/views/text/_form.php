<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model common\models\TextBlocks */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="text-blocks-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->errorSummary($model)?>

    <?= $form->field($model, 'name')->textInput(['maxlength' => true]) ?>


    <?=\backend\widgets\bazar\LanguageForm::widget([
        "model" => $model,
        'columns' => [
            'text' => ['type' => 'editor']
        ]
    ])?>



    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
