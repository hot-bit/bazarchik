<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\Pjax;
/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */
/* @var $searchModel common\models\search\TranslateSearch */

$this->title = Yii::t('app', 'Translates');
$this->params['breadcrumbs'][] = $this->title;
?>
    <div class="row">
        <div class="col-md-12">

            <div class="card">
                <div class="card-header">
                    <strong><?= Html::encode($this->title) ?></strong>
                </div>
                <div class="card-block">
                    <?php Pjax::begin(); ?>
                        <?= GridView::widget([
                            'dataProvider' => $dataProvider,
                            'filterModel' => $searchModel,
                                'columns' => [
                                'id',
                                'category',
                                'message',
                                'value',
                                [
                                    'class' => 'common\models\CMSActionColumn',
                                ],
                            ],
                        ]); ?>
                    <?php Pjax::end(); ?>
                </div>
        </div>
    </div>
</div>
