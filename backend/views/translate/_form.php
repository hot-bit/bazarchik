<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model common\models\Translate */
/* @var $form yii\widgets\ActiveForm */

$alt = \common\models\Translate::find()->where(['category' => $model->category, 'message' => $model->message])->all();
?>

<div class="translate-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'category')->textInput(['maxlength' => true, 'disabled' => true]) ?>

    <?= $form->field($model, 'message')->textInput(['maxlength' => true, 'disabled' => true]) ?>

    <?foreach($alt as $item):?>
        <div class="form-group field-translate-value">
            <label class="control-label" for="translate-category"><?= $item->_language->name?> value</label>
            <?= Html::activeTextInput($item, 'value', ['class' => 'form-control', 'name' => 'Translate[tmp_values]['.$item->language_id.']']) ?>
            <div class="help-block"></div>
        </div>
    <?endforeach?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? Yii::t('app', 'Create') : Yii::t('app', 'Update'), ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
