<?php

/* @var $this \yii\web\View */

/* @var $content string */

use backend\assets\AppAsset;
use yii\helpers\Html;
use yii\widgets\Breadcrumbs;

AppAsset::register($this);
?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">
<head>
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta charset="<?= Yii::$app->charset ?>">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <?= Html::csrfMetaTags() ?>
    <title><?= Html::encode($this->title) ?></title>
    <?php $this->head() ?>
</head>
<body class="app header-fixed sidebar-fixed aside-menu-fixed aside-menu-hidden">
<?php $this->beginBody() ?>
<header class="app-header navbar">
    <button class="navbar-toggler mobile-sidebar-toggler d-lg-none" type="button">☰</button>
    <a class="navbar-brand" href="#"></a>


    <ul class="nav navbar-nav d-md-down-none">
        <li class="nav-item">
            <a class="nav-link navbar-toggler sidebar-toggler" href="#">☰</a>
        </li>

        <li class="nav-item px-3">
            <a class="nav-link" href="/"><?=Yii::t('app', 'Go to the site');?> </a>
        </li>
    </ul>
    <ul class="nav navbar-nav ml-auto">
        <li class="nav-item dropdown">
            <a class="nav-link dropdown-toggle nav-link" data-toggle="dropdown" href="#" role="button"
               aria-haspopup="true" aria-expanded="false">
                <img src="<?= Yii::$app->user->identity->thumb ?>" class="img-avatar">
                <span class="d-md-down-none"><?= Yii::$app->user->identity->name ?></span>
            </a>
            <div class="dropdown-menu dropdown-menu-right">
                <div class="dropdown-header text-center">
                    <strong>Actions</strong>
                </div>

                <a class="dropdown-item" href="<?= \yii\helpers\Url::to(['site/logout']) ?>"><i class="fa fa-lock"></i>Logout</a>
            </div>
        </li>
        <li class="nav-item d-md-down-none">
        </li>

    </ul>
</header>

<div class="app-body">
    <div class="sidebar">
        <nav class="sidebar-nav">

            <?= yii\widgets\Menu::widget([
                'options' => ['class' => 'nav'],
                'linkTemplate' => '<a href="{url}" class="nav-link">{label}</a>',
                'itemOptions' => ['class' => 'nav-item'],
                'items' => common\models\Menu::AdminMenu(),
            ]); ?>
        </nav>
    </div>

    <!-- Main content -->
    <main class="main">

        <!-- Breadcrumb -->
        <?= Breadcrumbs::widget([
            'tag' => 'ol',
            'options' => ['class' => 'breadcrumb'],
            'itemTemplate' => '<li class="breadcrumb-item">{link}</li>',
            'activeItemTemplate' => '<li class="breadcrumb-item">{link}</li>',
            'links' => isset($this->params['breadcrumbs']) ? $this->params['breadcrumbs'] : [],
        ]) ?>

        <?= \common\widgets\Alert::widget([])?>

        <div class="container-fluid">
            <div class="animated fadeIn">
                <?= $content ?>
            </div>

        </div>
        <!-- /.conainer-fluid -->
    </main>

</div>

<footer class="app-footer">
    © <?= date('Y') ?><span class="float-right">Powered by York</span>
</footer>

<?php $this->endBody() ?>
</body>
</html>
<?php $this->endPage() ?>
