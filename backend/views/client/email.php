<?php
/**
 * Created by PhpStorm.
 * User: York
 * Date: 15.02.2018
 * Time: 16:55
 */

use yii\helpers\Html;
use yii\widgets\ActiveForm;

$this->title = Yii::t('app', 'Send email');
$this->params['breadcrumbs'] = [
    ['label' => Yii::t('app', 'Clients'), 'url' => ['index']],
    $this->title
];
?>



<div class="row">
    <div class="col-md-12">

        <div class="card">
            <div class="card-header">
                <strong><?= Html::encode($this->title) ?></strong>
            </div>
            <div class="card-block">
                <?php $form = ActiveForm::begin(['id' => 'send-sms']); ?>
                    <?= $form->field($model, 'category_id')->dropDownList([0 => 'All', -1 => 'Test'], [
                        'onchange' => "if($(this).val() == -1) $('.field-sendemailform-contacts').show();
                            else $('.field-sendemailform-contacts').hide();"
                    ])?>
                    <?= $form->field($model, 'contacts', ['options' => ['style' => 'display: none']])->textarea()->hint('name@gmail.com, name2@gmail.com, name3@gmail.com')?>
                    <?= $form->field($model, 'title')->textInput()?>
                    <?= $form->field($model, 'text')->textarea()?>
                    <?= $form->field($model, 'template_id')->dropDownList(\common\models\ClientsTemplate::map(2), ['prompt' => '-- None --', 'class' => 'load-template-list form-control',
                        'onchange' => "$('#sendemailform-text').load('".\yii\helpers\Url::to(['ajax/get-template-text'])."', {id:$(this).val()});"]) ?>

                    <div class="form-group">
                        <?= Html::submitButton( Yii::t('app', 'Send'), ['class' => 'btn btn-success']) ?>
                    </div>
                <?php ActiveForm::end(); ?>

            </div>
        </div>
    </div>
</div>

