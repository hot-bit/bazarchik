<?php
/**
 * Created by PhpStorm.
 * User: yorks
 * Date: 19.06.2017
 * Time: 14:07
 */

use yii\helpers\Html;
use yii\widgets\ActiveForm;

$this->title = Yii::t('app', 'Send Email');
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Clients'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="row">
    <div class="col-md-12">

        <div class="card">
            <div class="card-header">
                <strong><?= Html::encode($this->title) ?></strong>
            </div>
            <div class="card-block">

                <?php $form = ActiveForm::begin(); ?>

                    <?= $form->field($model, 'email')->textInput() ?>
                    <?= $form->field($model, 'title')->textInput() ?>
                    <?= $form->field($model, 'text')->textarea([ 'class' => 'load-template-area form-control']) ?>
                    <?= $form->field($model, 'template_id')->dropDownList(\common\models\ClientsTemplate::map(2), ['prompt' => '-- None --', 'class' => 'load-template-list form-control']) ?>
                    <?= $form->field($model, 'status')->checkbox() ?>

                    <div class="form-group">
                        <?= Html::submitButton( Yii::t('app', 'Send'), ['class' => 'btn btn-success']) ?>
                    </div>

                <?php ActiveForm::end(); ?>
            </div>
        </div>
    </div>
</div>

