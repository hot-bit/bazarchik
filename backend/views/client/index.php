<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\Pjax;
/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */
/* @var $searchModel common\models\search\ClientsSearch */

$this->title = Yii::t('app', 'Clients');
$this->params['breadcrumbs'][] = $this->title;
?>

<div class="row">
    <div class="col-md-12">

        <div class="card">
            <div class="card-header">
                <strong><?= Html::encode($this->title) ?></strong>
            </div>
            <div class="card-block">
                <p>
                    <?= Html::a(Yii::t('app', 'Create Client'), ['create'], ['class' => 'btn btn-success']) ?>
                    <?= Html::a(Yii::t('app', 'Send SMS'), ['sms'], ['class' => 'btn btn-primary']) ?>
                    <?= Html::a(Yii::t('app', 'Send email'), ['email'], ['class' => 'btn btn-primary']) ?>
                    <?= Html::a(Yii::t('app', 'Templates'), ['ctemplate/index'], ['class' => 'btn btn-secondary']) ?>
                    <?= Html::a(Yii::t('app', 'Parse clients'), ['#'], ['class' => 'btn btn-secondary']) ?>
                </p>
                <?php Pjax::begin(); ?>
                    <?= GridView::widget([
                        'dataProvider' => $dataProvider,
                        'filterModel' => $searchModel,
                        'columns' => [
                            'id',
                            'name',
                            'email:email',
                            'phone',
                            'send_date:date',
                            'f_status:html:Active',
                            'f_status_phone:html:Phone status',
                            'f_status_email:html:Email status',

                            [
                                'class' => 'common\models\CMSActionColumn',
                                'template' => '{send-sms} {send-email} {update} {delete}',
                                'buttons' => [
                                    'send-sms' => function($url, \common\models\Clients $model, $key){
                                        if($model->status_phone)
                                            return Html::a(Html::tag('i', '', ['class' => 'icon-paper-plane']), $url);
                                        return '';
                                    },
                                    'send-email' => function($url, \common\models\Clients $model, $key  ){
                                        if($model->status_email)
                                            return Html::a(Html::tag('i', '', ['class' => 'icon-envelope']), $url);
                                        return '';
                                    },
                                ]
                            ],
                        ],
                    ]); ?>
                <?php Pjax::end(); ?>

            </div>
        </div>
    </div>
</div>