<?php
/**
 * Created by PhpStorm.
 * User: York
 * Date: 15.02.2018
 * Time: 16:57
 */

namespace backend\models;
use common\models\Clients;
use yii\base\Model;
use yii\helpers\ArrayHelper;


class SendSmsForm extends Model
{
    public $category_id;
    public $text;
    public $template_id;
    public $contacts;

    public function rules()
    {
        return [
            [['category_id', 'text'], 'required'],
            [['template_id', 'category_id'], 'integer'],
            ['contacts', 'string'],
        ];
    }
    
    public function save(){
        $phoneList = [];
        if($this->category_id == -1) $phoneList = explode(',', str_replace(' ', '', $this->contacts));
        elseif($this->category_id == 0) $phoneList = ArrayHelper::map(Clients::find()->where(['status_phone' => 1])->all(), 'id', 'fullPhone');

        foreach ($phoneList as $phone){
            $curl = curl_init();
            $post = sprintf('AuthKey=x459klSSS34saTa29&Telephone=%d&Header=CyprusBazar&Body=%s', $phone, $this->text);
            curl_setopt($curl, CURLOPT_URL, 'http://www.commtor.com/webapi/AccountApi/PostMessage');
            curl_setopt($curl, CURLOPT_RETURNTRANSFER,true);
            curl_setopt($curl, CURLOPT_POST, true);
            curl_setopt($curl, CURLOPT_POSTFIELDS, $post);
            curl_exec($curl);
            curl_close($curl);
        }

        return true;
    }

}