<?php
/**
 * Created by PhpStorm.
 * User: York
 * Date: 15.02.2018
 * Time: 16:57
 */

namespace backend\models;

use common\models\Clients;
use Yii;
use yii\base\Model;
use yii\helpers\ArrayHelper;

class SendEmailForm extends Model
{
    public $category_id;
    public $title;
    public $text;
    public $template_id;
    public $contacts;

    public function rules()
    {
        return [
            [['category_id', 'text', 'title'], 'required'],
            [['title'], 'string', 'max' => 255],
            [['template_id', 'category_id'], 'integer'],
            ['contacts', 'string'],
        ];
    }

    public function save()
    {
        $emailList = [];
        if ($this->category_id == -1) $emailList = explode(',', str_replace(' ', '', $this->contacts));
        elseif ($this->category_id == 0) $emailList = ArrayHelper::map(Clients::find()->where(['status_email' => 1])->all(), 'id', 'email');


        foreach ($emailList as $email){
            $subject = 'You have received a new message from Cyprus Bazar';


            return Yii::$app->mailer
                ->compose(
                    ['html' => 'notification/notification-html', 'text' => 'notification/notification-text'],
                    ['data' => $this]
                )
                ->setTo($email)
                ->setFrom([Yii::$app->params['support_email'] => 'Cyprus Bazar'])
                ->setSubject($subject)
                ->send();
        }
    }
}