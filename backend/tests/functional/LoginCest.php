<?php

namespace backend\tests\functional;

use \backend\tests\FunctionalTester;
use common\fixtures\UserFixture as UserFixture;

/**
 * Class LoginCest
 */
class LoginCest
{
    public function _before(FunctionalTester $I)
    {
        $I->haveFixtures([
            'users' => [
                'class' => UserFixture::className(),
                'dataFile' => codecept_data_dir() . 'login_data.php'
            ]
        ]);
    }
    /**
     * @param FunctionalTester $I
     */
//    public function loginUser(FunctionalTester $I)
//    {
//        $I->amOnPage('/backend/web/site/login');
//        $I->fillField('email', 'yorkshp@gmail.com');
//        $I->fillField('password', '123qwe');
//        $I->click('login-button');
//
//        $I->see('Logout', 'a.dropdown-item');
//        $I->dontSeeLink('Login');
//    }
}
