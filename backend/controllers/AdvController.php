<?php

namespace backend\controllers;

use common\models\Adv;
use common\models\forms\ParamsForm;
use common\models\search\AdvSearch;
use Yii;
use yii\data\ActiveDataProvider;
use yii\filters\AccessControl;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\web\Response;
use yii\widgets\ActiveForm;

/**
 * AdvController implements the CRUD actions for Adv model.
 */
class AdvController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'actions' => ['index', ''],
                        'allow' => true,
                        'roles' => ['ADV']
                    ],
                    [
                        'actions' => ['update', 'view', 'delete', 'approve', 'reject'],
                        'allow' => true,
                        'roles' => ['ADV_UPDATE']
                    ],

                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Подтвердить объявление
     *
     * @param $id
     * @return Response
     * @throws NotFoundHttpException
     */
    public function actionApprove($id){
        $model = $this->findModel($id);
        $model->status = Adv::STATUS_ACTIVE;
        $model->save();
        return $this->redirect(['index']);
    }

    /**
     * Отклонить объявление
     *
     * @param $id
     * @param $text
     * @return Response
     * @throws NotFoundHttpException
     */
    public function actionReject($id, $text){
        $model = $this->findModel($id);
        $model->status = Adv::STATUS_BLOCK;
        $model->block = $text;
        $model->save();
        return $this->redirect(['index']);
    }

    /**
     * Lists all Adv models.
     * @return mixed
     */
    public function actionIndex()
    {

        $searchModel = new AdvSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single Adv model.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Updates an existing Adv model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        $params_model = new ParamsForm();
        $params_model->id = $id;
        $params_model->step = 5;
        $params_model->loadModel();
//        $params_form_model->upload($model->id);

        if ($model->load(Yii::$app->request->post()) && $model->save() && $params_model->upload($id)) {
            return $this->redirect(['view', 'id' => $model->id]);
        } else {


            if (Yii::$app->request->isAjax && $model->load(Yii::$app->request->post())) {
                Yii::$app->response->format = Response::FORMAT_JSON;
                return ActiveForm::validate($model);
            }

            return $this->render('update', [
                'model' => $model,
                'params_model' => $params_model
            ]);
        }
    }

    /**
     * Deletes an existing Adv model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException
     * @throws \Exception
     * @throws \Throwable
     * @throws \yii\db\StaleObjectException
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();
        Yii::$app->session->addFlash('success', Yii::t('app', 'Adv has been deleted'));

        return $this->redirect(['index']);
    }

    /**
     * Finds the Adv model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Adv the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Adv::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
