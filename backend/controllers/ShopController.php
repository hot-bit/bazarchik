<?php
/**
 * Created by PhpStorm.
 * User: York
 * Date: 01.02.2018
 * Time: 16:09
 */

namespace backend\controllers;


use common\models\User;
use yii\data\ActiveDataProvider;
use yii\filters\AccessControl;
use yii\filters\VerbFilter;
use yii\web\Controller;
use yii\web\NotFoundHttpException;

class ShopController extends Controller
{

    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'actions' => ['index', 'view', 'activate', 'block'],
                        'allow' => true,
                        'roles' => ['SHOP']
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all Pages models.
     * @return mixed
     */
    public function actionIndex()
    {
        $dataProvider = new ActiveDataProvider([
            'query' => User::find()->where(['is_business' => [1, 2]]),
        ]);

        return $this->render('index', [
            'dataProvider' => $dataProvider,
        ]);
    }

    public function actionView($id){
        $model = $this->getModel($id);
        \Yii::$app->user->returnUrl = ['shop/view', 'id' => $id];
        return $this->render('view', compact('model'));
    }

    public function actionActivate($id){
        $model = $this->getModel($id);
        $model->is_business = User::BUSINESS_ACTIVE;
        $model->save();
        return $this->goBack();
    }

    public function actionBlock($id){
        $model = $this->getModel($id);
        $model->is_business = User::BUSINESS_BANNED;
        $model->save();
        return $this->goBack();
    }

    private function getModel($id){
        $model = User::findOne($id);
        if(!$model) throw new NotFoundHttpException('Not found');
        return $model;
    }
}