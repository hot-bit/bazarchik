<?php
/**
 * Created by PhpStorm.
 * User: York
 * Date: 31.12.2017
 * Time: 8:52
 */

namespace backend\controllers;


use common\models\Adv;
use common\models\Categories;
use common\models\ClientsTemplate;
use common\models\CMS;
use common\models\Model;
use Yii;
use yii\filters\AccessControl;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\web\Controller;
use yii\web\Response;

class AjaxController extends Controller{


    public function beforeAction($action){
        if(!\Yii::$app->user->can('ADMIN_PANEL')) throw new \yii\web\ForbiddenHttpException();

        Yii::$app->response->format = Response::FORMAT_JSON;

        return parent::beforeAction($action);
    }

    public function actionTranslate($text){
        return CMS::Translit($text);
    }

    public function actionGenerateCar($id){
        $model = Adv::findOne($id);
        return [
            'title' => $model->GenerateTitleCar(),
            'desc' => $model->GenerateDescriptionCar(),
        ];
    }

    public function actionGetCategorySelect(){

        if(empty($_POST['parent'])) return false;
        $arr = ArrayHelper::map(Categories::find()->where(['header' => 0])->andWhere(['parent' => $_POST['parent']])->all(), 'id', 'name');

        if($arr)
            return Html::dropDownList('category', '', $arr, ['class' => 'form-control category-select', 'prompt' => Yii::t('app', '-- none --')]);

    }
    public function actionCarModels(){


        $form_name = $_POST['model'];
        $form_class = '\\'.str_replace('-', '\\', $_POST['dest']).'\\'.$form_name;
        $model = new $form_class();

        $id = $_POST['id'];
        $data  = Model::map($id);

        $field = Html::activeDropDownList($model, 'model', $data, ['class' => 'SelectBox-search', 'prompt' => Yii::t('app', 'Select model')]);

        return ['data' => $field];
    }

    public function actionGetTemplateText(){
//        Yii::$app->response->format = Response::FORMAT_JSON;
        $id = $_POST['id'];
        $model = ClientsTemplate::findOne($id);
        die($model->text);
    }
}