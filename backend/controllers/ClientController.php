<?php

namespace backend\controllers;

use common\models\search\ClientsSearch;
use common\models\SendClientEmailForm;
use common\models\SendClientSmsForm;
use common\models\Clients;
use backend\models\SendEmailForm;
use backend\models\SendSmsForm;
use Yii;
use yii\data\ActiveDataProvider;
use yii\filters\AccessControl;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * ClientController implements the CRUD actions for Clients model.
 */
class ClientController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'actions' => ['index', 'update', 'create', 'view', 'send-sms', 'send-email', 'sms', 'email'],
                        'allow' => true,
                        'roles' => ['ADMIN_PANEL']
                    ],

                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all Clients models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new ClientsSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single Clients model.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new Clients model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new Clients();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['update', 'id' => $model->id]);
        } else {
            return $this->render('create', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Updates an existing Clients model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['update', 'id' => $model->id]);
        } else {
            return $this->render('update', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Deletes an existing Clients model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException
     * @throws \Throwable
     * @throws \yii\db\StaleObjectException
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the Clients model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Clients the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Clients::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }


    public function actionSendSms($id){

        $client = Clients::findOne($id);
        $model = new SendClientSmsForm();
        $model->phone = $client->phone;

        if ($model->load(Yii::$app->request->post()) && $model->send() && $client->updateDate()) {
            return $this->redirect(['index']);
        }

        return $this->render('send_sms', [
            'model' => $model
        ]);
    }
    public function actionSendEmail($id){

        $client = Clients::findOne($id);
        $model = new SendClientEmailForm();
        $model->email = $client->email;

        if ($model->load(Yii::$app->request->post()) && $model->send()) {
            return $this->redirect(['index']);
        }

        return $this->render('send_email', [
            'model' => $model
        ]);
    }

    public function actionSms(){
        $model = new SendSmsForm();

        if($model->load(Yii::$app->request->post()) && $model->save()){
            return $this->refresh();
        }
        return $this->render('sms', compact('model'));
    }
    public function actionEmail(){
        $model = new SendEmailForm();
        if($model->load(Yii::$app->request->post()) && $model->save()){
            return $this->refresh();
        }
        return $this->render('email', compact('model'));
    }

}
