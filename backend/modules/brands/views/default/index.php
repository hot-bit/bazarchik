<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('app', 'Brands');
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="row">
    <div class="col-md-12">

        <div class="card">
            <div class="card-header">
                <strong><?= Html::encode($this->title) ?></strong>
            </div>
            <div class="card-block">

                <p>
                    <?= Html::a(Yii::t('app', 'Create Brand'), ['create'], ['class' => 'btn btn-success']) ?>
                </p>
                <?= GridView::widget([
                    'dataProvider' => $dataProvider,
                    'filterModel' => $searchModel,
                    'columns' => [

                        'id',
                        'name' => [
                            'attribute' => 'name',
                            'value' => function(\common\models\Brand $model, $id, $index){
                                return Html::a($model->name, ['model/index', 'id' => $id]);
                            },
                            'format' => 'html'
                        ],
                         'url',

                        ['class' => 'common\models\CMSActionColumn'],
                    ],
                ]); ?>
            </div>
        </div>
    </div>
</div>
