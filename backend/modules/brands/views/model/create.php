<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model common\models\Model */


$this->title = Yii::t('app', 'Create Model');

if(isset($_GET['id'])){
    $model->brand_id = $_GET['id'];
    $brand = \common\models\Brand::findOne($_GET['id']);
    $this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Brands'), 'url' => ['default/index']];
    $this->params['breadcrumbs'][] = ['label' => $brand->name, 'url' => ['default/view', 'id' => $brand->id]];
}
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Models'), 'url' => ['index', 'id' => $model->brand_id]];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="row">
    <div class="col-md-12">

        <div class="card">
            <div class="card-header">
                <strong><?= Html::encode($this->title) ?></strong>
            </div>
            <div class="card-block">

                <?= $this->render('_form', [
                    'model' => $model,
                ]) ?>

            </div>
        </div>
    </div>
</div>
