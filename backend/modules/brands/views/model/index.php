<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('app', 'Models');
if(isset($_GET['id'])){
    $brand = \common\models\Brand::findOne($_GET['id']);
    $this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Brands'), 'url' => ['default/index']];
    $this->params['breadcrumbs'][] = ['label' => $brand->name, 'url' => ['default/view', 'id' => $brand->id]];
}
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="row">
    <div class="col-md-12">

        <div class="card">
            <div class="card-header">
                <strong><?= Html::encode($this->title) ?></strong>
            </div>
            <div class="card-block">

                <p>
                    <?= Html::a(Yii::t('app', 'Create Model'), ['create', 'id' => $_GET['id']], ['class' => 'btn btn-success']) ?>
                </p>
                <?= GridView::widget([
                    'dataProvider' => $dataProvider,
                    'filterModel' => $searchModel,
                    'columns' => [

                        'id',
                        'brand_name:raw:Brand',
                        'name',
//                        'count_used',
//                        'count_new',
                        // 'count_rent',
                        'url:url',

                        ['class' => 'common\models\CMSActionColumn'],
                    ],
                ]); ?>
            </div>
        </div>
    </div>
</div>
