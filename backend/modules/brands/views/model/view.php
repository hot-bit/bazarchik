<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model common\models\Model */

$this->title = $model->name;
if (isset($_GET['id'])) {
    $this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Brands'), 'url' => ['default/index']];
    $this->params['breadcrumbs'][] = ['label' => $model->_brand->name, 'url' => ['default/view', 'id' => $model->brand_id]];
}
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Models'), 'url' => ['index', 'id' => $model->brand_id]];
$this->params['breadcrumbs'][] = $this->title;
?>

<div class="row">
    <div class="col-md-12">

        <div class="card">
            <div class="card-header">
                <strong><?= Html::encode($this->title) ?></strong>
            </div>
            <div class="card-block">

                <p>
                    <?= Html::a(Yii::t('app', 'Update'), ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
                    <?= Html::a(Yii::t('app', 'Delete'), ['delete', 'id' => $model->id], [
                        'class' => 'btn btn-danger',
                        'data' => [
                            'confirm' => Yii::t('app', 'Are you sure you want to delete this item?'),
                            'method' => 'post',
                        ],
                    ]) ?>
                </p>

                <?= DetailView::widget([
                    'model' => $model,
                    'attributes' => [
                        'id',
                        'brand_name',
                        'name',
                        'url',
                    ],
                ]) ?>

            </div>
        </div>
    </div>
</div>
