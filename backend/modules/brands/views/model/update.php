<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\Model */

$this->title = Yii::t('app', 'Update {modelClass}: ', [
    'modelClass' => 'Model',
]) . $model->name;
if(isset($_GET['id'])){
    $this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Brands'), 'url' => ['default/index']];
    $this->params['breadcrumbs'][] = ['label' => $model->_brand->name, 'url' => ['default/view', 'id' => $model->brand_id]];
}
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Models'), 'url' => ['index', 'id' => $model->brand_id]];
$this->params['breadcrumbs'][] = ['label' => $model->name, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = Yii::t('app', 'Update');
?>
<div class="row">
    <div class="col-md-12">

        <div class="card">
            <div class="card-header">
                <strong><?= Html::encode($this->title) ?></strong>
            </div>
            <div class="card-block">

                <?= $this->render('_form', [
                'model' => $model,
                ]) ?>

            </div>
        </div>
    </div>
</div>
