/**
 * Created by yorks on 08.02.2017.
 */


// Открыть/закрыть подкатегории
$('.tree_pl').click(function(){
    $(this).parent().parent().parent().parent().toggleClass('active');
    if($(this).hasClass('icon-plus'))
        $(this).removeClass('icon-plus').addClass('icon-minus');
    else
        $(this).removeClass('icon-minus').addClass('icon-plus');
});

// Открыть/закрыть форму редактирования категории
$('.open_tree_body').click(function(){
    $(this).parents('.admin_tree_item').find('.admin_tree_item_body').toggleClass('active');

    if($(this).hasClass('icon-arrow-down'))
        $(this).removeClass('icon-arrow-down').addClass('icon-arrow-up');
    else
        $(this).removeClass('icon-arrow-up').addClass('icon-arrow-down');
});

// Форма сохранения категории
$('.admin_category_form').submit(function () {
    $.post('/backend/web/category/update/', $(this).serialize(), function (ret) {
        if(ret == 200) Notify({text:"Category was saved"});
    });
    return false;
});


// Подгрузка шаблонов текстов для клиентов
$('.load-template-list').change(function () {
    var id = $('.load-template-list').val();
    $('.load-template-area').load('/backend/web/ctemplate/load/', {id:id});
});