/**
 * Created by yorks on 14.06.2017.
 */

function Notify(params) {
    if(typeof(params.text) == 'undefined') params.text = 'Hello stranger';
    if(typeof(params.type) == 'undefined') params.type = 'success';
    if(typeof(params.time) == 'undefined') params.time = 5000;

    var tag_start = '<div class="window_notify window_notify-'+params.type+'">';
    var tag_end = '</div>';

    $('body').append(tag_start+params.text+tag_end);
    $('.window_notify').fadeIn();
    setTimeout(function () {
        $('.window_notify').fadeOut();
        setTimeout(function () {
            $('.window_notify').remove();
        }, 2000);
    }, params.time);
}