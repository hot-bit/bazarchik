<?php
/**
 * Created by PhpStorm.
 * User: York
 * Date: 26.09.2017
 * Time: 17:17
 */

namespace backend\widgets\bazar;


use common\models\Lang;
use yii\base\Widget;

class LanguageForm extends Widget{

    public $model;
    public $columns;

    public function run(){

        $langs = Lang::find()->all();
        return $this->render('language_form', [
            'langs' => $langs,
            'model' => $this->model,
            'columns' => $this->columns
        ]);
    }




}